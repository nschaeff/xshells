**XSHELLS is a high performance simulation code for the rotating Navier-Stokes equation in spherical shells,
optionally coupled to the induction and temperature equation.**

Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
XSHELLS is distributed under the open source [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html)
(GPL compatible) located in the LICENSE file.


FEATURES:
---------

- Designed for speed, it is probably the **fastest spherical MHD simulation code**.
- Uses finite differences in radial direction, and spherical harmonic expansion.
- Low memory footprint; can handle huge spherical harmonic resolutions.
- Written in C++ with efficiency, extensibility and customizability in mind.
- Export data to python/matplotlib and paraview.
- Can time-step various initial value problems in spherical shells or full spheres:
  geodynamo, spherical Couette flow, precession, inertial waves, torsional Alfvén waves,
  double-diffusive convection...
- Can run on a laptop or on massively parallel supercomputers
  (hybrid parallelization using OpenMP and/or MPI, scales well up to 1 process/shell).
- [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible with GNU GPL):
  everybody is free to use, modify, contribute.


INSTALL:
--------

If you accept the open source [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html)
(GPL compatible french License), you can [download XSHELLS](https://bitbucket.org/nschaeff/xshells/downloads).

- **run `./setup` for the installation wizard.**

- Alternatively, a "manual" installation is also possible: the shell command `./configure` will prepare 
    the program for compilation, while `./configure --help` will list available options
    (among which `--disable-openmp` and `--disable-mpi`).
    The [SHTns library](https://bitbucket.org/nschaeff/shtns/) is required for spherical harmonic transforms,
    but is included in the tar archive.

Once setup is complete, you must choose a problem to solve.
Example problems are located in the `problems` directory.
For example, to build executables for the geodynamo benchmark (Christensen et al PEPI 2011):

        cp problems/geodynamo/* .
        make all

Adjust the parameters found in the `xshells.par` file, which will
be read by program at startup.
Depending on what you want you can then run:

  - `./xsbig` for the simple OpenMP version
  - `./xsbig_mpi` for the pure MPI code
  - `./xsbig_hyb` for the hybrid OpenMP-MPI executable.
  - `./xsbig_hyb2` for the radial MPI + in-shell OpenMP executable.
  - `./xsimp` for the OpenMP version with Coriolis force handled implicitely (beta).

Read the user manual for more details.


DOCUMENTATION:
--------------

- A user manual is available [online](https://nschaeff.bitbucket.io/xshells/manual.html)
  or for [download (pdf)](https://bitbucket.org/nschaeff/xshells/downloads).
  The latex source of the manual can be found in the `notes` directory.
- In addition, the main source files are documented using Doxygen comments.
  Run `make docs` to generate the documentation targeted at developers and contributors.
