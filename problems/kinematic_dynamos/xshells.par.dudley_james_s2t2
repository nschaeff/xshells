##########################################################################
# XSHELLS : eXtendable Spherical Harmonic Earth-Like Liquid Simulator    #
#  > this is the input parameter file                                    #
# syntax : name = value                                                  #
##########################################################################

### parameters to reproduce the Dudley&James kinematic dynamo (case s2t2)
### See "Time-Dependent Kinematic Dynamos with Stationary Flows",
### Dudley & James, Proceeding of the Royal Society A.,
### https://doi.org/10.1098/rspa.1989.0112

job = dud02

### USER DEFINED VARIABLES ###
Pm = 1

### PHYSICAL PARAMS ###
eta = 1/Pm		# magnetic diffusivity

#alpha = 14  # dynamo onset is near 13.19

#hyper_nu = 30
#hyper_kappa = 30
#hyper_diff_l0 = 0.8

### INITIAL FIELDS ###
## A file name containing a field may be given (load file), or 
## a predefined field name (run "./list_fields" for a complete list)
## or "random", "rands", "randa" and "0" for respectively :
## load file, xshells_init.cpp defined velocity and magnetic fields,
## random field, random symmetric, random anti-symmetric fields and zero field.
## Fields may be scaled by adding (e.g.) *1e-3 after the name.
## comment out the lines if the field does not exist to avoid time-stepping that field.

u0 = dud02(0.14) * 85	# imposed field: s2t2 from Duddley&James(1989), with epsilon=0.14. Dynamo action for R=54, which is amplitude 85 in our normalization
b = random	# initial magnetic field (scaled by sqrt(1/(Pm*E)))

### BOUNDARY CONDITIONS AND RADIAL DOMAINS ###
#r = rayon.dat		# file containing the radial grid to use (either a field file, or an ascii file).
BC_U = 1,1			# inner,outer boundary conditions (0=zero velocity, 1=no-slip, 2=free-slip)
R_U = 0 : 1	# Velocity field boundaries
R_B = 0 : 1	# Magnetic field boundaries

### NUMERICAL SCHEME ###
NR = 96		# total number of radial shells (overriden by r = ...)
N_BL = 10,10	# number of radial shells reserved for inner and outer boundaries.
dt_adjust = 0	# 0: fixed time-step (default), 1: variable time-step
dt = 0.0003		# time step
iter_max = 100	# iteration number (total number of text and energy file ouputs)
sub_iter = 100	# sub-iterations (the time between outputs = 2*dt*sub_iter is fixed even with variable time-step)
iter_save = 10  # number of iterations between field writes (if movie > 0, see below).

### SHT ###
Lmax = 20	# max degree of spherical harmonics
Mmax = 1	# max fourier mode (phi)
#Mres = 4	# phi-periodicity.
#Nlat = 32	# number of latitudinal points (theta). Optimal chosen if not specified.
#Nphi = 16	# number of longitudinal points (phi). Optimal chosen if not specified.

### OPTIONS ###
#interp = 1		# 1: allow interpolation of fields in case of grid mismatch. 0: fail if grids mismatch (default).
#restart = 1	# 1: try to restart from a previous run with same name. 0: no auto-restart (default).
#movie = 1		# 0=field output at the end only (default), 1=output every iter_save, 2=also writes time-averaged fields
#lmax_out = -1		# lmax for movie output (-1 = same as Lmax, which is also the default)
#mmax_out = -1		# mmax for movie output (-1 = same as Mmax, which is also the default)
#prec_out = 1		# 1: single precision movie output (default), 2: double precision movie outputs.
#zavg = 2			# 1=output z-averaged field, 2=also output rms z-averaged.
backup_time = 240		# ensures that full fields are saved to disk at least every backup_time minutes, for restart.
#nbackup = 0			# number of backups before terminating program (useful for time-limited jobs). 0 = no limit (default)

### ALGORITHM FINE TUNING ###

## C_vort and C_alfv control the time-step adjustment (active if dt_adjust=1),
## regarding vorticity and Alfven criteria.
## if dt_tol_lo < dt/dt_target < dt_tol_hi, no adjustement is done.
#C_u = 0.7
#C_vort = 0.2		# default: 0.2
#C_alfv = 1.0		# default: 1.0
#dt_tol_lo = 0.8		# default: 0.8
#dt_tol_hi = 1.1		# default: 1.1

## fine-tuning of PC2 time-scheme:
#pc2_ax = 0.4   # Most stable = 0.4. Most accurate = 0.5 (default). Should be >= 0.4
#pc2_ai = 0.9   # Most stable = 0.99. Most accurate = 0.5 (default). Should be >= 0.5

## sht_type : 0 = gauss-legendre (default), 1 = fastest with DCT enabled
##            2 = fastest on regular grid, 3 = full DCT, 4 = debug (quick_init), 6 = on-the-fly (good for parallel)
##   4 has the smallest init-time, useful for test/debug. Otherwise 0 or 6 should be used.
sht_type = 4

## sht_polar_opt_max = SHT polar optimization threshold : polar coefficients below that threshold are neglected (for high ms).
##    value under wich the polar values of the Legendre Polynomials Plm are neglected, leading to increased performance.
##    0 = no polar optimization;  1.e-14 = VERY safe (default);  1.e-10 = safe;  1.e-6 = aggresive.
sht_polar_opt_max = 1.0e-10

### growing magnetic field obtained for dud02 * 85
#TEST time=1.8; ref={'db_bdt':0.0137068}; opt='-alpha=0 -u0=dud02*85 -iter_max=30 -dt=3e-4 -dt_adjust=0 -Lmax=20 -Mmax=1 -sht_type=4'

