
#ifndef XS_H
	
	#define XS_H


#define XS_CUSTOM_DIAGNOSTICS

#define XS_ADJUST_DT

#define XS_LINEAR

#define VAR_LTR 0.05

#define XS_HYPER_DIFF


#ifdef XS_ETA_PROFILE
	#define XS_ETA_VAR
#endif

#else


#ifdef XS_CUSTOM_DIAGNOSTICS
void custom_diagnostic(diagnostics& all_diags)
{
	double *diags;

	int sb_removed = 0;

	
	if (evol_ubt & EVOL_U) {
		diags = all_diags.append(8,		
			"EZu_ps, EZu_pa, EZu_ts, EZu_ta, ENZu_ps, ENZu_pa, ENZu_ts, ENZu_ta\t ");
		Ulm.energy_split(diags);		
	}
	if (evol_ubt & EVOL_B) {
		diags = all_diags.append(8,		
			"EZb_ps, EZb_pa, EZb_ts, EZb_ta, ENZb_ps, ENZb_pa, ENZb_ts, ENZb_ta\t ");
		Blm.energy_split(diags);		
	}

	
	if ((evol_ubt & EVOL_U) && (MMAX > 0)) {
		double mspec[MMAX+1];
		
		int mlim = (MMAX > 20) ? 20 : MMAX;
		static std::ostringstream labels;
		if (labels.str().length() == 0) {	
			for (int m=0; m<=mlim; m++)  labels << "m=" << m << ", ";
			labels << "\t";
		}
		
		diags = all_diags.append(mlim+1, labels.str().c_str());		
		Ulm.spectrum(0, mspec);					
		for (int m=0; m<=mlim; m++) diags[m] = mspec[m];
	}

	
	if (evol_ubt & EVOL_U) {
		cplx Wxy;
		double Wz, dV;
		double r0 = mp.var["L_rmin"];
		double r1 = mp.var["L_rmax"];
		int ir0, ir1;
		int m1ok = ((MMAX>0)&&(MRES==1));
		diags = all_diags.append(3, "Wx, Wy, Wz\t ");		
		ir0 = r_to_idx(r0);		ir1 = r_to_idx(r1);
		if (Ulm.irs > ir0) ir0 = Ulm.irs;
		if ((ir1 == 0) || (Ulm.ire < ir1)) ir1 = Ulm.ire;
		Wxy=0;	Wz=0;
		for (int ir=ir0; ir<=ir1; ir++) {
			dV = r[ir]*r[ir]*r[ir] * Ulm.delta_r(ir);
			Wz += dV*real(Ulm.Tor[ir][LiM(shtns,1,0)]);
			if (m1ok) Wxy += dV*Ulm.Tor[ir][LiM(shtns,1,1)];
		}
		double n = (pow(r[Ulm.ir_bco],5)-pow(r[Ulm.ir_bci],5))/5.;	
		Wxy /= n*Y11_st;		Wz /= n*Y10_ct;			
		diags[0] = real(Wxy);
		diags[1] = -imag(Wxy);
		diags[2] = Wz;
	}

	
	if (evol_ubt & EVOL_B) {
		diags = all_diags.append(4, "Bx, By, Bz, f_dip\t ");		
		if (Blm.ire == Blm.ir_bco) {		
			int ir = Blm.ir_bco;
			if ((MMAX > 0) && (MRES == 1)) {
				diags[0] = real(Blm.Pol[ir][LiM(shtns,1,1)])/Y11_st;		
				diags[1] = imag(Blm.Pol[ir][LiM(shtns,1,1)])/Y11_st;		
			}
			diags[2] = real(Blm.Pol[ir][LiM(shtns,1,0)])/Y10_ct;		
			
			int llim = 12;		
			if (llim < LMAX) llim = LMAX;	
			double Br2dip = 0.0;
			double Br2all = 0.0;
			double pre = 1.0;
			double r_1 = 1.0/r[ir];
			for (int im=0; im<=MMAX; im++) {
				for (int l=im*MRES; l<=llim; l++) {
					cplx Br = l2[l] * Blm.Pol[ir][LiM(shtns,l,im)] * r_1;
					double Br2 = pre * norm(Br);
					Br2all += Br2;
					if (l==1) Br2dip += Br2;
				}
				pre = 2.0;		
			}
			diags[3] = sqrt( Br2dip / Br2all );		
		}
	}


	
	if (evol_ubt & EVOL_U) {
		diags = all_diags.append(4, "Epol_sym, Epol_asym, Etor_sym, Etor_asym\t ");		
		cplx* Q = (cplx*) malloc(2*NLM*sizeof(cplx) + 2*NLM*sizeof(double));
		cplx* S = Q + NLM;
		double* Pspec = (double*)(S + NLM);
		double* Tspec = Pspec + NLM;
		memset(Pspec, 0, 2*NLM*sizeof(double));		
		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
			Ulm.RadSph(ir, Q, S);
			double r2dr = r[ir]*r[ir]*Ulm.delta_r(ir);
			double dV = 0.5 * r2dr;
			double Esym  = 0.0; 	double Easym = 0.0;
			int lm=0;
			for (int im=0; im<=MMAX; im++) {
				
				for (int l=im*MRES; l<=LMAX; l++, lm++) {
					double Plm = (norm(Q[lm]) + l2[l]*norm(S[lm])) * dV;
					double Tlm = 	l2[l]*norm(Ulm.Tor[ir][lm]) * dV;
					Pspec[lm] += Plm;
					Tspec[lm] += Tlm;
					diags[l&1] += Plm;
					diags[2+1-(l&1)] += Tlm;
				}
				dV = 1.0 * r2dr;		
			}
		}
		free(Q);
	}

	
	if (evol_ubt & EVOL_U) {
		diags = all_diags.append(1, "D_nu\t ");		
		cplx* Q = (cplx*) malloc(3*NLM*sizeof(cplx));
		cplx* S = Q + NLM;		cplx* T = Q + 2*NLM;
		double Dnu = 0.0;
		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
			Ulm.curl_QST(ir, Q, S, T);
			double r2dr = r[ir]*r[ir]*Ulm.delta_r(ir);
			double dV = 1.0 * r2dr;
			int lm=0;
			for (int im=0; im<=MMAX; im++) {
				double Dm = 0.0;
				for (int l=im*MRES; l<=LMAX; l++, lm++) {
					Dm += norm(Q[lm]) + l2[l]*(norm(S[lm]) + norm(T[lm]));
				}
				Dnu += Dm * dV;
				dV = 2.0 * r2dr;		
			}
		}
		diags[0] = Dnu * jpar.nu;
		free(Q);
	}
	if (evol_ubt & EVOL_B) {
		diags = all_diags.append(1, "D_eta\t ");		
		cplx* Q = (cplx*) malloc(3*NLM*sizeof(cplx));
		cplx* S = Q + NLM;		cplx* T = Q + 2*NLM;
		double Deta = 0.0;
		for (int ir=Blm.irs; ir<=Blm.ire; ir++) {
			Blm.curl_QST(ir, Q, S, T);
			double r2dr = r[ir]*r[ir]*Blm.delta_r(ir);
			double dV = 1.0 * r2dr;
			double Deta_r = 0.0;
			int lm=0;
			for (int im=0; im<=MMAX; im++) {
				double Dm = 0.0;
				for (int l=im*MRES; l<=LMAX; l++, lm++) {
					Dm += norm(Q[lm]) + l2[l]*(norm(S[lm]) + norm(T[lm]));
				}
				Deta_r += Dm * dV;
				dV = 2.0 * r2dr;		
			}
			#ifdef XS_ETA_VAR
			Deta += Deta_r * etar[ir];
			#else
			Deta += Deta_r * jpar.eta;
			#endif
		}
		diags[0] = Deta;
		free(Q);
	}

	if (sb_removed) {		
		for (int ir=Ulm.irs-1; ir<=Ulm.ire+1; ir++) {		
			Ulm.Tor[ir][1] += Ulm.radius(ir)*Y10_ct;
		}
	}
}
#endif


#endif

