/** \file xshells.hpp
Compile-time parmeters and customizable functions for XSHELLS.
*/

#ifndef XS_H
	/* DO NOT COMMENT */
	#define XS_H

///  EXECUTION CONTROL  ///
/// 1. stable and tested features ///

/* call custom_diagnostic() (defined below) from main loop to perform and store custom diagnostics */
#define XS_CUSTOM_DIAGNOSTICS

/* enable variable time-step adjusted automatically */
#define XS_ADJUST_DT

/* XS_LINEAR use LINEAR computation : no u.grad(u), no J0xB0  */
#define XS_LINEAR

/* Impose arbitrary stationary flow at the boundaries (for Diapir/Monteux) */
//#define XS_SET_BC

/* use variable conductivity profile eta(r) [see definition of profile in calc_eta() below */
#define XS_ETA_PROFILE

/* Variable L-truncation : l(r) = LMAX * sqrt(r/(rmax*VAR_LTR))  +1 */
//#define VAR_LTR 0.5

///  EXECUTION CONTROL  ///
/// 2. unstable and beta features ///

/* Hyperdiffusion : enable enhanced diffusion constants (see xshells.par)*/
//#define XS_HYPER_DIFF

/* enable output magnetic field surface secular variation up to lmax=lmax_out_sv (set in xshells.par) */
//#define XS_WRITE_SV


#ifdef XS_ETA_PROFILE
	// variable magnetic diffusivity is used :
	#define XS_ETA_VAR
#endif

#else

/* TIME-DEPENDANT BOUNDARY FORCING */
/// This function is called before every time-step, and allows you to modify
/// the Pol/Tor components of the velocity field.
/**  Useful global variables are :
 *  a_forcing : the amplitude set in the .par file
 *  w_forcing : the frequency (pulsation) set in the .par file
 *  \param[in] t is the current time.
 *  \param Bi is the inner SolidBody, whose angular velocity can be modified.
 *  \param Bo is the outer SolidBody, whose angular velocity can be modified.
 */
inline void calc_Uforcing(double t, double a_forcing, double w_forcing, struct SolidBody *Bi, struct SolidBody *Bo)
{
	double DeltaOm = a_forcing;

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 1
		DeltaOm = a_forcing;
		t = - w_forcing*t;		// rotation of equatorial spin-axis.
	#define U_FORCING "Inner-core 'spin-over' forcing (l=1, m=1)"
		Bi->Omega_x = DeltaOm * cos(t); 	Bi->Omega_y = -DeltaOm * sin(t);
//	#define U_FORCING "Mantle 'spin-over' forcing (l=1, m=1)"
//		Bo->Omega_x = DeltaOm * cos(t); 	Bo->Omega_y = -DeltaOm * sin(t);
/*  ________
*/

	/*	add // at the begining of this line to uncomment the following bloc.
		#define FORCING_M 0
		if (w_forcing < 0.0)		// Periodic forcing on frequency |w_forcing|
		{
			DeltaOm *= sin(-w_forcing*t);
		}
		else if (w_forcing > 0.0)	// Impulsive forcing on time-scale 2.*pi/w_forcing.
		{
			if (t*w_forcing > 63.) {		// t > 10*2*pi/w_forcing
				DeltaOm = 0.0;
			} else {
				t = (t*w_forcing/(2.*M_PI) - 3.);	//t = (t - 3.*t_forcing)/t_forcing;
				DeltaOm *= exp(-t*t);	//   /(t_forcing*sqrt(pi)); //constant energy forcing.
			}
		}
	#define U_FORCING "Inner-core differential rotation (l=1, m=0)"
		Bi->Omega_z = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Inner-core equatorial differential rotation (l=1, m=1)"
//		Bi->Omega_x = DeltaOm;		// set differential rotation of inner core.
//	#define U_FORCING "Mantle differential rotation (l=1, m=0)"
//		Bo->Omega_z = DeltaOm;		// set differential rotation of mantle.
//	#define U_FORCING "Mantle differential rotation (l=1, m=1)"
//		Bo->Omega_x = DeltaOm;		// set differential rotation of mantle.
/*  ________
*/

}

#ifdef XS_SET_BC
void set_U_bc(double ampl)
{
	int l,m;

//	/*	add // at the begining of this line to uncomment the following bloc.
	// sinking diapir
//	l=1;	m=0;	Ulm.set_Prlm(NM+1, l, m, -ampl);		// set u_theta "polar bubble sinking" at the outer boundary.
	l=1;	m=1;	Ulm.set_Prlm(NM+1, l, m, -ampl);		// set u_theta "equatorial bubble sinking" at the outer boundary.
/*  ________
*/

}
#endif


#ifdef XS_CUSTOM_DIAGNOSTICS
/// compute custom diagnostics. They can be stored in the diags array (up to ndiags elements), which is written to the energy file.
/// own(ir) is a macro that returns true if the shell is owned by the process (useful for MPI).
/// i_mpi is the rank of the mpi process (0 is the root).
/// This function is called outside a parallel region, so it is safe to use openmp parallel constructs inside.
/// After this function returns, the diags array is summed accross processes and written in the energy.job file.
/// XS_NDIAGS must be set to the number of recorded custom diagnostics.
void custom_diagnostic(diagnostics& all_diags)
{
	// Uncomment the lines below to use particular diagnostic.
	#include "diagnostics/taw_energies.cpp"
	#include "diagnostics/fluid_rotation_vector.cpp"
	#include "diagnostics/dissipation_mantle_cond.cpp"
}
#endif


#ifdef XS_ETA_PROFILE

/// define magnetic diffusivity eta as a function of r
/// discontinuous profiles supported.
void calc_eta(double eta0)
{
	double etam = mp.var["etam"];		// get parameter "etam" set in xshells.par (zero if not set)

	if (etam > 0) printf("=> Magnetic diffusivity in the mantle set to etam = %g\n",etam);

	for (int i=0; i<NR; i++) {
		etar[i] = eta0;			// eta0 by default.

		// Earth Mantle
		if ((i > NM)&&(etam > 0))		etar[i] = etam; 	// mantle almost insulating
	}
}
#endif


#ifdef XS_WRITE_SV
void write_SV(char *fname, int lmax_out) {
	FILE *fp;
	double d1 = 0.5/dt;
	double d2 = 1.0/(dt*dt);
	double dx = 1.0/(r[NM-1]-r[NM]);
	int im, l, lm;
	int mmax_out = MMAX;

	if (lmax_out > LMAX) lmax_out = LMAX;
	if (MMAX*MRES > lmax_out) mmax_out = lmax_out/MRES;
	fp = fopen(fname, "w");
	fprintf(fp,"%% [XSHELLS] Bpol, dBpol/dt, d2Bpol/dt2, Su, Tu, dSu/dt, dTu/dt spherical Harmonics coefficients.\n%% LMAX=%d, MMAX=%d, MRES=%d", lmax_out, mmax_out, MRES);
	for (im=0; im<=mmax_out; ++im) {
		fprintf(fp,"\n%%  m=%d",im*MRES);
		for (l=im*MRES; l<=lmax_out; ++l) {
			lm = LiM(l,im);
			cplx Bp = Blm.Pol[Bp_i1][lm];
			cplx Bp_dt1 = (Bp - Bp2[lm]) * d1;
			cplx Bp_dt2 = ((Bp + Bp2[lm]) - (Bp1[lm]+Bp1[lm])) * d2;
			fprintf(fp,"\n%.10g %.10g \t%.10g %.10g \t%.10g %.10g",Bp.real(),Bp.imag(), Bp_dt1.real(),Bp_dt1.imag(), Bp_dt2.real(),Bp_dt2.imag());
			if (Ulm.bco == BC_FREE_SLIP) {
				cplx S = Ulm.Pol[Ulm.ire-1][lm] * dx;
				cplx T = Ulm.Tor[Ulm.ire][lm];
				cplx S_dt = (S - Up1[lm]*dx) * d1;
				cplx T_dt = (T - Ut1[lm]) * d1;
				fprintf(fp," \t%.10g %.10g \t%.10g %.10g \t%.10g %.10g \t%.10g %.10g",S.real(),S.imag(), T.real(),T.imag(), S_dt.real(),S_dt.imag(), T_dt.real(),T_dt.imag());
			}
		}
	}
	fclose(fp);
}
#endif

/* DO NOT REMOVE */
#endif
