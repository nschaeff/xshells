%XS Temperature flux lmax=2 mmax=2 norm=1
% this is a comment: the spherical harmonics coefficients are listed one per
% line, grouped by m starting at m=0 (m=0 implies a zero imaginary part)
% it is advised to avoid l=0 m=0, which has not always stationary solution.
0 0  #l=0,m=0
0 0  #l=1,m=0
0 0  #l=2,m=0
0 0  #l=1,m=1
0 0  #l=2,m=1
-0.7071067811865475 0  #l=2,m=2
