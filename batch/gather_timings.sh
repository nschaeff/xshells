#!/bin/bash

for fname in $*
do
	t=`grep -e average $fname | sed -e "s/[^0-9]*\([0-9.][0-9.]*\).*/\1/"`
	nmpi=`grep -e "processes, with" $fname  | sed -e "s/[^0-9]*\([0-9][0-9]*\).*/\1/"`
	nth=`grep -e "processes, with" $fname  | sed -e "s/[^0-9]*[0-9][0-9]*[^0-9]*\([0-9][0-9]*\).*/\1/g"`
	vid=`grep -e "version, id:" $fname`

	if [ "x$t" == "x" ]; then
		# try to read old version of output (without average time):
		t=`grep $fname -e "elapsed"  | sed -e "s/.*elapsed \([0-9.]\+\) s for \([0-9]\+\) .*/\1 \2/" | awk '{ sum += (n<=1) ? 0 : $1 / $2; n++} END { if (n > 2) print sum / (n-2) }'`
	fi

	ename=`echo $fname | sed -e "s/\.o$/\.e/"`
	if [ -f $ename ]; then
		vid=`grep -e "ccc_mprun" -e "srun" -e "mpirun" $ename | tail -1 | sed -e "s/.*run//"`
	fi

	if [ "x$t" != "x" ]; then
		echo $nmpi $nth $t "  # $fname  $vid"
	fi
done
