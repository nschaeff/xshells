
/// A simple, lightweight, generic 2D array class that allows efficient access with [i][j], with a possible shift for i.
/// requires C++11 compiler. There is no bound check. Mimics the behaviour of T**.

template <typename T>
class xs_array2d {
    template <typename U> friend class xs_array2d;	// needed to copy private data from other template classes.
  private:
    T* data;
    size_t dist;

  public:
    xs_array2d() : data(0), dist(0) { }
//    xs_array2d(std::nullptr_t) : data(0), dist(0) { }
    template <typename U>
    xs_array2d(const xs_array2d<U> &a) {
	static_assert( sizeof(U) == sizeof(T), "xs_array2d construction from incompatible type." );
	dist = a.dist;
	data = (T*) a.data;	
    }

    void init_from_buffer(T* buf, size_t dist_, int i0=0) {
	data = buf - i0*dist_;
	dist = dist_;
    }

    explicit operator bool() const {  return (dist > 0);  }	///< true for non-empty array, false for empty one.
//    bool operator == (std::nullptr_t) const { return (dist==0); }		///< comparison to null pointer

    T* operator[] (int i) const { return data + i*dist; }	///< returns a pointer to the beginning of line i.

    size_t get_dist() const { return dist; }		///< returns the length of line (the linear increment between two lines).

    void clear() { data = 0;	dist = 0; }

    /// assignement operator, including from a different (but compatible) type.
    template <typename U>
    xs_array2d<T> operator = (const xs_array2d<U> &a) {
	static_assert( sizeof(U) == sizeof(T), "xs_array2d assignement from incompatible type." );
	dist = a.dist;
	data = (T*) a.data;
	return *this;
    }
/*    /// assignement operator from nullptr (zero)
    xs_array2d<T> operator = (std::nullptr_t) {
	clear();
	return *this;
    }*/
};

/// A simple generic 2D array class that allows efficient access with [i][j], and self-manages its own memory.
template <typename T>
class xs_array2d_mem : public xs_array2d<T> {
  private:
    T* mem;

    void free_() {
	free(mem);
	mem = 0;
    }

  public:
    xs_array2d_mem() : mem(0) {}
    xs_array2d_mem(int isize, int jsize, int i0=0) { alloc(isize, jsize, i0); }
    ~xs_array2d_mem() { free_(); }

    void alloc(int isize, int jsize, int i0=0) {
	mem = (T*) malloc(sizeof(T) * isize * jsize);
	this->init_from_buffer(mem, jsize, i0);
    }

    /// assignement operator, including from a different (but compatible) type.
    template <typename U>
    xs_array2d_mem<T> operator = (const xs_array2d<U> &a) {
	static_assert( sizeof(U) == sizeof(T), "xs_array2d assignement from incompatible type." );
	if (this != a) {
	    free_();		// free allocated memory if needed.
	    this->init_from_buffer((T*) a.data, a.dist);
	}
	return *this;
    }
    void clear() {
	free_();
	xs_array2d<T>::clear();		// mark as empty.
    }
/*	/// assignement operator from nullptr (zero)
	xs_array2d_mem<T> operator = (std::nullptr_t) {
	free_();		// free allocated memory if needed.
	this->clear();		// mark as empty.
	return *this;
    }*/
};
