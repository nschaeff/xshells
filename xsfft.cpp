/*
 * Copyright (c) 2010-2016 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xsfft.c
/// XShells Fast Fourier Transform : temporal spectral analysis of 3D pol/tor data series from xshells.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <fftw3.h>
#include <shtns.h>

#include "grid.cpp"
#include "xshells_spectral.cpp"
#include "xshells_io.cpp"

#define fftw_fcplx(x) reinterpret_cast<fftwf_complex*>(x)

PolTor Blm;
ScalarSH Tlm;
Spectral *SH;

fcplx *big = NULL;

double pi = M_PI;

FieldInfo jpar;	// parameters from loaded file

void usage()
{
	printf("\nUsage: xsfft <base_name> <extension> [options] [istart [step [iend]]]\n");
	printf("   xsfft performs the temporal Fourier transform of the series of spatial field stored in files base_name<%%04d>.extension\n");
	printf(" filtering options may help reduce memory usage :\n");
	printf(" -rlim=<ri>:<ro> : the energy spectrum is reduced to this shell span.\n");
	printf(" -lmax=<lmax> : use only spherical harmonic degrees up to lmax.\n");
	printf(" -mmax=<mmax> : use only spherical harmonic orders up to mmax.\n");
	printf(" -m=<m> : use only spherical harmonic order m.\n");
}

double hamming(int k, int nf)
{
	const double alpha = 0.54;
	return alpha - (1.-alpha)*cos(2.*M_PI*k/nf);
}

int main (int argc, char *argv[])
{
	double *Espec;
	double rmin, rmax;
	double to, dt, df;
	float norm;
	long int ndata;
	size_t memsize;
	int i, istart, iend, istep;
	int k, nf, nc;
	int irs, ire, nr;
	char fname[128];
	char *fbase;
	char *fext;
	FILE *fp;

	printf(" [XSFFT] Xshells Fast Fourier Transform   by N. Schaeffer / CNRS, build %s, %s\n",__DATE__,__TIME__);
	if (argc < 3) { usage(); exit(1); }
	
	fbase = argv[1];	fext = argv[2];
	istart = 0;		istep = 1;	iend = 1024*1024;	// end when no more files are left.
	MMAX = 1024*1024;		LMAX = 1024*1024;		MRES = 0;		// no limits

	rmin = 0;	rmax=0;
	i = 3;
	while(i < argc) {
		if (argv[i][0] == '-') {		//option
			sscanf(argv[i],"-%[^=]=%lf",fname,&to);
			if (strcmp(fname,"lmax") == 0) LMAX = to;
			if (strcmp(fname,"mmax") == 0) MMAX = to;
			if (strcmp(fname,"m") == 0) {	MMAX = to;		MRES = to;	}
			if (strcmp(fname,"rlim") == 0) {	sscanf(argv[i],"-%[^=]=%lf:%lf",fname,&rmin,&rmax);	}
			i++;
		} else break;
	}
	if (i < argc) {
		sscanf(argv[i], "%d", &istart);
		if (i+1 < argc) sscanf(argv[i+1], "%d", &istep);
		if (i+2 < argc) sscanf(argv[i+2], "%d", &iend);
	}

	sprintf(fname, "%s%04d.%s", fbase, istart, fext);

// get File infos
	if (load_FieldInfo(fname, &jpar)) {
		printf("\nmetadata for file '%s' : \n",fname);
		print_FieldInfo(&jpar);		// print metadata
		printf("\n");
	} else {		// not a poltor file
		usage(); exit(1);
	}
	irs = jpar.irs;		ire = jpar.ire;
	nr = ire-irs+1;
	nc = jpar.nc;		// number of components.
	if (nc == 2) {
			SH = &Blm;
	} else	SH = &Tlm;

// set correct spherical harmonic sizes and grid.
	if (jpar.lmax < LMAX) LMAX = jpar.lmax;
	if (jpar.mmax < MMAX) MMAX = jpar.mmax;
	if (MRES == 0) MRES = jpar.mres;
	if (MMAX*MRES > LMAX) MMAX = LMAX/MRES;
	NLM = nlm_calc(LMAX, MMAX, MRES);
	init_sh_vars(jpar.shtnorm);		// initialize some sh variables
	load_Field_grid(fname);		// load radial grid
	init_Deriv_sph(r, NR);			// compute derivatives

//count and check files
	i = istart + istep;
	while(i <= iend) {
		sprintf(fname, "%s%04d.%s", fbase, i, fext);
		fp = fopen(fname, "r");
		if (fp == NULL) {
			iend = i - istep;
		} else
			fclose(fp);
		i+=istep;
	}
	nf = (iend-istart)/istep +1;
	printf("** About to perform FFT of %d files with %d radial shells of %d modes (lmax=%d, mmax=%d, mres=%d) **\n", nf, nr, NLM, LMAX, MMAX, MRES);
	Espec = (double*) malloc(sizeof(double) * nf);		// array to store the temporal spectrum.

// allocate big array, using float
	ndata = nr * NLM * nc;		// number of data points in a snapshot (pol/tor, radial shells, SH coeffs)
	to = (((double) ndata) * nf * sizeof(fcplx)) / (1024.*1024.);		// memory to allocate in Mb
	printf("=> try to allocate %.3lf Mb ... ", to);
	if ( (sizeof(memsize) <= 4) && (to > 2000.) ) runerr("Cannot allocate more than 2Gb on this system\n");		// check for more 64 bit allocation size
	memsize = sizeof(fcplx) * nf * ndata;		// fcplx data Pol/Tor
	big = (fcplx*) VMALLOC( memsize );
	if (big == NULL) {	printf("failed.\n filtering options -mmax, -lmax or -m may be used to reduce memory requirements.\n"); exit(1);  }
	printf("success.\n");

// load files and copy into big array
	printf(" => loading data...\n");
	i=istart;		dt = 0.0;	to = -1.0;
	for (k=0; k<nf; k++) {
		//const double w = hamming(k,nf);
		const double w = 1.0;
		sprintf(fname, "%s%04d.%s", fbase, i, fext);		i+=istep;
		if (nc == 2) {
				Blm.load(fname, &jpar);
		} else 	Tlm.load(fname, &jpar);
		printf("[%04d] loading %s  (t = %lf)\r", k, fname, jpar.t);		fflush(stdout);
		if ((jpar.irs != irs) || (jpar.ire != ire)) runerr("fields do not match");
		for (int ic=0; ic<nc; ic++) {
			for (int ir=irs; ir<=ire; ir++) {
				for (int lm=0; lm<=NLM; lm++) {
					big[k*ndata + (ir-irs+nr*ic)*NLM + lm] = SH->get_data(ic, ir)[lm] * w;
				}
				big[k*ndata + (ir-irs+nr*ic)*NLM] = 0.0;		// l=0 => zero
			}
		}
		if (to >= 0.0) {
			if ((dt > 0.0) && (abs(dt - (jpar.t - to)) > 1.e-4*dt))
				printf("  ! Warning : non equispaced samples !\n");
			dt = jpar.t - to;
		}
		to = jpar.t;
	}

	printf("\n => performing FFT...\n");
// perform fftw (in-place)
	fftwf_plan fft = fftwf_plan_many_dft(1, &nf, ndata, fftw_fcplx(big), NULL, ndata, 1, fftw_fcplx(big), NULL, ndata, 1, FFTW_FORWARD, FFTW_ESTIMATE);
	if (fft == NULL) runerr("fftw failed.");
	fftwf_execute(fft);

// radial span for spectrum
	int irs_e = irs;
	int ire_e = ire;
	if (rmax > rmin) {
		irs_e = r_to_idx(rmin);
		ire_e = r_to_idx(rmax);
		printf(" energy spectrum betwen r=%g and %g.\n", r[irs_e], r[ire_e]);
	}

	printf(" => storing result...\n");
	//norm = dt;		// physical normalization
	norm = 1.0/nf;		// "numerical" normalization : freq 0 is then the average.
	df = 1.0/(dt*nf);
	SpectralDiags sd(LMAX,MMAX);
// store results and compute spectra.
	for (k=0; k<nf; k++) {
		double f = df * k;
		if (k>nf/2) f -= nf*df;		// negative frequency.
		for (int ic=0; ic<nc; ic++) {
			for (int ir=irs; ir<=ire; ir++) {
				for (int lm=0; lm<=NLM; lm++) {
					SH->get_data(ic, ir)[lm] = big[k*ndata + (ir-irs+nr*ic)*NLM + lm] * norm;
				}
			}
		}
		sprintf(fname, "%sF%04d.%s", fbase, k, fext);
		SH->save_single(fname, LMAX, MMAX, f);
		SH->energy(sd, irs_e, ire_e);
		Espec[k] = sd.energy();
		printf("[%04d] %s stored  (f = %lf)  E=%g\r", k, fname, f, Espec[k]);		fflush(stdout);
	}

// save spectra
	sprintf(fname, "%sEfspec.%s", fbase, fext);
	fp = fopen(fname,"w");
	fprintf(fp,"%% [XSHELLS] temporal spectrum computed by xsfft. first column is the freqency, 2nd column is the energy.\n");
	for (k=nf/2+1; k<nf; k++)
		fprintf(fp,"%g  %.6g\n", df*(k-nf), Espec[k]);
	for (k=0; k<nf/2+1; k++)
		fprintf(fp,"%g  %.6g\n", df*k, Espec[k]);
	fclose(fp);
	free(Espec);
	printf("\nall done.\n");
}
