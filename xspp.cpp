/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xspp.cpp
/// XShells Post-Processing

#define DEB printf("%s:%u pass\n", __FILE__, __LINE__)
//#define DEB (0)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <shtns.h>

#ifdef _OPENMP
	#include <omp.h>
#endif

#ifndef XS_DEBUG
#define XS_DEBUG 1
#endif
#include "grid.cpp"
#include "xshells_spectral.cpp"
#include "xshells_spatial.cpp"
#include "xshells_render.cpp"
#include "xshells_io.cpp"

VectField B;
PolTor Blm;
ScalarSH Tlm;
ScalField T;
Spectral* SH;		// generic spectral field for generic operations.

double rot_angle = 0.;	// rotation angle
double alpha=0;
double beta=0;
double gamma_=0;
double r_min, r_max=0;	// requested rmin, rmax are saved here.
int irs, ire;	// loaded from file.
int ips=0, ipe;
int its=0, ite;
int ncomp;
int vchar = 'U';	// velocity field by default

double pi = M_PI;

double *lspec;	// spectra
double *mspec;

FieldInfo jpar;	// parameters from loaded file

/// find closest index to phi angle.
inline int phi_to_idx(double phi) {
	long int j = lround(phi/360. *NPHI*MRES) % NPHI;
	if (j < 0.0) j += NPHI;
	return j;
}

/*
/// find closest index to theta angle.
inline int theta_to_idx(double th) {
	long int j = lround(th/180. *(NLAT-1));
	return j;
}
*/

inline double xy_to_phi(double x, double y) {
	double p = atan(y/x);
	if (x < 0.) p += M_PI;
	return p;
}

int Nlayer(double r0, double r1, double dr0, double q)
{
	int i = 0;
	while(r0<r1) {
		r0 += dr0;
		dr0 *=q;
		i++;
	}
	return i;
}

/// prolongates a magnetic field with a potential field, or a no-slip velocity field with a solid body rotation.
void PolTor::prolongate_field(double rmin, double rmax)
{
	double q = 1.1;			// geometric reason for added points.
	double dr;
	int Ni,No, ir, NRo;
	int lm;
	double *rt;
	PolTor Bt;

	if (rmin > r[0]) rmin = r[0];		if (rmax < r[NR-1]) rmax = r[NR-1];		// avoid problems.
	Ni = 0;	No = 0;
	if ((bci == BC_MAGNETIC)||(bci == BC_NO_SLIP)) Ni = Nlayer(rmin,r[0], r[1]-r[0], q);
	if ((bco == BC_MAGNETIC)||(bco == BC_NO_SLIP)) No = Nlayer(r[NR-1],rmax, r[NR-1]-r[NR-2], q);
	if ((Ni+No) == 0) return;

	printf("> extending field : adding %d points in the inner part, and %d points in the outer part\n",Ni,No);

	rt = (double *) malloc((NR + Ni + No)*sizeof(double));	// alloc new grid.
	for(ir=0; ir<NR; ir++) rt[ir+Ni] = r[ir];		// copy current grid.
	dr = r[1]-r[0];					// generate inner part
	for(ir=Ni-1; ir>0; ir--) {
		rt[ir] = rt[ir+1] - dr;		dr *= q;
	}
	dr = r[NR-1]-r[NR-2];			// generate outer part
	for(ir=NR+Ni; ir<NR+Ni+No-1; ir++) {
		rt[ir] = rt[ir-1] + dr;		dr *= q;
	}
	rt[0] = rmin;		rt[NR+Ni+No-1] = rmax;

	NRo = NR;						// save old radial size
	r = rt;		NR += Ni+No;		// commit changes to radial structure.
	init_Deriv_sph(r, NR);				// recompute derivatives.
	
	Bt.alloc(0, NR-1);
	Bt.bci = bci;	Bt.bco = bco;		// copy bc
	for (ir=-1; ir<=NRo; ir++) {		// copy data including ghost shells.
		for (lm=0; lm<NLM; lm++) {
			Bt.Pol[ir+Ni][lm] = Pol[ir][lm];		Bt.Tor[ir+Ni][lm] = Tor[ir][lm];		// copy from source
		}
	}

	for (ir=0; ir<Ni; ir++) {		// inner prolongation
		q = r[ir]/r[Ni];
		if (bci == BC_MAGNETIC) {	// magnetic
			for (lm=0; lm<NLM; lm++) Bt.Pol[ir][lm] = Bt.Pol[Ni][lm] * pow(q, el[lm]);
		}
		else {		// no-slip
			lm = LiM(shtns,1,0);		set_Ylm(Bt.Tor[ir], 1, 0, Bt.Tor[Ni][lm] * q);
							//	set_Ylm(Bt.Tor[ir], 1, 0, Y10_ct * r[ir]);        // Omega = 1.0
			if ((MRES==1)&&(MMAX>=1)) {
				lm = LiM(shtns,1,1);	set_Ylm(Bt.Tor[ir], 1, 1, Bt.Tor[Ni][lm] * q);
			}
		}
	}
	for (ir=NRo+Ni; ir<NR; ir++) {		// outer prolongation
		q = r[ir]/r[NRo+Ni-1];
		if (bco == BC_MAGNETIC) {	// magnetic
			for (lm=0; lm<NLM; lm++) Bt.Pol[ir][lm] = Bt.Pol[NRo+Ni-1][lm] * pow(q, -(el[lm]+1.));
		}
		else {		// no slip
			lm = LiM(shtns,1,0);		set_Ylm(Bt.Tor[ir], 1, 0, Bt.Tor[NRo+Ni-1][lm] * q);
							//	set_Ylm(Bt.Tor[ir], 1, 0, Y10_ct * r[ir]);	// Omega = 1.0
			if ((MRES==1)&&(MMAX>=1)) {
				lm = LiM(shtns,1,1);	set_Ylm(Bt.Tor[ir], 1, 1, Bt.Tor[NRo+Ni-1][lm] * q);
			}
		}
	}

	// commit changes. This is prone to bugs as we deal with the internal representation of xs_array2d and Spectral classes...
	free_field();		// free original field memory.
	data = Bt.data;		// copy xs_array2d
	nelem = Bt.nelem;	// don't forget to copy nelem...
	nshell = Bt.nshell;	// ... and nshell !
	ir_bci = Bt.ir_bci;		ir_bco = Bt.ir_bco;		// update irs, ire
	irs = ir_bci;			ire = ir_bco;
	::irs = ir_bci;			::ire = ir_bco;			// update global variables.
	update_ptr();
	Bt.data.clear();		// prevent the freeing of the data of Bt (that is now used by this)
}

void write_HS(const char *fn, const xs_array2d<cplx> &HS)
{
	int ir,lm,l,m;
	FILE *fp;

	fp = fopen(fn,"w");
	fprintf(fp,"%% [XSHELLS] Spherical Harmonics coefficients : MMAX=%d, LMAX=%d, MRES=%d. l-contiguous storing", MMAX*MRES/jpar.mres, LMAX, jpar.mres);
	for (ir=irs-1;ir<=ire+1;ir++) {
		if (HS[ir] != NULL) {
			fprintf(fp,"\n%%  ir=%d, r=%f\n",ir,r[ir]);
			for (m=0; m<=MMAX*MRES; m+=jpar.mres) {
				for (l=m; l<=LMAX; l++) {
					lm = LM(shtns,l,m);
					fprintf(fp,"%.7g %.7g  ",real(HS[ir][lm]),imag(HS[ir][lm]));
				}
			}
		}
	}
	fprintf(fp,"\n");	fclose(fp);
}


/// sym must be 0 or 1 to select symmetric or antisymmetric modes.
/// ex: magnetic dipole is sym=1
void write_denys(const char *fn, PolTor *PT, int sym)
{
	xs_array2d<cplx> HS;
	int ir,lm,l,m;
	FILE *fp;
	
	fp = fopen(fn,"w");
//	fprintf(fp,"%% [XSHELLS] Spherical Harmonics coefficients : sym=%d, MMAX=%d, LMAX=%d, MRES=%d. Written for D. Schmitt", sym, MMAX*MRES/jpar.mres, LMAX, jpar.mres);
	for (m=0; m<=MMAX*MRES; m+=jpar.mres) {
		for (l=m; l<=LMAX; l++) {
			if (l==0) l++;		// skip l=0
			lm = LM(shtns,l,m);
//			fprintf(fp,"\n%%  l=%d, m=%d\n",l,m);
			if ( ((l-m)&1) == sym) {
				HS = PT->Pol;
			} else  HS = PT->Tor;
			for (ir=irs;ir<=ire;ir++) {
				if (HS[ir] != NULL)
					fprintf(fp,"%.6g %.6g\n",real(HS[ir][lm]),imag(HS[ir][lm]));
			}
		}
	}
	fclose(fp);	
}

void write_Spec_nrj(Spectral *S)
{
	int ir,l,im;
	FILE *fel, *fem;
	SpectralDiags sd(LMAX,MMAX);

	fel = fopen("o_Elr","w");
	fem = fopen("o_Emr","w");
	fprintf(fel,"%% [XSHELLS] Energy spectrum : MMAX=%d, MRES=%d. first row is r", MMAX, MRES);
	fprintf(fem,"%% [XSHELLS] Energy spectrum : LMAX=%d. first row is r", LMAX);
	for (ir=irs;ir<=ire;ir++) {
		S->r_spectrum(ir, sd);
		fprintf(fem,"\n%%  ir=%d, r=%f\n%.6g ",ir,r[ir],r[ir]);
		fprintf(fel,"\n%%  ir=%d, r=%f\n%.6g ",ir,r[ir],r[ir]);
		for (im=0; im<=MMAX; im++)
			fprintf(fem, "%.6g ", sd.Em[im]);
		for (l=0; l<=LMAX; l++)
			fprintf(fel, "%.6g ", sd.El[l]);
	}
	fprintf(fel,"\n");	fclose(fel);
	fprintf(fem,"\n");	fclose(fem);
}


void write_Spec_m(const char *fn, const xs_array2d<cplx> &HS)
{
	double sum, cr,ci;
	int ir,lm,l,m;
	FILE *fp;

	fp = fopen(fn,"w");
	fprintf(fp,"%% [XSHELLS] Spherical Harmonics m-spectrum : MMAX=%d, MRES=%d. first row is r", MMAX*MRES/jpar.mres, jpar.mres);
	for (ir=irs-1;ir<=ire+1;ir++) {
		if (HS[ir] != NULL) {
			double rr = NAN;
			if IN_RANGE_INCLUSIVE(ir,irs,ire) 	rr = r[ir];
			fprintf(fp,"\n%%  ir=%d, r=%f\n%.6g ",ir,rr,rr);
			for (m=0; m<=MMAX*MRES; m+=jpar.mres) {
				sum = 0.0;
				for (l=m; l<=LMAX; l++) {
					lm = LM(shtns,l,m);
					cr = real(HS[ir][lm]);	ci = imag(HS[ir][lm]);
					sum += cr*cr + ci*ci;
				}
				fprintf(fp,"%.6g ",sum);
			}
		}
	}
	fprintf(fp,"\n");	fclose(fp);
}

void write_Spec_l(const char *fn, const xs_array2d<cplx> &HS)
{
	double sum, cr,ci;
	int ir,lm,l,m;
	FILE *fp;

	fp = fopen(fn,"w");
	fprintf(fp,"%% [XSHELLS] Spherical Harmonics l-spectrum : LMAX=%d. first row is r", LMAX);
	for (ir=irs-1;ir<=ire+1;ir++) {
		if (HS[ir] != NULL) {
			double rr = NAN;
			if IN_RANGE_INCLUSIVE(ir,irs,ire) 	rr = r[ir];
			fprintf(fp,"\n%%  ir=%d, r=%f\n%.6g ",ir,rr,rr);
			for (l=0; l<=LMAX; l++) {
				sum = 0.0;
				for (m=0; (m<=MMAX*MRES)&&(m<=l); m+=jpar.mres) {
					lm = LM(shtns,l,m);
					cr = real(HS[ir][lm]);	ci = imag(HS[ir][lm]);
					sum += cr*cr + ci*ci;
				}
				fprintf(fp,"%.6g ",sum);
			}
		}
	}
	fprintf(fp,"\n");	fclose(fp);
}


void write_box_xdmf(const char* fn, int nx, int ny, int nz, int comp=3)
{
	char fn_xml[256];
	FILE *xml;

	sprintf(fn_xml, "%s.xdmf", fn);
	xml = fopen(fn_xml,"w");
	fprintf(xml, "<?xml version=\"1.0\" ?>\n");
	fprintf(xml, "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" [\n");
	fprintf(xml, "<!ENTITY DataFile \"%s\">\n]>\n", fn);
	fprintf(xml, "<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">\n");
	fprintf(xml, "<Domain>\n  <Grid Name=\"Structured Grid\" GridType=\"Uniform\">\n");
    fprintf(xml, "\t<Topology TopologyType=\"3DRECTMesh\" NumberOfElements=\"%d %d %d \"/>\n", nx,ny,nz);
    fprintf(xml, "\t<Geometry GeometryType=\"VXVYVZ\">\n");
    fprintf(xml, "\t\t<DataItem Dimensions=\"%d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">&DataFile;:/x</DataItem>\n",nx);
    fprintf(xml, "\t\t<DataItem Dimensions=\"%d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">&DataFile;:/y</DataItem>\n",ny);
    fprintf(xml, "\t\t<DataItem Dimensions=\"%d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">&DataFile;:/z</DataItem>\n",nz);
    if (comp==3) {
		fprintf(xml, "\t</Geometry>\n\t<Attribute Name=\"%c\" AttributeType=\"Vector\" Center=\"Node\">\n", fn[5]);	// U or B.
		fprintf(xml, "\t\t<DataItem ItemType=\"Function\" Dimensions=\"%d %d %d 3\" Function=\"JOIN($0 , $1 , $2)\">\n",nx,ny,nz);
		fprintf(xml, "\t\t\t<DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">&DataFile;:/vx</DataItem>\n",nx,ny,nz);
		fprintf(xml, "\t\t\t<DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">&DataFile;:/vy</DataItem>\n",nx,ny,nz);
		fprintf(xml, "\t\t\t<DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">&DataFile;:/vz</DataItem>\n",nx,ny,nz);
		fprintf(xml, "\t\t</DataItem>\n");
	} else {
		fprintf(xml, "\t</Geometry>\n\t<Attribute Name=\"T\" AttributeType=\"Scalar\" Center=\"Node\">\n");
		fprintf(xml, "\t\t<DataItem Dimensions=\"%d %d %d\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">&DataFile;:/s</DataItem>\n",nx,ny,nz);
	}
	fprintf(xml, "\t</Attribute>\n  </Grid>\n</Domain>\n</Xdmf>\n");
	fclose(xml);
}

/*
void angular_momentum(VectField *B, int irs, int ire)
{
	double Lx, Ly, Lz, L;
	double lx, ly, lz;
	double phi, dV0, dV;
	int ir,ip,it, j;
	
	Lx = 0.;	Ly = 0.;	Lz = 0.;
	if (irs == B->irs) irs++;		// discard boundaries
	if (ire == B->ire) ire--;
	for (ir=irs; ir<=ire; ir++) {
		dV0 = 0.5*(dr[ir-1]+dr[ir]) *r[ir]*r[ir] * (2.*M_PI/NPHI) * (M_PI/(NLAT-1));
		for (ip=0; ip<NPHI; ip++) {
			phi = phi_rad(ip);
			for (it=0, j=ip*NLAT; it<NLAT; it++, j++) {
				lx = 0.0;
				ly = - r[ir] * B->vp[ir][j];
				lz =   r[ir] * B->vt[ir][j];
				spher_to_cart(ct[it], phi, &lx, &ly, &lz);
				dV = dV0 * st[it];
				Lx += lx*dV;	Ly += ly*dV;	Lz += lz*dV;
			}
		}
	}
	L = sqrt(Lx*Lx + Ly*Ly + Lz*Lz);
	printf("> angular momentum (cartesian coords) : L=[%e,%e,%e], |L|=%e, theta=%.4f°, phi=%.4f°\n",Lx,Ly,Lz,L, acos(Lz/L)*180./M_PI, xy_to_phi(Lx,Ly)*180./M_PI);
}
*/

/// compute the rotation vector of the fluid (project flow on l=1). See PhD thesis of J. Noir, page 60-61.
/// and subtract it from the field.
void rotation_vector(PolTor &Blm, int irs, int ire)
{
	double Wx, Wy, Wz, w;
	double dV, n;
	int ir;

	Wx=0;	Wy=0;	Wz=0;	n=0;
	for (ir=irs; ir<=ire; ir++) {
		dV = r[ir]*r[ir]*r[ir] * Blm.delta_r(ir);
		n += r[ir]*dV;
		Wz += dV*real(Blm.Tor[ir][LiM(shtns,1,0)]);
		if ((MRES == 1)&&(MMAX > 0)) {
			Wx += dV*real( Blm.Tor[ir][LiM(shtns,1,1)] );
			Wy -= dV*imag( Blm.Tor[ir][LiM(shtns,1,1)] );
		}
	}
	Wx/=Y11_st;	Wy/=Y11_st;	Wz/=Y10_ct;
	dV = (8.*M_PI)/3.;		// geometric prefactor for a sphere (L=8pi/3*[r5/5]*<omega>)
	n = 1./n;
	w = sqrt(Wx*Wx + Wy*Wy + Wz*Wz);
	printf("%% Lx Ly Lz |L| \t\t Wx Wy Wz |W| \t\t  theta°  phi° \n");
	printf("%e %e %e %e \t %e %e %e %e \t %.4f %.4f %% angular momentum and rotation vector\n",Wx*dV,Wy*dV,Wz*dV,sqrt(Wx*Wx + Wy*Wy + Wz*Wz)*dV, Wx*n,Wy*n,Wz*n,w*n, acos(Wz/w)*180./M_PI, xy_to_phi(Wx,Wy)*180./M_PI);

	// subtract solid body rotation from field.
	Wx*=n*Y11_st;	Wy*=n*Y11_st;	Wz*=n*Y10_ct;
	for (ir=irs; ir<=ire; ir++) {
		Blm.Tor[ir][LiM(shtns,1,0)] -= Wz*r[ir];
		if ((MRES == 1)&&(MMAX > 0)) {
			Blm.Tor[ir][LiM(shtns,1,1)] -= cplx(Wx, -Wy)*r[ir];
		}
	}
	printf("> solid body rotation has been subtracted.\n");
}

double calc_TorqueMag(PolTor &Blm, int irs, int ire)
{
	VectField J;
	cplx *S;
	cplx *T;
	int lm = LiM(shtns,1,0);
	double torque = 0.0;

	J.alloc(irs, ire);
	B.alloc(irs, ire);
	Blm.to_spat(&B, irs, ire);
	Blm.to_curl_spat(&J, irs, ire);
	J.NL_vect(&J, &B, irs, ire);

	S = (cplx *) VMALLOC( sizeof(cplx) * 2*NLM);
	T = S +NLM;
	for (int ir=irs+1; ir<ire; ir++) {
		spat_to_SHsphtor_l(shtns, J.vt[ir], J.vp[ir], S, T, 1);
		torque += real(T[lm]) * r[ir]*r[ir]*r[ir]*(r[ir+1]-r[ir-1])*0.5;
	}
	torque *= 8*M_PI/3 /Y10_ct;		// normalization
	printf("Magnetic torque between r=%f and r=%f : %e\n",r[irs], r[ire], torque );
	VFREE(S);
	return(torque);
}

/// Magnetic torque computed using surface expression.
double calc_TorqueMag_surf(PolTor &Blm, int ir)
{
	MemBlock mem(2*NLM*sizeof(cplx) + 3*shtns->nspat*sizeof(double), 5);
	cplx *Q = mem.alloc_cplx(NLM);
	cplx *S = mem.alloc_cplx(NLM);
	Shell2d_mbk br(shtns,mem), bt(shtns,mem), bp(shtns,mem);

	Blm.RadSph(ir, Q, S);
	SHqst_to_spat(shtns, Q, S, Blm.Tor[ir], br, bt, bp);

	for (int ip=0; ip<NPHI; ip++) {
		for (int it=0; it<NLAT; it++) {
			bt(it,ip)  = bp(it,ip)*br(it,ip)*st[it];
		}
	}
	spat_to_SH_l(shtns, bt, Q, 0);
	double torque = real(Q[0]) * 4*M_PI/Y00_1  * r[ir]*r[ir]*r[ir];
	printf("Axial magnetic torque at r=%f : %e\n",r[ir], torque);
	return(torque);
}


double calc_TorqueVisc(PolTor &Ulm, double rr)
{
	double torque;
	int ir = r_to_idx(rr);
	int lm = LiM(shtns,1,0);

	if ((ir == NR-1) || (Ulm.Tor[ir+1]  == NULL)) {
		if (Ulm.Tor[ir-1] == NULL) return (0.0);
		torque = -real(Ulm.Tor[ir-1][lm]) / (r[ir-1]*(r[ir]-r[ir-1]));
	} else if ((ir == 0) || (Ulm.Tor[ir-1]  == NULL)) {
		if (Ulm.Tor[ir+1] == NULL) return (0.0);
		torque = real(Ulm.Tor[ir+1][lm]) / (r[ir+1]*(r[ir+1]-r[ir]));
	} else {
		if ((Ulm.Tor[ir+1] == NULL) || (Ulm.Tor[ir-1] == NULL)) return (0.0);
		OpGrad& G = GRAD(ir);
		torque = G.Gl * real(Ulm.Tor[ir-1][lm])*r_1[ir-1] + G.Gd * real(Ulm.Tor[ir][lm])*r_1[ir] + G.Gu * real(Ulm.Tor[ir+1][lm])*r_1[ir+1];
	}
	torque *= r[ir]*r[ir]*r[ir] /Y10_ct;
	printf("Viscous torque at r=%f : %e (for viscosity = 1)\n",r[ir], torque );
	return(torque);
}

/// compute an accurate profile along a straight line. No interpolation involved, but points will not be regularly spaced.
void write_line(const char *fn,double x0,double y0,double z0,double x1,double y1,double z1, Spectral *SH)
{
	double rr,cost,phi;
	double x,y,z, vx,vy,vz, nv;
	double bb,r0, alpha, d;
	double br,bt,bp, bx,by,bz;
	int ir, irinc;
	FILE *fp;
	
	fp = fopen(fn,"w");
	if (SH->get_ncomp() == 2) {
		fprintf(fp,"%% [XSHELLS] line profile starting from %f,%f,%f to %f,%f,%f with linear coordinate alpha\n%% alpha \tx y z\tr cos(theta) phi\tvx vy vz\tvr vt vp\n",x0,y0,z0,x1,y1,z1);
	} else {
		fprintf(fp,"%% [XSHELLS] line profile starting from %f,%f,%f to %f,%f,%f with linear coordinate alpha\n%% alpha \tx y z\tr cos(theta) phi\tvalue\n",x0,y0,z0,x1,y1,z1);
	}

	vx = x1-x0;	vy = y1-y0;	vz = z1-z0;
	nv = sqrt(vx*vx+vy*vy+vz*vz);
	vx /=nv; vy/=nv; vz/=nv;
	
	r0 = x0*x0 + y0*y0 + z0*z0;
	bb = vx*x0 + vy*y0 + vz*z0;
	ir = r_to_idx(sqrt(r0));			// initial shell guess
	if (bb < 0.) {
		irinc = -1;
		if (r[ir]*r[ir] > r0) ir--;
		if (ir>ire) ir=ire;
	} else {
		irinc = +1;
		if (r[ir]*r[ir] < r0) ir++;
		if (ir<irs) ir=irs;
	}

	while( (ir>=irs)&&(ir<=ire) ) {		// span the line
		d = bb*bb + r[ir]*r[ir] -r0;
		if (d>=0.0) {		// intersection exists
			alpha = - bb + irinc*sqrt(d);		// right intersection
			if (alpha > nv) break;	// finished.
			x = x0 + alpha*vx;		y = y0 + alpha*vy;		z = z0 + alpha*vz;
			rr = sqrt(x*x + y*y + z*z);		cost = z/rr;
			if ((x==0.)&&(y==0.)) {		// use angle made by the vectors.
				phi = atan(vy/vx);	if (vx<0.) phi += pi;
				if ((vy==0.)&&(vx==0.))  phi = 0.;		// or arbitrary phi=0.
				if (rr==0.) cost = vz;
			} else {
				phi = atan(y/x);	if (x<0.) phi += pi;		// we may use atan2(y,x) instead.
			}
			if (SH->get_ncomp() == 2) {
				Blm.to_point(ir, cost, phi, &br, &bt, &bp);
				bx = br;	by = bt;	bz = bp;
				spher_to_cart(cost, phi, &bx, &by, &bz);
				fprintf(fp,"%.12g %.12g %.12g %.12g %.12g %.12g %.12g %.12g %.12g %.12g %.12g %.12g %.12g\n",alpha, x,y,z, rr, cost, phi, bx,by,bz, br,bt,bp);
			} else {
				br = Tlm.to_point(ir, cost, phi);
				fprintf(fp,"%.12g %.12g %.12g %.12g %.12g %.12g %.12g %.12g\n",alpha, x,y,z, rr, cost, phi, br);
			}
		} else irinc = 1;		// reverse sign of increment.
		ir += irinc;
	}
	fclose(fp);
	printf("> linear profile from %.3f,%.3f,%.3f to %.3f,%.3f,%.3f written to %s\n",x0,y0,z0, x1,y1,z1, fn);
}

/*
/// 2D velocity field to 3D velocity field (spatial).
void zavg_to_spat3D(double *vs, double *vp, VectField *V)
{
	double *dr;

	double rr, sr,zr;
	double alp0, alp1;
	double vss, vpp, vzz;
	int ir,it,ip, is, nit, nlat_2;

	dr = (double *) malloc( sizeof(double) * NR );
	ir = 0;
		dr[ir] = (r[ir+1]-r[ir])*0.5;
	for (ir=1; ir< NR-1; ir++)
		dr[ir] = (r[ir+1]-r[ir-1])*0.5;
	ir = NR-1;
		dr[ir] = (r[ir]-r[ir-1])*0.5;

	nlat_2 = (NLAT+1)/2;
	for (ir=irs; ir<=ire; ir++) {
		rr = r[ir];
		is = irs;
		for (it=0; it<nlat_2; it++)
		{
			sr = rr*st[it]; 	zr = rr*ct[it];
			nit = NLAT-1-it;
			if ((sr >= r[irs])&&(sr <= r[ire])) {
				while (r[is+1] < sr) is++;
				alp1 = (sr-r[is])/(r[is+1]-r[is]);
				alp0 = 1. - alp1;
				for (ip=0; ip<NPHI; ip++) {
					vss = alp0*vs[is*NPHI + ip] + alp1*vs[(is+1)*NPHI + ip];
					vpp = alp0*vp[is*NPHI + ip] + alp1*vp[(is+1)*NPHI + ip];
					vzz = 0.0;		/// \todo : fix this !!!
					
					V->vr[ir][ip*NLAT +it] = vzz*ct[it] + vss*st[it];
					V->vr[ir][ip*NLAT +nit]= vzz*ct[it] + vss*st[it];
					V->vt[ir][ip*NLAT +it] = vss*ct[it] - vzz*st[it];
					V->vt[ir][ip*NLAT +nit]=-vss*ct[it] + vzz*st[it];
					V->vp[ir][ip*NLAT +it] = vpp;
					V->vp[ir][ip*NLAT +nit]= vpp;
				}
			} else {
				for (ip=0; ip<NPHI; ip++) {
					V->vr[ir][ip*NLAT +it] = 0.0;	V->vr[ir][ip*NLAT +nit] = 0.0;
					V->vt[ir][ip*NLAT +it] = 0.0;	V->vt[ir][ip*NLAT +nit] = 0.0;
					V->vp[ir][ip*NLAT +it] = 0.0;	V->vp[ir][ip*NLAT +nit] = 0.0;
				}
			}
		}
	}
}
*/

/// Compute the z-average of spatial field V. Store it to (vs,vp)
/// The arrays vs and vp, should be large enough to hold NPHI*(ire-irs+1) doubles.
void spat_zavg(Spatial *V, double *vs, double *vp)
{
	double ss,cost;
	int ir, is, ip;

	double* z = new double[2*NR];				// vertical coordinate.
	double* dz = z + NR;

	for (is = irs; is <= ire; is ++) {
		ss = r[is];			// same cylindrical radius discretization as spherical radii.

		// generate vertical coordinate z and dz :
		z[is] = 0.0;
		if (is < ire) {
			for (ir = is+1; ir <= ire; ir++)	z[ir] = sqrt( r[ir]*r[ir] - ss*ss);
			double norm = 0.5 * 0.5/(z[ire]-z[is]);
			ir = is;		dz[ir] = (z[ir+1]-z[ir])*norm;
			for (ir = is+1; ir < ire; ir++) 	dz[ir] = (z[ir+1]-z[ir-1])*norm;
			ir = ire;		dz[ir] = (z[ir]-z[ir-1])*norm;
		} else {
			dz[is] = 0.5;
		}

		// start with zero average
		for (ip=0; ip<NPHI; ip++) {
			vs[ip] = 0.0;		vp[ip] = 0.0;
		}

		int it = (NLAT-1)/2;		// start on the equator.
		for (ir = is; ir <= ire; ir++) {			// sample a vertical line
			if (ss == 0.0) {
				cost = 1.0;		it = 0;
			} else {
				cost = z[ir] * r_1[ir];
				while( (it>0) && (ct[it] < cost) ) it--;		// ct[it] > cost > ct[it+1]
			}
			double a0 = (cost - ct[it+1])/(ct[it]-ct[it+1]);		// angular interpolation (1D)
			double a1 = 1.0 - a0;		// interpolation coeffs
			int nit = NLAT-1-it;
			a0 *= dz[ir];		a1 *= dz[ir];		// average north and south + integral weighting
			for (ip=0; ip<NPHI; ip++) {
				double vs0 = (V->get_data(0,ir)[ip*NLAT +it] + V->get_data(0,ir)[ip*NLAT +nit])* st[it]
						   + (V->get_data(1,ir)[ip*NLAT +it] - V->get_data(1,ir)[ip*NLAT +nit])* ct[it];
				double vs1 = (V->get_data(0,ir)[ip*NLAT +it+1] + V->get_data(0,ir)[ip*NLAT +nit-1])* st[it+1]
						   + (V->get_data(1,ir)[ip*NLAT +it+1] - V->get_data(1,ir)[ip*NLAT +nit-1])* ct[it+1];
				vs[ip] += vs0*a0 + vs1*a1;
				double vp0 = (V->get_data(2,ir)[ip*NLAT +it] + V->get_data(2,ir)[ip*NLAT +nit]);
				double vp1 = (V->get_data(2,ir)[ip*NLAT +it+1] + V->get_data(2,ir)[ip*NLAT +nit-1]);
				vp[ip] += vp0*a0 + vp1*a1;
			}
		}

		vs += NPHI;		vp += NPHI;		// next cylindrical radius.
	}
	delete[] z;
}

/// split field into QG and non-QG components.
void QG_split()
{
	
}

void QG_energy(VectField *V)
{
	double vss, vpp, nrj, nrj0, nrj1, vol;
	double *vs, *vp;
	int is, ip, ns, it;

	ns = ire-irs+1;
	vs = (double*) VMALLOC( sizeof(double) * (2*ns*NPHI) );
	vp = vs + ns*NPHI;

	spat_zavg(V, vs, vp);		// perform averaging

	const double ro = r[ire];
	nrj0 = 0.0;		nrj1 = 0.0;		vol = 0.0;		// energy and volume.
	for (is = irs; is < ire; is ++) {
		double beta = (4./3.) * r[is]*r[is] / (ro*ro - r[is]*r[is]);	// to take vz into account.
		double dV = 2.0*sqrt(ro*ro - r[is]*r[is]) * r[is]*Blm.delta_r(is) * 2.0*M_PI/NPHI;
		vol += dV * NPHI;
		for (ip=0; ip<NPHI; ip++) {
			vss = vs[(is-irs)*NPHI +ip];		vpp = vp[(is-irs)*NPHI +ip];
			nrj0 += (vss*vss + vpp*vpp) * dV;		// without vz.
			nrj1 += (vss*vss*(1.+beta) + vpp*vpp) * dV;		// with vz.
		}
	}
	VFREE(vs);
	printf("    QG energy density = %g\n", nrj1/vol);
	printf("    QG energy density without vz = %g\n", nrj0/vol);

	// now compute total energy outside QG :
	double* stdt = new double[NLAT];		// array of size NLAT, required for integration.
	int nth_2 = (NLAT+1)/2;
	// compute dtheta, assume symmetry.
	for (it=0; it <= nth_2; it++)
		stdt[it+1] = acos(ct[it]);		// theta
	it = 0;
		stdt[it] = st[it] * 0.5*(stdt[it+2] + stdt[it+1]);
	for (it=1; it<nth_2; it++)
		stdt[it] = st[it] * 0.5*(stdt[it+2] - stdt[it]);
	for (it=nth_2; it<NLAT; it++)
		stdt[it] = stdt[NLAT-1-it];

	nrj = 0.0;
	for (is=irs; is<=ire; is++) {
		double er = 0.0;
		for (it=0; it<NLAT; it++) {
			if (st[it]*r[is] > r[irs]) {		// keep only if s > r[irs]
				for (ip=0; ip<NPHI; ip++) {
					int lm = ip*NLAT+it;
					er += (V->vr[is][lm]*V->vr[is][lm] + V->vt[is][lm]*V->vt[is][lm] + V->vp[is][lm]*V->vp[is][lm]) * stdt[it];
				}
			}
		}
		double dV = r[is]*r[is]*Blm.delta_r(is) * 2.*M_PI/NPHI;
		nrj += er *dV;
	}
	printf("    total energy density outside TC = %g\n", nrj/vol);
	delete[] stdt;
}

void QG_energy(int ns, double* s, double* vs, double* vp)
{
	double nrj0, nrj1, vol;
	const double ro = s[ns-1];

	nrj0 = 0.0;		nrj1 = 0.0;		vol = 0.0;		// energy and volume.
	for (int is = 0; is <ns-1; is ++) {
		double beta = (1./3.) * s[is]*s[is] / (ro*ro - s[is]*s[is]);	// to take vz = z*(-s/H^2)*vs into account.
		double ds = 0.0;
		if (is == 0)  ds = 0.5* (s[is+1]-s[is]);
		if ((is>0) && (is<ns-1)) ds = 0.5*(s[is+1]-s[is-1]);

		double dV = 2.0*sqrt(ro*ro - s[is]*s[is]) * s[is]*ds * 2.0*M_PI/NPHI;
		vol += dV * NPHI;
		for (int ip=0; ip<NPHI; ip++) {
			double vss = vs[is*NPHI +ip];
			double vpp = vp[is*NPHI +ip];
			nrj0 += 0.5*(vss*vss + vpp*vpp) * dV;		// without vz.
			nrj1 += 0.5*(vss*vss*(1.+beta) + vpp*vpp) * dV;		// with vz.
		}
	}
	printf("    QG energy = %g  (volume=%g)\n", nrj1, vol);
	printf("    QG energy without vz = %g\n", nrj0);
}

void write_zdisc(const char *fn, double z0, int nphi)
{
	double* s = new double[NR+nphi];
	double* phi = s + NR;
	for (int ip=0; ip<nphi; ip++) phi[ip] = (ip*2.*M_PI)/nphi;
	NumpyFile np;
	if (ncomp == 2) {
		double* vs = new double[NR * nphi * 3];
		double* vp = vs + NR*nphi;
		double* vz = vs + 2*NR*nphi;
		int ns = Blm.to_zdisc(z0, nphi, s, vs, vp, vz, irs, ire);
		np.init(nphi,ns,3);
		np.create_disc(fn, s, phi);
		np.write_slice(vs, vchar, 's');
		np.write_slice(vp, vchar, 'p');
		np.write_slice(vz, vchar, 'z');
		delete[] vs;
	} else {
		double* data = new double[NR*nphi];
		int ns = Tlm.to_zdisc(z0, nphi, s, data, irs, ire);
		np.init(nphi, ns, 1);
		np.create_disc(fn, s, phi);
		np.write_slice(data, 'T');
		delete[] data;
	}
	np.close();
	delete[] s;
}

/*
void write_disc(const char *fn,double x0,double y0,double z0, int nphi, PolTor *Blm)
{
	double r2, ct0, st0, phi0, s0;	// absolute coordinates of center.
	double s, ca, sa;			// disc coordinates
	double rr, cost,phi;		// spherical coordinates
	double x,y,z;				// cartesian coordinates
	double bx,by,bz;			// vector
	int ir, ip;
	FILE *fp;

	r2 = x0*x0 + y0*y0 + z0*z0;		rr = sqrt(r2);	// center position.
	ct0 = z0/rr;	st0 = sqrt(1. - ct0*ct0);
	phi0 = atan(y0/x0);	if (x0 < 0.) phi0 += pi;
	s0 = sqrt(x0*x0 + y0*y0);		// rotation of phi0.
	if (s0 == 0.) {
		if (rr >= r[NR-1]) z0 = 0.0;
		write_zdisc(fn,z0,nphi);
		return;
	}

	fp = fopen(fn,"w");
	if (rr < r[NR-1]) {		// vector is normal and center of disc
		fprintf(fp,"%%plot_disc.py\n%% [XSHELLS] disc slice centered at %f,%f,%f (first column is disc-radius)",x0,y0,z0);
	} else {		// vector is normal and center is 0,0,0
		fprintf(fp,"%% [XSHELLS] disc slice centered at 0 with normal %f,%f,%f (first column is disc-radius)",x0,y0,z0);
		s0 = 0.;	x0 = 0.;	y0 = 0.;	z0 = 0.;	rr = 0.;	r2 = 0.;
	}

	ir = r_to_idx(rr);	if (r[ir] <= rr) ir++;	//r[ir] > rr;
	if (ir<irs) ir=irs;			// don't draw excluded shells.
	while((Blm->Pol[ir] == NULL)&&(ir<=ire)) ir++;	// skip undef data.
	printf("> writing disc slice centered at x=%.3f, y=%.3f, z=%.3f to %s (nphi=%d)\n",x0,y0,z0,fn,nphi);
	printf("    normal : phi=%.1fdeg, cos(theta)=%.4f, sin(theta)=%.4f\n",phi0*180./pi,ct0,st0);
	printf("    start at ir=%d r=%.4f\n",ir, r[ir]);

	while ((ir<=ire)&&(Blm->Pol[ir] != NULL)) {
		rr = r[ir];
		printf("ir=%d  r=%f\r",ir,rr);	fflush(stdout);
		s = sqrt(rr*rr - r2);		// disc radius.
		fprintf(fp,"\n%.6g ",s);	// first column = disc radius.
		for (ip=0; ip<nphi; ip++) {
			phi = ip*(2.*pi/nphi);		// disc angle
			ca = cos(phi);	sa = sin(phi);
			x = s*ca;
			y = s0 - s*sa*ct0;	// normal is along y
			z = z0 + s*sa*st0;
			phi = atan(x/y);        if (y < 0.0) phi += pi;
			cost = z/rr;
			Blm->to_point(ir, cost, phi+phi0, &bx, &by, &bz);	// phi0 is normal vector (y) => -pi/2 for x.
			spher_to_cart(cost, phi, &bx, &by, &bz);	// now x is normal vector,
			x = by;			// project into disc-relative coordinates.
			y = -bx*ct0 + bz*st0;
			z = bz*ct0 + bx*st0;
			fprintf(fp,"%.6g %.6g %.6g ",x*ca+y*sa, y*ca-x*sa, z);	// write data
//			fprintf(fp,"%.6g %.6g %.6g ",x, y, z);	// write data
		}
		ir++;
	}
	fprintf(fp,"\n");	fclose(fp);
	printf("    end at ir=%d r=%.4f.\n",ir-1, rr);
}
*/

void calc_spec(const xs_array2d<cplx> &HS, double *spl, double *spm)
{
	double cr,ci;
	int ir,lm,l,im;

	for (l=0; l<=LMAX; l++)	spl[l] = 0.0;
	for (im=0; im<=MMAX; im++)	spm[im] = 0.0;

	for (ir=irs;ir<=ire;ir++) {
		if (HS[ir] != NULL) {
			for (im=0; im<=MMAX; im++) {
				for (l=im*jpar.mres; l<=LMAX; l++) {
					lm = LiM(shtns,l,im);
					cr = real(HS[ir][lm]);	ci = imag(HS[ir][lm]);
					spl[l] += cr*cr + ci*ci;
					spm[im] += cr*cr + ci*ci;
				}
			}
		}
	}
}

void usage()
{
	printf("\nUsage: xspp <field-file-saved-by-xshells> [op1] [op2] [...] command1 [args [...]] [command2 ...]\n");
	printf("  if no other arguments than a file name is given, xspp will print associated metadata.\n");
	
	printf("\n** list of available optional ops :\n");		// OPERATORS
	printf(" swap : exchange poloidal and toroidal components\n");
	printf(" curl : compute curl of field\n");
	printf(" square : compute square of field\n");
	printf(" rot <phi> : rotation along z-axi of angle <phi> degrees (reference frame rotated by -phi)\n");
	printf(" rotZYZ <alpha,beta,gamma> : rotation by Euler angles along Z,Y,Z axes in degrees.\n");
	printf(" x <value> : scale field by value\n");
	printf(" + <file-name> : add pol/tor field contained in file-name\n");
	printf(" - <file-name> : substract pol/tor field contained in file-name\n");
//	printf(" x <file-name> : cross-product of vector fields\n");
	printf(" rlim <rmin>:<rmax> : render only from rmin to rmax\n");
	printf(" philim <min>:<max> : render only from min to max azimutal degrees\n");
	printf(" llim <lmin>:<lmax> : use only spherical harmonic degrees from lmin to lmax.\n");
	printf(" sym [0/1] : keep only symmetric or anti-symmetric components.\n");
	printf(" poltor [p/t] : keep only poloidal or toroidal components.\n");
	printf(" mres=1 : use full phi=0..2pi grid.\n");
	printf(" mlim <mmin>:<mmax> : use only spherical harmonic orders from mmin to mmax.\n");
	printf(" nlat <nlat> : set number of latitudinal (theta) points.\n");
	printf(" nphi <nphi> : set number of longitudinal (phi) points.\n");
	printf(" bc in,out : override boundary conditions (0=none, 1=no-slip, 2=free-slip, 3=magnetic).\n");

	printf("\n** list of available commands :\n");		// COMMMANDS
	printf(" filter <scale>  : filter out scales smaller than scale\n");
	printf(" axi  : write meridional slice of axisymetric component (m=0)\n");
	printf(" equat  : write equatorial cut of vector field in cylindrical coordinates (r, phi)\n");
	printf(" merid [angle angle2 ...]  : write meridional slice at phi=angle in degrees (default=0°) of vector field in spherical coordinates\n");
	printf(" surf [r]  : write surface data at r (0 to 1) or ir (1 to NR-1) of vector field in spherical coordinates\n");
	printf(" max  : display maximum value of field\n");
	printf(" SH  : write full spherical harmonics decomposition\n");
	printf(" spec  : write spherical harmonic (l and m)-spectra for each shell\n");
	printf(" line x0,y0,z0 x1,y1,z1 [x0,...]  : write points of shells intersecting line profile from (x0,y0,z0) to (x1,y1,z1)\n");
	printf(" disc nphi x0,y0,z0  : write disc slice centered at (x0,y0,z0) with nphi azimutal points.\n      if center is outside data domain, then it is taken as a normal vector, and the center is 0\n");
	printf(" zavg [ns]  : write equatorial slice of z-averaged data, with ns radial points.\n");	
	printf(" dip_surf  : print amplitude of surface dipole.\n");
	printf(" moment  : compute angular momentum and global rotation of vector field, and subtract it from the main field.\n");
	printf(" magtorque  : compute magnetic torque, integral of r(jxb)_phi over volume (use rlim to specify boundaries).\n");
	printf(" visctorque <r> : compute viscous torque at radius r = r^3 d/dr(T(1,0)/r) .\n");
	printf(" nrj  : compute energy.\n");
	printf(" save <file>  : write data to binary field file.\n");
	printf(" fp48 <file>  : write data to binary field file, in compressed fp48 format (48 bits instead of 64 bits).\n");
#ifdef XS_HDF5
	printf(" hdf5 [sph|cart] [irstep] [file]  : write cartesian (or spherical) components of field into HDF5 and Xdmf files, with the spherical grid.\n");
	printf(" box [file]  : write cartesian components of vector data into an HDF5 file, in a box.\n");
#endif
}

	double scale = 1.0;		// scaling factor.
	int filter_req = 0;
	int curl_req = 0;
	int square_req = 0;
	int swap_req = 0;
	int symmetry = 2;		// both symmetries.
	int poltor = 3;			// both pol and tor
	boundary bci = BC_NONE;
	boundary bco = BC_NONE;
	int mmin=0, lmin=0, lmax;
	int sht_precomp = 0;
	int spat_comp = 0;		// number of spatial components.
	char math_op = 0;		// no math op by default.
	char fn[60];


	Spatial& spat(int i0, int i1)
	{
		if (spat_comp == 0) {
			B.free_field();		T.free_field();
			if (ncomp != 1) {
				spat_comp = 3;		// 3 spatial components.
				B.alloc(irs, ire);
				if (curl_req) {
				    #pragma omp parallel
					Blm.to_curl_spat(&B, i0, i1);
				} else {
					#pragma omp parallel
					Blm.to_spat(&B, i0, i1);
				}
				return B;
			} else {
				spat_comp = 1;		// 1 spatial component.
				if (curl_req) {
					B.alloc(irs, ire);
					#pragma omp parallel
					Tlm.to_grad_spat(&B, i0, i1);
					spat_comp = 3;		// gradient
					return B;
				} else {
					T.alloc(irs, ire);		// for scalar fields.
					#pragma omp parallel
					Tlm.to_spat(&T, i0, i1);
					return T;
				}
			}
		} else if (spat_comp == 3) {
			return B;
		}
		return T;
	}

	void init_sh_reg_poles()
	{
		if (sht_precomp == 0) {
			init_sh(sht_reg_poles, 0.0, 2);		sht_precomp = 1;
		}
	}

	int parse_op(int argc, char *argv[], int ic)	// returns number of parsed command line argument
	{
		double min, max;
		long int i,j;

		if (argc <= ic) return 0;
		if (strcmp(argv[ic],"curl") == 0)
		{
			curl_req ++;	// request curl of field
			return 1;	// 1 command line argument parsed.
		}
		if (strcmp(argv[ic],"square") == 0)
		{
			square_req = 1;	// request square of field
			return 1;	// 1 command line argument parsed.
		}
		if (strcmp(argv[ic],"swap") == 0)
		{
			swap_req = 1;	// request curl of field
			return 1;	// 1 command line argument parsed.
		}
		if (strcmp(argv[ic],"rot") == 0)
		{
			ic++;	// go to next command line argument.
			if (argc > ic) {
				sscanf(argv[ic],"%lf",&min);
			} else runerr("rot <angle>  => missing argument\n");
			rot_angle = min * M_PI/180.;
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"rotZYZ") == 0)
		{
			ic++;	// go to next command line argument.
			if (argc > ic) {
				sscanf(argv[ic],"%lf,%lf,%lf",&alpha,&beta,&gamma_);
			} else runerr("rotZYZ <alpha,beta,gamma>  => missing argument\n");
			alpha *= M_PI/180;		beta *= M_PI/180;	gamma_ *= M_PI/180;
			if (beta == 0.0) {		// only rotation along Z-axis is needed.
				rot_angle = alpha + gamma_;
				alpha = 0.;		gamma_ = 0.;
			}
			return 2;	// 2 command line argument parsed.
		}
		if ((strcmp(argv[ic],"+") == 0) || (strcmp(argv[ic],"-") == 0) || (strcmp(argv[ic],"x") == 0) || (strcmp(argv[ic],".") == 0))
		{
			if (math_op != 0) runerr("only one '+','-','.' or 'x' operation is supported\n");
			math_op = argv[ic][0];
			ic++;	// go to next command line argument.
			if (argc > ic) {
				sscanf(argv[ic],"%s", fn);
			} else runerr("[+-x.] <file-name>  => missing argument\n");
			if ((math_op == 'x') && (sscanf(argv[ic],"%lf", &min) == 1)) {		// multiply by constant
				math_op = 0;		scale = min;
			}
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"rlim") == 0)
		{
			ic++;	// go to next command line argument.
			if (argc > ic) {
				int n = sscanf(argv[ic],"%lf:%lf",&min,&max);
				if (n < 2) runerr("llim <min>:<max>  => bad argument\n");
			} else runerr("rlim <rmin>:<rmax>  => missing arguments\n");
			r_min = min;	r_max = max;		// save for prolongation of magnetic field.
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"philim") == 0)
		{
			ic++;	// go to next command line argument.
			if (argc > ic) {
				sscanf(argv[ic],"%lf:%lf",&min,&max);
			} else runerr("philim <min>:<max>  => missing arguments\n");
			ips = phi_to_idx(min);	ipe = phi_to_idx(max);
			printf("> restricting to slice #%d to #%d (that is phi=[%f, %f])\n",ips,ipe, phi_deg(ips), phi_deg(ipe));
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"thetalim") == 0)
		{
			ic++;	// go to next command line argument.
			if (argc > ic) {
				sscanf(argv[ic],"%lf:%lf",&min,&max);
			} else runerr("thetalim <min>:<max>  => missing arguments\n");
			its = theta_to_idx(min);	ite = theta_to_idx(max);
			printf("> restricting to latitudinal points #%d to #%d (that is theta=[%f, %f])\n",its,ite, its*180./(NLAT-1), ite*180./(NLAT-1));
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"mres=1") == 0)
		{
			MMAX *= MRES;	NPHI *= MRES;
			MRES=1;		ipe=NPHI-1;
			printf("> mres=1 : phi spans 0 to 2pi.\n");
			return 1;	// 1 command line argument parsed.
		}
		if (strcmp(argv[ic],"mlim") == 0)
		{
			ic++;	// go to next command line argument.
			if (argc > ic) {
				int n = sscanf(argv[ic],"%lf:%lf",&min,&max);
				if (n < 2) runerr("llim <min>:<max>  => bad argument\n");
			} else runerr("mlim <min>:<max>  => missing arguments\n");
			mmin = min;	MMAX = max;
			if (MMAX*MRES > jpar.mmax*jpar.mres) MMAX = jpar.mmax*jpar.mres/MRES;
			if (mmin < 0) mmin = 0;
			printf("> using only m = #%d to #%d\n",mmin, MMAX);
			filter_req = 1;
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"llim") == 0)
		{
			ic++;	// go to next command line argument.
			if (argc > ic) {
				int n = sscanf(argv[ic],"%lf:%lf",&min,&max);
				if (n < 2) runerr("llim <min>:<max>  => bad argument\n");
			} else runerr("llim <min>:<max>  => missing arguments\n");
			lmin = min;	LMAX = max;
			if (LMAX > jpar.lmax) LMAX = jpar.lmax;
			if (lmin < 0) lmin = 0;
			printf("> using only l = #%d to #%d\n",lmin, LMAX);
			filter_req = 1;
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"sym") == 0)
		{
			i=1;
			symmetry = 0;		// even
			if (argc > ic+1) {
				if (sscanf(argv[ic+1],"%ld",&j)) {
					symmetry = j;
					ic++;	i=2;	// 2 arguments parsed.
				}
			}
			return i;
		}
		if (strcmp(argv[ic],"poltor") == 0)
		{
			i=1;
			poltor = 1;		// pol
			if (argc > ic+1) {
				char c = argv[ic+1][0];
				if ((c== 't')||(c=='p')) {
					if (c == 't') poltor = 2;		// tor
					ic++;	i=2;	// 2 arguments parsed.
				}
			}
			return i;
		}
		if (strcmp(argv[ic],"nlat") == 0)
		{
			ic++;
			if (argc > ic) {
				sscanf(argv[ic],"%lf",&max);
			} else runerr("nlat <nlat>  => missing argument\n");
			NLAT = max;
			if (NLAT <= LMAX) {
				LMAX = NLAT-1;		filter_req = 1;
				printf("> using Nlat = %d (and setting Lmax to %d)\n",NLAT, LMAX);
			} else printf("> using Nlat = %d\n",NLAT);
			ite=NLAT-1;
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"nphi") == 0)
		{
			ic++;
			if (argc > ic) {
				sscanf(argv[ic],"%lf",&max);
			} else runerr("nphi <nphi>  => missing argument\n");
			NPHI = max;
			if (NPHI <= MMAX*2) {
				MMAX = (NPHI-1)/2;	filter_req = 1;
				printf("> using Nphi = %d (and setting Mmax to %d)\n",NPHI, MMAX);
			} else printf("> using Nphi = %d\n",NPHI);
			ipe=NPHI-1;
			return 2;	// 2 command line argument parsed.
		}
		if (strcmp(argv[ic],"bc") == 0)
		{
			ic++;
			if (argc > ic) {
				sscanf(argv[ic],"%ld,%ld",&i,&j);
			} else runerr("bc <in,out>  => missing argument\n");
			printf("> overriding boundary conditions with %ld, %ld\n",i,j);
			bci = (boundary) i;	bco = (boundary) j;
			return 2;	// 2 command line argument parsed.
		}
		return 0;
	}

int main (int argc, char *argv[])
{
	int i, ic, iloop;

	printf(" [XSPP] Xshells Post-Processing   by N. Schaeffer / CNRS, build %s, %s\n",__DATE__,__TIME__);
	if (argc <= 1) { usage(); exit(1); }
	xsio_allow_interp = 1;		// allow interpolation.

// get File infos
	ncomp = load_FieldInfo(argv[1], &jpar);
	if (ncomp) {
		LMAX = jpar.lmax;	MMAX = jpar.mmax;	MRES = jpar.mres;	NLM = jpar.nlm;
		NLAT = (LMAX+1)*2;		// defaults for visualization.
		if (NLAT < 32) NLAT = 32;		// minimum NLAT
		if (NLAT % VSIZE) NLAT = ((NLAT+VSIZE-1)/VSIZE)*VSIZE;		// ensure that we have a multiple of VSIZE.
		if (MMAX > 63)   NPHI = ((MMAX+1)*3 > 256) ? (((MMAX+1)*3)/4)*4 : 256;          // use rather MMAX*3 for large MMAX
		else NPHI = (MMAX>0) ? (MMAX+1)*4 : 1;

		ipe=NPHI-1;	ite=NLAT-1;
		bci = (boundary) jpar.BCi;		bco = (boundary) jpar.BCo;
		if ((bci==BC_MAGNETIC) || (bco == BC_MAGNETIC))  vchar='B';		// record field 'name'.

		printf("\nmetadata for file '%s' : \n",argv[1]);
		print_FieldInfo(&jpar);		// print metadata
		printf("\n");
		ic = 2;		// current argument count (skip filename)
	} else runerr("File not found, or wrong format. Was it produced by XSHELLS?\n");
	irs = jpar.irs;	ire = jpar.ire;

// parse optional op...
	do {
		i = parse_op(argc,argv,ic);
		ic += i;
	} while(i!=0);
	if (argc <= ic) exit(0);	// nothing else to do, so we quit.

// if the only command is fp48, check if the file is actually double-precision
	if ((strcmp(argv[ic],"fp48") == 0) && (GET_FILE_PREC(jpar) != 2))
		runerr("input is not double-precision. I'm not using a larger format to save.");

// init
	if (MMAX*MRES > LMAX) MMAX=LMAX/MRES;
	lmax = LMAX;
	if (LMAX < 2) LMAX=2;
	shtns_use_threads(0);
	if (beta != 0.0) { MMAX = LMAX;  MRES = 1; }		// for arbitrary rotation, one needs lmax=mmax !
	if (NPHI<=2*MMAX)	NPHI = ((MMAX+1)*3 > 256) ? (((MMAX+1)*3)/4)*4 : 256;          // use rather MMAX*3 for large MMAX
	init_sh_vars(jpar.shtnorm);

//load
	load_Field_grid(argv[1]);
	if (ncomp == 2) {
		Blm.load(argv[1], &jpar);
		SH = &Blm;
	} else {
		Tlm.load(argv[1], &jpar);
		SH = &Tlm;
	}
	SH->bci = bci;		SH->bco = bco;		// set correct BC.
	init_Deriv_sph(r, NR);				// compute derivatives

	if ((swap_req)&&(ncomp==2)) {
		printf("> exchanging poloidal and toroidal components\n");
		xs_array2d<cplx> tmp = Blm.Pol;		Blm.Pol = Blm.Tor;	Blm.Tor = tmp;
	}

// math operations
	if (math_op != 0) {
		PolTor B2lm;
		ScalarSH T2lm;
		Spectral *SH2;
		int nc2 = load_FieldInfo(fn);		// load other field.
		if (nc2 == 0) runerr("file not found.\n");
		if (nc2 == 1) {
			T2lm.load(fn);
			SH2 = &T2lm;
		} else {
			B2lm.load(fn);
			SH2 = & B2lm;
		}
		if (math_op == '-') {
			*SH -= *SH2;
		} else if (math_op == '+') {
			*SH += *SH2;
		} else if (math_op == 'x') {
			if ((ncomp == 2)&&(nc2 == 2)) {		// two vectors => cross product.
				B.alloc(irs, ire);
				VectField C(irs,ire);
				init_sh_reg_poles();
				#pragma omp parallel
				{
					B2lm.to_spat(&C, irs, ire);
					Blm.to_spat(&B, irs, ire);
					B.NL_vect(&B, &C);		// cross product
				}
				spat_comp = 3;
				printf("!! Warning: only 'surf' and 'merid' commands supported for cross product\n");
				runerr("cross product no more supported... would require non-solenoidal fields\n");
			} else runerr("cross product not defined for scalars");
		} else if (math_op == '.') {
			if ((ncomp == 2)&&(nc2 == 2)) {		// two vectors => dot product.
				Tlm.alloc(irs, ire);
				VectField C(irs,ire);
				B.alloc(irs, ire);
				init_sh(sht_quick_init, 0.0, 2);
				#pragma omp parallel
				{
					B2lm.to_spat(&C, irs, ire);
					Blm.to_spat(&B, irs, ire);
					B.NL_dot(&B, &C);		// dot product (result in r-component)
					Tlm.from_spat(&B);
				}
				B.free_field();
				Blm.free_field();
				shtns_unset_grid(shtns);		sht_precomp = 0;
				ncomp = 1;
				SH = &Tlm;
			} else runerr("dot product: only with vector fields.");
		}
	}

// apply scaling
	if (scale != 1.0) {
		printf("> scaling by %g\n",scale);
		*SH *= scale;
	}

// apply rotation
	if (rot_angle != 0.) {
		printf("> rotation of angle phi=%.3f°\n",rot_angle*180./M_PI);
		SH->Zrotate(rot_angle);
	}

// apply ZYZ rotation:
	if (beta != 0) {
		printf("> ZYZ rotation of angles (%.3f,%.3f,%.3f) degrees\n",alpha*180./M_PI, beta*180./M_PI, gamma_*180./M_PI);
		#if SHTNS_INTERFACE >= 0x30500
		shtns_rot rot = shtns_rotation_create(LMAX,LMAX,sht_orthonormal);
		#else
		shtns_rot rot = shtns_rotation_create(LMAX,LMAX);
		#endif
		shtns_rotation_set_angles_ZYZ(rot, alpha, beta, gamma_);
		#pragma omp parallel for
		for (int ir=SH->ir_bci-1; ir<=SH->ir_bco+1; ir++) {
			for (int ic=0; ic<ncomp; ic++) {
				cplx* d = SH->get_data(ic, ir);
				shtns_rotation_apply_real(rot, d, d);
			}
		}
	}

// define r limits.
	if (r_max > 0.) {
		if (ncomp == 2) Blm.prolongate_field(r_min, r_max);		// prolongate B if needed.
		i = r_to_idx(r_min);	if (i>irs) irs=i;
		i = r_to_idx(r_max);	if (i<ire) ire=i;
		printf("> restricting to shells #%d to #%d (that is r=[%f, %f])\n",irs,ire, r[irs], r[ire]);
	}

// apply (l,m) restrictions
	if (filter_req) SH->filter_lm(lmin, lmax, mmin, MMAX);
	printf("> lmin=%d, lmax=%d, mmin*mres=%d, mmax*mres=%d\n",lmin, LMAX, mmin*MRES, MMAX*MRES);
	if (symmetry < 2) SH->filter_parity(symmetry);
	if ((poltor < 3)&&(ncomp==2)) Blm.zero_out_comp(2-poltor);

// apply curl
	if (ncomp == 2) {
		while (curl_req > 0) {
			printf("> taking curl of field\n");
			Blm.curl();	// compute curl of field.
			if (vchar == 'U') vchar = 'W';		// update field name
			else if (vchar == 'B') vchar = 'J';
			else vchar = '?';
			curl_req --;
		}
	}

// apply squaring
	if (square_req) {
		init_sh(sht_quick_init, 0.0, 2);
		if (ncomp == 2) {
			Tlm.alloc(irs, ire);
			B.alloc(irs, ire);
			#pragma omp parallel
			{
				Blm.to_spat(&B, irs, ire);
				B.square();
				Tlm.from_spat(&B);
			}
			B.free_field();
			Blm.free_field();
			ncomp = 1;
			SH = &Tlm;
		} else {
			T.alloc(irs, ire);		// for scalar fields.
			#pragma omp parallel
			{
				Tlm.to_spat(&T, irs, ire);
				T.square();
				Tlm.from_spat(&T);
			}
			T.free_field();
		}
		shtns_unset_grid(shtns);		sht_precomp = 0;
	}

// write radial grid
	if ( write_vect("o_r",&r[irs],ire-irs+1,"r : radial grid points") )
		printf("> radial grid points written to file : o_r\n");
	write_vect_npy("o_r.npy", &r[irs], ire-irs+1);
//	write_vect("o_cost",&ct[its],ite-its+1,"cos(theta) : latitudinal grid points");
//	printf("> angular grid cos(theta) written to file : o_cost\n");

// parse commands ...
	iloop = 0;
    while(argc > ic) {
	if (strcmp(argv[ic],"axi") == 0)
	{
		ic++;
		int nphi_org = NPHI;
		int mmax_org = MMAX;
		if (nphi_org > 1) {
			MMAX=0;		NPHI=1;
			init_sh_vars(jpar.shtnorm);
		}
		init_sh_reg_poles();
		int nmerid = NLAT*(ire-irs+1);
		NumpyFile np(NLAT, ire-irs+1, ncomp==2 ? 3 : 1);
		np.create_merid("o_axi.npy", &r[irs], ct);
		if (ncomp == 2) {
			double *vpol, *vp, *v0;
			vpol = new double[3*nmerid];
			vp = vpol + nmerid;		v0 = vpol + 2*nmerid;
			#pragma omp parallel for schedule(static)
			for (int i=0; i<=ire-irs; i++) {
				SHtor_to_spat(shtns, Blm.Tor[i+irs], v0+i*NLAT, vp +i*NLAT);		// v0 discarded
				SHtor_to_spat(shtns, Blm.Pol[i+irs], vpol+i*NLAT, v0 +i*NLAT);		// vpol discarded
				for (int l=0;l<NLAT;l++) vpol[i*NLAT +l] = -r[i+irs]*st[l] * v0[i*NLAT +l];		// stream function
			}
			np.write_slice(vp, vchar, 'p');			// write phi component
			np.write_slice(vpol, vchar, 'P');			// write stream function
			np.write_slice(v0, 'A', 'p');			// phi-component of vector-potential.
			delete[] vpol;
		} else {
			double *sc = new double[nmerid];
			#pragma omp parallel for schedule(static)
			for (int i=0; i<=ire-irs; i++)
				SH_to_spat(shtns, Tlm[i+irs], sc + i*NLAT);
			np.write_slice(sc, 'T');
			delete[] sc;
		}
		np.close();
		printf("> axisymmetric components written to files : o_axi.npy\n");
		if (nphi_org > 1) {		// restore shtns config
			shtns_destroy(shtns);	shtns = NULL;
			MMAX = mmax_org;
			NPHI = nphi_org;
			init_sh_vars(jpar.shtnorm);
		}
	}
	else if (strcmp(argv[ic],"equat") == 0)
	{
		ic++;
		int nphi = NPHI*MRES > 64 ? NPHI*MRES : 64;		// at least 64 points for an equatorial disc.
		write_zdisc("o_equat.npy", 0.0, nphi);
		printf("> equatorial slice written to file : o_equat.npy (cylindrical vector components)\n");
	}
	else if (strcmp(argv[ic],"surf") == 0)
	{
		double rr = (ncomp!=1) ? r[Blm.ire] : r[Tlm.ire];		// outer shell
		ic++;
		if (argc > ic) {
			if (sscanf(argv[ic],"%lf",&rr)) ic++;
		}
		int i = r_to_idx(rr);
		if ((i<irs)||(i>ire)) runerr("requested r not available");
		init_sh_reg_poles();
		double *vr, *vt, *vp;
		long nspat = shtns->nspat;
		vr = (double*) VMALLOC(3 * nspat*sizeof(double));
		vt = vr + nspat;		vp = vr + 2*nspat;
		NumpyFile np(NLAT, NPHI, ncomp != 1 ? 3 : 1,  NLAT_PADDED);
		sprintf(fn,"o_shell_%d.npy",i);
		np.create_surf(fn, ct);
		if (ncomp!=1) {
			Blm.to_surf(i, vr, vt, vp);
			np.write_slice(vr, vchar, 'r');
			np.write_slice(vt, vchar, 't');
			np.write_slice(vp, vchar, 'p');
		} else {
			Tlm.to_surf(i, vr);
			np.write_slice(vr, 'T');
		}
		np.close();
		printf("> surface #%d (r=%.4f) written to file : o_shell_%d.npy\n",i,r[i],i);
		VFREE(vr);
	}
	else if (strcmp(argv[ic],"merid") == 0)
	{
		double phi = 0.0;
		if (argc > ic+1) {
			if (sscanf(argv[ic+1],"%lf",&phi)) ic++;
		}
		init_sh_reg_poles();
		int im = 0;
		double *vr, *vt, *vp;
		int nmerid = NLAT*(ire-irs+1);
		vr = (double*) malloc(3 * nmerid*sizeof(double));
		vt = vr + nmerid;		vp = vr + 2*nmerid;
		do {
			NumpyFile np(NLAT, ire-irs+1, ncomp != 1 ? 5 : 1);
			sprintf(fn,"o_merid_%d.npy",im);
			np.create_merid(fn, &r[irs], ct);
			printf("irs=%d, ire=%d\n",irs,ire);
			if (ncomp != 1) {
				Blm.to_merid(phi*M_PI/180, vr, vt, vp, irs, ire);
				np.write_slice(vr, vchar, 'r');
				np.write_slice(vt, vchar, 't');
				np.write_slice(vp, vchar, 'p');
				for (int i=0;i<=(ire-irs);i++) {
					for(int l=0;l<NLAT;l++) {
						vp[i*NLAT +l] = vr[i*NLAT +l]*ct[l] - vt[i*NLAT +l]*st[l];	// z
						vr[i*NLAT +l] = vr[i*NLAT +l]*st[l] + vt[i*NLAT +l]*ct[l];	// s
					}
				}
				np.write_slice(vr, vchar, 's');
				np.write_slice(vp, vchar, 'z');				
			} else {
				Tlm.to_merid(phi*M_PI/180, vr, irs, ire);
				np.write_slice(vr, 'T');
			}
			np.close();
			printf("> meridional slice #%d @ phi=%.1f° written to file : o_merid_%d.npy\n",im,phi,im);
			ic++;
			im++;
		} while ((argc > ic) && (sscanf(argv[ic],"%lf",&phi)));
		free(vr);
	}
	else if (strcmp(argv[ic],"max") == 0)
	{
		ic++;
		init_sh_reg_poles();
		double tmp = SH->absmax(irs+1, ire-1);
		printf("> maximum value of field = %g\n",tmp);
	}
	else if (strcmp(argv[ic],"filter") == 0)
	{
		double scale;
		ic++;
		if (ic >= argc) runerr("scale is missing...");
		sscanf(argv[ic],"%lf",&scale);

			printf("test0: %g == %g\n", real(SH->get_data(0,ire-10)[4]), real(Blm.Pol[ire-10][4]));

		SH->filter_scale(scale);

			printf("test1: %g == %g\n", real(SH->get_data(0,ire-10)[4]), real(Blm.Pol[ire-10][4]));

		ic++;
	}
	else if ((strcmp(argv[ic],"HS") == 0) || (strcmp(argv[ic],"SH") == 0))
	{
		ic++;
		if (ncomp == 2) {
			write_HS("o_Plm",Blm.Pol);	write_HS("o_Tlm",Blm.Tor);
			printf("> spherical harmonics decomposition written to files : o_Plm o_Tlm (poloidal/toroidal components)\n");
		} else {
			write_HS("o_Slm",Tlm.Sca);
			printf("> spherical harmonics decomposition written to files : o_Slm (scalar component)\n");
		}
	}
	else if ((ncomp==2)&&(strcmp(argv[ic],"denys") == 0))
	{
		int sym;
		ic++;
		if (ic >= argc) {
			if (symmetry == 2) runerr("symmetry must be specified.");
			sym = symmetry;
		} else {
			if (sscanf(argv[ic],"%d",&sym) == 0) runerr("symmetry must be specified.");
			ic++;
		}
		sprintf(fn,"o_denys.sym%d",sym);	write_denys(fn, &Blm, sym);
		printf("> spherical harmonics decomposition written to file : %s (as an input for Denys' code)\n",fn);
	}
	else if (strcmp(argv[ic],"spec") == 0)
	{
		ic++;
		lspec = (double*) malloc( sizeof(double) * (LMAX+1) );		mspec = (double*) malloc( sizeof(double) * (MMAX+1) );
		if (ncomp==2) {
			write_Spec_l("o_Plr",Blm.Pol);	write_Spec_l("o_Tlr",Blm.Tor);
			write_Spec_m("o_Pmr",Blm.Pol);	write_Spec_m("o_Tmr",Blm.Tor);
			printf("> spherical harmonics spectrum written to files : o_Plr o_Tlr o_Pmr o_Tmr (poloidal/toroidal, l/m, at each r)\n");
			calc_spec(Blm.Pol, lspec, mspec);
			write_vect("o_Pl",lspec, LMAX+1, "Pol(l) : poloidal l-spectrum");
			write_vect("o_Pm",mspec, MMAX+1, "Pol(m) : poloidal m-spectrum");
			calc_spec(Blm.Tor, lspec, mspec);
			write_vect("o_Tl",lspec, LMAX+1, "Tor(l) : toroidal l-spectrum");
			write_vect("o_Tm",mspec, MMAX+1, "Tor(m) : toroidal m-spectrum");
			printf("     o_Pl o_Tl o_Pm o_Tm (poloidal/toroidal, l/m summed over r)\n");
			write_Spec_nrj(&Blm);
			printf("     o_Elr o_Emr (energy spectrum, l/m at each r)\n");
		} else {
			write_Spec_l("o_Slr",Tlm.Sca);		write_Spec_m("o_Smr",Tlm.Sca);
			printf("> spherical harmonics spectrum written to files : o_Slr o_Smr (l/m, at each r)\n");
			calc_spec(Tlm.Sca, lspec, mspec);
			write_vect("o_Sl",lspec, LMAX+1, "S(l) : l-spectrum");
			write_vect("o_Sm",mspec, MMAX+1, "S(m) : m-spectrum");
			printf("     o_Sl o_Sm (spectra l/m summed over r)\n");
			write_Spec_nrj(&Tlm);
			printf("     o_Elr o_Emr (energy spectrum, l/m at each r)\n");
		}
	}
	else if (strcmp(argv[ic],"line") == 0)
	{
		double x0,y0,z0, x1,y1,z1;
		static int iline = 0;
		ic++;
		if (ic+1 >= argc) runerr("line definition is missing...");
		sscanf(argv[ic],"%lf,%lf,%lf",&x0, &y0, &z0);
		sscanf(argv[ic+1],"%lf,%lf,%lf",&x1, &y1, &z1);
		sprintf(fn,"o_line.%d",iline);	iline++;
		write_line(fn,x0,y0,z0,x1,y1,z1,SH);
		ic+=2;
	}
	else if (strcmp(argv[ic],"disc") == 0)
	{
		double x,y,z;
		long int nphi;
		static int idisc = 0;
		ic++;
		if (ic+1 >= argc) runerr("disc definition is missing...");
		sscanf(argv[ic],"%lf",&x);	nphi = x;
		sscanf(argv[ic+1],"%lf,%lf,%lf",&x, &y, &z);
		sprintf(fn,"o_disc_%d.npy",idisc);	idisc++;
		if (ncomp == 2) {
			if ((x==0)&&(y==0)) {
				write_zdisc(fn, z, nphi);
			} else runerr("only x=0 and y=0 discs supported (for now).");
		} else {
			if ((x!=0)||(y!=0)) {
				runerr("for scalar field, only x=0 and y=0 discs supported.");
			} else write_zdisc(fn,z,nphi);
		}
		printf("> disc slice written to file : o_disc_%d.npy (cylindrical vector components)\n", idisc-1);
		ic+=2;
	}
	else if (strcmp(argv[ic],"zavg") == 0)
	{
		double x = NLAT/2;
		long int nphi = NPHI;
		ic++;
		if (argc > ic) {
			if (sscanf(argv[ic],"%lf",&x)) ic++;
		}
		int ns = x;
		init_sh_reg_poles();
		double *s = (double*) malloc(sizeof(double)*ns);
		for (int j=0; j<ns; j++) s[j] = j*r[ire]/(ns-1);
		double *vs = (double*) malloc(sizeof(double)*nphi*ns*6);
		#pragma omp parallel
		Blm.special_z_avg(ns, s, vs, 1, r_max);
		QG_energy(ns, s, vs, vs+nphi*ns);
		NumpyFile np(NPHI, ns, 6);
		np.create_disc("o_zavg.npy", s);
		np.write_slice(vs, vchar,'s');
		np.write_slice(vs+nphi*ns, vchar,'p');
		np.write_slice(vs+nphi*2*ns, vchar,'z');
		np.write_slice(vs+nphi*ns*3, (vchar<<8) + '2','s');		// vs^2
		np.write_slice(vs+nphi*ns*4, (vchar<<8) + '2','p');		// vp^2
		np.write_slice(vs+nphi*ns*5, (vchar<<8) + '2');			// vs*vp
		printf("> z-averages written to file : o_zavg.npy\n");
		free(vs);		free(s);
	}
	else if (strcmp(argv[ic],"qg") == 0)
	{
		ic++;
		init_sh_reg_poles();
		spat(irs,ire);
		QG_energy(&B);
	}
	else if (strcmp(argv[ic],"nrj") == 0)
	{
		ic++;
		SpectralDiags SD(LMAX,MMAX);
		SH->energy(SD, irs, ire);
		printf("> rms value = %g\n", sqrt(2.0 * SD.energy() / shell_volume(r[irs], r[ire])) );

		double *E = SD.Esplit;
		printf("  Energy :     equat-sym     equat-asym          zonal      non-zonal    central-sym   central-asym\n");
		if (ncomp == 2) {
			printf(" Poloidal %14.8g %14.8g %14.8g %14.8g %14.8g %14.8g\n", E[Ez_es]+E[Enz_es], E[Ez_ea]+E[Enz_ea], E[Ez_es]+E[Ez_ea], E[Enz_es]+E[Enz_ea], E[E_cs], E[E_ca]);
			E += N_Esplit;
			printf(" Toroidal %14.8g %14.8g %14.8g %14.8g %14.8g %14.8g\n", E[Ez_es]+E[Enz_es], E[Ez_ea]+E[Enz_ea], E[Ez_es]+E[Ez_ea], E[Enz_es]+E[Enz_ea], E[E_cs], E[E_ca]);
		} else {
			printf("          %14.8g %14.8g %14.8g %14.8g %14.8g %14.8g\n", E[Ez_es]+E[Enz_es], E[Ez_ea]+E[Enz_ea], E[Ez_es]+E[Ez_ea], E[Enz_es]+E[Enz_ea], E[E_cs], E[E_ca]);
		}
		printf("total energy (r=[%.3f,%.3f]) : %.15g   (Sconv= %g, %g)\n", r[irs], r[ire], SD.energy(),SD.Sconv[0], SD.Sconv[1]);
	}
	else if (strcmp(argv[ic],"co_nrj") == 0)
	{
		ic++;
		if (ic >= argc) runerr("other field is missing...");
		double E = 0;
		SpectralDiags sd(LMAX,MMAX);
		SpectralDiags sd2(LMAX,MMAX);
		if (ncomp == 2) {
			PolTor B2lm;
			B2lm.load(argv[ic]);
			E = coenergy(Blm, B2lm);
			B2lm.energy(sd2);
		} else {
			ScalarSH T2lm;
			T2lm.load(argv[ic]);
			E = coenergy(Tlm, T2lm);
			T2lm.energy(sd2);
		}
		SH->energy(sd, irs, ire);
		double E1 = sd.energy();
		double E2 = sd2.energy();
		ic++;
		printf("normalized coenergy = %.10g\n", E/sqrt(E1*E2));
	}
	else if ((ncomp==2)&&(strcmp(argv[ic],"dip_surf") == 0))
	{
		ic++;
		double tmp = real(Blm.Pol[ire][LiM(shtns,1,0)])/Y10_ct;
		if ((jpar.mres == 1)&&(MMAX >= 1)) {
			double cr = real(Blm.Pol[ire][LiM(shtns,1,1)])/Y11_st;
			double ci = imag(Blm.Pol[ire][LiM(shtns,1,1)])/Y11_st;
			tmp = sqrt(tmp*tmp + cr*cr + ci*ci);
		}
		printf("surface dipole amplitude = %g\n",tmp * 2/r[ire]);		// * l(l+1)/r
	}
	else if ((ncomp==2)&&(strcmp(argv[ic],"moment") == 0))
	{
		ic++;
		rotation_vector(Blm, irs, ire);
	}
	else if ((ncomp==2)&&(strcmp(argv[ic],"magtorque") == 0))
	{
		ic++;
		init_sh(sht_quick_init, 0.0, 2);		// gauss grid required for inverse SHT
		calc_TorqueMag(Blm, irs, ire);
		if (irs==0)	calc_TorqueMag_surf(Blm, ire);
		else calc_TorqueMag_surf(Blm, irs);
		shtns_unset_grid(shtns);		sht_precomp = 0;
	}
	else if ((ncomp==2)&&(strcmp(argv[ic],"visctorque") == 0))
	{
		double rr;
		ic++;
		if (ic >= argc) runerr("radius is missing...");
		sscanf(argv[ic],"%lf",&rr);
		calc_TorqueVisc(Blm, rr);
		ic++;
	}
	else if (strcmp(argv[ic],"save") == 0)
	{
		ic++;
		if (ic >= argc) runerr("file name is missing...");
		int prec = (GET_FILE_PREC(jpar) != 1) ? 2 : 1;		// save as double precision, except if input was single precision.
		SH->save_generic(argv[ic], LMAX, MMAX, jpar.t, jpar.iter, jpar.step, prec);
		printf("> data written to file : '%s'\n",argv[ic]);
		ic++;
	}
	else if (strcmp(argv[ic],"fp48") == 0)
	{
		ic++;
		if (ic >= argc) {
			sprintf(fn,"%s.fp48",argv[1]);
		} else sprintf(fn,"%s",argv[ic]);
		SH->save_generic(fn, LMAX, MMAX, jpar.t, jpar.iter, jpar.step, 3);		// 3 means FP48 format (6 bytes per real value).
		printf("> fp48 data written to file (fp48 compressed format) : '%s'\n",fn);
		ic++;
	}
	else if (strcmp(argv[ic],"fp32") == 0)
	{
		ic++;
		if (ic >= argc) {
			sprintf(fn,"%s.fp32",argv[1]);
		} else sprintf(fn,"%s",argv[ic]);
		SH->save_single(fn, LMAX, MMAX, jpar.t, jpar.iter, jpar.step);		// save data with fp32 compression.
		printf("> fp32 data written to file (single precision) : '%s'\n",fn);
		ic++;
	}
#ifdef XS_HDF5
	else if (strcmp(argv[ic],"hdf5") == 0)
	{
		int cart = 1;	// default is cartesian components.
		int irstep = 1;	// default writes every shell.
		ic++;
		init_sh_reg_poles();
//		Spatial& F = spat(irs, ire);
		if (ic < argc) {
			if (strcmp(argv[ic],"sph") == 0) {
				ic++;	cart = 0;
			} else if (strcmp(argv[ic],"cart") == 0) {
				ic++;	cart = 1;
			}
		}
		if (ic < argc) {
			if (sscanf(argv[ic],"%d",&irstep))  ic++;		// optional irstep argument
		}
		if (ic < argc) {
			sprintf(fn,"%s",argv[ic]);	ic++;
		} else sprintf(fn,"%s.%s",argv[1],"h5");		// output HDF5 data file
		int ir0 = irs;
		if (r[ir0] == 0.0) ir0 += 1;		// exclude 0 to avoid problems in paraview.
		if (ncomp==2) {
			Blm.write_hdf5(fn, ir0, ire, cart, irstep);		// + grid and xdmf file.
		} else {
			Tlm.write_hdf5(fn, ir0, ire, irstep);		// + grid and xdmf file.
		}
		printf("> spatial data written to HDF5 file : '%s'",fn);
		if (cart) printf(" (cartesian components)\n");
		else printf(" (spherical components)\n");
	}
	else if (strcmp(argv[ic],"box") == 0)
	{
		int nbox = -1;		// default size
		double x0 = (r[irs]+r[ire])/2;
		double Lx = (r[ire]-r[irs])/2;
		double Lz = 0.8*r[ire];
		ic++;
		if (ic >= argc) runerr("box size is missing...");
		if (sscanf(argv[ic],"%d",&nbox))  ic++;
		if (nbox<0) {		// auto-resolution
			int ir = r_to_idx(x0);
			double dx = (r[ir+1]-r[ir-1])/2;
			nbox = Lx/dx;
		}
		if (ic < argc) {
			sprintf(fn,"%s",argv[ic]);	ic++;
		} else sprintf(fn,"%s.box%d.h5",argv[1],nbox);		// output HDF5 data file
		if (ncomp==1) {
			Tlm.write_box_hdf5(fn, x0, 0, 0, Lx, Lx, Lz, nbox, nbox, nbox);
			write_box_xdmf(fn, nbox, nbox, nbox, 1);
		} else {
			Blm.write_box_hdf5(fn, x0, 0, 0, Lx, Lx, Lz, nbox, nbox, nbox);
			write_box_xdmf(fn, nbox, nbox, nbox, 3);
		}
		printf("> box cartesian spatial data (%dx%dx%d) written to HDF5/Xdmf file : '%s'\n",nbox,nbox,nbox,fn);
	}
	else if (strcmp(argv[ic],"uncurlbox") == 0)
	{
		int nbox = 32;		// default size
		ic++;
		if (ic >= argc) runerr("box size is missing...");
		if (sscanf(argv[ic],"%d",&nbox))  ic++;
		if (ic < argc) {
			sprintf(fn,"%s",argv[ic]);	ic++;
		} else sprintf(fn,"%s.uncurl_box%d.h5",argv[1],nbox);		// output HDF5 data file
		Blm.write_box_hdf5(fn, (r[irs]+r[ire])/2, 0, 0, (r[ire]-r[irs])/2, (r[ire]-r[irs])/2, 0.8*r[ire], nbox, nbox, nbox, 1);
		printf("> uncurl box cartesian spatial data (%dx%dx%d) written to HDF5 file : '%s'\n",nbox,nbox,nbox,fn);
	}
	else if (strcmp(argv[ic],"box2") == 0)
	{
		int nbox = -1;          // default size
		double z0 = 1.005;
		double L = 0.85;
		ic++;
		if (ic >= argc) runerr("box size is missing...");
		if (sscanf(argv[ic],"%d",&nbox))  ic++;
		if (nbox<0) {           // auto-resolution
			int ir = r_to_idx(z0);
			double dx = (r[ir+1]-r[ir-1])/2;
			nbox = L/dx;
		}
		if (ic < argc) {
			sprintf(fn,"%s",argv[ic]);      ic++;
		} else sprintf(fn,"%s.boxpol%d.h5",argv[1],nbox);          // output HDF5 data file
		if (ncomp==1) {
			Tlm.write_box_hdf5(fn, 0, 0, z0, L, L, L, nbox, nbox, nbox);
			write_box_xdmf(fn, nbox, nbox, nbox, 1);
		} else {
			Blm.write_box_hdf5(fn, 0, 0, z0, L, L, L, nbox, nbox, nbox);
			write_box_xdmf(fn, nbox, nbox, nbox, 3);
		}
		printf("> box cartesian spatial data (%dx%dx%d) written to HDF5/Xdmf file : '%s'\n",nbox,nbox,nbox,fn);
	}
	else if (strcmp(argv[ic],"uncurlbox2") == 0)
	{
		int nbox = 32;          // default size
		double z0 = 1.005;
		double L = 0.85;
		ic++;
		if (ic >= argc) runerr("box size is missing...");
		if (sscanf(argv[ic],"%d",&nbox))  ic++;
		if (ic < argc) {
				sprintf(fn,"%s",argv[ic]);      ic++;
		} else sprintf(fn,"%s.uncurl_boxpol%d.h5",argv[1],nbox);           // output HDF5 data file
		Blm.write_box_hdf5(fn, 0, 0, z0, L, L, L, nbox, nbox, nbox, 1);
		printf("> uncurl box cartesian spatial data (%dx%dx%d) written to HDF5 file : '%s'\n",nbox,nbox,nbox,fn);
	}
#endif
	else {
		printf("!!! warning: command #%d \"%s\" was not understood !!!\n",iloop,argv[ic]);
		exit(1);
	}
	iloop ++;
    }

	exit(0);
}

