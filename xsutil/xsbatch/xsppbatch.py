#!/usr/bin/env python3
import argparse
from os import sep, path, getcwd
from glob import glob
from itertools import chain
import re
import shutil
from subprocess import call, check_output, DEVNULL, CalledProcessError
from tempfile import TemporaryDirectory

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.colors import SymLogNorm, LogNorm
from matplotlib.widgets import Slider, Button, RadioButtons
from matplotlib.gridspec import GridSpec
from scipy.integrate import trapz
from scipy.ndimage.filters import gaussian_filter
from scipy.interpolate import interp2d

from xspar.xspar import parse_xspar
from xsbatch.xspp_read import (xspp_run,
    load_line, load_spec, load_fullspectrum, check_file, get_kind,
    load_merid, load_surf, load_disc, plot_merid, plot_surf, plot_disc)
from xsbatch.utilfuncs import Dumpdir, boolcheck, conv_kwargs
from xspar.xspar import parse_xspath, parse_xspar


sns.set_context('talk')

XSPAR_REG = re.compile(r'(?P<prefix>.*)(?P<root>field[UBT](avg)?(_kill|_back|_\d*)?)(?P<suffix>\..*)')
LOAD_DICT = {'merid':load_merid,
             'disc': load_disc,
             'surf': load_surf}

# routines for dealing with the plot interactively 
def onpick1(event):
    if isinstance(event.artist, Line2D):
        print(event.artist.get_label())
def onrelease(event):
    print('')

class LogButt:
    def __init__(self, target, fname):
        self.target = target
        self.fname = fname
    def __repr__(self):
        return 'LogButt: targetfile={}, filename={}'.format(self.target.name, self.fname)
    def log(self, event):
        print(self.target.name, '<-', self.fname)
        self.target.write(self.fname + '\n')

def setup_figures(flist, xsppkinds, components, transpose=False, **kwargs):
    """Makes lists of figures and subplots so that everything is displayed in the right spot"""
    # first get the number of different plots for each file
    xslabs = [f + c for f in xsppkinds['merid']  for c in clin.components if c in f]
    xslabs += [f + c for f in xsppkinds['disc'] for c in clin.components if c in ('s', 'z', 'p')]
    xslabs += [f + c for f in xsppkinds['surf'] for c in clin.components if c in ('r', 't', 'p')]
    xslabs += xsppkinds['line']
    xslabs += [f for f in xsppkinds['spec'] for c in clin.components if c in f]
    xslabs += [f for f in xsppkinds['SH'] for c in clin.components if c in f]
    if transpose:
        figs, subs = zip(*[plt.subplots(num=lab, squeeze=False, **kwargs)
                        for lab in xslabs])
        subs = {x: chain(*s) for x, s in zip(xslabs, subs)}
        return figs, subs
    else:
        if not ('nrows' in kwargs.keys() or 'ncols' in kwargs.keys()):
            kwargs['ncols'] = len(xslabs)
            kwargs['nrows'] = 1
        figs, subs = zip(*[plt.subplots(num=f, squeeze=False, **kwargs)
                           for f in flist])
        subs = [chain(*s) for s in subs]
        subs = {x: (s.__next__() for s in subs) for x in xslabs}
    return figs, subs

def fix_opts(fname, xsppopts):
    """fixes the xspp arguments to make sure that xspp runs properly.

    the nlat is set to a value that doesn't return an error. If mlim or
    llim are specified, but no maximum is set, the Lmax and Mmax in the
    xshells.par file are used.
    """
    # find the xshells.par file
    # if the path is relative, make it absolute
    if fname[0] != sep:
        fname = basedir+sep+fname
    xsparfile = XSPAR_REG.search(fname).expand(r'\g<prefix>xshells.par\g<suffix>')

    # for l and m, if a min is specified, but no max, use the remaining values
    for spec in ('l', 'm'):
        clopt = spec+'lim'
        xspopt = spec.upper()+'max'
        try:
            ind = xsppopts.index(clopt)+1
            if xsppopts[ind][-1] == ':':
                xsppopts[ind] += parse_xspar(xsparfile, xspopt)
        except ValueError:
            pass

    # make sure that there is a usable number of latitudinal points specified,
    # but only if necessary for the call
    fixnlat = False
    for opt in ('disc', 'surf', 'merid', 'axi'):
        fixnlat = fixnlat or (opt in xsppopts)
    fixnlat = fixnlat and (not 'nlat' in xsppopts)
    if fixnlat:
        nlat = (int(parse_xspar(xsparfile, 'Lmax')) * 3) / 2 + 1
        nlat -= nlat % 4 # number of latitudinal points must be a multiple of 4
        nlat = str(nlat)
        # command line has to end with the specific type of xspp
        xsppopts = ['nlat', nlat] + xsppopts

    # disc requires an azimuthal resolution. If none is specified, used 2x Mmax
    try:
        discind = xsppopts.index('disc') + 1
        if xsppopts[discind].index(',') != -1:
            xsppopts.insert(discind, str(int(parse_xspar(xsparfile, 'Mmax'))*2))
    except ValueError:
        pass
    return xsppopts

def xspp_merid(ax, fname, rlim=None, maskrho=None, ang=0, **kwargs):
    r, ct, a = load_merid(fname, maskrho=maskrho, ang=ang)
    stream = kwargs.get('stream', 0.0)
    try:
        *rest, stream = load_merid(stream)
    except ValueError:
        pass
    kwargs['stream'] = stream
    if rlim is not None:
        r0, rf = np.searchsorted(r.ravel(), rlim)
        r = r[r0:rf]
        a = a[r0:rf]
    plt.sca(ax)
    plot_merid(r, ct, a, **kwargs)
    return ax

def xspp_disc(ax, fname, component, rlim=None, **kwargs):
    r, phi, vs, vp, vz = load_disc(fname)
    v = {'s': vs, 'p': vp, 'z': vz}[component]
    if rlim is not None:
        r0, rf = np.searchsorted(r.ravel(), rlim)
        r = r[r0:rf]
        v = v[r0:rf]
    plt.sca(ax)
    plot_disc(r, phi, v, **kwargs)
    return ax

def xspp_surf(ax, fname, component, **kwargs):
    t, p, vr, vt, vp = load_surf(fname)
    v = {'r': vr, 't': vt, 'p': vp}[component]
    plt.sca(ax)
    plot_surf(t, p, v, **kwargs)
    return ax

def xspp_line(ax, fname, components, **kwargs):
    dat = load_line(fname)
    # add an entry for the angular momentum
    try:
        dat[('ang', 'velocity', '$\omega$')] = dat[('sph','velocity','vp')] / np.sqrt(dat[('cart', 'coord', 'x')]**2 + dat[('cart', 'coord', 'y')]**2 )
    except KeyError:
        pass
    for comp in components:
        dat[comp].plot(ax=ax, **kwargs)
    return ax

def xspp_spec(ax, fname, **kwargs_in):
    kwargs = {'legend': False}
    kwargs.update(kwargs_in)
    dat = load_spec(fname, dropzero=False)
    dat.columns.name = path.basename(fname)
    if kwargs.pop('specconv', False):
        prefact = (1.0 / dat[dat.columns[-1]])
        prefact = prefact.where(prefact!=np.inf).dropna()
        for col in dat.columns:
            dat[col].ix[prefact.index] *= prefact

    rlim = kwargs.get('rlim', False)
    if rlim:
        dat = dat[rlim[0]:rlim[1]]
    specgram = kwargs.get('specgram')
    if specgram:
        r = dat.index
        m = dat.columns
        mlabel = r'$'+m.name[2]+r'_{'+m.name[3]+r'}(r)$'
        if (m.min() == 0) and (kwargs.get('yscale','log') != 'linear') :
            m += 1
            mlabel = mlabel.replace('l', 'l+1')
            mlabel = mlabel.replace('m', 'm+1')
        maxeng = kwargs.get('maxeng', dat.__array__().max())
        levels = maxeng * np.logspace(specgram, 0, 8)
        cmap = sns.cubehelix_palette(start=0.3, light=1, as_cmap=True)
        norm = LogNorm(vmin=levels.min(), vmax=dat.__array__().max(), clip=True)
        plt.sca(ax)
        if kwargs.get('contour', False):
            dat = gaussian_filter(dat, 2)
            cnt = plt.contourf(r, m, dat.T, levels,
                               cmap=cmap, norm=norm)
        else:
            R = np.r_[r, 2*r[-1]-r[-2]]
            M = np.r_[m, 2*m[-1]-m[-2]]
            cnt = plt.pcolormesh(R, M, dat.T.__array__(), cmap=cmap, norm=norm)
            ax.set_xlim(R.min(), R.max())
            ax.set_ylim(M.min(), M.max())
            if kwargs.get('overline', False):
                dat = gaussian_filter(dat, 2)
                plt.contour(r, m, dat.T, levels,
                            colors='black')
                
        ax.set_yscale(kwargs.get('yscale', 'log'))
        if not ax.yaxis_inverted():
            ax.invert_yaxis()
        cbar = plt.colorbar(cnt, ticks=levels)
        for i, el in enumerate(levels):
            cbar.ax.annotate('{:7.1e}'.format(el), (1, float(i) / 7),
                             va='center', ha='left')
        ax.set_xlabel('r')
        ax.set_ylabel(mlabel)
        return ax
    rline = kwargs.pop('rline', False)
    if rline:
        ir = np.searchsorted(dat.index.__array__(), rline)
        dat = dat.irow(ir).T
        dat.index = pd.Index(dat.index.__array__().astype(np.double), name=path.basename(fname))
        # scale the ells or ms by radius if kspec is specified.
        if kwargs.pop('kspec', False):
            for m in dat:
                ser = pd.Series(dat[m].__array__(), index=dat.index.__array__() / m, name=m)
                ser.plot(ax=ax, **kwargs)
            ax.set_xlabel(path.basename(fname) + '/r')
            return ax
    dat.plot(ax=ax, **kwargs)
    ax.set_xlabel('r')
    ax.set_ylabel(dat.columns.name)
    # plot the last mode extra thick
    if not rline:
        dat[dat.columns[-1]].plot(ax=ax, lw=5, **kwargs)
    ax.set_yscale(kwargs.get('yscale', 'log'))
    return ax

def xspp_sh(ax, fname, **kwargs):
    dat = load_fullspectrum(fname)
    dat.plot(ax=ax, legend=False, **kwargs)
    return ax

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''runs xspp and associated plotting programs
        on a batch of xshells output files''',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('flist', nargs='+', type=str,
                        help='files to plot')
    parser.add_argument('-xs', '--xsppopts', nargs='+', type=str,
                        help='options for xspp')
    parser.add_argument('-c', '--components', nargs='+', type=str,
                        help='components of output to plot.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='set to send the xspp output to stdout instead of devnull')
    parser.add_argument('--transpose', type=int, nargs='?', const=1,
                        help='''set to put the same output of different fields in the same figure
                        (normal is group outputs by fields), input value sets number of rows''')
    parser.add_argument('--fixopts', action='store_true',
                        help = '''Set to automatically modify the arguments to xspp based on the
                        xshells.par file''')
    parser.add_argument('-po', '--plotopts', type=str, nargs='+', default=[],
                        help='options for plotting in the form OPT=VALUE')
    parser.add_argument('-so', '--subplotopts', type=str, nargs='+', default=['nrows=1', 'ncols=1'],
                        help='options for subplots in the form OPT=VALUE')
    parser.add_argument('--context', type=str, default='poster',
                        help='set seaborn context')
    parser.add_argument('--labelcomponent', action='store_true',
                        help='label the components')
    parser.add_argument('--annotationsize', type=str, default='small',
                        help='size of annotation')
    parser.add_argument('--streamlines', action='store_true',
                        help='set to plot the streamlines on the surface')
    parser.add_argument('--saveout', action='store_true',
                        help='set to save the outputs, suppresses plotting.')
    parser.add_argument('--selectout', type=str, default=None,
                        help='''If not None, creates a "mark file" button, and text file that
                        notes the figures that are marked. NOT TESTED WITH TRANSPOSE!''')
    clin = parser.parse_args()

    sns.set_context(clin.context)
        
    kwargs = {k: v for k, v in [opt.split('=') for opt in clin.plotopts]}
    subplotkwargs = {k: v for k, v in [opt.split('=') for opt in clin.subplotopts]}
    # convert strings to floats and ints where possible
    conv_kwargs(kwargs)
    conv_kwargs(subplotkwargs)
    stdout = {True: None, False: DEVNULL}[clin.verbose]
    flist = [path.abspath(f) for f in clin.flist]
    commonprefix = path.commonprefix(flist)
    commonsuffix = path.commonprefix([f[::-1] for f in flist])[::-1]
    print(commonprefix, commonsuffix)
    # setup the log file
    if clin.selectout:
        selectfobj = open(clin.selectout, 'a')
        logbutts = {f: LogButt(selectfobj, f) for f in flist}
    # with TemporaryDirectory() as targetdir:
    #     shutil.os.chdir(targetdir)
    with Dumpdir() as targetdir:
        for f_index, fname in enumerate(flist):
            print(fname)
            if clin.fixopts:
                xsppopts = fix_opts(list(clin.xsppopts))
            else:
                xsppopts = list(clin.xsppopts)
            # run xspp, collect the names of the output files
            try:
                xsppkinds = xspp_run(fname, xsppopts, verbose=(stdout!=DEVNULL), oneshell=True)
            except CalledProcessError:
                print('failed')
                continue
            # if we want to log to a text file, we need a separate call to show
            # for each file
            if clin.selectout:
                figs, subplots = setup_figures([fname, ], xsppkinds,
                                               clin.components, clin.transpose,
                                               **subplotkwargs)
                butt = Button(figs[0].add_subplot(
                    GridSpec(1, 1, left=0.81, right=0.95, bottom=0.91, top=0.95)[0, 0]),
                    clin.selectout)
                butt.on_clicked(logbutts[fname].log)
            elif fname == flist[0]:
                figs, subplots = setup_figures(clin.flist, xsppkinds,
                                               clin.components, clin.transpose,
                                               **subplotkwargs)
            fname = fname[len(commonprefix):-len(commonsuffix)]
            for f in xsppkinds['merid']:
                try:
                    c = [c for c in clin.components if c in f][0]
                except IndexError:
                    continue
                ax = xspp_merid(subplots[f+c].__next__(), f, **kwargs)
                if clin.transpose:
                    ax.annotate(fname, (0.5, 0.99), ha='center', va='top',
                                xycoords='axes fraction', size=clin.annotationsize)
                if clin.labelcomponent:
                    label = {'o_Vr': r'$V_r$', 'o_Vt': r'$V_\theta$', 'o_Vp': r'$V_\phi$',
                             'o_Vs': r'$V_s$', 'o_Vz': r'$V_z$',
                             'o_Vpol': r'$\Psi$', 'o_Ap': r'$\Phi$'}[f.split('.')[0]]
                    ax.annotate(label, (0.95, 0.95),
                                xycoords='axes fraction',
                                ha='right', va='top', size='x-large')

            for f in xsppkinds['disc']:
                comps = [c for c in clin.components if c in ('s', 'z', 'p')]
                for c in comps:
                    ax = xspp_disc(subplots[f+c].__next__(), f, c, **kwargs)
                    if clin.transpose:
                        ax.annotate(fname, (0.5, 0.99), ha='center', va='top',
                                    xycoords='axes fraction', size=clin.annotationsize)
                    if clin.labelcomponent:
                        label = r'${\sf V_'+c.replace('p', r'\phi') + r'}$'
                        if f.endswith('v2'):
                            label = label[:6]+r'\left|\left|'+label[6:-2]+r'\right|\right|^2'+label[-2:]
                        ax.annotate(label, (0.95, 0.95), ha='right', va='top', size='x-large',
                                    xycoords='axes fraction')
                    if clin.streamlines:
                        r, p, vs, vp, vz = load_disc(f)
                        x = r[::2].reshape(-1, 1) * np.cos(p[:-1])
                        y = r[::2].reshape(-1, 1) * np.sin(p[:-1])
                        X = np.linspace(x.min(), x.max(), r.size)
                        Y = np.linspace(y.min(), y.max(), r.size)
                        XX, YY = np.meshgrid(X, Y) 
                        vx = vs[::2, :-1] * np.cos(p[:-1]) - vp[::2, :-1] * np.sin(p[:-1])
                        vy = vs[::2, :-1] * np.sin(p[:-1]) + vp[::2, :-1] * np.cos(p[:-1])
                        # print('interp1')
                        # vx_interp = interp2d(x, y, vx, kind='linear', fill_value=0.0, copy=False)
                        # print('interp2')
                        # vy_interp = interp2d(x, y, vy, kind='linear', fill_value=0.0, copy=False)
                        # print('interp3')
                        # ax.streamplot(XX, YY, vx_interp(X, Y), vy_interp(X, Y))
            for f in xsppkinds['line']:
                ax = xspp_line(subplots[f].__next__(), f, clin.components, **kwargs)
                if clin.transpose:
                    ax.annotate(fname, (0.5, 0.99), ha='center', va='top',
                                xycoords='axes fraction', size=clin.annotationsize)
            for f in xsppkinds['spec']:
                if not any(c in f for c in clin.components):
                    continue
                ax = xspp_spec(subplots[f].__next__(), f, **kwargs)
                if clin.transpose:
                    ax.annotate(fname, (0.5, 0.99), ha='center', va='top',
                                xycoords='axes fraction', size=clin.annotationsize)
            for f in xsppkinds['surf']:
                comps = [c for c in clin.components if c in ('r', 't', 'p')]
                for c in comps:
                    ax = xspp_surf(subplots[f+c].__next__(), f, c, **kwargs)
                    if clin.transpose:
                        ax.annotate(fname, (0.5, 0.99), ha='center', va='top',
                                    xycoords='axes fraction', size=clin.annotationsize)
                    if clin.labelcomponent:
                        label = r'$V_'+c.replace('p', r'\phi') + r'$'
                        ax.annotate(label, (0.95, 0.95), ha='right', va='top', size='large')
            if clin.selectout:
                plt.show()
    # clear all the unused axes
    for sub in subplots.values():
        for s in sub:
            s.set_axis_off()
    if clin.saveout:
        for f in figs:
            f.savefig('/home/kaplane/movie/'+f.get_label().strip('/').replace('/', '_')+'.png')
    else:
        plt.show()
