import re
import numpy as np
from os import sep

OPDICT = {'+': np.add,
          '*': np.multiply,
          '/': np.divide}

OPREG= re.compile(r'[{}]'.format(''.join(OPDICT.keys())))


def parse_xspar(xspath, *xsopts, empty=None, evaluate=False):
    """parse xshells.par file

    reads an xshells.par file and returns a dictionary containing the
    values of the requested options. If evaluate is False, only strings
    are returned. If evaluate is True, floats are returned where
    possible, and some simple arithmetic can be performed.
    """
    parreg = re.compile(r'''^
    (?P<opt>{}) # options to change
    \s*=\s*
    (?P<value>[^#\s]*)
    '''.format('|'.join(xsopts)), re.VERBOSE)

    # set up a dictionary to hold the values
    xsoptdict = {}
    with open(xspath, 'r') as f:
        for line in f:
            match = parreg.match(line)
            if match:
                xsoptdict[match.group('opt')] = match.group('value')
    if empty is not None:
        for opt in xsopts:
            if opt not in xsoptdict.keys():
                xsoptdict[opt] = empty
    if evaluate:
        return evaluate_xsoptdict(xspath, xsoptdict)
    return xsoptdict


def _evaluate_piece(xspath, xseval, expression):
    # function for evalutating each piece of an expression
    # first do a straight evaluation
    try:
        return float(expression)
    # next see if it's already been calculated
    except ValueError:
        try:
            return xseval[expression]
        # next pull it from the xshells.par file (recursive)
        except KeyError:
            return parse_xspar(xspath, val_string, evaluate=True)[val_string]


def evaluate_xsoptdict(xspath, xsoptdict):
    """Evalues expressions in the xshells.par file."""

    # do straight conversions to floats for cases with no operations
    xseval = {key: float(val) for key, val in xsoptdict.items() if OPREG.search(val) is None}
    for key in xseval.keys():
        xsoptdict.pop(key)
    xseval['pi'] = np.pi
    # now step through each string with operations
    for key, value in xsoptdict.items():
        val_float = 0.0
        op = '+'
        start = -1
        for match in OPREG.finditer(value):
            val_string = value[start+1:match.start()]
            val_float = OPDICT[op](val_float, _evaluate_piece(xspath, xseval, val_string))
            op = match.group(0)
            start = match.start()
        val_string = value[start+1:]
        xseval[key] = OPDICT[op](val_float, _evaluate_piece(xspath, xseval, val_string))
    xseval.pop('pi')
    return xseval


def parse_xspath(xspath, regex=r'_(?P<param>\D*)(?P<val>[-e.\d]*)'):
    '''parses filepaths

    The filepaths must end with
    [label]_parval1_parval2..._parvalN. Where each parval is a
    combination of a string and a number. Alternate regexprs can be
    provided
    '''
    assert xspath.rfind('xshells.par') != -1, 'must provide xshells.par files'
    return {match.group('param'): float(match.group('val')) for match in re.finditer(regex, xspath)}

