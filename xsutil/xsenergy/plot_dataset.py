#!/usr/bin/python3
# -*- coding: utf-8 -*-

# stdlib imports
from glob import glob
from itertools import chain, cycle
from colorama import Fore
import sys
import re
# scipy stack imports
import numpy as np
import pandas as pd
from pandas import DataFrame, Series
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec
from matplotlib.widgets import Slider, Button, RadioButtons
import seaborn as sns
from scipy.signal import periodogram

sns.set_style('darkgrid')
sns.set_palette('dark')

from xsenergy.read_dataset import (
    read_table, parse_header, sort_columns,
    regex_columns, to_magnitude_phase, to_difference)
from xsenergy.mark_timesteps import get_inds

# routines for dealing with the plot interactively 
def onpick1(event):
    if isinstance(event.artist, Line2D):
        print(event.artist.get_label())
def onrelease(event):
    print('')

class logButt:
    def __init__(self, target, fname):
        self.target = target
        self.fname = fname
    def log(self, event):
        print(self.target.name, '<-', self.fname)
        self.target.write(self.fname + '\n')
        
def tokwargs(optlist, splitstring='='):
    split = (opt.split(splitstring) for opt in optlist)
    def form(v):
        if v[0]=='[' and v[-1] == ']':
            return [form(w.strip()) for w in v[1:-1].split(',')]
        if v == 'False':
            return False
        if v == 'True':
            return True
        if re.match('\d+', v):
            return np.int(v)
        if re.match('[\d.]+', v):
            return np.float(v)
        return v
    return {k: form(v) for k, v in split}
        
def heirarchy_index(columns, keys):
    rowinds = []
    colinds = []
    # get the level of each label
    for key in keys:
        [rowinds.append(i) for i in [next(j for j, lev in enumerate(columns.levels) if key in lev)]]
    # get the index of each label within the level
    for key, row in zip(keys, rowinds):
        [colinds.append(i) for i in [next(j for j, lev in enumerate(columns.levels[row]) if key in lev)]]
    # make a new index where each entry matches
    print(rowinds, colinds)    
#heirarchy_index(mp2.columns, ['kinetic', 'm=32', 'Phase'])                    

def plot_dataset(df, ax, title, sortcolumns=False, pkwargs={}, **kwargs):
    if sortcolumns:
        # ind = df.max()
        # ind.sort(ascending=False)
        # if sortcolumns != -1:
        #     ind = ind[ind.index[:min(sortcolumns, len(ind.index))]]
        # df = df.reindex(columns=ind.index)
        df = sort_columns(df, limit=sortcolumns)
    if kwargs.get('periodogram', False):
        columns = df.columns
        f0 = np.mean(np.diff(df.index))
        fs, Pxx = periodogram(df, f0, axis=0)
        # drop the zero frequency term
        fs = fs[1:]
        Pxx = Pxx[1:, :]
        df = DataFrame(Pxx, index=fs, columns=columns)
        
    df.plot(ax=ax, picker=5, legend=False, **pkwargs)
    try:
        ax.set_yscale(kwargs.get('yscale', 'log'))
        ax.set_xscale(kwargs.get('xscale', 'linear'))
    except ValueError:
        pass
    if not kwargs.get('legendoff', False):
        ax.legend(ncol=2, fontsize=10,
                  title=title,
                  ).draggable(True)

        
if __name__ == '__main__':
    import argparse
    import os

    parser = argparse.ArgumentParser(
        description='plots time traces of energy and custom diagnostics output by xshells',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('fname', type=str, nargs='+',
                        help='file to read, must be output by xshells')
    parser.add_argument('--printcolumns', action='store_true',
                        help='set to names and indices for all of the columns')
    parser.add_argument('--index_col', type=str, default='t',
                        help='column to index data by')
    parser.add_argument('-cn', '--column_names', nargs='+', type=str,
                        help='plot columns with specified names')
    parser.add_argument('-cr', '--column_regex', type=str, default=None,
                        help='regular expression to determine which columns to plot')
    parser.add_argument('-sc', '--sortcolumns', type=int, nargs='?', const=-1,
                        help='''set to index the plots by max value of the series.
                        If a number n is specified, only plot the first n columns''')
    parser.add_argument('-po', '--plot_opts', nargs='+', default=[],
                        help='options piped to the plotting routine in the form OPT=VALUE')
    parser.add_argument('-so', '--subplot_opts', nargs='+', default=[],
                        help='options piped to the subplots routine in the form OPT=VALUE')
    parser.add_argument('--scaletime', action='store_true',
                        help='set to scale time by 2pi, from phase in radians to phase in periods')
    parser.add_argument('--detrend', action='store_true',
                        help='subtract off the mean signals')
    parser.add_argument('--marksaves', type=int, nargs='?', const=1,
                        help='set to mark the locations of the saves on the plot, fieldU and Eu are used')
    parser.add_argument('--markkey', type=str, default='Eu',
                        help='if marksaves is active, line to fix the marks to')
    parser.add_argument('--legendoff', action='store_true',
                        help='set to inhibit legend')
    parser.add_argument('--xshells_dir', type=str, default=os.environ['HOME']+'/xshells',
                        help='location of xspp executable')
    parser.add_argument('--heirarchy', type=str, nargs='?', const=-1,
                        help='set to use heirarchical dataframes.')
    parser.add_argument('--energysplit', action='store_true',
                        help='''shortcut for column_regex='^E',
                        heirarchy=kinetic.zonal,kinetic.nonzonal,magnetic.zonal,magnetic.nonzonal''')
    parser.add_argument('--xscale', type=str, default='linear',
                        help='scale to plot on (linear, log)')
    parser.add_argument('--xlim', type=np.double, nargs=2, default=None,
                        help='limits for y axis')
    parser.add_argument('--yscale', type=str, default='log',
                        help='scale to plot on (linear, log)')
    parser.add_argument('--ylim', type=np.double, nargs=2, default=None,
                        help='limits for y axis')
    parser.add_argument('--tomagpha', action='store_true',
                        help='set to convert the dataframe to one with magnitude and phase data')
    parser.add_argument('--topotdiff', action='store_true',
                        help='set to plot potential differences in lieu of raw potentials')
    parser.add_argument('--periodogram', action='store_true',
                        help='set to plot the periodogram of the signal')
    parser.add_argument('--nrows', type=int, default=None,
                        help='set to manually set number of rows in figure')
    parser.add_argument('--ncols', type=int, default=None,
                        help='set to manually set number of columns in figure')
    parser.add_argument('--selectout', type=str, default=None,
                        help='''If not None, creates a "mark file" button, and text file that
                        notes the figures that are marked.''')
    opts = vars(parser.parse_args())
    fnames = opts.pop('fname')
    pkwargs = tokwargs(opts.pop('plot_opts'))
    subplotkwargs = tokwargs(opts.pop('subplot_opts'))
    kwargs = {k : opts[k] for k in opts if opts[k] != None}
    if kwargs.get('energysplit', False):
        kwargs['column_regex'] = '^(EZ|ENZ)'
        kwargs['heirarchy'] = 'kinetic.zonal,kinetic.nonzonal,magnetic.zonal,magnetic.nonzonal'
        rcParams['axes.prop_cycle'] = \
          plt.cycler('color', sns.color_palette('colorblind')) * plt.cycler('ls', ['-', '--'])
    if kwargs.get('selectout', False):
        selectfobj = open(kwargs.get('selectout'), 'w')
        logButts = {f: logButt(selectfobj, f) for f in fnames}
    if kwargs.get('heirarchy', -1) != -1:
        kwargs['heirarchy'] = kwargs['heirarchy'].split(',')
    if kwargs.get('tomagpha', False):
        kwargs['heirarchy'] = ['Mag.pt1', 'Mag.pt2', 'Phase.pt1', 'Phase.pt2']

    if kwargs.get('printcolumns', False):
        fobj = open(fnames[0], 'r')
        names, columns = parse_header(fobj.readline(), **kwargs)
        fobj.close()
        indexcol = names.pop(names.index(kwargs['index_col']))
        if kwargs.get('heirarchy', False):
            index = pd.MultiIndex.from_arrays(columns)
            print(index)
            sys.exit()
        # format the output into columns
        c1, c2, c3 = [[names.pop(0) for i in range(int(np.ceil(len(names) / j)))]
                      for j in range(3,0,-1)]
        step1, step2 = len(c1), len(c1+c2)
        ind = 0
        print('index:\t'+kwargs['index_col'])
        print('column ind:\tcolumn name')
        while(c3):
            print('{}:\t{}\t\t{}:\t{}\t\t{}:\t{}'.format(
                ind, c1.pop(0), ind+step1, c2.pop(0), ind+step2, c3.pop(0)))
            ind += 1
        # last row may have more entries
        if c1:
            out = '{}:\t{}\t\t'.format(ind, c1.pop(0))
            if c2:
                out += '{}:\t{}\t\t'.format(ind+step1, c2.pop(0))
            print(out)
        sys.exit()
        
    
    for fname in fnames:
        # read in the data\
        print(fname)
        try:
            dat = read_table(fname, **kwargs)
            if kwargs['scaletime']:
                arr = dat.index.__array__()
                arr /= 2.0 * np.pi
                arr -= arr[0]
            if kwargs['detrend']:
                dat -= dat.mean(axis=0)
        except ValueError as err:
            print(Fore.RED+'\tfailed'+Fore.RED)
            continue
        if kwargs.get('topotdiff', False):
            to_difference(dat)
        # only need the legend once
        if fname != fnames[0]:
            kwargs['legendoff'] = True
        # handle reindexing calls
        # convert to magnitude and phase
        if kwargs.get('tomagpha', False):
            dat = to_magnitude_phase(dat)
        # if kwargs.get('heirarchy', -1) != -1:
        #     dat = dat[kwargs['heirarchy']]
        plotcols = dat.columns
        if len(plotcols) == 0:
            continue
        heirarchy = kwargs.get('heirarchy', -1)
        if heirarchy  != -1:
            # get the number of unique headers
            if not subplotkwargs:
                root = {h.split('.')[0] for h in heirarchy}
                subplotkwargs = {'nrows': np.int(len(root)),
                                 'ncols': np.int(np.ceil(len(heirarchy) / len(root))),
                                 'sharex': True,
                                 'sharey': False,
                                }
            fig, axes = plt.subplots(num=fname, **subplotkwargs)
            if not hasattr(axes, '__iter__'): 
                axes = [axes,]
            try:
                axes = [ax for ax in chain(*axes)]
            except TypeError:
                pass
            for i, key in enumerate(kwargs['heirarchy']):
                ax = axes[i]
                tmp = dat
                for k in key.split('.'):
                    tmp = tmp[k]
                plot_dataset(tmp, ax, key, pkwargs=pkwargs, **kwargs)
                ax.set_title(key)
        else:
            fig, ax = plt.subplots(ncols=1, nrows=1, num=fname)
            dat = dat.reindex(columns=plotcols)
            plot_dataset(dat, ax, '', pkwargs=pkwargs, **kwargs)

        if kwargs.get('ylim', False):
            ax.set_ylim(*kwargs['ylim'])
        if kwargs.get('xlim', False):
            ax.set_xlim(*kwargs['xlim'])

        if kwargs.get('marksaves', False):
            spec = fname.split('energy.')
            markfiles = sorted(glob('{}/fieldU_[0-9][0-9][0-9][0-9].{}'.format(*spec)))
            markfiles += ['{}/fieldU_back.{}'.format(*spec), '{}/fieldU.{}'.format(*spec)]
            markfiles = markfiles[::kwargs['marksaves']]
            sp_args = ['{}/xspp'.format(kwargs.get('xshells_dir', '.')), '', 'nrj']
            files, markdf = get_inds(dat, 'u', markfiles, sp_args)
            for i, f in enumerate(files):
                t = markdf.index[i]
                eng = markdf[kwargs['markkey']][markdf.index[i]]
                ax.text(t, eng, f.split('/')[-1])
            # for (f, t, eng) in zip(files, tarr, engarr):
            #     ax.text(t, eng, f.split('/')[-1])
        # set up a button to record the noted files
        if kwargs.get('selectout', False):
            Butt = Button(fig.add_subplot(
                GridSpec(1, 1, left=0.81, right=0.95, bottom=0.91, top=0.95)[0, 0]),
                kwargs.get('selectout'))
            Butt.on_clicked(logButts[fname].log)
        

        fig.canvas.mpl_connect('pick_event', onpick1)
        fig.canvas.mpl_connect('button_release_event', onrelease)

    plt.show()
    if kwargs.get('selectout', False):
        selectfobj.close()
