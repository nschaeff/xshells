#!/usr/bin/python3
from xspar.xspar import use_template

if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='update parameter files')
    parser.add_argument('src', type=str,
                        help='template xshells.par file')
    parser.add_argument('dest', type=str,
                        help='destination xshells.par file')
    parser.add_argument('opts', type=str, nargs='+',
                        help='options to replace, in form option=value')

    clin = parser.parse_args()
    src = clin.src
    dest = clin.dest
    kv = (opt.split('=') for opt in clin.opts)
    xsopts = {k: v for k, v in kv}
    use_template(src, dest, **xsopts)
