/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xshells_big.cpp main program for
/// XSHELLS (eXtendable Spherical Harmonic Earth-Like Liquid Simulator).

#ifdef XS_MPI
  #include <mpi.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string>
#include <string.h>
#include <math.h>

//#define _GNU_SOURCE
//#include <fenv.h>

#include <shtns.h>
#if defined(XS_MKL) && defined(_OPENMP)
	#include <fftw3_mkl.h>
#endif

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define XS_PURE_SPECTRAL	// no spatial fields (mandatory for xshells_big.cpp).

#ifndef _HGID_
  #define _HGID_ "unknown"
#endif

#ifndef XS_NOPLOT
#include "gnuplot_pipes.hpp"
#endif

enum field_id { U, B, T, C, MAXFIELD };				///< field enumeration.
const char symbol[MAXFIELD] = { 'U', 'B', 'T', 'C' };	///< symbols for these fields.
#define EVOL(f) (1U<<(f))


/// Accumulate values without too much rounding error, using compensated summation.
/// See https://en.wikipedia.org/wiki/Kahan_summation_algorithm
/// Warning: don't compile with gcc/clang -ffast-math. For intel compiler, add -fp-model=precise
class accurate_accumulator {
#ifdef __FAST_MATH__
#warning Compensated summation is optimized away with -ffast-math (/fp:fast)
#endif
	double sum;
	double cor;
  public:
	accurate_accumulator(double x=0.) { sum=x;	cor=0.; }
	void operator = (double x) { sum=x;  cor=0.; }
	void operator += (double x) {	// https://en.wikipedia.org/wiki/Kahan_summation_algorithm
		x += cor;
		double s = sum + x;
		cor = x - (s-sum);		// 0 in infinite precision, non-zero otherwise
		sum = s;
	}
	operator double() const { return sum; }		// implicit conversion to double.
};

int iter = 0;				///< current iteration.
accurate_accumulator ftime = 0.0;			///< current fluid time.
double dt_log;				///< time between logs

enum need { NEED_U=1, NEED_W=2, NEED_B=4, NEED_J=8, 
	NEED_T=16, NEED_GT=32, NEED_C=64, NEED_GC=128, 
	NEED_UXW=1024, NEED_UXB=2048, NEED_JXB=4096, NEED_UGT=8192, NEED_UGC=16384, NEED_COR=32768 };				///< field enumeration.

int spat_need = 0;		///< flags for required computations of spatial fields.

#define EVOL_U EVOL(::U)
#define EVOL_B EVOL(::B)
#define EVOL_T EVOL(::T)
#define EVOL_C EVOL(::C)
int evol_ubt = 0;		///< flags for evolution of fields.

const double pi = M_PI;			///< pi = 3.14...

#include "xshells.hpp"
#if defined(XS_ELLIPTIC) || defined(XS_MASK) || defined(XS_JxB_M0) || defined(XS_SURF_PAIS) || defined(XS_DTS_POTENTIAL) || defined(XS_LIBRATION)
	#error "unsupported feature enabled in xshells.hpp"
#endif
#ifndef XS_MPI
	#undef XS_MPI_ALL2ALL
#endif
#ifdef _OPENMP
	int nthreads;
	#include <omp.h>
	#define DEB printf("process %d, thread %d :: %s:%u pass\n",i_mpi, omp_get_thread_num(), __FILE__, __LINE__)
#else
	#define nthreads 1
	#define DEB printf("process %d, %s:%u pass\n", i_mpi, __FILE__, __LINE__); MPI_Barrier(MPI_COMM_WORLD)
#endif

#include "grid.cpp"

// spatial buffer (one for each thread)
static double* spat_mem = 0;
#if XS_OMP == 1
#pragma omp threadprivate(spat_mem)
#endif

#ifndef XS_DEBUG
	#undef DEB
	#define DEB (0)
#endif

/// for real-time performance measurements, returns elapsed time in seconds.
#ifdef XS_MPI
  inline double xs_wtime() {  return MPI_Wtime();  }
#elif defined( _OPENMP )
  inline double xs_wtime() {  return omp_get_wtime();  }
#else
  #include <sys/time.h>
  double xs_wtime() {			// use gettimeofday
	static long sec_base = 0;
	struct timeval tv;
	gettimeofday(&tv, NULL);
	if (sec_base == 0) sec_base = tv.tv_sec;
	return tv.tv_usec*1e-6 + (tv.tv_sec - sec_base);
  }
#endif


#include "xshells_spectral.cpp"
#include "xshells_io.cpp"


class StateVector {
  public:
	PolTor U, B;
	ScalarSH T,C;
	static Spectral *Su, *Sb;					// temporary storage for spheroidal part of non-linear terms (before curl)

  private:
	Spectral* field[MAXFIELD];			// access as generic fields
	xs_array2d<cplx> comp[MAXFIELD*2-2];			// direct access to components.
	static int ir_bc[MAXFIELD*2];		// radial limits

  public:
	void commit();
	void clone(const StateVector &X);
	void copy(const StateVector &X);
	void accumulate_scaled(const StateVector &X, double pre_scale=1.0, double post_scale=1.0);	// accumulate, with optional scaling
	void zero_out();
	inline static int ir_bci(int f) { return ir_bc[f*2]; }
	inline static int ir_bco(int f) { return ir_bc[f*2+1]; }
	inline Spectral& operator[](int f) const { return *(field[f]); }
	inline const xs_array2d<cplx>& get_comp(int ic) const { return comp[ic]; }

	void shell_gradT(int ir, cplx *Q, cplx *S, const int lms, const int lme) const;
	void shell_r_gradT(int ir, cplx *S, double *gt, double *gp) const;

	void free_state() {
		for (int f=0; f<MAXFIELD; f++) if (field[f]) {
			field[f]->free_field();
			field[f] = 0;
		}
	}
	~StateVector() { free_state(); }
};

Spectral* StateVector::Su;
Spectral* StateVector::Sb;
int StateVector::ir_bc[MAXFIELD*2];

/// Holds and update a time-average of StateVector.
class StateVectorAverage : public StateVector {
	accurate_accumulator n_avg;		///< current total averaging weight (norm), or number of states that have been averaged so far if all weight 1.
	double w_prev = 0.;		///< previous weight
  public:
	explicit StateVectorAverage(const StateVector& X) {
		n_avg = 0;
		clone(X);
		zero_out();
	}
	void update_average(const StateVector& X, const double w);	///< update average with contribution X, weighted by w.
	void reset() { n_avg = 0;	w_prev = 0; }		// n_avg is also the weight of the current average, so no need to set current average to zero.
};


void StateVector::commit()
{
	for (int f=0; f<MAXFIELD; f++)  field[f] = 0;
	if (evol_ubt & EVOL(::U)) {
		field[::U] = &U;
		comp[0] = U.get_comp(0);
		comp[1] = U.get_comp(1);
	}
	if (evol_ubt & EVOL(::B)) {
		field[::B] = &B;
		comp[2] = B.get_comp(0);
		comp[3] = B.get_comp(1);
	}
	if (evol_ubt & EVOL(::T)) {
		field[::T] = &T;
		comp[4] = T.get_comp(0);
	}
	if (evol_ubt & EVOL(::C)) {
		field[::C] = &C;
		comp[5] = C.get_comp(0);
	}
	for (int f=0; f<MAXFIELD; f++) {
		if (field[f])  {
			ir_bc[f*2]   = field[f]->ir_bci;
			ir_bc[f*2+1] = field[f]->ir_bco;
		}
	}
}

void StateVector::clone(const StateVector &X)
{
	if (evol_ubt & EVOL(::U)) U.clone(X.U);		// non-linear terms require additional stroage.
	if (evol_ubt & EVOL(::B)) B.clone(X.B);
	if (evol_ubt & EVOL(::T)) T.clone(X.T);
	if (evol_ubt & EVOL(::C)) C.clone(X.C);
	commit();
}

void StateVector::zero_out()
{
	for (int f=0; f<MAXFIELD; f++) {
		if (evol_ubt & EVOL(f))		field[f]->zero_out();
	}
}
void StateVector::copy(const StateVector &X)
{
	for (int f=0; f<MAXFIELD; f++) {
		if (evol_ubt & EVOL(f))		field[f]->copy(*(X.field[f]));
	}
}

SolidBody InnerCore, Mantle;		// Solid boundaries.

// function pointer for explicit terms
void (*explicit_terms_)(StateVector& NL, StateVector& Xlm, int calc_cfl);

inline void explicit_terms(StateVector& NL, StateVector& Xlm, int calc_cfl) {
  #if XS_OMP > 0 && defined( XS_DEBUG )
  if (omp_in_parallel()) runerr("!!! forbidden !!!");	// runtime check in debug mode to prevent stupid mistakes
  #endif
  #if XS_OMP == 1
	#pragma omp parallel
  #endif
	explicit_terms_(NL, Xlm, calc_cfl);
}

StateVector Xlm;
StateVector* Lin[5];		// up to five vectors for linear part.
StateVector* NL[4];			// up to four vectors for non-linear terms.
PolTor& Ulm = Xlm.U;		// aliases for easy access of fields in StateVector Xlm
PolTor& Blm = Xlm.B;
ScalarSH& Tlm = Xlm.T;
ScalarSH& Clm = Xlm.C;

StatSpecScal *T0lm = NULL;	///< Imposed background temperature.
StatSpecScal *C0lm = NULL;	///< Imposed background composition.
double* Grav0_r = NULL;		///< central gravity divided by r (l=0 only).
#define NL_ORDER 2
#ifdef XS_WRITE_SV
	cplx *Bp1, *Bp2;		// for time derivative of surface field
	cplx *Up1, *Ut1;
#endif
volatile int SAVE_QUIT = 0;	// for signal catching.

#ifdef XS_LINEAR
// base fields for linear stability.
SpatVect *U0 = 0;
SpatVect *W0 = 0;
SpatVect *B0 = 0;
SpatVect *J0 = 0;
SpatVect *J0xB0 = 0;		// Lorentz force driving by J0xB0
SpatVect *gradT0 = 0;		// gradient of T0
SpatVect *gradPhi0 = 0;		///< general gravity field (non-central).
SpatVect *gradC0 = 0;		// gradient of C0
#endif

StateVectorAverage* Xavg=0;		// for optional time-averaging
StateVector* dXdt=0;			// for optional time-derivative


#include "xshells_physics.cpp"
#ifdef XS_SPARSE
#include "xshells_sparse.cpp"
#endif

#include "xshells_init.cpp"

RefFrame frame;		// the reference frame (with possibly variable rotation)

#ifdef XS_MEAN_FIELD
spat3D alpha;	// a scalar alpha effect.
#endif

#include "xshells.hpp"
#ifndef U_FORCING
	#define FORCING_M 0
#endif

/// saves snapshot in a secure way (never overwrite data, avoid data corruption).
void save_snapshot(double ftime, int iter)
{
	static double last_ftime = 0;
	if (last_ftime == ftime) return;			// do nothing if snapshot already saved at this time.

	static char fnew[MAXFIELD][100];
	static char fbak[MAXFIELD][100];
	static char ftmp[MAXFIELD][100];
	for (int f=0; f<MAXFIELD; f++) {
		if (evol_ubt & EVOL(f)) {
			// generate filenames
			sprintf(ftmp[f],"field%c_tmp.%s",symbol[f],jpar.job);
			sprintf(fbak[f],"field%c_back.%s",symbol[f],jpar.job);
			sprintf(fnew[f],"field%c.%s",symbol[f],jpar.job);
			// remove old backup files
			if (i_mpi==0) remove(fbak[f]);
		}
	}
#ifdef XS_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif
	// write new snapshots in temp files
	for (int f=0; f<MAXFIELD; f++) {
		if (evol_ubt & EVOL(f))	Xlm[f].save(ftmp[f], ftime, iter);
	}
	// backup previous snapshot if it exists and rename new snapshots
	if (i_mpi==0) {
		for (int f=0; f<MAXFIELD; f++) {
			if (evol_ubt & EVOL(f)) {
				rename(fnew[f], fbak[f]);		rename(ftmp[f], fnew[f]);
			}
		}
	}
#ifdef XS_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif
	last_ftime = ftime;		// remember the ftime of this snapshot dump.
	if (i_mpi==0) {
		printf(" > snapshot dumped.\n");	fflush(stdout);
	}
}


/// saves fields and exit.
void save_quit() {
	char fname[100];
	for (int f=0; f<MAXFIELD; f++) {
		if (evol_ubt & EVOL(f)) {
			sprintf(fname,"field%c_kill.%s",symbol[f],jpar.job);
			Xlm[f].save(fname, ftime, iter);
		}
	}
	PRINTF0("=> terminated by signal, snapshot dumped (t=%g)\n", (double) ftime);
#ifdef XS_MPI
	MPI_Abort(MPI_COMM_WORLD,2);
#endif
	exit(2);		// exit code 2 => restart is possible.
}


/// time-average solution by accumulation, with n_avg the number of fields that have been averaged so far.
/// /!\ should not be called from an omp parallel region
void StateVectorAverage::update_average(const StateVector& Xlm, double w) {
	{	// Order one time-averaging: the weight of current field is 0.5*dt_old + 0.5*dt.   0.5*dt is stored in w_prev for next time-step.
		double ww = w_prev;
		w *= 0.5;
		w_prev = w;
		w += ww;
		n_avg += w;		// update the total weight
	}
	#pragma omp parallel
	{
		const double a = w/n_avg;
		for (int f=0; f<MAXFIELD; f++) {
			if (evol_ubt & EVOL(f)) {
				int ir0 = Xlm[f].irs-1;
				int ir1 = Xlm[f].ire+1;
				int lm0 = 0;
				int lm1 = NLM - 1;
				thread_interval_rad(ir0, ir1);
				thread_interval_lm(lm0, lm1);
				for (int ic=0; ic<Xlm[f].get_ncomp(); ic++) {
					for (int ir=ir0; ir<=ir1; ir++) {
						cplx* __restrict acc = (*this)[f].get_data(ic, ir);
						const cplx* cur = Xlm[f].get_data(ic, ir);
						//#pragma omp simd
						for (long lm=lm0; lm <= lm1; lm++)
							acc[lm] = acc[lm] - a*(acc[lm] - cur[lm]);
					}
				}
			}
		}
	}
	barrier_shared_mem();
}

void StateVector::accumulate_scaled(const StateVector& Xlm, double pre_scale, double post_scale) {
	#pragma omp parallel
	{
		for (int f=0; f<MAXFIELD; f++) {
			if (evol_ubt & EVOL(f)) {
				int ir0 = Xlm[f].irs-1;
				int ir1 = Xlm[f].ire+1;
				int lm0 = 0;
				int lm1 = NLM - 1;
				thread_interval_rad(ir0, ir1);
				thread_interval_lm(lm0, lm1);
				for (int ic=0; ic<Xlm[f].get_ncomp(); ic++) {
					for (int ir=ir0; ir<=ir1; ir++) {
						cplx* __restrict acc = (*this)[f].get_data(ic, ir);
						const cplx* cur = Xlm[f].get_data(ic, ir);
						//#pragma omp simd
						for (long lm=lm0; lm <= lm1; lm++)
							acc[lm] = (acc[lm]*pre_scale + cur[lm])*post_scale;
					}
				}
			}
		}
	}
	barrier_shared_mem();
}

// **************************
// ***** TIME STEPPING ******
// **************************

enum cfl_id { cfl_ur, cfl_uh, cfl_br, cfl_bh, cfl_w, cfl_j, MAX_CFL };		///< cfl enumeration.
double dt_cfl[MAX_CFL];	///< dt required by CFL condition (radial and horizontal directions)
double C_cfl[MAX_CFL];	///< variable time-step CFL safety factors.
double C_cfl_cori;		///< variable time-step CFL safety factors for Coriolis term
double dt_tol_lo, dt_tol_hi;	///< variable time-step tolerance range.
double dt_max = 1e200;	///< maximum acceptable time-step.

struct OpSolver {
	int f, ic;		// component index
	#ifdef XS_MPI_ALL2ALL
	cplx* a2a_buffer;
	#endif
	virtual void solve(xs_array2d<cplx> x, int lmstart, int lmend, int lm_shift=0) const = 0;
	#ifdef XS_MPI
	virtual void solve_up(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const = 0;
	virtual void solve_dn(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const = 0;
	virtual void solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const = 0;
	virtual void solve_finish(Spectral& X, xs_array2d<cplx> x, int tag, MPI_Request **req, int lms_block, int nlm_block) const = 0;
	#endif
};

struct OpSet3 : public OpSolver {
	LOp3l* M;
	LOp3l_LU* Mlhs_1;
	LOp3l* Mrhs;
	virtual void solve(xs_array2d<cplx> x, int lmstart, int lmend, int lm_shift=0) const override {
		Mlhs_1->solve(x, lmstart, lmend, lm_shift);
	}
	#ifdef XS_MPI
	virtual void solve_up(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const override {
		Mlhs_1->solve_up(x, lmstart, lmend, tag, req, lms_block, nlm_block);
	}
	virtual void solve_dn(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const override {
		Mlhs_1->solve_dn(x, lmstart, lmend, tag, req, lms_block, nlm_block);
	}
	virtual void solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const override {
		Mlhs_1->solve_dn_halo(x, lmstart, lmend);
	}
	virtual void solve_finish(Spectral& X, xs_array2d<cplx> x, int tag, MPI_Request **req, int lms_block, int nlm_block) const override {
		Mlhs_1->solve_finish(X, x, tag, req, lms_block, nlm_block);
	}
	#endif
};

struct OpSet5 : public OpSolver {
	LOp5l *M;
	LOpLap5 *Lap;
	LOp5l_LU* Mlhs_1;
	LOp5l* Mrhs;
	virtual void solve(xs_array2d<cplx> x, int lmstart, int lmend, int lm_shift=0) const override {
		Mlhs_1->solve(x, lmstart, lmend, lm_shift);
	}
	#ifdef XS_MPI
	virtual void solve_up(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const override {
		Mlhs_1->solve_up(x, lmstart, lmend, tag, req, lms_block, nlm_block);
	}
	virtual void solve_dn(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const override {
		Mlhs_1->solve_dn(x, lmstart, lmend, tag, req, lms_block, nlm_block);
	}
	virtual void solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const override {
		Mlhs_1->solve_dn_halo(x, lmstart, lmend);
	}
	virtual void solve_finish(Spectral& X, xs_array2d<cplx> x, int tag, MPI_Request **req, int lms_block, int nlm_block) const override {
		Mlhs_1->solve_finish(X, x, tag, req, lms_block, nlm_block);
	}
	#endif
};

struct comp_rhs {
	int irs, ire;	// radial boundaries local to MPI process
	char f;			// field index
	char ic, ic_nl;	// component index
	signed char nl_sign;	// sign of the NL term ('char' may be signed or unsigned, so we need to specify 'signed char' here)
};

class LinSolver {
  protected:
	int nop3, nop5;
	OpSet5 op5[1];
	OpSet3 op3[5];
	int nop_solve;
	OpSolver* op_solve[MAXFIELD*2-2];
	std::vector<comp_rhs> comp_list;
  #ifdef XS_MPI
	int nblocks;
	std::vector<MPI_Request> req;		// needed for distributed, blocked solve over MPI.
	#ifdef XS_MPI_ALL2ALL
	long chunk_a2a;		// chunk size for all2all solver
	#endif
  #endif
    int nim, nex;		// number of linear and non-linear vectors to hold
    double CM_rhs;		// coefficient in front of linear operators (rhs)
    double CM_lhs;		// coefficient to apply to linear operators (lhs)
    double Cdt_rhs;
    double Cdt_lhs;

	int set_block_nbr_linsolve(int nb);		// returns the actual number of blocks
	double time_linear_solver(int nblk, int nloops, int clear_cache=0);

	void apply_M(StateVector& Y, const StateVector& X);
	void apply_Mrhs(StateVector& Y, const StateVector& X);
	void calc_matrices_parallel(double dt, double CM_lhs);

public:
	void init(const StateVector& Vlm);
	void calc_matrices(double dt, const int timeit=1);
	void optimize_linear_solver();
	void solve_state(StateVector& Xrhs, int kill_sbr);
};

class Stepper : public LinSolver {
  public:
	double dt;			///< time step
	int nex_eval;		///< number of evaluations of explicit terms per time step.
	bool dt_adjust;		///< variable time-step, turned off by default.
  protected:
	int i_nl = 0;		///< index of current explicit term evaluation; used in step_1_start_explicit_terms_cfl()
  public:

	Stepper();
	void step_1_start_explicit_terms_cfl() {
		explicit_terms(*NL[i_nl], Xlm, 1);		// NL(t)
	}
	virtual void step_2_adjust_dt(double delta_t, int& istep);
	virtual void step_3_end(int kill_sbr) = 0;

	void set_default_cfl(double& var, const double c) {  if (var == 0.) var = c;  }	// do not override user-defined values !
	void set_default_cfl_u_b_cori(double c_u, double c_b, double c_cori, double c_vort=1000.) {
		#ifdef XS_SPARSE
			c_cori = 10;	// Implicit Coriolis
		#endif
		set_default_cfl(C_cfl[cfl_ur], c_u);		set_default_cfl(C_cfl[cfl_uh], c_u);
		set_default_cfl(C_cfl[cfl_br], c_b);		set_default_cfl(C_cfl[cfl_bh], c_b);
		set_default_cfl(C_cfl_cori, c_cori);
		set_default_cfl(C_cfl[cfl_w], c_vort);		set_default_cfl(C_cfl[cfl_j], c_vort);
	}
};

Stepper::Stepper() : nex_eval(0) {
	double c = 0.0;
	// initialize the CFL safety factors to default values (0) so that it can be set subsequently by specialized constructors.
	for (int k=0; k<MAX_CFL; k++) C_cfl[k] = c;
	// set user-defined values from xshells.par
	c = mp.var["C_u"];		C_cfl[cfl_ur] = c;	C_cfl[cfl_uh] = c;
	c = mp.var["C_alfv"];	C_cfl[cfl_br] = c;	C_cfl[cfl_bh] = c;
	c = mp.var["C_vort"];	C_cfl[cfl_w] = c;	C_cfl[cfl_j] = c;
	c = mp.var["C_cori"];	C_cfl_cori = c;
	// tolerance for time-step change:
	dt_tol_lo = 0.9;		dt_tol_hi = 1.05;
	#ifdef XS_SPARSE
	dt_tol_lo = 0.8;		dt_tol_hi = 1.1;		// with XS_SPARSE: use larger default tolerance to avoid too many matrix recomputations.
	#endif
	c = mp.var["dt_tol_lo"];	if (c > 0.) dt_tol_lo = c;
	c = mp.var["dt_tol_hi"];	if (c > 0.) dt_tol_hi = c;
	#ifdef XS_LINEAR
		// resolve diffusive decay rates accurately (based on SBDF2's value):
		dt_max = 3e-3 * get_diffusion_time();	// decay rate at 3e-4 relative precision
		//dt_max = 1.3e-3 * get_diffusion_time();	// decay rate at less than 1e-4 precision
	#endif
}

Stepper *stepper;

//#define LM_LOOP( action ) for (lm=0; lm<NLM; lm++) { action }
#ifdef VAR_LTR
  #define LTR ltr[ir]
#endif

void LinSolver::calc_matrices_parallel(double dt, double CM_lhs)
{
//	const double CM_lhs = -jpar.pc2_ai/(1.0 - jpar.pc2_ai);		// default for true PC2: ci = -1
	const double dt_1 = 1.0/dt;

//	printf("Mrhs = %g/dt + %g*M\n", Cdt_rhs, CM_rhs);
//	printf("Mlhs = %g/dt + %g*M\n", Cdt_lhs, CM_lhs);

	#ifdef XS_SPARSE
	//printf("banded LU: Cdt=%g, CM=%g   RHS: Cdt=%g, CM=%g\n",Cdt_lhs*dt_1, CM_lhs, Cdt_rhs*dt_1, CM_rhs);
	xs_sparse::prepare_solve(Cdt_lhs*dt_1, CM_lhs, Cdt_rhs*dt_1, CM_rhs);		// TODO check that CM_lhs is the correct coeff here.
	#endif
	#pragma omp for schedule(dynamic,1) nowait
	for (int op=0; op<nop5; op++) {
		if (op5[op].Lap) {
			op5[op].Mrhs->set_op(-Cdt_rhs*dt_1, *op5[op].Lap, -CM_rhs, *op5[op].M);
			op5[op].Mlhs_1->set_op(-Cdt_lhs*dt_1, *op5[op].Lap, -CM_lhs, *op5[op].M);
		} else {
			op5[op].Mrhs->set_op(Cdt_rhs*dt_1, CM_rhs, *op5[op].M);
			op5[op].Mlhs_1->set_op(Cdt_lhs*dt_1, CM_lhs, *op5[op].M);
		}
		op5[op].Mlhs_1->precalc_solve();
	}
	#pragma omp for schedule(dynamic,1) nowait
	for (int op=0; op<nop3; op++) {
		op3[op].Mrhs->set_op(Cdt_rhs*dt_1, CM_rhs, *op3[op].M);
		op3[op].Mlhs_1->set_op(Cdt_lhs*dt_1, CM_lhs, *op3[op].M);
		op3[op].Mlhs_1->precalc_solve();
	}
}

void LinSolver::calc_matrices(double dt, const int timeit)
{
	#ifdef XS_DEBUG
	double time1, time2;
	if (timeit)	time1 = xs_wtime();
	#endif

	#pragma omp parallel
	{
		calc_matrices_parallel(dt, CM_lhs);
	}

	#ifdef XS_DEBUG
	if (timeit) {
		time2 = xs_wtime();
		PRINTF0("	    [matrix recomputed in %gs]\n", time2-time1);
	}
	#endif
}


/*
void calc_Matrices(double dt)
{
	const double ci = -jpar.pc2_ai/(1.0 - jpar.pc2_ai);		// default for true PC2: ci = -1
	const double dt_1 = 1.0/dt;

	#pragma omp parallel
	{
		#ifdef XS_SPARSE
		xs_sparse::prepare_solve(dt_1, jpar.pc2_ai);
		#else
		if (evol_ubt & EVOL_U) {
			#pragma omp single nowait
			{
				// (d/dt - NU.Lap)(-Lap.Up) = Toroidal[curl_NL]
				MUp.set_op(-dt_1, MU_Lr, -1.0, MU_LLr);
				MUp_1.set_op(-dt_1, MU_Lr, -ci, MU_LLr);
				MUp_1.precalc_solve();
			}
			#pragma omp single nowait
			{
				// (d/dt - NU.Lap) Ut = Poloidal(curl_NL)
				MUt_1.set_op(dt_1, ci, MUt);
				MUt_1.precalc_solve();
			}
		}
		if (evol_ubt & EVOL_T) {
			#pragma omp single nowait
			{
				MT_1.set_op(dt_1, ci, MT);
				MT_1.precalc_solve();
			}
			if (evol_ubt & EVOL_C) {
				#pragma omp single nowait
				{
					MC_1.set_op(dt_1, ci, MC);
					MC_1.precalc_solve();
				}
			}
		}
		#endif
		if (evol_ubt & EVOL_B) {
			#pragma omp single nowait
			{
				MBp_1.set_op(dt_1, ci, MBp);
				MBp_1.precalc_solve();
			}
			#pragma omp single nowait
			{
				MBt_1.set_op(dt_1, ci, MBt);
				MBt_1.precalc_solve();
			}
		}
	}
}
*/

void LinSolver::init(const StateVector& Vlm)
{
	comp_rhs cmp;
	cmp.nl_sign = 1;

	// Allocate vectors:
	for (int i=0; i<nim; i++) {
		Lin[i] = new StateVector;
		Lin[i]->clone(Vlm);
	}
	for (int i=0; i<nex; i++) {
		NL[i] = new StateVector;
		NL[i]->clone(Vlm);
		NL[i]->zero_out();
	}

	// prepare component list:
	comp_list.clear();
	nop3 = 0;		nop5 = 0;
	if (evol_ubt & EVOL_U) {
		cmp.f = ::U;
		cmp.ic = 0;			cmp.ic_nl = 1;		// Pol and Tor are exchanged in NL.
		cmp.irs = NG+1;		cmp.ire = NM-1;
		mpi_interval(cmp.irs, cmp.ire);
		comp_list.push_back(cmp);

		cmp.ic = 1;				cmp.ic_nl = 0;		// Pol and Tor are exchanged in NL.
		cmp.irs = MUt.irs;		cmp.ire = MUt.ire;
		comp_list.push_back(cmp);

	  #ifndef XS_SPARSE
		op5[nop5].Lap = &MU_Lr;
		op5[nop5].M = &MUp;		op5[nop5].f = ::U;		op5[nop5].ic = 0;
		op3[nop3].M = &MUt;		op3[nop3].f = ::U;		op3[nop3].ic = 1;
		nop5++;		nop3++;
	  #endif
	}
	if (evol_ubt & EVOL_B) {
		cmp.f = ::B;
		cmp.ic = 2;				cmp.ic_nl = 2;
		cmp.irs = MBp.irs;		cmp.ire = MBp.ire;
		comp_list.push_back(cmp);

		cmp.ic = 3;				cmp.ic_nl = 3;
		cmp.irs = MBt.irs;		cmp.ire = MBt.ire;
		comp_list.push_back(cmp);

		op3[nop3].M = &MBp;		op3[nop3].f = ::B;		op3[nop3].ic = 2;
		op3[nop3+1].M = &MBt;	op3[nop3+1].f = ::B;	op3[nop3+1].ic = 3;
		nop3 += 2;
	}
	if (evol_ubt & EVOL_T) {
		cmp.f = ::T;
		cmp.ic = 4;				cmp.ic_nl = 4;
		cmp.irs = MT.irs;		cmp.ire = MT.ire;
		cmp.nl_sign = -1;		// negative sign for the non-linear part.
		comp_list.push_back(cmp);
		// note that NL and NLo are defined as u.grad(T), that's why there is a minus in front of NL.
	  #ifndef XS_SPARSE
		op3[nop3].M = &MT;		op3[nop3].f = ::T;		op3[nop3].ic = 4;
		nop3++;
	  #endif
	}
	if (evol_ubt & EVOL_C) {
		cmp.f = ::C;
		cmp.ic = 5;				cmp.ic_nl = 5;
		cmp.irs = MC.irs;		cmp.ire = MC.ire;
		cmp.nl_sign = -1;		// negative sign for the non-linear part.
		comp_list.push_back(cmp);
		// note that NL and NLo are defined as u.grad(C), that's why there is a minus in front of NL.
	  #ifndef XS_SPARSE
		op3[nop3].M = &MC;		op3[nop3].f = ::C;		op3[nop3].ic = 5;
		nop3++;
	  #endif
	}

	nop_solve = nop5+nop3;	// total number of fields to solve
	int iop_solve_reg = 0;		// index of solve in regular direction
	int bidir_split = 0;
	#ifdef XS_MPI
	// Bidirectional solve logic:
	bidir_split = mp.var["bidir_solve"];	// bidir_solve user control. 0=default, 1=bidir V-solve (with shared_mem > 1), 2=X-solve (whith n_mpi_net > 1), <0 disabled
	if ((n_mpi_net > 1 && n_mpi_shared > 1) && (bidir_split == 1  ||  bidir_split == 0)) {
		bidir_split = -1;		// enable bidirectional V-solver
		PRINTF0(COLOR_WRN "using bidirectional V-solver" COLOR_END "\n");
	} else if ((n_mpi_net > 1) && (bidir_split == 2  ||  bidir_split == 0)) {	// X-solver
		int ir_middle = 0;
		while (r_owner[ir_middle]/n_mpi_shared < n_mpi_net/2) ir_middle ++;		// find the middle radius, at interface between processes
		if ((r_owner[ir_middle]/n_mpi_shared == r_owner[ir_middle+1]/n_mpi_shared)
				&& (r_owner[ir_middle-1]/n_mpi_shared == r_owner[ir_middle-2]/n_mpi_shared))	{	// check that LU5l can work (requires at least 2 shells owned at interface).
			bidir_split = ir_middle;
			PRINTF0(COLOR_WRN "using bidirectional X-solver (split solver at ir=%d)" COLOR_END "\n", ir_middle);
		} else if (bidir_split != 0) runerr("X-solver requires two shells at the interface process.");
	} else bidir_split = 0;		// disable bidirectional solve
	#ifdef XS_MPI_ALL2ALL
		const int cl = CACHE_LINE/sizeof(cplx);
		chunk_a2a = ((((NLM+cl-1)/cl)+n_mpi-1)/n_mpi) * cl;		// each process gets part of the harmonic coefficients, of size chunk (cache-line aligned).
		if (chunk_a2a * (n_mpi-1) >= NLM) runerr("Not enough work to share. Reduce number of MPI processes.");
	#endif
	#endif
	// allocate matrices
	for (int op=0; op<nop5; op++) {
		LOp5l *M = op5[op].M;
		op5[op].Mrhs = new LOp5l;
		op5[op].Mrhs->alloc(M->irs, M->ire, LMAX);		// local shells only
		op5[op].Mlhs_1 = new LOp5l_LU;
		op5[op].Mlhs_1->alloc(M->ir_bci, M->ir_bco, LMAX);		// all shells
		op5[op].Mlhs_1->init_solve(Xlm[op5[op].f], bidir_split);		// set MPI communication logic.
		#ifdef XS_MPI_ALL2ALL
		op5[op].a2a_buffer = (cplx*) VMALLOC(sizeof(cplx) * chunk_a2a * (NR+4) * 2);
		memset(op5[op].a2a_buffer, 0, sizeof(cplx) * chunk_a2a * (NR+4) * 2);
		#endif
		print_debug("i_mpi=%d :: op5 > op = %d, solve_dir=%d\n", i_mpi, iop_solve_reg, op5[op].Mlhs_1->get_solve_direction() );
		op_solve[iop_solve_reg++] = &op5[op];
	}
	for (int op=0; op<nop3; op++) {
		LOp3l *M = op3[op].M;
		op3[op].Mrhs = new LOp3l;
		op3[op].Mrhs->alloc(M->irs, M->ire, LMAX);		// all shells
		op3[op].Mrhs->flags = M->flags;	// propagate flags
		op3[op].Mrhs->mx_idx = M->mx_idx;	// propagate mx_idx
		op3[op].Mlhs_1 = new LOp3l_LU;
		op3[op].Mlhs_1->alloc(M->ir_bci, M->ir_bco, LMAX);			// all shells
		op3[op].Mlhs_1->flags = M->flags;	// propagate flags
		op3[op].Mlhs_1->mx_idx = M->mx_idx;	// propagate mx_idx
		op3[op].Mlhs_1->init_solve(Xlm[op3[op].f], bidir_split);			// set MPI communication logic.
		#ifdef XS_MPI_ALL2ALL
		op3[op].a2a_buffer = (cplx*) VMALLOC(sizeof(cplx) * chunk_a2a * (NR+4) * 2);
		memset(op3[op].a2a_buffer, 0, sizeof(cplx) * chunk_a2a * (NR+4) * 2);
		#endif
		print_debug("i_mpi=%d :: op3 > op = %d, solve_dir=%d\n", i_mpi, iop_solve_reg, op3[op].Mlhs_1->get_solve_direction() );
		op_solve[iop_solve_reg++] = &op3[op];
	}
}

double adjust_dt(const double dt, const double delta_t, int& istep, const bool dt_adjust)
{
	double dt_new = dt;
	double dt_target = (dt_max < delta_t) ? dt_max : delta_t;		// time-step cannot exceed the total time-span between outputs delta_t !

	#ifdef XS_MPI
		MPI_Allreduce( MPI_IN_PLACE, dt_cfl, MAX_CFL, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD );
	#endif
	double dt_cori = fabs(C_cfl_cori/frame.Omega_z);
	#ifndef XS_SPARSE
	if (dt_cori < dt_target) dt_target = dt_cori;
	#endif
	for (int k=0; k<MAX_CFL; k++) {
		dt_cfl[k] = C_cfl[k]/sqrt(dt_cfl[k]);
		if ((dt_cfl[k] < dt_target) && (k < cfl_w)) dt_target = dt_cfl[k];		// use only ur,uh, br,bh
	}

	if ((dt_adjust) && ((dt < dt_target*dt_tol_lo) || (dt > dt_target*dt_tol_hi))) {
		dt_new = dt_target;
	}

	istep = ceil(delta_t/dt_new - 1./64.);	// this is my remaing steps... (1/64 prevents some unwanted changes du to finite precision).
	dt_new = delta_t/istep;					// ... with my new time-step.
	if (fabs(dt-dt_new) > 1e-12*(dt+dt_new)) {
		PRINTF0("	=> new time-step dt=%g  (dt_target=%g) (remaining substeps=%d)  [istep=ceil(%.15g)]\n", dt_new, dt_target, istep, delta_t/dt_target);
		if UNLIKELY(dt_new < 0.0) runerr("ooops !");
	}

	#ifdef XS_DEBUG
	if (istep == 1)
		PRINTF0("    dt=%g, dt_target=%g (dt_cori=%g, dt_ur=%g, dt_uh=%g, dt_br=%g, dt_bh=%g, dt_w=%g, dt_j=%g)\n",dt_new, dt_target, 
			dt_cori, dt_cfl[cfl_ur], dt_cfl[cfl_uh], dt_cfl[cfl_br], dt_cfl[cfl_bh], dt_cfl[cfl_w], dt_cfl[cfl_j]);
	#endif
	return dt_new;
}

void compute_local_cfl(int ir, double* vcfl)
{
	double dr_2 = 1.0/Ulm.delta_r(ir);
	#ifdef VAR_LTR
	double dh_2 = ltr[ir];
	dh_2 = (r[ir] == 0.0) ? dh_2/(M_PI*r[ir+1]) : dh_2/(M_PI*r[ir]);
	#else
	double dh_2 = ((double) LMAX)/(M_PI*r[ir]);
	#endif
	dr_2 *= dr_2;		dh_2 *= dh_2;
	vcfl[cfl_ur] *= dr_2;		vcfl[cfl_uh] *= dh_2;		// standard CFL
	if (((evol_ubt & (EVOL_B | EVOL_U)) == (EVOL_B | EVOL_U)) && (spat_need & NEED_J)) {		// if u and b are coupled:
		// we must resolve the shortest period of Alfven waves (see also Christensen et. al GJI 1999, section 2.2)
		#ifdef XS_ETA_VAR
		double eta_nu = (etar[ir]+jpar.nu)*0.5;
		#else
		double eta_nu = (jpar.eta+jpar.nu)*0.5;
		#endif
		double dx_2 = (vcfl[cfl_br] + vcfl[cfl_bh])/(eta_nu*eta_nu);		// no Alfven waves below this length.
		if (dx_2 < dr_2) dr_2 = dx_2;
		if (dx_2 < dh_2) dh_2 = dx_2;
		vcfl[cfl_br] *= dr_2;		vcfl[cfl_bh] *= dh_2;
	}
	int flag = 0;
	#pragma omp flush(dt_cfl)
	for (int k=0; k<MAX_CFL; k++) {
		if (vcfl[k] > dt_cfl[k]) flag = 1;
	}
	if (flag) {		// enter critical region only if we are actually updating the value of dt_cfl.
		#pragma omp critical
		{
			for (int k=0; k<MAX_CFL; k++) {
				if (vcfl[k] > dt_cfl[k]) dt_cfl[k] = vcfl[k];		// NaN should be eliminated here
			}
		}
	}
}

void Stepper::step_2_adjust_dt(double delta_t, int& istep)
{
	double dto = dt;
	dt = adjust_dt(dt, delta_t, istep, dt_adjust);		// adjust time-step and istep
	if (differ(dto,dt)) calc_matrices(dt);
}


/// Update the imposed potential field associated to a solid body.
void update_BC_Mag(SolidBody *SB, cplx *P)
{
	cplx* P0 = SB->PB0lm;
	if (P0 != NULL) {
		if ((SB->Omega_x != 0.0)||(SB->Omega_y != 0.0)) runerr("Equatorial rotation not allowed with imposed potential field.");
		//for (int l=0; l <= SB->lmax; l++)	P[l] = P0[l];	// m=0 does not change.
		P0 += SB->lmax +1;
		for (int im=1; im <= SB->mmax; im++) {
			cplx a = cplx( cos(im*MRES * SB->phi0) , -sin(im*MRES * SB->phi0) );
			for (int l=im*MRES; l<=SB->lmax; l++)
				P[LiM(shtns,l,im)] = (*P0++) * a;
				// the externally imposed field has been multiplied by 2*(l+1) in load_init().
		}
	}
}

/// Update the velocity field at the boundary of a solid body.
inline void update_BC_U(SolidBody *SB, cplx *T, double rr)
{
	if (SB->freely_rotate) return;	// don't update if the solid evolves according to moment of inertia!
	if ((MRES==1)&&(MMAX>0)) T[LiM(shtns,1,1)] = Y11_st * rr * cplx(SB->Omega_x, - SB->Omega_y);
	T[LiM(shtns,1,0)] = Y10_ct * rr * SB->Omega_z;
}

/// Computes the velocity of solid boundaries, and set boundary conditions of fluid if required.
void calc_U_solid(double ftime, double dt)
{
	double om0i = InnerCore.Omega_z;	double om0o = Mantle.Omega_z;		// save axial rotation at time t.
	#ifdef U_FORCING
		InnerCore.invalidate_spatial();		// mark the spatial field as invalid (must be recomputed if needed)
		Mantle.invalidate_spatial();		//
		calc_Uforcing(ftime, jpar.a_forcing, jpar.w_forcing, &InnerCore, &Mantle);	// update axial rotation (time t+dt).
		if ((Ulm.bci == BC_NO_SLIP)&&(own(NG) || own(NG+1)))	update_BC_U(&InnerCore, Ulm.Tor[NG], r[NG]);
		if ((Ulm.bco == BC_NO_SLIP)&&(own(NM) || own(NM-1)))	update_BC_U(&Mantle, Ulm.Tor[NM], r[NM]);
	#endif

	om0i += InnerCore.Omega_z;		om0o += Mantle.Omega_z;
	InnerCore.phi0 += (0.5*dt) * om0i;		// follow angular position (phi, trapeze integration of OmegaZ)
	Mantle.phi0    += (0.5*dt) * om0o;

	if (evol_ubt & EVOL_B) {
		if ((om0i != 0.0)&&(own(Blm.ir_bci)))	update_BC_Mag(&InnerCore, Blm.Pol[Blm.ir_bci-1]);	// the inner-core has rotated, update magnetic BC
		if ((om0o != 0.0)&&(own(Blm.ir_bco)))	update_BC_Mag(&Mantle, Blm.Pol[Blm.ir_bco+1]);		// the mantle has rotated, update magnetic BC
		#ifdef XS_B_FORCING
		calc_Bforcing(ftime, jpar.a_forcing, jpar.w_forcing);
		#endif
	}
}


void vec_cpy(cplx* x, cplx*x0, int lms, int lme)
{
	memcpy(x+lms, x0+lms, (lme-lms+1)*sizeof(cplx));
}

void vec_sum(cplx* x, cplx* x0, cplx* a, cplx* b,
			const double alpha, const double beta,
			int lms, int lme)
{
	for (size_t k=lms; k<=lme; k++)
		((v2d*)x)[k] = ((v2d*)x0)[k] + vdup(alpha)*((v2d*)a)[k] + vdup(beta)*((v2d*)b)[k];
}

/*
void vec_sum(cplx* x, cplx* x0, cplx* a, cplx* b, cplx* c, 
			const double alpha, const double beta, const double gamma,
			int lms, int lme)
{
	if (gamma == 0.0) {
		vec_sum(x, x0,a,b, alpha, beta, lms, lme);
		return;
	}
	for (size_t k=lms; k<=lme; k++)
		((v2d*)x)[k] = ((v2d*)x0)[k] + vdup(alpha)*((v2d*)a)[k] + vdup(beta)*((v2d*)b)[k]
						+ vdup(gamma)*((v2d*)c)[k];
}

void vec_sum(cplx* x, cplx* x0, cplx* a, cplx* b, cplx* c, cplx* d,
			const double alpha, const double beta, const double gamma, const double delta,
			int lms, int lme)
{
	if (delta == 0.0) {
		vec_sum(x, x0,a,b,c, alpha, beta, gamma, lms, lme);
		return;
	}
	for (size_t k=lms; k<=lme; k++)
		((v2d*)x)[k] = ((v2d*)x0)[k] + vdup(alpha)*((v2d*)a)[k] + vdup(beta)*((v2d*)b)[k]
						+ vdup(gamma)*((v2d*)c)[k] + vdup(delta)*((v2d*)d)[k];
}
*/

// TODO: for XS_SPARSE, Xlin.U and Xlin.T are zero, and some operations could be saved here.
void build_rhs2(StateVector& Xrhs, StateVector& Xlin, StateVector& Xnl0, StateVector& Xnl1,
	const double alpha, const double beta)
{
	int i0, i1, lm0, lm1;

	lm0 = 0;		lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	if (evol_ubt & EVOL_U) {
		i0 = MUt.irs;	i1 = MUt.ire;
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum(Xrhs.U.Tor[ir], Xlin.U.Tor[ir], 
					Xnl0.U.Pol[ir], Xnl1.U.Pol[ir],
					alpha, beta, lm0, lm1);		// Pol and Tor are exchanged in NL.
		}
		i0 = NG+1;		i1 = NM-1;
		mpi_interval(i0, i1);
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum(Xrhs.U.Pol[ir], Xlin.U.Pol[ir], 
					Xnl0.U.Tor[ir], Xnl1.U.Tor[ir],
					alpha, beta, lm0, lm1);		// Pol and Tor are exchanged in NL.
		}
	}
	if (evol_ubt & EVOL_B) {
		/* POLOIDAL */
		i0 = MBp.irs;		i1 = MBp.ire;
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum(Xrhs.B.Pol[ir], Xlin.B.Pol[ir],
				Xnl0.B.Pol[ir], Xnl1.B.Pol[ir],
				alpha, beta, lm0, lm1);
		}

		/* TOROIDAL */
		i0 = MBt.irs;		i1 = MBt.ire;
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum(Xrhs.B.Tor[ir], Xlin.B.Tor[ir],
				Xnl0.B.Tor[ir], Xnl1.B.Tor[ir],
				alpha, beta, lm0, lm1);
		}
	}
	if (evol_ubt & EVOL_T) {
		i0 = MT.irs;		i1 = MT.ire;
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum(Xrhs.T.Sca[ir], Xlin.T.Sca[ir],
				Xnl0.T.Sca[ir], Xnl1.T.Sca[ir],
				-alpha, -beta, lm0, lm1);
				// note that NL and NLo are defined as u.grad(T), that's why there is a minus in front of alpha and beta above.
		}
		if (evol_ubt & EVOL_C) {
			i0 = MC.irs;		i1 = MC.ire;
			thread_interval_rad(i0, i1);
			for (int ir=i0; ir<=i1; ir++) {
				vec_sum(Xrhs.C.Sca[ir], Xlin.C.Sca[ir],
					Xnl0.C.Sca[ir], Xnl1.C.Sca[ir],
					-alpha, -beta, lm0, lm1);
					// note that NL and NLo are defined as u.grad(T), that's why there is a minus in front of alpha and beta above.
			}
		}
	}
}

/// this will also advance time step by 1.
void update_all_BC(double dt)
{
	#pragma omp barrier
	#pragma omp master
	{
		#ifdef XS_MPI
			if (n_mpi_shared > 1) MPI_Barrier(comm_shared);
		#endif
		ftime += dt;			// update time
		frame.update(ftime);	// update rotation rates
		calc_U_solid(ftime, dt);	// update boundary conditions (for U and B)
		#ifdef XS_MPI
			if (n_mpi_shared > 1) MPI_Barrier(comm_shared);
		#endif
	}
	#pragma omp barrier
}


/// transform the global interval ir0 and ir1 (inclusive) into an interval assigned to a thread.
inline void thread_interval_solve(int& i0, int& i1, const int ith, const int nth, const int chunk=4)		// 2 divs
{
	int n = (i1 - i0 + 1 + (chunk-1))/chunk;		// how many chunks are there?
	int i1new = i0-1 + (((ith+1)*n)/nth)*chunk;
	i0 += ((ith*n)/nth)*chunk;
	if LIKELY(i1new < i1) i1 = i1new;
//	if UNLIKELY(i1 < i0) runerr("[thread_interval_solve] not enough work to share.");
}


#ifdef XS_MPI_ALL2ALL

inline
void my_memcpy(cplx* dst, cplx* src, size_t sze) {
    sze /= sizeof(cplx);
    #pragma omp simd
    for (long i=0; i<sze*2; i++) {
        ((double*)dst)[i] = ((double*)src)[i];
    }
}

void transpose_shells_to_lm_chunk_init(cplx* buf, xs_array2d<cplx> x, long nr_loc, long chunk)
{
	#pragma omp for nowait
	for (int j=0; j<n_mpi_net; j++) {
		long ofs =  (j + i_mpi_shared*n_mpi_net)*chunk;
		long sze = chunk;
		if (sze + ofs > NLM) sze = NLM-ofs;		// handle the edge case
		if (sze < 0) sze = 0;
		for (int i=0; i<nr_loc; i++) {
			my_memcpy(buf + i*chunk + j*nr_loc*chunk,  x[i+irs_shared] + ofs, sizeof(cplx)*sze);
		}
	}
	cplx* dst = buf + chunk*(NR+5);
	//MPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, MPI_Comm)
	//	MPI_Send(sendbuf+i*sendcount*sizeof(sendtype), sendcount,sendtype, i, ...),
	//	MPI_Recv(recvbuf+i*recvcount*sizeof(recvtype), recvcount,recvtype, i, ...).
	#pragma omp barrier
	#pragma omp master
	MPI_Alltoall(buf, 2*chunk*nr_loc, MPI_DOUBLE, dst+chunk, 2*chunk*nr_loc, MPI_DOUBLE, comm_net);
}

xs_array2d<cplx> transpose_shells_to_lm_chunk_wait(cplx* buf, long chunk)
{
	xs_array2d<cplx> y;
	buf += chunk*(NR+5);
	y.init_from_buffer(buf+chunk, chunk);
	return y;
}

void transpose_lm_chunk_to_shells_init(cplx* buf, xs_array2d<cplx> y, long nr_loc, long chunk)
{
	#pragma omp barrier
	#pragma omp master
	MPI_Alltoall(y[0], 2*chunk*nr_loc, MPI_DOUBLE, buf, 2*chunk*nr_loc, MPI_DOUBLE, comm_net);
}

void transpose_lm_chunk_to_shells_finish(cplx* buf, xs_array2d<cplx> x, long nr_loc, long chunk)
{
	#pragma omp for nowait
	for (int j=0; j<n_mpi_net; j++) {
		long ofs =  (j + i_mpi_shared*n_mpi_net)*chunk;
		long sze = chunk;
		if (sze + ofs > NLM) sze = NLM-ofs;		// handle the edge case
		if (sze < 0) sze = 0;
		for (int i=0; i<nr_loc; i++) {
			my_memcpy(x[i+irs_shared] + ofs, buf + i*chunk + j*nr_loc*chunk, sizeof(cplx)*sze);
		}
	}
}
#endif

/// solve in-place left-hand side of equation (semi-implicit at t+dt).
/// - right-hand side has to be computed with rhs_U, rhs_B, and rhs_T
/// - boundary conditions should have been updated to t+dt with update_all_BC()
/// - kill_sbr: controls solid-body rotation mode (0: do nothing, 1: keep to 0, 2: keep to initial value)
void LinSolver::solve_state(StateVector& Xrhs, int kill_sbr)
{
#ifndef XS_MPI
	int lms = 0;		int lme = NLM -1;
	thread_interval_any(lms, lme);
	#pragma omp barrier
	const int nop = nop_solve;
	for (int op=0; op<nop; op++) {
		int ic = op_solve[op]->ic;
		const xs_array2d<cplx>& x = Xrhs.get_comp(ic);
		op_solve[op]->solve(x, lms, lme);
	}
  #ifndef XS_SPARSE
	if (kill_sbr) {
		#pragma omp barrier
		#pragma omp master
		conserve_momentum(Xrhs.U, kill_sbr);
	}
  #else
	xs_sparse::solve_state(Xrhs, kill_sbr);
  #endif
	#pragma omp barrier
#else

	#ifdef XS_MPI_ALL2ALL

	const long chunk = chunk_a2a;		// each process gets part of the harmonic coefficients, of size chunk.
	const int nr_loc = ire_shared-irs_shared+1;		// number of shells accessible by each process
	/// WARNING! nr_loc must be the same for each process.

	// distribution of lm accross processes depends on transpose_shells_to_lm_chunk_init()
	int lms_mpi = (i_mpi_net + i_mpi_shared*n_mpi_net)*chunk;
	int lme_mpi = lms_mpi + (chunk-1);
	if (lme_mpi >= NLM) lme_mpi = NLM-1;

	int lms = lms_mpi;
	int lme = lme_mpi;
	thread_interval_any(lms, lme);

	barrier_shared_mem();

	const int nop = nop_solve;
	for (int op=0; op<nop; op++) {		// start pipeline
		int ic = op_solve[op]->ic;
		const xs_array2d<cplx>& x = Xrhs.get_comp(ic);
		cplx* buf = op_solve[op]->a2a_buffer;
		transpose_shells_to_lm_chunk_init(buf, x, nr_loc, chunk);
	}
	if UNLIKELY(nop <= 1) {
		#pragma omp barrier
	}
	for (int op=0; op<nop; op++) {		// solve
		cplx* buf = op_solve[op]->a2a_buffer;
		xs_array2d<cplx> y = transpose_shells_to_lm_chunk_wait(buf, chunk);
		if (lms <= lme) op_solve[op]->solve(y, lms, lme, lms_mpi);
		transpose_lm_chunk_to_shells_init(buf, y, nr_loc, chunk);
	}
	for (int op=0; op<nop; op++) {		// finish
		if (op == nop-1) {	// ensure the last MPI_Alltoall is over
			#pragma omp barrier
		}
		int ic = op_solve[op]->ic;
		const xs_array2d<cplx>& x = Xrhs.get_comp(ic);
		cplx* buf = op_solve[op]->a2a_buffer;
		transpose_lm_chunk_to_shells_finish(buf, x, nr_loc, chunk);
	}

	barrier_shared_mem();
	#pragma omp master
	{
		Blm.sync_mpi();			// sync ghost shells
		Ulm.sync_mpi();
		Tlm.sync_mpi();
	}
	barrier_shared_mem();

	#else

	MPI_Request *req_ptr = &req[0];		// pointer to vector start.
  #if XS_OMP > 0
	const int nth = omp_get_num_threads();		// number of threads
	const int ith = omp_get_thread_num();		// thread id.
  #else
	const int nth = 1;		// number of threads
	const int ith = 0;		// thread id.
  #endif
	const int tag_ic = 8;
	
	int lms_mpi = 0;
	int lme_mpi = NLM-1;
	thread_interval_solve(lms_mpi, lme_mpi, i_mpi_shared, n_mpi_shared);	// split work between process sharing memory

	// The following barriers are NECESSARY.
	barrier_shared_mem();
	const int nop = nop_solve;

	
	if (ith==0) print_debug("*** solve_up ...\n");
	// BLOCKED UP SOLVE
	{
		const int tag=0;			// tag counter
		for (int op=0; op<nop; op++) {
			const int ic = op_solve[op]->ic;
			const xs_array2d<cplx>& x = Xrhs.get_comp(ic);
			for (int k=0; k<nblocks; k++) {
				int lms_block = lms_mpi;		int lme_block = lme_mpi;
				thread_interval_solve(lms_block, lme_block, k, nblocks);
				int lms = lms_block;		int lme = lme_block;
				thread_interval_solve(lms, lme, ith, nth);
				int nlm_block = lme_block - lms_block + 1;

				op_solve[op]->solve_up(x, lms, lme, tag + (ic + k*MAXFIELD*2)*tag_ic, &req_ptr, lms_block, nlm_block);
			}
		}
	}

	// this barrier is necessary for the X-solver at the interface to ensure all threads have finished their work before exchanging the data.
	#pragma omp barrier

	if (ith==0) print_debug("*** solve_dn ...\n");
	// BLOCKED DOWN SOLVE
	{
		const int tag=0;			// tag counter
		//for (int op=0; op<nop; op++) {
		for (int op=nop-1; op>=0; op--) {
			const int ic = op_solve[op]->ic;
			const xs_array2d<cplx>& x = Xrhs.get_comp(ic);
			for (int k=0; k<nblocks; k++) {
			//for (int k=nblocks-1; k>=0; k--) {
				int lms_block = lms_mpi;		int lme_block = lme_mpi;
				thread_interval_solve(lms_block, lme_block, k, nblocks);
				int lms = lms_block;		int lme = lme_block;
				thread_interval_solve(lms, lme, ith, nth);
				int nlm_block = lme_block - lms_block + 1;

				op_solve[op]->solve_dn(x, lms, lme, tag + (ic + k*MAXFIELD*2)*tag_ic, &req_ptr, lms_block, nlm_block);
			}
		}
	}

	// BLOCKED DOWN SOLVE FOR HALO (no MPI communication here, allows to reduce the number of communication in the SYNC HALO step)
	{
		for (int op=0; op<nop; op++) {
			const int ic = op_solve[op]->ic;
			const xs_array2d<cplx>& x = Xrhs.get_comp(ic);
			for (int k=0; k<nblocks; k++) {
				int lms_block = lms_mpi;		int lme_block = lme_mpi;
				thread_interval_solve(lms_block, lme_block, k, nblocks);
				int lms = lms_block;		int lme = lme_block;
				thread_interval_solve(lms, lme, ith, nth);

				op_solve[op]->solve_dn_halo(x, lms, lme);
			}
		}
	}

	#pragma omp barrier
	#pragma omp master
	{	// SYNC HALOS
		print_debug("*** sync halos ...\n");

		const int tag = 4;
		for (int op=0; op<nop; op++) {
			const int f = op_solve[op]->f;
			const int ic = op_solve[op]->ic;
			op_solve[op]->solve_finish(Xrhs[f], Xrhs.get_comp(ic), tag + ic*tag_ic, &req_ptr, lms_mpi, lme_mpi-lms_mpi+1);
		}

		if (req_ptr-&req[0] > 0) {
			print_debug("wait for %ld requests...\n", req_ptr-&req[0]);

			int ierr = MPI_Waitall(req_ptr-&req[0], &req[0], MPI_STATUSES_IGNORE);
			if UNLIKELY(ierr != MPI_SUCCESS) runerr("[solve_state] MPI_Waitall failed.\n");
		}
		if (n_mpi_shared > 1) MPI_Barrier(comm_shared);		// we don't need to wait for everyone. Or do we?
		if (kill_sbr) {
			conserve_momentum(Xrhs.U, kill_sbr);		// implicit MPI_Barrier from the MPI_Allreduce
		}
		
		print_debug("*** solve done.\n");
	}
	#pragma omp barrier
	#endif
#endif
}


int LinSolver::set_block_nbr_linsolve(int nb)		// returns the actual number of blocks
{
#ifdef XS_MPI
	int lms_mpi = 0;
	int lme_mpi = NLM-1;
	thread_interval_solve(lms_mpi, lme_mpi, i_mpi_shared, n_mpi_shared);	// split work between process sharing memory
	const int cl = CACHE_LINE/sizeof(cplx);
	int nlm_4 = (lme_mpi-lms_mpi+cl)/cl;			// don't split cache lines.
	if (nb < 1) nb = 1;							// at least one block !
	if (nlm_4/nb < nthreads) nb = nlm_4/nthreads;	// each thread should have at least one cache line to work on.
	if (nb > 2048) nb = 2048;					// no more than 2048 blocks.
	if (nb < 1) nb = 1;							// at least one block !
	nblocks = nb;
	if (n_mpi_shared > 1) {		// IMPORTANT! everyone must have the same value for nblocks !
		MPI_Allreduce( MPI_IN_PLACE, &nblocks, 1, MPI_INT, MPI_MIN, comm_shared );
	}
	req.resize( (nop3*4 + nop5*8)*nblocks );	// up to 28 requests per block.

	#ifndef XS_MPI_ALL2ALL
	// check if there is enough work to share (mimics the partitionning used in solve_state())
	for (int k=0; k<nblocks; k++) {		// split into blocks
		int lms_block = lms_mpi;		int lme_block = lme_mpi;
		thread_interval_solve(lms_block, lme_block, k, nblocks);
		for (int ith=0; ith<nthreads; ith++) {		// split into threads
			int lms = lms_block;		int lme = lme_block;
			thread_interval_solve(lms, lme, ith, nthreads);
			if UNLIKELY(lme < lms) {
				printf("i_mpi_shared = %d, block = %d/%d, thread=%d : lms=%d, lme=%d\n", i_mpi_shared, k, nblocks, ith, lms, lme);
				runerr("[set_block_nbr_linsolve] not enough work to share.");
			}
		}
	}
	#endif

	return nblocks;
#else
	return 1;
#endif
}

/// returns time in seconds for nloops linear solve with a nblk blocks
/// WARNING: after this function, the number of blocks is changed.
double LinSolver::time_linear_solver(int nblk, int nloops, int clear_cache)
{
	static std::map<int,double> cache;		// cache timings.
	double t = 0.0;
	
	
	nblk = set_block_nbr_linsolve(nblk);		// set the block size (global variable)

	if (clear_cache) cache.clear();		// clear cache if requested.
	t = cache[nblk];		// get cached value if nloops did not change.
	if (t == 0.0) {		// value not cached
		#ifdef XS_MPI
		MPI_Barrier(MPI_COMM_WORLD);
		#endif
		double time1 = xs_wtime();
		#pragma omp parallel
		{
			for (int j=0; j<nloops; j++) solve_state(Xlm, 0);		// don't save angular momentum during these timings.
		}
		double time2 = xs_wtime();
		t = (time2-time1)/nloops;
		#ifdef XS_MPI
		MPI_Bcast((void*) &t, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);	// Broadcast time information.
		#endif
		cache[nblk] = t;
		#ifdef XS_DEBUG
			if (i_mpi==0) printf("\n   %d blocks : t=%g  (nloops=%d)",nblk, t, nloops);
		#else
			if (i_mpi==0) {	printf(".");	fflush(stdout);	}
		#endif
	} else {
		#ifdef XS_DEBUG
			if (i_mpi==0) printf("[time_linear_solver] cache hit for block number %d\n",nblk);
		#endif
	}
	return t;
}

/// find the best block size for MPI communication in the linear solver.
void LinSolver::optimize_linear_solver()		// TODO : do this in terms of number of blocks instead of block size.
{
	const double alpha = 0.61803;
	const double beta = 1. - alpha;
	int bmin = 1;		// minimum number of blocks.
	int bmax = set_block_nbr_linsolve( NLM );		// maximum number of blocks.

	// start with a default block number.
	int nblk = set_block_nbr_linsolve( (n_mpi+1)/2 );

	int b[3];
	double t[3];
	int nloops = 1;

	if (i_mpi==0) printf("[Linear Solver] timing");
	if (n_mpi > 1) {
		nblk = set_block_nbr_linsolve( pow(bmin,beta)*pow(bmax,alpha) );	// go for some golden section in log-space.
	}
	b[1] = set_block_nbr_linsolve(nblk);
	do {	// first adjust the number of solves to get accurate timing.
		t[1] = time_linear_solver(b[1], nloops, 1);
		nloops *= 2;
	} while (t[1]*(nloops/2) < 0.03);
	nloops /= 2;
	if (nloops == 1)	t[1] = time_linear_solver(b[1], nloops, 1);	// time again to prevent cache effects.

#ifdef XS_MPI
	const double t_accuracy = 1.02;			// we are happy if within 2% of best performance.
	if (n_mpi > 1) {
		double mul;
		if ( bmax*bmin > b[1]*b[1]) {		// choose a value in the largest interval (log-scale).
			mul = bmax;
		} else {
			mul = bmin;
		}
		mul = pow(mul/b[1], beta);
		b[0] = set_block_nbr_linsolve( b[1]*mul );
		t[0] = time_linear_solver( b[0], nloops);
		if (t[0] < t[1]) {		// swap values to have t[1] < t[0]
			int bb = b[0];		b[0] = b[1];	b[1] = bb;
			double tt = t[0];	t[0] = t[1];	t[1] = tt;
		}
		//PRINTF0("\nbracketing interval : b = [%d %d ?] (mul=%g)", b[0], b[1], mul);
		if (b[1] > b[0]) {
			mul = bmax;
		} else {
			mul = bmin;
		}
		mul = pow(mul/b[1], beta);
		b[2] = b[1];		// base for next try.
		// t[1] is the smallest
		int it=0;
		do {		// until we hit the wall !
			b[2] = set_block_nbr_linsolve( b[2]*mul );		// next try
			//PRINTF0("\nbracketing interval : b = [%d %d %d] (mul=%g, bmin=%d, bmax=%d)", b[0], b[1], b[2], mul, bmin, bmax);
			t[2] = time_linear_solver( b[2] , nloops);
			if (t[2] < t[1]) {		// new minimum
				if (t[1] > t[2]*t_accuracy) {		// change the interval limit
					t[0] = t[1];
					b[0] = b[1];
				}
				t[1] = t[2];
				b[1] = b[2];
			}
			if ((b[2] == bmin) || (b[2] == bmax) || (t[2] > t[1]*t_accuracy)) {		// if we reach the boundaries, or go really up, we stop !
				break;
			}
			if (it++ > 20) break;
		} while ((b[1] > bmin) && (b[1] < bmax));
		//PRINTF0("\nbracketing done: b = [%d %d %d], t = [%g %g %g]", b[0], b[1], b[2],  t[0], t[1], t[2]);
		// now we have bracketed the minimum between t[0] and t[2]. t[1] is smaller (or equal) than t[0] and t[2]

		// golden section search.
		double tt;
		int bb;
		if (fabs(log(b[2])-log(b[0])) > fabs(log(b[0])-log(b[1]))) {		// split the largest interval in log scale
			bb = set_block_nbr_linsolve( round(pow(b[1],alpha)*pow(b[2],beta)) );
			tt = time_linear_solver( bb , nloops);
		} else {
			bb = b[1];		tt = t[1];
			b[1] = set_block_nbr_linsolve( round(pow(b[1],alpha)*pow(b[0],beta)) );
			t[1] = time_linear_solver( b[1] , nloops);
		}
		//PRINTF0("\ngolden search : b = [%d %d %d %d], t = [%g %g %g %g]", b[0], b[1], bb, b[2],  t[0], t[1], tt, t[2]);
		while ((t[2] > fmin(t[1],tt)*t_accuracy) && (t[0] > fmin(t[1],tt)*t_accuracy) && (abs(b[2]-b[0]) > 1)) {
			if (tt < t[1]) {		// this is a better candidate
				t[0] = t[1];	b[0] = b[1];
				t[1] = tt;		b[1] = bb;
				bb = set_block_nbr_linsolve( round(pow(b[1],alpha)*pow(b[2],beta)) );
				tt = time_linear_solver( bb , nloops);
			} else {
				t[2] = tt;		b[2] = bb;
				tt = t[1];		bb = b[1];
				b[1] = set_block_nbr_linsolve( round(pow(b[1],alpha)*pow(b[0],beta)) );
				t[1] = time_linear_solver( b[1] , nloops);
			}
			PRINTF0("\ngolden search : b = [%d %d %d %d], t = [%g %g %g %g]", b[0], b[1], bb, b[2],  t[0], t[1], tt, t[2]);
		}
		if (tt < t[1]) {
			t[1] = tt;		b[1] = bb;
		}

		// we are done, b[1] is the optimal block number.
		MPI_Bcast((void*) &b[1], 1, MPI_INT, 0, MPI_COMM_WORLD);		// Broadcast best block size information, just to be sure !
		MPI_Barrier(MPI_COMM_WORLD);
		set_block_nbr_linsolve( b[1] );
		nblk = b[1];
	}
	PRINTF0("\n   + using %d blocks (size about %d), linear solve time = %g\n", nblk, ((NLM/n_mpi_shared+nblk-1)/nblk), t[1]);
#else
	PRINTF0("\n   + linear solve time = %g\n", t[1]);
#endif
}


/// compute the spatial terms for a solid body (called by explicit_terms())
/// Q, S, nlr, nlt, nlp are temporary arrays.
/// Note: this does not couple different degrees l, but may couple m+1 and m-1 (if solid.Omega_x != 0 or solid.Omega_y != 0)
int spatial_terms_SolidBody(SolidBody& solid, StateVector& NL, StateVector& X, 
		cplx* Q=0, cplx* S=0, double* nlr=0, double* nlt=0, double* nlp=0)
{
	if (solid.freely_rotate & 1) solid.set_rotation(r[solid.ir_interface], Ulm.Tor[solid.ir_interface]);
	const size_t nspat = shtns->nspat;					// real physical size of spatial data
	if ((solid.Omega_x == 0) && (solid.Omega_y == 0)) {		// no SHT needed in that case.
		if (solid.Omega_z == 0) {
			#if XS_OMP == 1
			#pragma omp for schedule(FOR_SCHED_SH) nowait
			#endif
			for (int ir=solid.irs; ir<=solid.ire; ir++) {		// stationnary solid shell => zero forcing
				if (evol_ubt & EVOL_B) NL.B.zero_out_shell(ir);
				if (evol_ubt & EVOL_T) NL.T.zero_out_shell(ir);
				if (evol_ubt & EVOL_C) NL.C.zero_out_shell(ir);
			}
		} else {
			if (evol_ubt & EVOL_B) Axial_rot_induction(NL.B, solid, X.B);
			if (evol_ubt & EVOL_T) Axial_rot_advection(NL.T, solid, X.T);
			if (evol_ubt & EVOL_C) Axial_rot_advection(NL.C, solid, X.C);
		}
		return 0;		// curl_from_TQS should not include these solid shells
	} else {
		if (Q==0) runerr("non-axial solid body rotation is not supported.\n");
		solid.calc_spatial();
		#pragma omp for schedule(FOR_SCHED_SH) nowait
		for (int ir=solid.irs; ir<=solid.ire; ir++) {		// rotating solid shell => advection
			if (evol_ubt & EVOL_B) {
				X.B.RadSph(ir, Q, S);
				SHV3_SPAT(Q, S, X.B.Tor[ir], nlr, nlt, nlp);
				for (size_t k=0; k<nspat; k++) {
					double rt = solid.vp_r[k]*r[ir];
					double rp = solid.vt_r[k]*r[ir];
					double rr = rp*nlp[k] - rt*nlt[k];
					nlt[k] = rt*nlr[k];		nlp[k] = -rp*nlr[k];		nlr[k] = rr;		// u x b
				}
				SPAT_SHV3(nlr, nlt, nlp, NL.B.Tor[ir], NL.Sb->get_data(0,ir), NL.B.Pol[ir]);
			}
			if (evol_ubt & EVOL_T) {
				X.T.shell_r_grad(T0lm, ir,S, nlt,nlp);
				for (size_t k=0; k<nspat; k++)
					nlr[k] = solid.vt_r[k]*nlt[k] + solid.vp_r[k]*nlp[k];		// u.grad(T)
				SPAT_SH(nlr, NL.T[ir]);
				if (evol_ubt & EVOL_C) {
					X.C.shell_r_grad(C0lm, ir,S, nlt,nlp);
					for (size_t k=0; k<nspat; k++)
						nlr[k] = solid.vt_r[k]*nlt[k] + solid.vp_r[k]*nlp[k];		// u.grad(C)
					SPAT_SH(nlr, NL.C[ir]);
				}
			}
		}
		return 1;		// curl_from_TQS needed on these solid shells (induction equation)
	}
}

// time stepper logic:
#include "xshells_stepper.cpp"

#ifndef XS_LINEAR

spat_vect* gradPhi0 = 0;		///< general gravity field (non-central).

/*
class spat2D {
  private:
	double* spat;
	size_t spat_cdist;

  public:
	spat2D() { spat=0; }
	void alloc(size_t nelem_spat, int nc_spat) {		// alloc memory
		spat = 0;
		const int chunk = ((VSIZE < 8) ? 8 : VSIZE);		// at least 64 byte alignement.
		nelem_spat = ((nelem_spat+chunk-1)/chunk)*chunk;		// round to VSIZE
		spat = (double*) VMALLOC( sizeof(double)*(nelem_spat*nc_spat) );		// buffer
		memset(spat, 0, sizeof(double)*(nelem_spat*nc_spat) );
		spat_cdist = nelem_spat;
	}
	void* alloc(size_t nelem_spat, int nc_spat, void* mem) {		// use already allocated buffer
		spat = (double*) mem;
		spat_cdist = nelem_spat;
		return (spat + nelem_spat*nc_spat);
	}
	~spat2D() {
		if (spat) VFREE(spat);
	}
	double* operator() (int c) const {		// get pointer to the c component
		return spat + spat_cdist*c;
	}
};
*/

class data2D {
  private:
	cplx* spec;
	size_t spec_cdist;
	double* spat;
	size_t spat_cdist;

  public:
	data2D() { spec=0; spat=0; }
	void alloc(size_t nelem_spat, int nc_spat, size_t nelem_spec=0, int nc_spec=0) {
		spec = 0;		spat = 0;
		const int chunk = ((VSIZE < 8) ? 8 : VSIZE);		// at least 64 byte alignement.
		nelem_spec = ((nelem_spec*2 +chunk-1)/chunk)*chunk;		// round to VSIZE
		nelem_spat = ((nelem_spat+chunk-1)/chunk)*chunk;		// round to VSIZE
		const long bytes = sizeof(double)*(nelem_spec*nc_spec + nelem_spat*nc_spat);
		spat = (double*) VMALLOC( bytes );		// buffer
		memset(spat, 0, bytes );				// important for correctness of absmax2() when there is padding.
		spec_cdist = nelem_spec/2;		// because cplx is twice double
		spat_cdist = nelem_spat;
		spec = (cplx*)(spat + spat_cdist*nc_spat);
	}
	~data2D() {
		if (spat) VFREE(spat);
	}
	double* x(int c) const {			// get pointer to the c spatial component
		return spat + spat_cdist*c;
	}
	cplx* z(int c) const {				// get pointer to the c spectral component
		return spec + spec_cdist*c;
	}
	size_t nspat() { return spat_cdist; }		// number of spatial elements
	size_t nspec() { return spec_cdist; }		// number of spectral elements
};

namespace spatial_buffers1 {
	data2D *u,*b,*w,*j,*gt,*gc,*tp;
}


/*
#include <x86intrin.h>
inline unsigned long cpu_time() {
        unsigned long t;
        _mm_lfence();
        t = __rdtsc();
        _mm_lfence();
        return t;
}

long acc_time1, acc_time2;
*/

void explicit_terms_omp_rad(StateVector& NL, StateVector& Xlm, int calc_cfl)
{
	using namespace spatial_buffers1;
//	printf("this is explicit terms, thread=%d\n", omp_get_thread_num());
	// compute spatial terms, shell by shell, without storing the full spatial fields (lower memory requirement, significantly faster).
	// TODO: support base fields.

	spat_vect v,w,nl;
	cplx *Q, *S, *T, *Q2, *S2;
	#ifndef XS_SPARSE
	const double Wz0 = 2.*frame.Omega_z * Y10_ct;		// multiply by representation of cos(theta) in spherical harmonics (l=1,m=0)
	#endif
	const cplx Wxy0 = 2.*frame.Omega_xy * Y11_st;
	int NLb_istart = Ulm.ir_bci;		// a priori, non-linear terms for induction are only
	int NLb_iend   = Ulm.ir_bco;		// needed in the fluid domain.
	const size_t nspat_alloc = shtns->nspat;		// with padding for fft and alignement
	const size_t nspat = nspat_alloc;	//NLAT*NPHI;					// real physical size of spatial data
	const int lm10 = LiM(shtns,1,0);
	const int lm11 = ((MMAX > 0) && (MRES==1)) ? LiM(shtns,1,1) : -1;
	#ifdef XS_STRAIN
	const int lm22 = ((MRES <= 2) && (MMAX*MRES >=2)) ? LM(shtns,2,2) : -1;
	const cplx strain_ampl = frame.strain_22;
	#endif

	void* mem = spat_mem;				// get thread-private spatial buffer
	mem = v.alloc(nspat_alloc, mem);
	mem = w.alloc(nspat_alloc, mem);
	mem = nl.alloc(nspat_alloc, mem);
	Q = (cplx*) mem;
	S = Q + NLM;		T = Q + 2*NLM;
	Q2 = Q + 3*NLM;		S2 = Q + 4*NLM;

	//barrier_shared_mem();

//	acc_time1 = 0;
//	acc_time2 = 0;

	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {		// FLUID SHELLS //
		#ifdef XS_ADJUST_DT
		double vcfl[MAX_CFL];
		for (int k=0; k<MAX_CFL; k++) vcfl[k] = 0.0;
		#endif
		int nlu_status = 0;
		//long tt = cpu_time();
		/*if ((spat_need & (NEED_U|NEED_W)) == (NEED_U|NEED_W)) {		// spatial u & w
			Xlm.U.RadSph_curl_T(ir, Q2, S2, T);
			Xlm.U.curl_QS(ir, Q, S);
			SHV3_SPAT(Q2, S2, Xlm.U.Tor[ir], v.r, v.t, v.p);
			#ifndef XS_SPARSE
			Q[lm10] += Wz0; 	S[lm10] += Wz0;		// Coriolis for free.
			#endif
			if (lm11 >= 0) { Q[lm11] += Wxy0;	S[lm11] += Wxy0; }
			SHV3_SPAT(Q, S, T, nl.r, nl.t, nl.p);
			#ifdef XS_ADJUST_DT
			if (calc_cfl) {
				vcfl[cfl_ur] = absmax2(v.r, 0,nspat);
				vcfl[cfl_uh] = absmax2(v.t, v.p, 0,nspat);
				vcfl[cfl_w] = absmax2(nl.r, nl.t, nl.p, 0,nspat);
			}
			#endif
			cross_prod(nl.r, v.r, nl.r, nspat, nspat_alloc);		// u x w
			nlu_status = 1;
		} else */{
		if (spat_need & NEED_U) {		// spatial u
			Xlm.U.RadSph(ir, Q, S);
			#ifdef XS_STRAIN
			if (lm22 >= 0) {
				Q[lm22] += r[ir]*strain_ampl*2.;
				S[lm22] += r[ir]*strain_ampl;
			}
			#endif
			SHV3_SPAT(Q, S, Xlm.U.Tor[ir], v.r, v.t, v.p);
			#ifdef XS_ADJUST_DT
			if (calc_cfl) {
				vcfl[cfl_ur] = absmax2(v.r, 0,nspat);
				vcfl[cfl_uh] = absmax2(v.t, v.p, 0,nspat);
			}
			#endif
		}
		if (spat_need & NEED_W) {		// spatial w
			Xlm.U.curl_QST(ir, Q, S, T);
			#ifndef XS_SPARSE
			Q[lm10] += Wz0; 	S[lm10] += Wz0;		// Coriolis for free.
			#endif
			if (lm11 >= 0) { Q[lm11] += Wxy0;	S[lm11] += Wxy0; }
			SHV3_SPAT(Q, S, T, nl.r, nl.t, nl.p);
			#ifdef XS_ADJUST_DT
			if (calc_cfl)	vcfl[cfl_w] = absmax2(nl.r, nl.t, nl.p, 0,nspat);
			#endif
			cross_prod(nl.r, v.r, nl.r, nspat, nspat_alloc);		// u x w
			nlu_status = 1;
		}
		}
		//acc_time1 += cpu_time() - tt;
		if (spat_need & NEED_T) {
			spat_vect& g = gradPhi0[ir];
			cplx* a = Xlm.T[ir];
			if (spat_need & NEED_C) {
				cplx* b = Xlm.C[ir];
				for (int lm=0; lm<NLM; lm++)
					Q[lm] = a[lm] + b[lm];
				a = Q;
			}
			SH_SPAT(a, w.r);		// only one transform for both fields
			if (nlu_status) {
				scal_vect_add(nl, w.r, g, 0, nspat);
			} else {
				scal_vect(nl, w.r, g, 0, nspat);
				nlu_status = 1;
			}
		}
		if (spat_need & NEED_GT) {		// spatial grad(T)
			if ((ir>=Tlm.irs)&&(ir<=Tlm.ire)) {
				Xlm.T.shell_grad(T0lm, ir, Q,S, 0, NLM-1);
				SH_SPAT(Q, w.r);
				SHS_SPAT(S, w.t, w.p);
				for (size_t k=0; k<nspat; k++)
					w.r[k] = v.r[k]*w.r[k] + v.t[k]*w.t[k] + v.p[k]*w.p[k];		// u.grad(T)
				SPAT_SH(w.r, NL.T[ir]);
			}
		}
		if (spat_need & NEED_GC) {		// spatial grad(T)
			if ((ir>=Clm.irs)&&(ir<=Clm.ire)) {
				Xlm.C.shell_grad(C0lm, ir, Q,S, 0, NLM-1);
				SH_SPAT(Q, w.r);
				SHS_SPAT(S, w.t, w.p);
				for (size_t k=0; k<nspat; k++)
					w.r[k] = v.r[k]*w.r[k] + v.t[k]*w.t[k] + v.p[k]*w.p[k];		// u.grad(C)
				SPAT_SH(w.r, NL.C[ir]);
			}
		}
		//tt = cpu_time();
		/*if ((spat_need & (NEED_B|NEED_J)) == (NEED_B|NEED_J)) {
			Xlm.B.RadSph_curl_T(ir, Q2, S2, T);
			Xlm.B.curl_QS(ir, Q, S);
			SHV3_SPAT(Q2, S2, Xlm.B.Tor[ir], w.r, w.t, w.p);
			if (evol_ubt & EVOL_B) {
				cross_prod(v.r, v.r, w.r, nspat, nspat_alloc);		// u x b
				#ifdef XS_MEAN_FIELD
					alpha_effect_add(v, alpha(ir,0), w, 1);		// alpha.b
				#endif
				SPAT_SHV3(v.r, v.t, v.p, NL.B.Tor[ir], NL.Sb->get_data(0,ir), NL.B.Pol[ir]);
			}
			SHV3_SPAT(Q, S, T, v.r, v.t, v.p);
			#ifdef XS_ADJUST_DT
			if (calc_cfl) {
				vcfl[cfl_j] = absmax2(v.r, v.t, v.p, 0,nspat);
				vcfl[cfl_bh] = absmax2(w.t, w.p, 0,nspat);
				vcfl[cfl_br] = absmax2(w.r, 0,nspat);
			}
			#endif
			if  (nlu_status) {
				cross_prod_add(nl.r, v.r, w.r, nspat, nspat_alloc);	// j x b
			} else {
				cross_prod(nl.r, v.r, w.r, nspat, nspat_alloc);	// j x b
				nlu_status = 1;
			}
		} else*/ {
		if (spat_need & NEED_B) {		// spatial b
			Xlm.B.RadSph(ir, Q, S);
			SHV3_SPAT(Q, S, Xlm.B.Tor[ir], w.r, w.t, w.p);
			#ifdef XS_ADJUST_DT
			if (calc_cfl) {
				vcfl[cfl_bh] = absmax2(w.t, w.p, 0,nspat);
				vcfl[cfl_br] = absmax2(w.r, 0,nspat);
			}
			#endif
			if (evol_ubt & EVOL_B) {
				cross_prod(v.r, v.r, w.r, nspat, nspat_alloc);		// u x b
				#ifdef XS_MEAN_FIELD
					alpha_effect_add(v, alpha(ir,0), w, 1);		// alpha.b
				#endif
				SPAT_SHV3(v.r, v.t, v.p, NL.B.Tor[ir], NL.Sb->get_data(0,ir), NL.B.Pol[ir]);
			}
		}
		if (spat_need & NEED_J) {		// spatial j
			Xlm.B.curl_QST(ir, Q, S, T);
			SHV3_SPAT(Q, S, T, v.r, v.t, v.p);
			#ifdef XS_ADJUST_DT
			if (calc_cfl)	vcfl[cfl_j] = absmax2(v.r, v.t, v.p, 0,nspat);
			#endif
			if  (nlu_status) {
				cross_prod_add(nl.r, v.r, w.r, nspat, nspat_alloc);	// j x b
			} else {
				cross_prod(nl.r, v.r, w.r, nspat, nspat_alloc);	// j x b
				nlu_status = 1;
			}
		}
		}
		//acc_time2 += cpu_time() - tt;
		if (nlu_status) {
			SPAT_SHV3(nl.r, nl.t, nl.p, NL.U.Tor[ir], NL.Su->get_data(0,ir), NL.U.Pol[ir]);
		} else NL.U.zero_out_shell(ir);
		#ifdef XS_ADJUST_DT
		if (calc_cfl) compute_local_cfl(ir, vcfl);
		#endif
	}

	if (evol_ubt & (EVOL_B | EVOL_T | EVOL_C)) {			// SOLID SHELLS //
		if ((InnerCore.Omega_x != 0) || (InnerCore.Omega_y != 0))
			NLb_istart = NL.B.ir_bci;	// spatial terms for B span also inner-core
		if ((Mantle.Omega_x != 0) || (Mantle.Omega_y != 0))
			NLb_iend = NL.B.ir_bco;		// spatial terms for B span also mantle
		if (InnerCore.irs <= InnerCore.ire)
			spatial_terms_SolidBody(InnerCore, NL, Xlm,  Q, S, nl.r, nl.t, nl.p);
		if (Mantle.irs <= Mantle.ire)
			spatial_terms_SolidBody(Mantle, NL, Xlm,  Q, S, nl.r, nl.t, nl.p);
	}

	#ifdef XS_MPI
	#pragma omp barrier
	#pragma omp master
	{
		MPI_Request req[8];
		MPI_Request *req_ptr = req;
		if ((n_mpi_shared > 1) && (evol_ubt & (EVOL_U|EVOL_B))) MPI_Barrier(comm_shared);
		if (evol_ubt & EVOL_B)	NL.Sb->sync1_mpi(0, NLb_istart, NLb_iend, 0, NLM-1, ::B, &req_ptr);
		if (evol_ubt & EVOL_U)	NL.Su->sync1_mpi(0, Ulm.ir_bci, Ulm.ir_bco, 0, NLM-1, ::U, &req_ptr);
		if (req_ptr-req > 0) {
			int ierr = MPI_Waitall(req_ptr-req, req, MPI_STATUSES_IGNORE);
			if UNLIKELY(ierr != MPI_SUCCESS) runerr("[explicit_terms] MPI_Waitall failed.\n");
		}
		if (n_mpi_shared > 1) MPI_Barrier(comm_shared);
	}
	#endif
	#pragma omp barrier
	if (evol_ubt & EVOL_B) NL.B.curl_from_TQS(NL.Sb, NLb_istart, NLb_iend, -1);		// no sync, it has been performed before.
	if (evol_ubt & EVOL_U) NL.U.curl_from_TQS(NL.Su, Ulm.ir_bci, Ulm.ir_bco, -1);	// no sync, it has been performed before.
	#pragma omp barrier
	if (evol_ubt & EVOL_U) {
		if (InnerCore.freely_rotate && own(Ulm.ir_bci)) {
			#pragma omp single nowait
			{
				// /!\ Pol and Tor are exchanged
				NL.U.Pol[Ulm.ir_bci][1] = explicit_term_T10_free_solid(InnerCore, Ulm.ir_bci, 1, Ulm.Tor, Blm, jpar.nu/jpar.Inertia_i);
			}
			#pragma omp barrier
		}
		if (Mantle.freely_rotate && own(Ulm.ir_bco)) {
			#pragma omp single nowait
			{
				// /!\ Pol and Tor are exchanged
				NL.U.Pol[Ulm.ir_bco][1] = - explicit_term_T10_free_solid(Mantle, Ulm.ir_bco, -1, Ulm.Tor, Blm, jpar.nu/jpar.Inertia_o);
			}
			#pragma omp barrier
		}
		#ifndef XS_SPARSE
		if (((spat_need & NEED_W) == 0) && (frame.Omega_z != 0.0)) {
			Coriolis_force_add(NL.U, Xlm.U, frame.Omega_z, spat_mem);
			#pragma omp barrier
		}
		if (Grav0_r) {
			Radial_gravity_buoyancy(NL.U, Xlm.T, Xlm.C, Grav0_r);		// add buoyancy due to central gravity.
			#pragma omp barrier
		}
		#endif
		frame.add_Poincare_force(NL.U);
		#pragma omp barrier
	}
	#ifdef XS_BULK_FORCING
		bulk_forcing.add_forcing(NL, ftime);
		#pragma omp barrier
	#endif
	//printf("time1 = %ld  time2 = %ld\n", acc_time1, acc_time2);
}


namespace spatial_buffers2 {
	xs_array2d<double> u,b,w,j,gt,gc;	// physical vectors, 3 components per shell
	xs_array2d<double> tspat;			// physical scalar, 1 component per shell
	xs_array2d<cplx> u_qs, b_qs, gt_qs, gc_qs;	// spectral domain, 2 components
	xs_array2d<cplx> w_qst, j_qst;		// spectral domain, 3 components
	xs_array2d<cplx> tspec;				// spectral domain, 1 component
	
	long spat_cdist;	// distance between vector components.
	long spec_cdist;	// distance between spectral components.

	const int nlm_block = 256;	// initial guess for blocking
	const int nspat_blk = 384;	// initial guess for blocking
}

inline void max_reduce(double& acc, double v) {
	if (v > acc) acc = v;
}

void explicit_terms_omp_inshell(StateVector& NL, StateVector& Xlm, int calc_cfl)
{
	using namespace spatial_buffers2;
	// compute spatial terms storing the full spatial fields (much higher memory requirement, but significantly faster).
	// TODO: support base fields.
	#ifndef XS_SPARSE
	const double Wz0 = 2.*frame.Omega_z * Y10_ct;		// multiply by representation of cos(theta) in spherical harmonics (l=1,m=0)
	#endif
	const cplx Wxy0 = 2.*frame.Omega_xy * Y11_st;
	int NLb_istart = Ulm.ir_bci;		// a priori, non-linear terms for induction are only
	int NLb_iend   = Ulm.ir_bco;		// needed in the fluid domain.
	const size_t nspat = shtns->nspat;	//NLAT*NPHI;					// real physical size of spatial data
	const int lm10 = LiM(shtns,1,0);
	const int lm11 = ((MMAX > 0) && (MRES==1)) ? LiM(shtns,1,1) : -1;
	#ifdef XS_STRAIN
	const int lm22 = ((MRES <= 2) && (MMAX*MRES >=2)) ? LM(shtns,2,2) : -1;
	const cplx strain_ampl = frame.strain_22;
	#endif

	/// 1) compute spectral components to transform
	#pragma omp parallel
	{
	int nlm00 = 0;		int nlm11 = NLM-1;
	thread_interval_lm(nlm00, nlm11);
	for (int nlm0=nlm00; nlm0<=nlm11; nlm0 += nlm_block) {
		int nlm1 = nlm0 + nlm_block -1;
		if (nlm1 > nlm11) nlm1 = nlm11;

		if ((spat_need & (NEED_B|NEED_J)) == (NEED_B|NEED_J)) {
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++)
				Xlm.B.RadSph_curl_T(ir, b_qs[ir], b_qs[ir] + spec_cdist, j_qst[ir]+2*spec_cdist, nlm0, nlm1);
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++)
				Xlm.B.curl_QS(ir, j_qst[ir], j_qst[ir]+spec_cdist, nlm0, nlm1);
		} else
		if (spat_need & NEED_B) {		// spatial b
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++)
				Xlm.B.RadSph(ir, b_qs[ir], b_qs[ir] + spec_cdist, nlm0, nlm1);
		} else
		if (spat_need & NEED_J) {		// spatial j
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++)
				Xlm.B.curl_QST(ir, j_qst[ir], j_qst[ir]+spec_cdist, j_qst[ir]+2*spec_cdist, nlm0, nlm1);
		}
		if ((spat_need & (NEED_U|NEED_W)) == (NEED_U|NEED_W)) {
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
				Xlm.U.RadSph_curl_T(ir, u_qs[ir], u_qs[ir] + spec_cdist, w_qst[ir] + 2*spec_cdist, nlm0, nlm1);
				#ifdef XS_STRAIN
				if (IN_RANGE_INCLUSIVE(lm22, nlm0, nlm1)) {
					u_qs[ir][lm22]              += r[ir]*strain_ampl*2.;	// Q
					u_qs[ir][lm22 + spec_cdist] += r[ir]*strain_ampl;		// S
				}
				#endif
			}
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
				Xlm.U.curl_QS(ir, w_qst[ir], w_qst[ir] + spec_cdist, nlm0, nlm1);
				#ifndef XS_SPARSE
				if (IN_RANGE_INCLUSIVE(lm10, nlm0, nlm1)) {
					w_qst[ir][lm10] += Wz0; 	w_qst[ir][lm10 + spec_cdist] += Wz0;		// Coriolis for free.
				}
				#endif
				if (IN_RANGE_INCLUSIVE(lm11, nlm0, nlm1)) {
					w_qst[ir][lm11] += Wxy0;	w_qst[ir][lm11 + spec_cdist] += Wxy0;
				}
			}
		} else
		if (spat_need & NEED_U) {		// spatial u
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
				Xlm.U.RadSph(ir, u_qs[ir], u_qs[ir] + spec_cdist, nlm0, nlm1);
				#ifdef XS_STRAIN
				if (IN_RANGE_INCLUSIVE(lm22, nlm0, nlm1)) {
					u_qs[ir][lm22]              += r[ir]*strain_ampl*2.;	// Q
					u_qs[ir][lm22 + spec_cdist] += r[ir]*strain_ampl;		// S
				}
				#endif
			}
		} else
		if (spat_need & NEED_W) {		// spatial w
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
				Xlm.U.curl_QST(ir, w_qst[ir], w_qst[ir] + spec_cdist, w_qst[ir] + 2*spec_cdist, nlm0, nlm1);
				#ifndef XS_SPARSE
				if (IN_RANGE_INCLUSIVE(lm10, nlm0, nlm1)) {
					w_qst[ir][lm10] += Wz0; 	w_qst[ir][lm10 + spec_cdist] += Wz0;		// Coriolis for free.
				}
				#endif
				if (IN_RANGE_INCLUSIVE(lm11, nlm0, nlm1)) {
					w_qst[ir][lm11] += Wxy0;	w_qst[ir][lm11 + spec_cdist] += Wxy0;
				}
			}
		}
		if (spat_need & (NEED_GC|NEED_GT|NEED_T|NEED_C)) {
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {		// FLUID SHELLS //
				if (spat_need & NEED_GT) {		// spatial grad(T)
					if ((ir>=Tlm.irs)&&(ir<=Tlm.ire)) {
						Xlm.T.shell_grad(T0lm, ir, gt_qs[ir], gt_qs[ir] + spec_cdist, nlm0, nlm1);
					}
				}
				if (spat_need & NEED_GC) {		// spatial grad(T)
					if ((ir>=Clm.irs)&&(ir<=Clm.ire)) {
						Xlm.C.shell_grad(C0lm, ir, gc_qs[ir], gc_qs[ir] + spec_cdist, nlm0, nlm1);
					}
				}
				if ((spat_need & (NEED_T|NEED_C)) == (NEED_T|NEED_C)) {	// two buoyancy sources: add them
					for (int lm=nlm0; lm<=nlm1; lm++)	tspec[ir][lm] = Xlm.T[ir][lm] + Xlm.C[ir][lm];
				}
			}
		}
	}}

	/// 2) SHTs (multi-threaded)
	for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {		// FLUID SHELLS //
		if (spat_need & NEED_U) 		// spatial u
			SHV3_SPAT(u_qs[ir], u_qs[ir] + spec_cdist, Xlm.U.Tor[ir], u[ir], u[ir]+spat_cdist, u[ir]+2*spat_cdist);
		if (spat_need & NEED_W)		// spatial w
			SHV3_SPAT(w_qst[ir], w_qst[ir]+spec_cdist, w_qst[ir]+2*spec_cdist, w[ir], w[ir]+spat_cdist, w[ir]+2*spat_cdist);
		if (spat_need & NEED_GT) {		// spatial grad(T)
			if ((ir>=Tlm.irs)&&(ir<=Tlm.ire)) {
				SH_SPAT(gt_qs[ir], gt[ir]);		// not always needed.
				SHS_SPAT(gt_qs[ir]+spec_cdist, gt[ir]+spat_cdist, gt[ir]+2*spat_cdist);
			}
		}
		if (spat_need & NEED_GC) {		// spatial grad(T)
			if ((ir>=Clm.irs)&&(ir<=Clm.ire)) {
				SH_SPAT(gc_qs[ir], gc[ir]);		// not always needed.
				SHS_SPAT(gc_qs[ir]+spec_cdist, gc[ir]+spat_cdist, gc[ir]+2*spat_cdist);
			}
		}
		if (spat_need & NEED_T) {
			cplx* a = Xlm.T[ir];
			if (spat_need & NEED_C) a = tspec[ir];
			SH_SPAT(a, tspat[ir]);		// only one transform for both fields
		}
		if (spat_need & NEED_B)		// spatial b
			SHV3_SPAT(b_qs[ir], b_qs[ir]+spec_cdist, Xlm.B.Tor[ir], b[ir], b[ir]+spat_cdist, b[ir]+2*spat_cdist);
		if (spat_need & NEED_J)		// spatial j
			SHV3_SPAT(j_qst[ir], j_qst[ir]+spec_cdist, j_qst[ir]+2*spec_cdist, j[ir], j[ir]+spat_cdist, j[ir]+2*spat_cdist);
	}
		
	/// 3) compute non-linear terms + CFL
	#pragma omp parallel
	{

		int nspat00 = 0;		int nspat11 = (nspat-1)/VSIZE;
		thread_interval_lm(nspat00, nspat11);

		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {		// FLUID SHELLS //
			#ifdef XS_ADJUST_DT
			double vcfl[MAX_CFL];
			for (int k=0; k<MAX_CFL; k++) vcfl[k] = 0.0;
			#endif

		for (int xx=nspat00; xx<=nspat11; xx += nspat_blk) {
			int yy = xx + nspat_blk -1;
			if (yy > nspat11) yy = nspat11;

		//nspat0 *= VSIZE;		nspat1 = (nspat1+1)*VSIZE;
		int nspat0 = xx*VSIZE;		int nspat1 = (yy+1)*VSIZE;


			if (spat_need & NEED_W) {
				#ifdef XS_ADJUST_DT
				if (calc_cfl) {
					max_reduce( vcfl[cfl_ur], absmax2(u[ir], nspat0,nspat1) );
					max_reduce( vcfl[cfl_uh], absmax2(u[ir]+spat_cdist, u[ir]+2*spat_cdist, nspat0,nspat1) );
					max_reduce( vcfl[cfl_w], absmax2(w[ir], w[ir]+spat_cdist, w[ir]+2*spat_cdist, nspat0,nspat1) );
				}
				#endif
				cross_prod(w[ir]+nspat0, u[ir]+nspat0, w[ir]+nspat0, nspat1-nspat0, spat_cdist);		// u x w
			}
			if (spat_need & NEED_J) {
				#ifdef XS_ADJUST_DT
				if (calc_cfl) {
					max_reduce( vcfl[cfl_bh], absmax2(b[ir]+spat_cdist, b[ir]+2*spat_cdist, nspat0,nspat1) );
					max_reduce( vcfl[cfl_br], absmax2(b[ir], nspat0,nspat1) );
					max_reduce( vcfl[cfl_j], absmax2(j[ir], j[ir]+spat_cdist, j[ir]+2*spat_cdist, nspat0,nspat1) );
				}
				#endif
				cross_prod_add(w[ir]+nspat0, j[ir]+nspat0, b[ir]+nspat0, nspat1-nspat0, spat_cdist);	// + j x b
			}
			if (evol_ubt & EVOL_B) {
				cross_prod(b[ir]+nspat0, u[ir]+nspat0, b[ir]+nspat0, nspat1-nspat0, spat_cdist);		// u x b
			}
			if (spat_need & NEED_GT) {
				for (size_t k=nspat0; k<nspat1; k++)
					gt[ir][k] = u[ir][k]*gt[ir][k] + u[ir][k+spat_cdist]*gt[ir][k+spat_cdist] + u[ir][k+2*spat_cdist]*gt[ir][k+2*spat_cdist];		// u.grad(T)
			}
			if (spat_need & NEED_GC) {
				for (size_t k=nspat0; k<nspat1; k++)
					gc[ir][k] = u[ir][k]*gc[ir][k] + u[ir][k+spat_cdist]*gc[ir][k+spat_cdist] + u[ir][k+2*spat_cdist]*gc[ir][k+2*spat_cdist];		// u.grad(C)
			}
			if (spat_need & (NEED_T | NEED_C)) {
				//spat_vect& g = gradPhi0[ir];		// non-central gravity
				//scal_vect_add(w[ir]+nspat0, tspat[ir]+nspat0, g, nspat0, nspat1);
				runerr("not supported\n");
			}
		}
		
			#ifdef XS_ADJUST_DT
			if (calc_cfl) compute_local_cfl(ir, vcfl);
			#endif
		}
	}

	/// 4) transform back (SHT)
	for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {		// FLUID SHELLS //
			if (spat_need & (NEED_W | NEED_J | NEED_T | NEED_C)) 		// spatial u
				SPAT_SHV3(w[ir], w[ir]+spat_cdist, w[ir]+2*spat_cdist, NL.U.Tor[ir], NL.Su->get_data(0,ir), NL.U.Pol[ir]);
			if (spat_need & NEED_B)		// spatial b
				SPAT_SHV3(b[ir], b[ir]+spat_cdist, b[ir]+2*spat_cdist, NL.B.Tor[ir], NL.Sb->get_data(0,ir), NL.B.Pol[ir]);
			if (spat_need & NEED_GT)		// spatial grad(T)
				SPAT_SH(gt[ir], NL.T[ir]);
			if (spat_need & NEED_GC)		// spatial grad(T)
				SPAT_SH(gc[ir], NL.C[ir]);
	}
	
	if (evol_ubt & (EVOL_B | EVOL_T | EVOL_C)) {			// SOLID SHELLS //
		if ((InnerCore.Omega_x != 0) || (InnerCore.Omega_y != 0))
			NLb_istart = NL.B.ir_bci;	// spatial terms for B span also inner-core
		if ((Mantle.Omega_x != 0) || (Mantle.Omega_y != 0))
			NLb_iend = NL.B.ir_bco;		// spatial terms for B span also mantle
		if (InnerCore.irs <= InnerCore.ire) {
			#pragma omp parallel
			spatial_terms_SolidBody(InnerCore, NL, Xlm);
		}
		if (Mantle.irs <= Mantle.ire) {
			#pragma omp parallel
			spatial_terms_SolidBody(Mantle, NL, Xlm);
		}
	}

	#ifdef XS_MPI
	{	// we are not in a parallel region
		MPI_Request req[8];
		MPI_Request *req_ptr = req;
		if ((n_mpi_shared > 1) && (evol_ubt & (EVOL_U|EVOL_B))) MPI_Barrier(comm_shared);
		if (evol_ubt & EVOL_B)	NL.Sb->sync1_mpi(0, NLb_istart, NLb_iend, 0, NLM-1, ::B, &req_ptr);
		if (evol_ubt & EVOL_U)	NL.Su->sync1_mpi(0, Ulm.ir_bci, Ulm.ir_bco, 0, NLM-1, ::U, &req_ptr);
		if (req_ptr-req > 0) {
			int ierr = MPI_Waitall(req_ptr-req, req, MPI_STATUSES_IGNORE);
			if UNLIKELY(ierr != MPI_SUCCESS) runerr("[explicit_terms_omp_inshell] MPI_Waitall failed.\n");
		}
		if (n_mpi_shared > 1) MPI_Barrier(comm_shared);
	}
	#endif

	#pragma omp parallel
	{
		if (evol_ubt & (EVOL_B | EVOL_T | EVOL_C)) {			// SOLID SHELLS //
			// only axial rotation of SolidBody is supported here:
			if (InnerCore.irs <= InnerCore.ire)	spatial_terms_SolidBody(InnerCore, NL, Xlm);
			if (Mantle.irs <= Mantle.ire)		spatial_terms_SolidBody(Mantle, NL, Xlm);
		}
		if (evol_ubt & EVOL_B) NL.B.curl_from_TQS(NL.Sb, NLb_istart, NLb_iend, -1);		// no sync, already done before.
		if (evol_ubt & EVOL_U) {
			NL.U.curl_from_TQS(NL.Su, Ulm.ir_bci, Ulm.ir_bco, -1);		// no sync, already done before.

			#ifndef XS_SPARSE
			if (((spat_need & NEED_W) == 0) && (frame.Omega_z != 0.0)) Coriolis_force_add(NL.U, Xlm.U, frame.Omega_z, spat_mem);
			if (Grav0_r) Radial_gravity_buoyancy(NL.U, Xlm.T, Xlm.C, Grav0_r);		// add buoyancy due to central gravity.
			#endif

			if (InnerCore.freely_rotate && own(Ulm.ir_bci)) {
				#pragma omp barrier
				#pragma omp single nowait
				{
					// /!\ Pol and Tor are exchanged
					NL.U.Pol[Ulm.ir_bci][1] = explicit_term_T10_free_solid(InnerCore, Ulm.ir_bci, 1, Ulm.Tor, Blm, jpar.nu/jpar.Inertia_i);
				}
			}
			if (Mantle.freely_rotate && own(Ulm.ir_bco)) {
				#pragma omp barrier
				#pragma omp single nowait
				{
					// /!\ Pol and Tor are exchanged
					NL.U.Pol[Ulm.ir_bco][1] = - explicit_term_T10_free_solid(Mantle, Ulm.ir_bco, -1, Ulm.Tor, Blm, jpar.nu/jpar.Inertia_o);
				}
			}
		}
	}

	if (evol_ubt & EVOL_U) {
		frame.add_Poincare_force(NL.U);
	}
	#ifdef XS_BULK_FORCING
		bulk_forcing.add_forcing(NL, ftime);
	#endif
}

#else /* XS_LINEAR */

#if XS_OMP == 2
#error "linear mode not supported with XS_OMP=2"
#endif

#ifdef XS_SPARSE
#error "We are not ready for XS_SPARSE with XS_LINEAR yet"
#endif

void explicit_terms_lin(StateVector& NL, StateVector& Xlm, int calc_cfl)
{
	int NLb_istart = NG;	// a priori, non-linear terms for induction are only
	int NLb_iend = NM;		// needed in the fluid domain.
	if (U0) {
		if (U0->irs < NLb_istart)	NLb_istart = U0->irs;
		if (U0->ire > NLb_iend)		NLb_iend = U0->ire;
	}

  if (spat_need) {
	// compute spatial terms, shell by shell, without storing the full spatial fields (lower memory requirement, significantly faster).
	// TODO: support base fields.

	spat_vect v,w,nlu,nlb;
	double *nltemp;
	cplx *Q, *S, *T;
	const size_t nspat_alloc = shtns->nspat;		// with padding for fft and alignement
	const size_t nspat = nspat_alloc;	//NLAT*NPHI;					// real physical size of spatial data
	const int lm10 = LiM(shtns,1,0);
	const int lm11 = ((MMAX > 0) && (MRES==1)) ? LiM(shtns,1,1) : -1;
	const double Wz0 = 2.*frame.Omega_z * Y10_ct;		// multiply by representation of cos(theta) in spherical harmonics (l=1,m=0)
	const cplx Wxy0 = 2.*frame.Omega_xy * Y11_st;
	#ifdef XS_STRAIN
		runerr("Linear mode does not support strain yet.");
	#endif

	void* mem = spat_mem;				// get thread-private spatial buffer
	mem = v.alloc(nspat_alloc, mem);
	mem = w.alloc(nspat_alloc, mem);
	if (evol_ubt & EVOL_U) mem = nlu.alloc(nspat_alloc, mem);
	if (evol_ubt & EVOL_B) mem = nlb.alloc(nspat_alloc, mem);
	if (evol_ubt & (EVOL_T | EVOL_C)) { nltemp = (double*) mem;		mem = nltemp + nspat_alloc; }
	Q = (cplx*) mem;		S = Q + NLM;		T = Q + 2*NLM;

	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (int ir=irs_mpi; ir<=ire_mpi; ir++) {		// ALL LOCAL SHELLS //
		#ifdef XS_ADJUST_DT
		double vcfl[MAX_CFL];
		for (int k=0; k<MAX_CFL; k++) vcfl[k] = 0.0;
		#endif
		int nl_status_U = 0;		// mark non-zero non-linear terms
		int shell_ubt = 0;
		for (int f=0; f<MAXFIELD; f++) {
			if ((evol_ubt & EVOL(f)) && (ir <= Xlm[f].ire) && (ir >= Xlm[f].irs)) shell_ubt |= EVOL(f);
		}

		// NAVIER-STOKES EQUATION (except Lorentz Force, computed later)
		if (shell_ubt & EVOL_U) {	// dynamic u domain
			if (spat_need & NEED_U) {
				Xlm.U.RadSph(ir, Q, S);
				SHV3_SPAT(Q, S, Xlm.U.Tor[ir], v.r, v.t, v.p);		// compute spatial u -> v
				if (W0) {
					nl_status_U = W0->w_cross_V0(ir, nlu.r,nlu.t,nlu.p, v.r,v.t,v.p, nl_status_U);	// nlu = u x W0
					#ifdef XS_ADJUST_DT
						if (calc_cfl) W0->absmax2(ir, vcfl[cfl_w]);
					#endif
				}
				if (spat_need & NEED_COR) {
					Q[0] = 0.0;		Q[lm10] = Wz0;			if (lm11 >= 0) Q[lm11] = Wxy0;
					SH_to_spat_l(shtns, Q, w.r, 1);
					SHsph_to_spat_l(shtns, Q, w.t, w.p, 1);		// w = 2*Omega_coriolis
					#ifdef XS_CORIOLIS_BASE
					if (U0) {
						nl_status_U += U0->V0_cross_w(ir, nlu.r,nlu.t,nlu.p, w.r,w.t,w.p, nl_status_U);		// nlu += U0 x 2*Omega_coriolis
					}
					#endif
					cross_prod_condadd(nlu, v, w, 0,nspat, nl_status_U);		// nlu += u x 2*Omega_coriolis
					nl_status_U = 1;
				}
			}
			if (spat_need & NEED_W) {
				Xlm.U.curl_QST(ir, Q, S, T);
				SHV3_SPAT(Q, S, T, w.r, w.t, w.p);
				if (U0) {
					nl_status_U += U0->V0_cross_w(ir, nlu.r,nlu.t,nlu.p, w.r,w.t,w.p, nl_status_U);		// nlu += U0 x w
				}
				if (spat_need & NEED_UXW) {
					cross_prod_condadd(nlu, v, w, 0,nspat, nl_status_U);		// nlu += u x w
					nl_status_U = 1;
				}
			}
			#ifdef XS_ADJUST_DT
			if ( (calc_cfl) && (spat_need & (NEED_UGC | NEED_UGT | NEED_UXB | NEED_UXW)) ) {	// transport term => CFL condition
				double vr2 = absmax2(v.r, 0,nspat);
				double vh2 = absmax2(v.t, v.p, 0,nspat);
				if (vr2 > vcfl[cfl_ur]) vcfl[cfl_ur] = vr2;
				if (vh2 > vcfl[cfl_uh]) vcfl[cfl_uh] = vh2;
			}
			#endif
			if (shell_ubt & (EVOL_T|EVOL_C)) {		// BUOYANCY
				if ( ((shell_ubt & (EVOL_T | EVOL_C)) == (EVOL_T | EVOL_C)) 
				  && ((spat_need & (NEED_T | NEED_C)) == (NEED_T | NEED_C))) {
					const cplx* a = Xlm.T[ir];		const cplx* b = Xlm.C[ir];
					for (int lm=0; lm<NLM; lm++)
						Q[lm] = a[lm] + b[lm];
					SH_SPAT(Q, w.r);		// only one transform for both fields
					nl_status_U += gradPhi0->V0_times_s(ir, nlu.r,nlu.t,nlu.p, w.r, nl_status_U);	// nlu += (T+C).grad(Phi0)
				} else
				if ((shell_ubt & EVOL_T) && (spat_need & NEED_T)) {		// dynamic T domain
					SH_SPAT(Xlm.T[ir], w.r);
					nl_status_U += gradPhi0->V0_times_s(ir, nlu.r,nlu.t,nlu.p, w.r, nl_status_U);	// nlu += T.grad(Phi0)
				} else
				if ((shell_ubt & EVOL_C) && (spat_need & NEED_C)) {		// dynamic C domain
					SH_SPAT(Xlm.C[ir], w.r);
					nl_status_U += gradPhi0->V0_times_s(ir, nlu.r,nlu.t,nlu.p, w.r, nl_status_U);	// nlu += C.grad(Phi0)
				}
			}
		}

		// TEMPERATURE EQUATION //
		if (shell_ubt & EVOL_T) {
			int nl_status_T = 0;
			if (spat_need & NEED_GT) {		// spatial grad(T)
				Xlm.T.Gradr(ir, Q, S);
				SH_SPAT(Q, w.r);		// not always needed.
				SHS_SPAT(S, w.t, w.p);
				if (U0)
					nl_status_T = U0->V0_dot_w(ir, nltemp, w.r,w.t,w.p, nl_status_T);		// w.r = U0.grad(T)
			}
			if (shell_ubt & EVOL_U) {
				if (spat_need & NEED_UGT) {
					if (nl_status_T) {
						for (size_t k=0; k<nspat; k++)
							nltemp[k] += v.r[k]*w.r[k] + v.t[k]*w.t[k] + v.p[k]*w.p[k];		// nltemp = u.grad(T)
					} else {
						for (size_t k=0; k<nspat; k++)
							nltemp[k] = v.r[k]*w.r[k] + v.t[k]*w.t[k] + v.p[k]*w.p[k];		// nltemp = u.grad(T)
						nl_status_T = 1;
					}
				}
				if (gradT0) {	// u.grad(T0)
					nl_status_T += gradT0->V0_dot_w(ir, nltemp, v.r,v.t,v.p, nl_status_T);				// nltemp = u.grad(T0)
				}
			}
			if (nl_status_T) {
				SPAT_SH(nltemp, NL.T[ir]);
			} else NL.T.zero_out_shell(ir);
		}

		// CONCENTRATION EQUATION //
		if (shell_ubt & EVOL_C) {
			int nl_status_C = 0;
			if (spat_need & NEED_GC) {		// spatial grad(C)
				Xlm.C.Gradr(ir, Q, S);
				SH_SPAT(Q, w.r);		// not always needed.
				SHS_SPAT(S, w.t, w.p);
				if (U0)
					nl_status_C = U0->V0_dot_w(ir, nltemp, w.r,w.t,w.p, nl_status_C);		// w.r = U0.grad(C)
			}
			if (shell_ubt & EVOL_U) {
				if (spat_need & NEED_UGC) {
					if (nl_status_C) {
						for (size_t k=0; k<nspat; k++)
							nltemp[k] += v.r[k]*w.r[k] + v.t[k]*w.t[k] + v.p[k]*w.p[k];		// nltemp = u.grad(C)
					} else {
						for (size_t k=0; k<nspat; k++)
							nltemp[k] = v.r[k]*w.r[k] + v.t[k]*w.t[k] + v.p[k]*w.p[k];		// nltemp = u.grad(C)
						nl_status_C = 1;
					}
				}
				if (gradC0) {	// u.grad(C0)
					nl_status_C += gradC0->V0_dot_w(ir, nltemp, v.r,v.t,v.p, nl_status_C);				// nltemp = u.grad(C0)
				}
			}
			if (nl_status_C) {
				SPAT_SH(nltemp, NL.C[ir]);
			} else NL.C.zero_out_shell(ir);
		}

		if (shell_ubt & EVOL_B) {		// dynamic b domain
			int nl_status_B = 0;
			// INDUCTION EQUATION
			if (spat_need & NEED_B) {
				Xlm.B.RadSph(ir, Q, S);
				SHV3_SPAT(Q, S, Xlm.B.Tor[ir], w.r, w.t, w.p);		// overwrite w with spatial B
				if (U0) {
					nl_status_B = U0->V0_cross_w(ir, nlb.r,nlb.t,nlb.p, w.r,w.t,w.p, nl_status_B);		// nlb += U0 x b
				}
				#ifdef XS_MEAN_FIELD
					alpha_effect_add(nlb, alpha(ir,0), w, nl_status_B);
					nl_status_B = 1;
				#endif
			}
			if (shell_ubt & EVOL_U) {
				if (spat_need & NEED_UXB)  {
					cross_prod_condadd(nlb, v, w, 0,nspat, nl_status_B);		// nlb += u x b
					nl_status_B = 1;
				}
				if (B0) {
					nl_status_B = B0->w_cross_V0(ir, nlb.r,nlb.t,nlb.p, v.r,v.t,v.p, nl_status_B);	// nlb = u x B0
				}
			}
			if (nl_status_B) {
				SPAT_SHV3(nlb.r, nlb.t, nlb.p, NL.B.Tor[ir], NL.Sb->get_data(0,ir), NL.B.Pol[ir]);
			} else {
				NL.B.zero_out_shell(ir);		NL.Sb->zero_out_shell(ir);
			}
			// LORENTZ FORCE
			if (shell_ubt & EVOL_U) {
				if (J0) {
					nl_status_U += J0->V0_cross_w(ir, nlu.r,nlu.t,nlu.p, w.r,w.t,w.p, nl_status_U);		// nlu += J0 x b
					#ifdef XS_ADJUST_DT
						if (calc_cfl) J0->absmax2(ir, vcfl[cfl_j]);
					#endif
					if (J0xB0) {
						nl_status_U += J0xB0->add_V0(ir, nlu.r,nlu.t,nlu.p, nl_status_U);		// nlu += J0 x B0
					}
				}
				if (spat_need & NEED_J) {		// spatial j
					Xlm.B.curl_QST(ir, Q, S, T);
					SHV3_SPAT(Q, S, T, v.r, v.t, v.p);		// overwrite v with spatial J
					if (B0) {
						nl_status_U += B0->w_cross_V0(ir, nlu.r,nlu.t,nlu.p, v.r,v.t,v.p, nl_status_U);	// nlu += j x B0
						#ifdef XS_ADJUST_DT
							if (calc_cfl) B0->absmax2(ir, vcfl[cfl_br], vcfl[cfl_bh]);
						#endif
					}
					if (spat_need & NEED_JXB) {
						cross_prod_condadd(nlu, v, w, 0,nspat, nl_status_U);		// nlu += j x b
						nl_status_U = 1;
						#ifdef XS_ADJUST_DT
						if ((calc_cfl) && (spat_need & (NEED_UXB)) ) {		// possibly dynamic Alfvén waves => CFL on B
							double b2r = absmax2(w.r, 0,nspat);
							double b2h = absmax2(w.t, w.p, 0,nspat);
							if (b2r > vcfl[cfl_br]) vcfl[cfl_br] = b2r;
							if (b2h > vcfl[cfl_bh]) vcfl[cfl_bh] = b2h;
						}
						#endif
					}
				}
			}
		}
		if (shell_ubt & EVOL_U) {
			if (nl_status_U) {
				SPAT_SHV3(nlu.r, nlu.t, nlu.p, NL.U.Tor[ir], NL.Su->get_data(0,ir), NL.U.Pol[ir]);
			} else {
				NL.U.zero_out_shell(ir);		NL.Su->zero_out_shell(ir);
			}
		}
		#ifdef XS_ADJUST_DT
		if (calc_cfl) {
			if (U0) {
				double vr2, vh2;
				U0->absmax2(ir, vr2, vh2);
				if (vr2 > vcfl[cfl_ur]) vcfl[cfl_ur] = vr2;
				if (vh2 > vcfl[cfl_uh]) vcfl[cfl_uh] = vh2;
			}
			compute_local_cfl(ir, vcfl);
		}
		#endif
	}

	if (evol_ubt & (EVOL_B | EVOL_T | EVOL_C)) {			// SOLID SHELLS //
		if ((InnerCore.Omega_x != 0) || (InnerCore.Omega_y != 0))
			NLb_istart = NL.B.ir_bci;	// spatial terms for B span also inner-core
		if ((Mantle.Omega_x != 0) || (Mantle.Omega_y != 0))
			NLb_iend = NL.B.ir_bco;		// spatial terms for B span also mantle
		if (InnerCore.irs <= InnerCore.ire)
			spatial_terms_SolidBody(InnerCore, NL, Xlm,  Q, S, nlb.r, nlb.t, nlb.p);
		if (Mantle.irs <= Mantle.ire)
			spatial_terms_SolidBody(Mantle, NL, Xlm,  Q, S, nlb.r, nlb.t, nlb.p);
	}
  }
  else NL.zero_out();
	#pragma omp barrier

	if (evol_ubt & EVOL_U) {
		NL.U.curl_from_TQS(NL.Su, Ulm.ir_bci, Ulm.ir_bco, ::U);		// includes MPI sync with tag ::U
		#pragma omp barrier
		#ifndef XS_SPARSE
		if ( ((spat_need & NEED_COR) == 0) && (frame.Omega_z != 0.0) ) {		// check if Coriolis is needed.
			Coriolis_force_add(NL.U, Xlm.U, frame.Omega_z);
			#pragma omp barrier
		}
		if (Grav0_r) {
			Radial_gravity_buoyancy(NL.U, Xlm.T, Xlm.C, Grav0_r);		// add buoyancy due to central gravity.
			#pragma omp barrier
		}
		#endif
		frame.add_Poincare_force(NL.U);
		#ifdef XS_BULK_FORCING
			bulk_forcing.add_forcing(NL, ftime);
		#endif
	}
	if (evol_ubt & EVOL_B) {
		NL.B.curl_from_TQS(NL.Sb, NLb_istart, NLb_iend, ::B);		// includes MPI sync with tag ::B
	}
	#ifndef XS_SPARSE
	if (evol_ubt & EVOL_T) {
		if ((T0lm) && (T0lm->lmax == 0)) {	// for l=0 imposed profile, no spherical harmonic transforms needed.
			#pragma omp for schedule(FOR_SCHED_SH) nowait
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
				double dT0dr_r = real(T0lm->TdT[ir*2+1]) * r_1[ir] * (1.0/Y00_1);
				if (r[ir]==0.0) dT0dr_r = (2.0/Y00_1) * ( real(T0lm->TdT[(ir+1)*2]) - real(T0lm->TdT[ir*2]) ) * (r_1[ir+1]*r_1[ir+1]);		// compute 1/r*dT/dr for r=0,l=0 as 2*(T(dr)-T(0))/dr^2
				for (int lm=0; lm<NLM; lm++) {
					NL.T[ir][lm] += l2[lm]*Xlm.U.Pol[ir][lm] * dT0dr_r;		// ur.dT0/dr
				}
			}
		}
	}
	if (evol_ubt & EVOL_C) {
		if ((C0lm) && (C0lm->lmax == 0)) {	// for l=0 imposed profile, no spherical harmonic transforms needed.
			#pragma omp for schedule(FOR_SCHED_SH) nowait
			for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
				double dT0dr_r = real(C0lm->TdT[ir*2+1]) * r_1[ir] * (1.0/Y00_1);
				if (r[ir]==0.0) dT0dr_r = (2.0/Y00_1) * ( real(C0lm->TdT[(ir+1)*2]) - real(C0lm->TdT[ir*2]) ) * (r_1[ir+1]*r_1[ir+1]);		// compute 1/r*dC/dr for r=0,l=0 as 2*(C(dr)-C(0))/dr^2
				for (int lm=0; lm<=NLM; lm++) {
					NL.C[ir][lm] += l2[lm]*Xlm.U.Pol[ir][lm] * dT0dr_r;		// ur.dC0/dr
				}
			}
		}
	}
	#endif
	barrier_shared_mem();
}

#endif

/// apply the linear operator
void LinSolver::apply_M(StateVector& Y, const StateVector& X)
{
	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	for (int op=0; op<nop3; op++) {
		int ic = op3[op].ic;
		op3[op].M->apply(X.get_comp(ic), Y.get_comp(ic), lm0, lm1);
	}
	for (int op=0; op<nop5; op++) {
		int ic = op5[op].ic;
		op5[op].M->apply(X.get_comp(ic), Y.get_comp(ic), lm0, lm1);
	}
	#ifdef XS_SPARSE
	xs_sparse::apply_M(Y, X);
	#endif

	barrier_shared_mem();
}

/// compute the linear right hand side term
void LinSolver::apply_Mrhs(StateVector& Y, const StateVector& X)
{
	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	for (int op=0; op<nop3; op++) {
		int ic = op3[op].ic;
		op3[op].Mrhs->apply(X.get_comp(ic), Y.get_comp(ic), lm0, lm1);
	}
	for (int op=0; op<nop5; op++) {
		int ic = op5[op].ic;
		op5[op].Mrhs->apply(X.get_comp(ic), Y.get_comp(ic), lm0, lm1);
	}
	#ifdef XS_SPARSE
	xs_sparse::apply_Mrhs(Y, X);
	#endif

	barrier_shared_mem();
}

/// returns the actual number of explicit rhs evaluations.
long step_UBT(double delta_t, StateVectorAverage* Xavg=0, StateVector* dXdt=0, int kill_sbr=0)
{
	long ex_evals = 0;
	#ifdef XS_DEBUG
		PRINTF0("  Omega : IC = (%g, %g, %g) \t M = (%g, %g, %g)\n", InnerCore.Omega_x, InnerCore.Omega_y, InnerCore.Omega_z, Mantle.Omega_x, Mantle.Omega_y, Mantle.Omega_z);
	#endif

	int istep = 0;
	do {
		#ifndef XS_MPI
		if (SAVE_QUIT & 4) save_quit();
		#endif
		for (int k=0; k<MAX_CFL; k++) dt_cfl[k] = 0.0;

		stepper->step_1_start_explicit_terms_cfl();
		stepper->step_2_adjust_dt(delta_t, istep);
		if (Xavg) Xavg->update_average(Xlm,stepper->dt);
		if (dXdt && istep==1) {
			printf("time_derivative: istep=%d, dt=%g : copy state at time %g\n",istep, stepper->dt, (double) ftime);
			dXdt->copy(Xlm);	// copy state vector to compute derivative after step is finished
		}
		stepper->step_3_end(kill_sbr);

		delta_t -= stepper->dt;
		ex_evals += stepper->nex_eval;
	} while (--istep > 0);
	return ex_evals;
}

/// signal handler for saving/aborting during run.
void sig_handler(int sig_num)
{
	switch(sig_num) {
	  #ifndef XS_MPI
		case 15 : SAVE_QUIT = 4; break;	// TERM signal : save state as soon as possible !
	  #endif
		//case 30 : SAVE_QUIT = 1; break;	// Save snapshot at next iter.
		//case 31 : SAVE_QUIT = 2;			// Save & quit
		default:  SAVE_QUIT = sig_num - 29;
	}
}

void nrj_and_summed_diagnostics(FILE* &fnrj, diagnostics& nrj, double treal, long nex, const char* par_str=0)
{
	static SpectralDiags sdU(LMAX,MMAX);
	static SpectralDiags sdB(LMAX,MMAX);
	static SpectralDiags sdT(LMAX,MMAX);
	static SpectralDiags sdC(LMAX,MMAX);
	double rspec_max = 0.0;

	double nrjUo = 0, nrjBo = 0;
	if (nrj.size() >= 2) {
		nrjUo = nrj[0];		nrjBo = nrj[1];
	}

	nrj.reset();		// the energies of the 3 fields.
	nrj.append(4, "% t\t Eu, Eb, Et, Ec\t dt\t ");
	if (evol_ubt & EVOL_U) { Ulm.energy(sdU);  nrj[0] = sdU.energy(); }
	if (evol_ubt & EVOL_B) { Blm.energy(sdB);  nrj[1] = sdB.energy(); }
	if (evol_ubt & EVOL_T) { Tlm.energy(sdT);  nrj[2] = sdT.energy(); }
	if (evol_ubt & EVOL_C) { Clm.energy(sdC);  nrj[3] = sdC.energy(); }
  #ifdef XS_CUSTOM_DIAGNOSTICS
	custom_diagnostic(nrj);		// call custom_diagnostic() (defined in xshells.hpp)
  #endif
	// record spectral lengths
	if (evol_ubt & EVOL_U) { double* lel = nrj.append(2, "lEu(l) lEur(l) ");		lel[0] = sdU.lEl(LMAX);  lel[1] = sdU.lErl(LMAX);  }
	if (evol_ubt & EVOL_B) { double* lel = nrj.append(2, "lEb(l) lEbr(l) ");		lel[0] = sdB.lEl(LMAX);  lel[1] = sdB.lErl(LMAX);  }
	if (evol_ubt & EVOL_T) { double* lel = nrj.append(1, "lEt(l) ");				lel[0] = sdT.lEl(LMAX);  }
	if (evol_ubt & EVOL_C) { double* lel = nrj.append(1, "lEc(l) ");				lel[0] = sdC.lEl(LMAX);  }
  #ifdef XS_MPI
	MPI_Reduce((i_mpi==0) ? MPI_IN_PLACE : &nrj[0], &nrj[0] , nrj.size(), MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);		// sum all contributions.
  #endif

	double* rspec = nrj.append(8, " Sconv_U_l Sconv_U_m Sconv_B_l Sconv_B_m Sconv_T_l Sconv_T_m Sconv_C_l Sconv_C_m \t ");
	rspec[0] = sdU.Sconv[0];		rspec[1] = sdU.Sconv[1];
	rspec[2] = sdB.Sconv[0];		rspec[3] = sdB.Sconv[1];
	rspec[4] = sdT.Sconv[0];		rspec[5] = sdT.Sconv[1];
	rspec[6] = sdC.Sconv[0];		rspec[7] = sdC.Sconv[1];
//	printf("Sconv l = %g, %g, %g\n", sdU.Sconv[0],sdT.Sconv[0],sdB.Sconv[0]);
//	printf("Sconv m = %g, %g, %g\n", sdU.Sconv[1],sdT.Sconv[1],sdB.Sconv[1]);
  #ifdef XS_MPI
	MPI_Reduce((i_mpi==0) ? MPI_IN_PLACE : rspec, rspec, 8, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);		// max Sconv
  #endif

	if (i_mpi==0) {
		double gru=0, grb=0;
	  #ifdef XS_LINEAR
		int print_gr = 1;	// always print growth rate in linear mode
		const double dt_1 = 1.0/(2.*dt_log);
		if (evol_ubt & EVOL_U) {
			double* diags = nrj.append(1, " du_udt");
			if (nrjUo > 0.0) gru = log(nrj[0]/nrjUo) * dt_1;
			diags[0] = gru;
		}
		if (evol_ubt & EVOL_B) {
			double* diags = nrj.append(1, " db_bdt");
			if (nrjBo > 0.0) grb = log(nrj[1]/nrjBo) * dt_1;
			diags[0] = grb;
		}
	  #else
		int print_gr = 0;
		if ((jpar.no_jxb) && (evol_ubt & EVOL_B)) {
			double* diags = nrj.append(1, " db_bdt");
			double grb = log(nrj[1]/nrjBo)/(2.*dt_log);
			diags[0] = grb;
			print_gr = 1;
		}
	  #endif
		if (jpar.debug_no_write<=1) {
			if (fnrj == 0) {
				std::string s = "energy." + std::string(jpar.job);
				fnrj = fopen(s.c_str(),"w");
			}
			if (par_str) {
				fprintf(fnrj, "%s", nrj.header.c_str());
				fprintf(fnrj, "\n%%PAR%% %s", par_str);
			}
			fprintf(fnrj,"\n%.10g\t %.10g %.10g %.10g %.10g\t %.10g\t",(double)ftime, nrj[0], nrj[1], nrj[2], nrj[3], stepper->dt);		// record energies.
			for (int k=4; k<nrj.size(); k++) fprintf(fnrj," %.10g", nrj[k]);		// record custom diags (if any).
			fflush(fnrj);
		}
		for (int k=0; k<8; k++)   if (rspec_max < rspec[k]) rspec_max = rspec[k];		// maximum of spectral convergence
		printf("[it %d] t=%.10g, Eu=%g, Eb=%g, Et=%g, dt=%g  [Sconv=%g] (elapsed %g s for %ld evals)\n",iter,(double)ftime,nrj[0], nrj[1], nrj[2], stepper->dt, rspec_max, treal, nex);
		if (print_gr) printf("        growth rate  u:%g  b:%g\n", gru, grb);
	    if (rspec_max > 0.01) {
			if (rspec_max > 0.1) {
				printf(COLOR_ERR "  !!! WARNING, bad spectral convergence !!! " COLOR_END "\n");
			} else {
				printf(COLOR_WRN "  Warning, low spectral convergence. " COLOR_END "\n");
			}
		}
		fflush(stdout);
	}
	if UNLIKELY( isNaN(nrj[0]+nrj[1]+nrj[2]) ) runerr("NaN encountered");
	#ifndef XS_LINEAR
	if UNLIKELY((rspec_max >= jpar.sconv_stop)&&(iter > 0)) runerr("No spectral convergence. Increase resolution or decrease time-step or CFL factors.");
	#endif
}


int main (int argc, char *argv[])
{
	FILE *fnrj = NULL;
	diagnostics nrj;
	double rmin, rmax;
	double tmp, b0, u0, gmax;
	char* job;
	int i;
	int iter_max, iter_save;
	int kill_sbr;
	long nbackup = 0;
	char command[100];

//	feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);		// turn this on to track NaNs or other floating-point exceptions (requires gcc, #define _GNU_SOURCE and #include <fenv.h>)

#ifdef XS_MPI
  #ifndef _OPENMP
	MPI_Init(&argc, &argv);
  #else
	MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &i);		// init MPI with some thread support.
	if (i < MPI_THREAD_FUNNELED) runerr("MPI does not support multiple threads.");
  #endif
	MPI_Comm_rank(MPI_COMM_WORLD, &i_mpi);
	MPI_Comm_size(MPI_COMM_WORLD, &n_mpi);
#endif

	PRINTF0("[XSHELLS " XS_VER "] eXtendable Spherical Harmonic Earth-Like Liquid Simulator\n          by Nathanael Schaeffer / CNRS, build %s, %s\n",__DATE__,__TIME__);
#ifdef XS_LINEAR
	PRINTF0("          ++ LINEAR,");
#else
	PRINTF0("          ++ Non-Linear,");
#endif
#ifdef XS_SPARSE
	PRINTF0(" Implicit Coriolis,");
	Eigen::initParallel();		// in order to call eigen from multiple threads.
#endif
	PRINTF0(" %ld bit version, id: " _HGID_ "\n",8*sizeof(void *));
#ifdef _OPENMP
	omp_set_dynamic(0);			// disable dynamic adjustment of the number of threads (use fixed number of threads).
	nthreads = omp_get_max_threads();		// number of threads.
	#if XS_OMP==1 && defined(XS_MKL)
		fftw3_mkl.number_of_user_threads = nthreads;		// required to call the fft of mkl from multiple threads.
	#endif
#endif
	PRINTF0("          ++ XSBIG using %d processes, with %d threads per process.\n", n_mpi, nthreads);
#if XS_DEBUG > 1
	PRINTF0(COLOR_ERR "WARNING! DEBUG MODE ON. Disk will be flooded by one debug_*.txt file per process." COLOR_END "\n");
#endif

	xsio_par_string = read_Par(argc, argv);
	job = jpar.job;
	double dt = jpar.dt;
	iter_max = jpar.iter_max;
	iter_save = jpar.iter_save;
	nbackup = jpar.nbackup;
	dt_log = 2.*dt*jpar.modulo;		// time interval between logs (legacy)
	kill_sbr = jpar.kill_sbr;
	xsio_debug_no_write = jpar.debug_no_write;
	xsio_allow_interp = jpar.allow_interp;
#ifndef XS_WRITE_SV
	if (jpar.lmax_out_sv) runerr("Surface data output not compiled.");
#endif
#ifndef XS_HYPER_DIFF
	if (jpar.hyper_diff_l0 > 0) runerr("Hyper-diffusivity not compiled.");
#endif

	for (int f=0; f<MAXFIELD; f++)	if (jpar.ffile[f]) evol_ubt |= EVOL(f);
	if (jpar.rfile) {		// load external grid
		if ( load_Field_grid(jpar.rfile) || load_grid(jpar.rfile) ) jpar.rfile = 0;		//	we have now handled this, no need to worry about it later.
	}
	if (evol_ubt == 0)	runerr("Nothing to do ?! at least u, b or tp must be defined.");
	#ifndef XS_MEAN_FIELD
	if ((jpar.ffile[U] == NULL) && (jpar.f0file[U] == NULL)) PRINTF0(COLOR_WRN "!!! WARNING !!! No velocity field defined: solve the diffusion analytically ;-)" COLOR_END "\n");
	#endif

	if (n_mpi > NR) runerr("Too many MPI processes. MPI processes cannot exceed NR (radial shell number).");
#if XS_OMP == 2
	if (nthreads > MMAX+1) runerr("Thread number cannot exceed Mmax+1. Use less threads or xsbig_hyb instead.");
	if (nthreads>1) {
		size_t l2m = (LMAX+1)*(LMAX+1)*(MMAX+1);
		if (l2m < 64*64*64*nthreads) PRINTF0(COLOR_WRN "!!! WARNING !!! innefficient multi-threaded SHT due to low spherical harmonic resolution. Use less threads or xsbig_hyb instead. " COLOR_END "\n");
//		if (l2m < 32*32*32*nthreads) runerr("too low spherical harmonic resolution for multi-threaded SHT. Use less threads or xsbig_hyb instead.");
	}
	if ((NR >= n_mpi*nthreads*4) || (NR % (n_mpi*nthreads) == 0)) PRINTF0(COLOR_WRN "!!! WARNING !!! xsbig_hyb should be faster in this case ! " COLOR_END "\n");
#else
	if (NR % (n_mpi*nthreads) != 0) {		// load imbalance
		if (NR < n_mpi*nthreads*5) PRINTF0(COLOR_WRN "!! WARNING !! optimal usage requires radial shells being a multiple of total threads. " COLOR_END "\n");
		if (n_mpi*nthreads > 4*NR) runerr("Too many threads and processes. Reduce number of threads or processes, or try xsbig_hyb2 instead.");
	}
#endif

	#ifndef XS_LINEAR
	if ((jpar.parity & 1) && (jpar.no_ugradu == 0)) runerr("Odd parity not conserved by u.grad(u)");
	#endif

	init_sh(jpar.sht_type, jpar.polar_opt_max, NL_ORDER);
	if (nthreads > (NLM+3)/4) runerr("Too many threads. Reduce number of threads, or increase spherical harmonic resolution.");
	#ifdef XS_VEC
		if (NLAT & (VSIZE-1)) runerr("NLAT must be a multiple of vector size !");
	#endif

	if (jpar.framecmd) {
		init_Frame(frame, jpar.framecmd, jpar.Omega0);
	} else {
		frame.set_rotation(jpar.Omega0, jpar.Omega0_angle);
	}
	if (frame.check_non_axial()) {
		#ifndef XS_LINEAR
		if (jpar.no_ugradu != 0) runerr("Non-axial global rotation not supported without u.grad(u). Use XS_LINEAR mode instead.\n");
		#endif
	}

	rmin = jpar.Rbs;		rmax = jpar.Rbe;
	if (rmin > jpar.Rus)	rmin = jpar.Rus;
	if (rmax < jpar.Rue)	rmax = jpar.Rue;
	if (rmin > jpar.Rts)	rmin = jpar.Rts;
	if (rmax < jpar.Rte)	rmax = jpar.Rte;
	if (jpar.rfile) {		// grid name
		if (strcmp(jpar.rfile,"regular") == 0) {
			Reg_Grid(rmin, jpar.Rus, jpar.Rue, rmax);
		} else {
			sprintf(command, "grid '%s' not found!", jpar.rfile);		runerr(command);
		}
	}
	init_rad_sph(rmin, jpar.Rus, jpar.Rue, rmax, jpar.Nin, jpar.Nout);
	#ifdef VAR_LTR
	init_ltr(jpar.rsat_ltr);
	#else
	if (jpar.rsat_ltr != 0.0) runerr("rsat_ltr != 0 needs variable l-truncation (VAR_LTR).");
	if (jpar.Rus == 0.) runerr("Full-sphere calculation requires compilation with '#define VAR_LTR 0.5' in the xshells.hpp file.\n");
	#endif

	srand( time(NULL) );		// we might need random numbers.
// set field limits :
	Blm.ir_bci = r_to_idx(jpar.Rbs);		Blm.ir_bco = r_to_idx(jpar.Rbe);
	Tlm.ir_bci = r_to_idx(jpar.Rts);		Tlm.ir_bco = r_to_idx(jpar.Rte);
	Clm.ir_bci = r_to_idx(jpar.Rcs);		Clm.ir_bco = r_to_idx(jpar.Rce);

/* PRINT GRID */
	if (i_mpi == 0) {
		printf("[Grid] NR=%d => NG=%d, NM=%d, [rmin,rg,rm,rmax]=[%.4f, %.4f, %.4f, %.4f]\n       [dr(rmin), dr(rg), dr((NG+NM)/2), dr(rm), dr(rmax)]=[%.5e, %.5e, %.5e, %.5e, %.5e]\n", NR, NG,NM, r[0], r[NG], r[NM], r[NR-1], r[1]-r[0], r[NG+1]-r[NG], r[(NG+NM)/2+1]-r[(NG+NM)/2], r[NM]-r[NM-1], r[NR-1]-r[NR-2]);
		if ((r[0] != rmin)||(r[NR-1] != rmax)) {
			printf(COLOR_WRN "    !! Warning : rmin or rmax have changed ! " COLOR_END "\n");
			if ((r[0] > rmin) || (r[NR-1] < rmax)) runerr("the grid does not cover the whole domain");
		}
	}
#ifdef XS_MPI
	distribute_shells(NG, NM, jpar.shared_mem);
	MPI_Barrier(MPI_COMM_WORLD);
#endif
// set solid body limits AFTER calling distribute_shells():
	InnerCore.set_limits(0, NG);
	Mantle.set_limits(NM, NR-1);

/* MEMORY ALLOCATION AND MATRIX INITIALIZATION */
	if (evol_ubt & EVOL_U) {
		Ulm.alloc(NG, NM, 2, 1);		// 2 ghost shells for poloidal here + allow MPI-3 shared memory if possible
		if (r[Ulm.ir_bci] == 0)	jpar.uBCin = BC_FREE_SLIP;		// mark as free-slip if the inner shell is at r=0 !
		Ulm.bci = (boundary) jpar.uBCin;	Ulm.bco = (boundary) jpar.uBCout;
		init_Umatrix(Ulm, jpar.nu, jpar.hyper_diff_l0, mp.var["hyper_nu"], jpar.Inertia_i, jpar.Inertia_o);			diffusivity[::U] = jpar.nu;
		InnerCore.set_inertia_moment(jpar.Inertia_i, r[NG]);
		Mantle.set_inertia_moment(jpar.Inertia_o, r[NM]);
		// don't conserve angular momentum if one of the boundaries is not free-slip :
		if ((Ulm.bco != BC_FREE_SLIP && Mantle.freely_rotate==0) || (r[Ulm.ir_bci]>0. && Ulm.bci!=BC_FREE_SLIP && InnerCore.freely_rotate==0)) kill_sbr = 0;
		StateVector::Su = new Spectral(1, NLM, Ulm.ir_bci, Ulm.ir_bco);		// shared storage for non-linear terms before curl
	} else kill_sbr = 0;
	if (evol_ubt & EVOL_B) {
		Blm.alloc(Blm.ir_bci, Blm.ir_bco, 1, 1);		// 1 ghost shell is enough here + allow MPI-3 shared memory if possible
		Blm.bci = (boundary) jpar.bBCin;	Blm.bco = (boundary) jpar.bBCout;
		if ((Blm.bci != BC_MAGNETIC) || (Blm.bco != BC_MAGNETIC)) {
			PRINTF0(COLOR_WRN "!!! WARNING magnetic field has non-standard boundary conditions. Did you mean it? " COLOR_END "\n");
		}
		init_Bmatrix(Blm, jpar.eta, jpar.hyper_diff_l0, mp.var["hyper_eta"]);		diffusivity[::B] = jpar.eta;
		StateVector::Sb = new Spectral(1, NLM, Blm.ir_bci, Blm.ir_bco);		// shared storage for non-linear terms before curl
	}
	if (evol_ubt & EVOL_T) {
		Tlm.alloc(Tlm.ir_bci, Tlm.ir_bco, 1, 1);	//  allow MPI-3 shared memory if possible
		Tlm.bci = (boundary) jpar.tBCin;	Tlm.bco = (boundary) jpar.tBCout;
		init_Tmatrix(Tlm, MT, jpar.kappa, jpar.hyper_diff_l0, mp.var["hyper_kappa"], mp.var["Biot_Ti"], mp.var["Biot_To"]);		diffusivity[::T] = jpar.kappa;
	}
	if (evol_ubt & EVOL_C) {
		Clm.alloc(Clm.ir_bci, Clm.ir_bco, 1, 1);	//  allow MPI-3 shared memory if possible
		Clm.bci = (boundary) jpar.cBCin;	Clm.bco = (boundary) jpar.cBCout;
		init_Tmatrix(Clm, MC, jpar.kappa_c, jpar.hyper_diff_l0, mp.var["hyper_kappa_c"], mp.var["Biot_Ci"], mp.var["Biot_Co"]);		diffusivity[::C] = jpar.kappa_c;
	}
	Xlm.commit();
	if (jpar.make_movie == 2) Xavg = new StateVectorAverage(Xlm);		// alloc state-vector to store the time-averages...
	if (jpar.make_time_derivative == 1) {
		dXdt = new StateVector;		// alloc state-vector to store the time derivative
		dXdt->clone(Xlm);
	}

/* create time-stepper instance */
	stepper = create_stepper(jpar.stepper_name, jpar.dt, jpar.dt_adjust);
	stepper->init(Xlm);
	stepper->calc_matrices(jpar.dt);

/* CHOOSE MPI BLOCK SIZE FOR LINEAR SOLVER */
	#ifndef XS_MPI_ALL2ALL
	stepper->optimize_linear_solver();
	#endif

/* TRY TO FIND RESTART DATA */
	if (jpar.allow_restart) {
		const char *sfx[] = {"_kill","_back",""};		// suffix to look for.
		char fname[240], ftest[240];
		int it_file = 0;
		int found = 0;
		char c;
		for (int f=MAXFIELD-1; f>=0; f--) {
			c=symbol[f];
			if (jpar.ffile[f]) break;
		}
		for (int k=0; k<3; k++) {
			sprintf(ftest, "field%c%s.%s",c,sfx[k],job);
			iter_from_file(ftest, &it_file);
			#ifdef XS_DEBUG
			PRINTF0("probing '%s' : it = %d\n",ftest, it_file);
			#endif
			if (it_file > iter) {
				strncpy(fname,  ftest, 239);
				iter = it_file;
				found = 1;
			}
		}
		if (found) {	// found.
			PRINTF0("=> restarting from iter=%d (%s)\n", iter, fname);
			if (iter == iter_max) runerr("job already finished.");
			for (int f=0; f<MAXFIELD; f++) {
				if (jpar.ffile[f]) { strncpy(jpar.ffile[f],  fname, 239);	 jpar.ffile[f][5] = symbol[f]; }
			}
			if ((i_mpi==0) && (jpar.debug_no_write<=1)) {
				sprintf(command, "energy.%s",job);		fnrj = fopen(command,"a");		// append to energy file.
				fprintf(fnrj, "\n");		// add new line.
			}
		}
	}

#ifdef XS_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif

/* INITIALIZATION OF FIELDS */

	/* INIT IMPOSED VELOCITY FIELD */
	u0 = 0.0;
	if (jpar.f0file[U]) {
		#ifndef XS_LINEAR
			runerr("base velocity field is supported only for linear calculations.");
		#else
		PRINTF0("=> Imposed velocity field U0 :\n");
		PolTor* Vlm = new PolTor(NG,NM);
		Vlm->bci = BC_NONE;		Vlm->bco = BC_NONE;
		load_init(jpar.f0file[U], Vlm);
		u0 = Vlm->absmax(NG, NM);
		if (u0 == 0) runerr("zero imposed field. did you mean it ?\n");
		SpectralDiags sd(LMAX,MMAX);
		Vlm->energy(sd);
		int ltr = LMAX;
		int mtr = MMAX;
		sd.get_lmax_mmax(ltr, mtr);
		sprintf(command,"fieldU0.%s",job); 	Vlm->save_double(command, ltr, mtr);
		U0 = new SpatVect;
		U0->from_SH(*Vlm);
		if (Vlm->zero_curl == 0) {
			W0 = new SpatVect;
			W0->from_SH(*Vlm,1);		// curl
		} else PRINTF0("   Velocity field has no vorticity\n");
		delete Vlm;
		#endif
	}
	if (evol_ubt & EVOL_U) {
		tmp = 0.5*(jpar.Rus+jpar.Rue)*jpar.a_forcing;		// as we do not know which boundary will force, we use average radius.
		if (fabs(tmp) > fabs(u0)) u0 = tmp;		// maximum imposed velocity.
	}

	/* INIT MAGNETIC FIELDS */
	b0 = 0.0;
	if (evol_ubt & EVOL_B) {
		Blm.zero_out();
		PRINTF0("=> Initial magnetic field b :\n");
		Blm.bco = BC_MAGNETIC;		// this selects magnetic fields in the field lists.
		ftime = load_init(jpar.ffile[B], &Blm);
		Blm.bci = (boundary) jpar.bBCin;		Blm.bco = (boundary) jpar.bBCout;		// reset the right BC for magnetic field.
		if (Blm.bci == BC_ZERO) Blm.zero_out_shell(Blm.ir_bci);	// ensure zero boundary conditions (unorthodox, normally BC_MAGNETIC)
		if (Blm.bco == BC_ZERO) Blm.zero_out_shell(Blm.ir_bco);
		if (Blm.bci == BC_MAGNETIC) Blm.zero_out_comp_shell(1, Blm.ir_bci);	// Toroidal field must be ZERO at insulator
		if (Blm.bco == BC_MAGNETIC) Blm.zero_out_comp_shell(1, Blm.ir_bco);	// Toroidal field must be ZERO at insulator
		if (own(Blm.ir_bci)) {
			InnerCore.set_external_B(Blm.Pol[Blm.ir_bci-1]);		// copy boundary condition for future evolution.
			if (InnerCore.PB0lm != NULL) printf("   + Potential field imposed at inner boundary (lmax=%d, mmax=%d).\n", InnerCore.lmax, MRES*InnerCore.mmax);
		}
		if (own(Blm.ir_bco)) {
			Mantle.set_external_B(   Blm.Pol[Blm.ir_bco+1]);
			if (Mantle.PB0lm != NULL)    printf("   + Potential field imposed at outer boundary (lmax=%d, mmax=%d).\n", Mantle.lmax, MRES*Mantle.mmax);
		}
		b0 = Blm.absmax(NG, NM);		// max value of magnetic field in fluid domain.

		#ifdef XS_WRITE_SV
		if (jpar.lmax_out_sv) {
			Bp1 = (cplx *) malloc(4*NLM * sizeof(cplx));
			if (Bp1==0) runerr("lmax_out_sv allocation error");
			Bp2 = Bp1 + NLM;
			Up1 = Bp1 + 2*NLM;		Ut1 = Bp1 + 3*NLM;
			memset(Bp1, 0, 4*NLM*sizeof(cplx));
		}
		#endif
		if (jpar.f0file[B]) {
			#ifndef XS_LINEAR
				runerr("base magnetic field supported only for linear calculations, use boundary conditions instead.");
			#else
			PRINTF0("=> Imposed magnetic field B0 :\n");
			PolTor* Vlm = new PolTor(Blm.ir_bci,Blm.ir_bco);
			Vlm->bci = BC_MAGNETIC;		Vlm->bco = BC_MAGNETIC;
			load_init(jpar.f0file[B], Vlm);
			b0 = Vlm->absmax(NG, NM);
			if (b0 == 0) runerr("zero imposed field. did you mean it ?\n");
			SpectralDiags sd(LMAX,MMAX);
			Vlm->energy(sd);
			int ltr = LMAX;
			int mtr = MMAX;
			sd.get_lmax_mmax(ltr, mtr);
			sprintf(command,"fieldB0.%s",job); 	Vlm->save_double(command, ltr, mtr);
			B0 = new SpatVect;
			B0->from_SH(*Vlm);
			if (Vlm->zero_curl == 0) {
				J0 = new SpatVect;
				J0->from_SH(*Vlm,1);		// curl
				if (jpar.nonlin && strstr(jpar.nonlin, "J0xB0")) {
					J0xB0 = new SpatVect;
					J0xB0->from_cross_prod(*J0, *B0);		// optional Lorentz-force driving.
				}
			} else PRINTF0("   Magnetic field is current-free\n");
			delete Vlm;
			#endif
		}
	}

	/* INIT TEMPERATURE FIELDS */
	gmax = 0.0;
	if (evol_ubt & EVOL_T) {
		if (jpar.phi0file) {		/* INIT IMPOSED GRAVITY */
			StatSpecScal *Phi0 = NULL;
			ScalarSH *Phi = new ScalarSH(0, NR-1);
			PRINTF0("=> Imposed gravity potential Phi0 :\n");
			load_init(jpar.phi0file, Phi);
			i = Phi->make_static(&Phi0);
			if (i == 0) runerr("Imposed potential field has 0 modes !");		// probably something is wrong...
			PRINTF0("   + Imposed potential field has %d modes (l<=%d, m<=%d).\n",i,Phi0->lmax,Phi0->mmax);
			sprintf(command,"fieldPhi0.%s",job); 	Phi->save_double(command, Phi0->lmax, Phi0->mmax/MRES);
			if (evol_ubt & EVOL_U) {
				if  (Phi0->lmax == 0) {
					Grav0_r = (double*) malloc(sizeof(double) * (NM-NG+1));
					if (Grav0_r==0) runerr("Grav0_r allocation error");
					Grav0_r -= NG;		// shift to access fluid shells.
					for (i=NG; i<=NM; i++) {
						Grav0_r[i] = Phi0->TdT[2*i+1].real() / (r[i] * Y00_1);		// dPhi/dr already computed.
						if (fabs(Grav0_r[i]*r[i]) > gmax) gmax = fabs(Grav0_r[i]*r[i]);		// record gmax
					}
					if (r[NG] == 0.0) {		// Gravity is zero, but Gravity/r is not. Compute it assuming dPhi0/dr = 0 because l=0
						int i = NG;
						Grav0_r[0] = (2.0/Y00_1) * ( Phi0->TdT[2*(i+1)].real() - Phi0->TdT[2*i].real() )/(r[i+1]*r[i+1]);		// 2*(T(dr)-T(0))/dr^2 
					}
					write_vect_npy("g_r.npy", &Grav0_r[NG], NM-NG+1);
				} else {
					Grav0_r = NULL;		// no central gravity.
					// we need a spatial gravity field...
					#ifdef XS_LINEAR
					gradPhi0 = new SpatVect;
					gradPhi0->from_SH(*Phi);
					gmax = gradPhi0->absmax2();
					#else
					gradPhi0 = new spat_vect[NR];
					size_t nr = Ulm.ire-Ulm.irs+1;		// only needed in the fluid.
					void* mem = VMALLOC(nr*3*sizeof(double)*shtns->nspat);
					if (mem==0) runerr("Not enough memory for gravity field.");
					cplx* Q = (cplx*) VMALLOC(2*NLM*sizeof(cplx));
					cplx* S = Q + NLM;
					for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
						mem = gradPhi0[ir].alloc(shtns->nspat, mem);		// allocate shells in the fluid
						Phi->Gradr(ir, Q, S);		// compute gradient
						SH_SPAT(Q, gradPhi0[ir].r);
						SHS_SPAT(S, gradPhi0[ir].t, gradPhi0[ir].p);
						double gr = absmax2(gradPhi0[ir].r, gradPhi0[ir].t, gradPhi0[ir].p, 0, shtns->nspat);
						if (gr > gmax) gmax = gr;
					}
					VFREE(Q);
					#endif
					gmax = sqrt(gmax);
				}
				#ifdef XS_MPI
				MPI_Allreduce( MPI_IN_PLACE, &gmax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD );
				#endif
				if (gmax == 0.0) runerr("zero gravity, something wrong ?");
			}
			free_StatSpecScal(Phi0);
			delete Phi;
		} else {
			Grav0_r = NULL;
			PRINTF0("=> No gravity field, temperature treated as a passive scalar.");
		}
		if (jpar.f0file[T]) {
			PRINTF0("=> Imposed Temperature field T0 :\n");
			load_init(jpar.f0file[T], &Tlm);
			i = Tlm.make_static(&T0lm);
			if (i == 0) runerr("Imposed temperature field has 0 modes !");		// probably something is wrong...
			#ifdef XS_LINEAR
			if (T0lm->lmax > 0) {
				gradT0 = new SpatVect;
				gradT0->from_SH(Tlm);
			}
			#endif
			PRINTF0("   + Imposed temperature field has %d modes (l<=%d, m<=%d).\n",i,T0lm->lmax,T0lm->mmax);
			sprintf(command,"fieldT0.%s",job); 	Tlm.save_double(command, T0lm->lmax, T0lm->mmax/MRES);
			Tlm.bci = (boundary) jpar.tBCin;	Tlm.bco = (boundary) jpar.tBCout;		// reset the right BC for Tlm
		}
		PRINTF0("=> Temperature field boundary conditions (%d, %d)\n",Tlm.bci, Tlm.bco);

		PRINTF0("=> Initial temperature field t :\n");
		ftime = load_init(jpar.ffile[T], &Tlm);
		Tlm.bci = (boundary) jpar.tBCin;		Tlm.bco = (boundary) jpar.tBCout;		// reset the right BC.
		if (Tlm.bci == BC_ZERO) Tlm.zero_out_shell(Tlm.ir_bci);	// ensure zero boundary conditions.
		if (Tlm.bco == BC_ZERO) Tlm.zero_out_shell(Tlm.ir_bco);
	
		if (evol_ubt & EVOL_C) {
			if (jpar.f0file[C]) {
				PRINTF0("=> Imposed composition field C0 :\n");
				load_init(jpar.f0file[C], &Clm);
				i = Clm.make_static(&C0lm);
				if (i == 0) runerr("Imposed composition field has 0 modes !");		// probably something is wrong...
				#ifdef XS_LINEAR
				if (C0lm->lmax > 0) {
					gradC0 = new SpatVect;
					gradC0->from_SH(Clm);
				}
				#endif
				PRINTF0("   + Imposed composition field has %d modes (l<=%d, m<=%d).\n",i,C0lm->lmax,C0lm->mmax);
				sprintf(command,"fieldC0.%s",job); 	Clm.save_double(command, C0lm->lmax, C0lm->mmax/MRES);
				Clm.bci = (boundary) jpar.cBCin;	Clm.bco = (boundary) jpar.cBCout;		// reset the right BC for Clm
			}
			PRINTF0("=> Composition field boundary conditions (%d, %d)\n",Clm.bci, Clm.bco);

			PRINTF0("=> Initial composition field c :\n");
			ftime = load_init(jpar.ffile[C], &Clm);
			Clm.bci = (boundary) jpar.cBCin;		Clm.bco = (boundary) jpar.cBCout;		// reset the right BC.
			if (Clm.bci == BC_ZERO) Clm.zero_out_shell(Clm.ir_bci);	// ensure zero boundary conditions.
			if (Clm.bco == BC_ZERO) Clm.zero_out_shell(Clm.ir_bco);
		}
	}

	/* HANDLE Non-Linear terms related to base flow  (J0xB0, U0xW0, rho0.grad(Phi0)) */
	// unsupported.

	/* INIT VELOCITY FIELDS */
	if (evol_ubt & EVOL_U) {
		PRINTF0("=> Velocity field boundary conditions (%d, %d)\n",Ulm.bci, Ulm.bco);
		#ifdef U_FORCING
			PRINTF0("=> Forcing : " U_FORCING "\n");
			if ( (MRES*MMAX < FORCING_M) || (FORCING_M % MRES) ) {
				sprintf(command, "forcing requires mode m=%d !", FORCING_M);
				runerr(command);
			}
			if (jpar.w_forcing > 0.0) {
				PRINTF0("   + Time-localized forcing on time-scale t_forcing=%f\n",2.*M_PI/jpar.w_forcing);
			} else if (jpar.w_forcing < 0.0) {
				PRINTF0("   + Periodic forcing with pulsation w_forcing=%f\n",-jpar.w_forcing);
			}
		#else
			PRINTF0("=> No boundary forcing\n");
		#endif
		if (i_mpi==0) fflush(stdout);

		Ulm.zero_out();
		if (jpar.ffile[U]) {
			PRINTF0("=> Initial velocity field u :\n");
			ftime = load_init(jpar.ffile[U], &Ulm);
	// *****  Feature request by Elliot Kaplan : ****
			double a = mp.var["randa"];
			if (a != 0.0) {
				Ulm.add_random(a, 0, MMAX, 1);		// add random noise on the odd symmetry.
				PRINTF0("=> Anti-symmetric random noise added to velocity field.\n");
			}
	// *****  end of feature request by Elliot Kaplan. ****
			Ulm.bci = (boundary) jpar.uBCin;		Ulm.bco = (boundary) jpar.uBCout;		// reset the right BC.
			if (Ulm.bci == BC_ZERO) Ulm.zero_out_shell(Ulm.ir_bci);	// ensure zero boundary conditions.
			if (Ulm.bco == BC_ZERO) Ulm.zero_out_shell(Ulm.ir_bco);
			tmp = Ulm.absmax(NG, NM);
			if (tmp > fabs(u0)) u0 = tmp;
			// save solid body rotation to SolidBodies:
			if ((Ulm.bci == BC_NO_SLIP)&&(own(Ulm.ir_bci))) InnerCore.set_rotation(r[Ulm.ir_bci], Ulm.Tor[Ulm.ir_bci]);
			if ((Ulm.bco == BC_NO_SLIP)&&(own(Ulm.ir_bco))) Mantle.set_rotation(r[Ulm.ir_bco], Ulm.Tor[Ulm.ir_bco]);
		}
		#ifdef XS_SET_BC
			if (jpar.a_forcing != 0.0) set_U_bc(jpar.a_forcing);		// set arbitray boundary conditions.
		#endif
		if (kill_sbr) 	conserve_momentum(Ulm, kill_sbr);		// save initial momentum.
		#ifdef XS_BULK_FORCING
			bulk_forcing.init(mp, Xlm);
		#endif
	}

  #ifdef XS_MEAN_FIELD
	/* MEAN FIELD */
	double alpha0 = mp.var["alpha"];
	PRINTF0("=> using alpha effect, amplitude alpha0 = %g.\n", alpha0);
	alpha.alloc(Blm.irs, Blm.ire, NLAT_PADDED, 1);		// allocate memory for an axisymmetric field of 1 component.
	for (int ir=Blm.irs; ir<=Blm.ire; ir++) {
		init_alpha(ir, alpha(ir,0), alpha0 );
	}
  #endif

	if (iter==0) {
		#ifndef XS_B_FORCING
		if (MMAX>0) {
			double enz = 0;
			SpectralDiags sd(LMAX,MMAX);
			for (int f=0; f<MAXFIELD; f++) {
				if (evol_ubt & EVOL(f)) {
					Xlm[f].energy(sd);
					enz += sd.energy_nz();		// sum non-zonal parts
				}
			}
			#ifdef XS_MPI
				MPI_Allreduce(MPI_IN_PLACE, &enz, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
			#endif
			if (enz == 0) {		// if non-zonal energy is zero, add some random noise for m>0
				PRINTF0("=> adding random noise to m>0\n");
				if (evol_ubt & EVOL_U) Ulm.add_random(u0*1e-3, 1, MMAX, 0);
				if (evol_ubt & EVOL_B) Blm.add_random(b0*1e-3, 1, MMAX, 0);
			}
		}
		#endif
		for (int f=0; f<MAXFIELD; f++) {
			if (evol_ubt & EVOL(f)) {
				sprintf(command,"field%c_0000.%s",symbol[f],job);
				Xlm[f].save_generic(command, LMAX, MMAX, ftime,0,0, jpar.prec_out);
			}
		}
	}

	#ifdef XS_MPI
		// sync halo
		for (int f=0; f<MAXFIELD; f++) {
			if (evol_ubt & EVOL(f))  Xlm[f].sync_mpi();
		}
	#endif

  if (i_mpi == 0) {
/* PRINT PARAMS */
	printf("[Params] job name : ***  %s  ***\n",job);
	double Omega0 = jpar.Omega0;
	if (Omega0 != 0.0)
	printf("         Ek=%.2e, Ro=%.2e, Elsasser=%.2e, Lehnert=%.2e\n",jpar.nu/Omega0, u0/Omega0, b0*b0/(jpar.eta*Omega0), b0/Omega0);
	printf("         Pm=%.2e, Re=%.2e, S=%.2e, N=%.2e, M=%.2e\n",jpar.nu/jpar.eta, u0/jpar.nu, b0/jpar.eta, b0*b0/(u0*u0) , b0/sqrt(jpar.nu*jpar.eta));
	if (gmax)
	printf("         Pr=%.2e, gmax=%.2e, DeltaT=%.2e, Ra=%.2e, N^2=%.2e\n",jpar.nu/jpar.kappa, gmax, 0.0, gmax/(jpar.nu*jpar.kappa), 0.0);		// TODO: compute numbers.
	printf("         dt.Omega=%.2e, dt.nu.R^2=%.2e, dt.eta.R^2=%.2e\n",dt*Omega0, dt*jpar.nu*rmax*rmax, dt*jpar.eta*rmax*rmax);
  }

/* CHECK GRID RESOLUTION */
	if ((evol_ubt & EVOL_U) && (i_mpi==0)) {
		if (jpar.nu/(jpar.Omega0*r[Ulm.ir_bco]*r[Ulm.ir_bco]) < 1.0) {		// Ekman layers may exist
			double d = sqrt(jpar.nu/fabs(jpar.Omega0));		// Ekman layer thickness
			int nin = r_to_idx(r[Ulm.ir_bci]+d) - Ulm.ir_bci;
			int nout = Ulm.ir_bco - r_to_idx(r[Ulm.ir_bco]-d);
			printf("  points in inner and outer Ekman layers (thickness = %g): %d and %d.\n", d, nin, nout);
			if ((Ulm.bci != BC_FREE_SLIP) && (nin < 5))  printf(COLOR_WRN "    !! Warning : inner Ekman layer underresolved ! " COLOR_END "\n");
			if ((Ulm.bco != BC_FREE_SLIP) && (nout < 5)) printf(COLOR_WRN "    !! Warning : outer Ekman layer underresolved ! " COLOR_END "\n");
		}
	}

// Initializing signal handler.
		signal(30,&sig_handler);	signal(31,&sig_handler);	
	#ifndef XS_MPI
		signal(15,&sig_handler);	// catch TERM signal only without MPI
	#endif

/* DETERMINE WHAT NEEDS TO BE COMPUTED */
	if (evol_ubt & EVOL_U) {		// for Navier-Stokes equation
		PRINTF0("=> Navier-Stokes integration\n");
		if (jpar.no_ugradu == 0) spat_need |= NEED_U | NEED_W;		// u.grad u
		if ((evol_ubt & EVOL_B) && (jpar.no_jxb == 0)) spat_need |= NEED_B | NEED_J;		// Laplace force.
		if (gradPhi0) {
			if (evol_ubt & EVOL_T) spat_need |= NEED_T;				// spatial temperature
			if (evol_ubt & EVOL_C) spat_need |= NEED_C;				// spatial temperature
		}
	}
	if (evol_ubt & EVOL_B) {		// for Induction equation
		PRINTF0("=> Induction integration\n");
		spat_need |= NEED_U | NEED_B;
	}
	if (evol_ubt & EVOL_T) {		// for energy equation
		PRINTF0("=> Temperature integration\n");
		spat_need |= NEED_U | NEED_GT;
	}
	if (evol_ubt & EVOL_C) {		// for energy equation
		PRINTF0("=> Composition integration\n");
		spat_need |= NEED_U | NEED_GC;
	}
	#ifdef XS_LINEAR
		spat_need = 0;
		if (evol_ubt & EVOL_U) {
			if (U0) spat_need |= NEED_W;
			if (W0) spat_need |= NEED_U;
			if (B0) spat_need |= NEED_J;
			if (J0) spat_need |= NEED_B;
			if (gradPhi0) {
				if (evol_ubt & EVOL_T) spat_need |= NEED_T;
				if (evol_ubt & EVOL_C) spat_need |= NEED_C;
			}
			if (jpar.nonlin && strstr(jpar.nonlin, "ugu"))	spat_need |= NEED_UXW | NEED_U | NEED_W;
			if (frame.check_non_axial())	spat_need |= NEED_COR | NEED_U;		// Coriolis force needs to be computed as spatial terms.
			#ifdef XS_CORIOLIS_BASE
			if (!frame.check_inertial()) {
				spat_need |= NEED_COR | NEED_U;	// There is a Coriolis force, we need to compute it spatilly for the base flow.
				PRINTF0(COLOR_WRN "**WARNING** adding forcing due to Coriolis force of base flow (XS_CORIOLIS_BASE). " COLOR_END "\n");
			}
			#endif
		}
		if (evol_ubt & EVOL_B) {
			#ifdef XS_MEAN_FIELD
				spat_need |= NEED_B;
			#endif
			if (U0) spat_need |= NEED_B;
			if (B0) spat_need |= NEED_U;
			if (jpar.nonlin) {
				if (strstr(jpar.nonlin, "uxb"))	spat_need |= NEED_UXB | NEED_B | NEED_U;
				if (strstr(jpar.nonlin, "jxb"))	spat_need |= NEED_JXB | NEED_J | NEED_B;
			}
		}
		if (evol_ubt & EVOL_T) {
			if (U0) spat_need |= NEED_GT;
			if (gradT0) spat_need |= NEED_U;
			if (jpar.nonlin && strstr(jpar.nonlin, "ugt"))	spat_need |= NEED_UGT | NEED_GT | NEED_U;
		}
		if (evol_ubt & EVOL_C) {
			if (U0) spat_need |= NEED_GC;
			if (gradC0) spat_need |= NEED_U;
			if (jpar.nonlin && strstr(jpar.nonlin, "ugc"))	spat_need |= NEED_UGC | NEED_GC | NEED_U;
		}
	#else
		if (jpar.nonlin) PRINTF0(COLOR_WRN "**WARNING** Non-Linear term selection through 'nonlin' is only available when compiled with XS_LINEAR " COLOR_END "\n");
	#endif
	#ifdef XS_DEBUG
		PRINTF0("*DEBUG*:");
		PRINTF0("  evol_ubt = %d%d%d  spat_need = %d\n",(evol_ubt & EVOL_U) > 0, (evol_ubt & EVOL_B) > 0, (evol_ubt & EVOL_T) > 0, spat_need);
	#endif

	if (jpar.parity) {		// filter parity (for U and T only)
		if (evol_ubt & EVOL_U) Ulm.filter_parity(jpar.parity);
		if (evol_ubt & EVOL_T) Tlm.filter_parity(jpar.parity);
		if (evol_ubt & EVOL_C) Clm.filter_parity(jpar.parity);
	}

	// fix mean temperature to avoid spurious drift (it has no dynamical consequences, but reduces accuracy).
	if ((evol_ubt & EVOL_T) && (Tlm.bci == BC_IMPOSED_FLUX) && (Tlm.bco == BC_IMPOSED_FLUX))	fix_mean_temperature(Tlm);
	if ((evol_ubt & EVOL_C) && (Clm.bci == BC_IMPOSED_FLUX) && (Clm.bco == BC_IMPOSED_FLUX))	fix_mean_temperature(Clm);

	#ifdef XS_SPARSE
	xs_sparse::prepare_state(Xlm, jpar.parity, jpar.Omega0, kill_sbr);				// sparse state vectors.
	stepper->calc_matrices(dt);		// we need to compute the sparse matrices now.
	if (jpar.dt_adjust != 0) PRINTF0(COLOR_WRN "!!! WARNING !!! Implicit Coriolis leads to expensive time-step changes." COLOR_END "\n");
	#endif

/* PREPARE FIRST ITERATION */
	PRINTF0("let's go for %d iterations ! (%g sub-steps each, %s)\n",iter_max, dt_log/(2*dt), jpar.stepper_name ? jpar.stepper_name : "PC2");
	for (int k=0; k<MAX_CFL; k++) dt_cfl[k] = 0.0;
	frame.update(ftime);
	calc_U_solid(ftime, 0);
	{
		// allocate temporary fields for each shell (private).
	  #if XS_OMP == 2
		#ifdef XS_MEAN_FIELD
			#error "mean field not supported by xsbig_hyb2."
		#endif
		using namespace spatial_buffers2;
		explicit_terms_ = &explicit_terms_omp_inshell;
		size_t Nspec = 0;		size_t Nspat = 0;
		long Nfluid = Ulm.ire-Ulm.irs+1;		if (Nfluid < 0)	Nfluid = 0;		// fluid shells.
		if (spat_need & NEED_U) {  Nspec += 2;		Nspat += 3;	 }
		if (spat_need & NEED_B) {  Nspec += 2;		Nspat += 3;	 }
		if (spat_need & NEED_GT) { Nspec += 2;		Nspat += 3;	 }
		if (spat_need & NEED_GC) { Nspec += 2;		Nspat += 3;	 }
		if (spat_need & NEED_W) {  Nspec += 3;		Nspat += 3;	 }
		if (spat_need & NEED_J) {  Nspec += 3;		Nspat += 3;	 }
		if (spat_need & (NEED_T|NEED_C)) Nspat += 1;
		if ((spat_need & (NEED_T|NEED_C)) == (NEED_T|NEED_C)) Nspec += 1;		// if both terms are present we need to add them.
		
		size_t nspat_alloc = shtns->nspat;
		spat_cdist = nspat_alloc * Nfluid;
		if (spat_cdist % 512 == 0) spat_cdist += 32;	// unalign components.
		const int cl = CACHE_LINE/sizeof(cplx);
		size_t NLM_alloc = ((NLM + cl-1)/cl)*cl;		// round to cache line
		spec_cdist = NLM_alloc * Nfluid;
		if (spec_cdist % 256 == 0) spec_cdist += 16;	// unalign components.
		spat_mem = (double*) VMALLOC(sizeof(double)*(Nspec*2*spec_cdist + Nspat*spat_cdist));		// big buffer !
		if (!spat_mem)	runerr("not enough memory for spatial buffers.\n");
		double * spat_mem0 = spat_mem;		// spatial data
		cplx * spec_mem0 = (cplx*) (spat_mem + Nspat*spat_cdist);	// spectral data
		memset(spat_mem0, 0, Nspat*spat_cdist*sizeof(double));

		if (spat_need & NEED_U) {
			u.init_from_buffer( spat_mem0, nspat_alloc, Ulm.irs );		spat_mem0 += spat_cdist * 3;
			u_qs.init_from_buffer( spec_mem0, NLM_alloc, Ulm.irs );		spec_mem0 += spec_cdist * 2;
		}
		if (spat_need & NEED_W) {
			w.init_from_buffer( spat_mem0, nspat_alloc, Ulm.irs );		spat_mem0 += spat_cdist * 3;
			w_qst.init_from_buffer( spec_mem0, NLM_alloc, Ulm.irs );	spec_mem0 += spec_cdist * 3;
		}
		if (spat_need & NEED_B) {
			b.init_from_buffer( spat_mem0, nspat_alloc, Ulm.irs );		spat_mem0 += spat_cdist * 3;
			b_qs.init_from_buffer( spec_mem0, NLM_alloc, Ulm.irs );		spec_mem0 += spec_cdist * 2;
		}
		if (spat_need & NEED_J) {
			j.init_from_buffer( spat_mem0, nspat_alloc, Ulm.irs );		spat_mem0 += spat_cdist * 3;
			j_qst.init_from_buffer( spec_mem0, NLM_alloc, Ulm.irs );	spec_mem0 += spec_cdist * 3;
		}
		if (spat_need & NEED_GT) {
			gt.init_from_buffer( spat_mem0, nspat_alloc, Ulm.irs );		spat_mem0 += spat_cdist * 3;
			gt_qs.init_from_buffer( spec_mem0, NLM_alloc, Ulm.irs );	spec_mem0 += spec_cdist * 2;
		}
		if (spat_need & NEED_GC) {
			gc.init_from_buffer( spat_mem0, nspat_alloc, Ulm.irs );		spat_mem0 += spat_cdist * 3;
			gc_qs.init_from_buffer( spec_mem0, NLM_alloc, Ulm.irs );	spec_mem0 += spec_cdist * 2;
		}
		if (spat_need & (NEED_T|NEED_C)) {		// buoyancy (temperature+composition)
			tspat.init_from_buffer( spat_mem0, nspat_alloc, Ulm.irs );	spat_mem0 += spat_cdist * 1;
			tspec.init_from_buffer( spec_mem0, NLM_alloc, Ulm.irs );	spec_mem0 += spec_cdist * 1;
		}
	  #else
		#ifdef XS_LINEAR
		long nfa = 6;		// number of field arrays
		if (evol_ubt & EVOL_U) nfa += 3;
		if (evol_ubt & EVOL_B) nfa += 3;
		if (evol_ubt & (EVOL_T | EVOL_C)) nfa += 1;
		#pragma omp parallel
		{	// allocate thread-private buffers.
			const long bytes = sizeof(double)*(6*NLM + nfa*shtns->nspat);
			spat_mem = (double*) VMALLOC(bytes);
			if (!spat_mem)	runerr("not enough memory for spatial buffers.\n");
			memset(spat_mem, 0, bytes);				// important for correctness of absmax2() when there is padding.
		}
		explicit_terms_ = &explicit_terms_lin;		// linear version of explicit terms.
		#else
		using namespace spatial_buffers1;
		const int nf = Ulm.ire-Ulm.irs+1;		// number of fluid shells
		{		// openmp threads get assigned to shells.
			explicit_terms_ = &explicit_terms_omp_rad;
			#pragma omp parallel
			{	// allocate thread-private buffers.
				const long bytes = sizeof(double)*(10*NLM + 9*shtns->nspat);
				spat_mem = (double*) VMALLOC(bytes);
				if (!spat_mem)	runerr("not enough memory for spatial buffers. Try xsbig_hyb2 instead.\n");
				memset(spat_mem, 0, bytes);				// important for correctness of absmax2() when there is padding.
			}
		}
		#endif
	  #endif
	}
	#ifdef XS_DEBUG
	PRINTF0("    CFL parameters: dt_tol_lo=%g, dt_tol_hi=%g, C_u=%g, C_vort=%g, C_b=%g, C_j=%g, C_cori=%g\n",dt_tol_lo, dt_tol_hi, C_cfl[cfl_ur], C_cfl[cfl_w], C_cfl[cfl_br], C_cfl[cfl_j], C_cfl_cori);
	#endif

	if (jpar.sconv_stop == 0.0)	jpar.sconv_stop = INFINITY;		// infinite value (c++11) to disable Sconv checks.
	nrj_and_summed_diagnostics(fnrj, nrj, 0., 0, xsio_par_string);

	#ifndef XS_NOPLOT
	double tplot = 0.;
	Gnuplot* plot = 0;
	if ((i_mpi == 0) && (jpar.plot != 0)) {		// only MPI rank 0 does the plotting.
		plot = open_gnuplot("energy." + std::string(job) + ".png", jpar.plot);
	}
	if (plot) {
		plot->cmd("set style data linespoints\n");
		plot->cmd("set logscale y 10\n");
		plot->set_title(job);
		plot->set_xlabel("time");
		plot->set_plotcmd("plot 'energy." + std::string(job) + "' using 1:2 title 'Eu', '' using 1:3 title 'Eb'\n");
	}
	#endif

/* MAIN LOOP */
	std::vector<float> time_per_eval;		// record average time per eval
	time_per_eval.reserve(iter_max - iter);
	double tsave = 0.;
	double tsave_limit = jpar.tsave_limit * 60.;		// convert time-limit to seconds
	const int make_movie = jpar.make_movie;
	const int movie_cycle = mp.var["movie_cycle"];
	double time1 = xs_wtime();
	int safe_save_quit = 0;
	while (iter < iter_max) {
		#if XS_DEBUG > 1
			PRINTF0(COLOR_ERR "WARNING! VERBOSE DEBUG MODE ON: flooding disk with debug_*.txt files, computation dramatically slowed down." COLOR_END "\n");
		#endif

		long nex = step_UBT(dt_log, Xavg, dXdt, kill_sbr);	// go for time integration !!

		double time2 = xs_wtime();
		double treal = time2 - time1;
		tsave += treal;  // elapsed time since last save (or iteration number if less than 1 sec)
		time_per_eval.push_back(treal/nex);

		iter++;

		// fix mean temperature to avoid spurious drift (it has no dynamical consequences, but reduces accuracy).
		if ((evol_ubt & EVOL_T) && (Tlm.bci == BC_IMPOSED_FLUX) && (Tlm.bco == BC_IMPOSED_FLUX))	fix_mean_temperature(Tlm);
		if ((evol_ubt & EVOL_C) && (Clm.bci == BC_IMPOSED_FLUX) && (Clm.bco == BC_IMPOSED_FLUX))	fix_mean_temperature(Clm);
		// compute energies and other diagnostics summed between processes.
		nrj_and_summed_diagnostics(fnrj, nrj, treal, nex);
		#ifndef XS_NOPLOT
		if ((plot) && (tsave-tplot >= 2. || iter==iter_max)) {		// at least two seconds have passed.
			tplot = tsave;
			plot->plot();
		}
		#endif

		safe_save_quit = SAVE_QUIT;		// read from volatile location
		#ifdef XS_STOP_CRITERION
		if (i_mpi==0) safe_save_quit |= (stop_criterion(nrj, iter) ? 6 : 0);
		#endif
		if (tsave + treal*1.05 >= tsave_limit) safe_save_quit |= 1;       // save full fields if more than tsave_limit will be elapsed at next iteration
		#ifdef XS_MPI
			MPI_Bcast((void*) &safe_save_quit, 1, MPI_INT, 0, MPI_COMM_WORLD);		// Broadcast SAVE_QUIT information, in a safe way.
		#endif
		if (safe_save_quit) {
			save_snapshot(ftime, iter);
			SAVE_QUIT = 0;		tsave = 0.;		// mark snapshot as saved.
			nbackup--;
			if (nbackup == 0) safe_save_quit |= 2;      // don't continue if it is our last backup
		}

		#ifdef XS_WRITE_SV
		if (jpar.lmax_out_sv) {
			if ((evol_ubt & EVOL_B)&&(own(Bp_i1))) { sprintf(command,"UBsurf_vs_%05d.%s",iter,job);		write_SV(command,jpar.lmax_out_sv); }
		}
		#endif
		if ((make_movie != 0)&&(iter % iter_save == 0)) {		// save data snapshot.
			i = iter/iter_save;
			if (movie_cycle > 0) i = i % movie_cycle;
			if (dXdt)	{
				PRINTF0("time_derivative at iter=%d : compute with fields at times %g and %g (time-step = %g)\n",iter, (double) ftime-stepper->dt, (double) ftime, stepper->dt);
				dXdt->accumulate_scaled(Xlm, -1.0, 1.0/stepper->dt);		// compute time-derivative
			}
			for (int f=0; f<MAXFIELD; f++) {
				if (evol_ubt & EVOL(f)) {
					sprintf(command,"field%c_%04d.%s",symbol[f],i,job);
					Xlm[f].save_generic(command, jpar.lmax_out, jpar.mmax_out, ftime,iter,0, jpar.prec_out);
					if (dXdt) {
							sprintf(command,"fieldd%cdt_%04d.%s",symbol[f],i,job);
							(*dXdt)[f].save_generic(command, jpar.lmax_out, jpar.mmax_out, ftime-0.5*stepper->dt, iter,0, jpar.prec_out);
					}
				}
			}
			if (make_movie == 2) {
				Xavg->update_average(Xlm, 0);		// add contribution of end-point
				for (int f=0; f<MAXFIELD; f++) {
					if (evol_ubt & EVOL(f)) {
						sprintf(command,"field%cavg_%04d.%s",symbol[f],i,job);
						(*Xavg)[f].save_generic(command, jpar.lmax_out, jpar.mmax_out, ftime,iter,0, jpar.prec_out);
					}
				}
				Xavg->reset();		// reset averaging.
			}
			if (make_movie == 3) {	// output linear and non-linear terms with a quick hack; does not work for every stepper
				if (jpar.stepper_name && jpar.stepper_name[0] == 'S') printf(COLOR_WRN "terms cannot be saved with SBDF family stepper" COLOR_END "\n");
				else if (evol_ubt & EVOL_B) {
					#ifndef XS_CUDA
					// does not work on GPU, as NL lives on the GPU only. TODO: copy to CPU, or make save_generic gpu aware?
					sprintf(command,"termB_nl_%04d.%s",i,job);
					NL[0]->B.save_generic(command, jpar.lmax_out, jpar.mmax_out, ftime, iter,0, jpar.prec_out);
					#endif
					#if XS_OMP != 1
						// only for XS_OMP != 1, because otherwise the .apply() methods below are already parallelized in radius.
						#pragma omp parallel for schedule(dynamic)
					#endif
					for (int im=0; im<=jpar.mmax_out; im++) {		// only compute what we need
						printf("jpar.mmax_out=%d, jpar.lmax_out=%d\n", jpar.mmax_out, jpar.lmax_out);
						const int lm0 = LiM(shtns, im*MRES, im);
						const int lm1 = LiM(shtns, jpar.lmax_out, im);
						MBp.apply(Blm.Pol, Lin[0]->B.Pol, lm0, lm1);		// /!\ Overwrite Lin[0]
						MBt.apply(Blm.Tor, Lin[0]->B.Tor, lm0, lm1);		// /!\ Overwrite Lin[0]
					}
					sprintf(command,"termB_lin_%04d.%s",i,job);
					Lin[0]->B.save_generic(command, jpar.lmax_out, jpar.mmax_out, ftime, iter,0, jpar.prec_out);
				}
			}
		}
		if (safe_save_quit & 6) {
			PRINTF0("save & quit\n");
			break;
		}

		time1 = xs_wtime();
		treal = time1 - time2;
		#ifdef XS_DEBUG
		if ((i_mpi==0) && (treal > 1e-2)) printf("\tpost-process and saving time : %g\n", treal);
		#endif
		tsave += treal;
	}

	if (fnrj) fclose(fnrj);
	// save full double-precision data at the end (for restart)
	save_snapshot(ftime, iter);
	#ifndef XS_NOPLOT
	if (plot) {
		plot->save();		// always save plot to png at the end.
		delete plot;	plot = 0;
	}
	#endif

	// Free memory and other resources, before we leave the MPI world.
	Xlm.free_state();
	if (Xavg) Xavg->free_state();
	if (dXdt) dXdt->free_state();
	for (int i=0; i<5; i++) if (Lin[i]) Lin[i]->free_state();
	for (int i=0; i<4; i++) if (NL[i])	NL[i]->free_state();

	// compute average and standard deviation of time per non-linear evaluation.
	double t_avg = 0.;
	double t2 = 0.;
	const long i0 = 1;		// don't count the first iteration which is often significantly slower.
	for (long i=i0; i<time_per_eval.size(); i++) {
		double te = time_per_eval[i];
		t_avg += te;
		t2 += te*te;
	}
	t_avg /= time_per_eval.size()-i0;
	t2 /= time_per_eval.size()-i0;
	PRINTF0(" > average time per eval = %.4g (+/- %.4g)\n", t_avg, sqrt(t2-t_avg*t_avg));

#ifdef XS_MPI
	MPI_Finalize();
#endif
	if ( (iter < iter_max) && ((safe_save_quit & 4)==0) )  return(2);     // return 2 to signal the job should be resubmitted.
	PRINTF0(COLOR_OK "[XSHELLS] job %s completed. " COLOR_END "\n",job);
	return(0);
}
