/*
 * Copyright (c) 2010-2019 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xshells_stepper.cpp 
/// XSHELLS: definitions of time-stepper and their logic

#ifdef XS_DEBUG
#define XS_UNSAFE_STEPPERS
#endif

/// Compute the maximum allowed time-step in order to have an accurate dipole decay rate with the Crank-Nicolson time scheme.
double get_max_dt_CN()
{
	double xmin = 1e200;
	for (int i=1; i<NR-1; i++) {
		double dr = r[i+1]-r[i-1];
		double x = dr;
		if (x < xmin) xmin = x;
	}
	return 0.1 * xmin/r[NR-1] * get_diffusion_time();
}

/* GOOD TIME-STEPPERS : */

class PC : public Stepper {
/* PC(2) has been inspired by 
 @article{jameson1981numerical,
  title={Numerical solutions of the Euler equations by finite volume methods using Runge-Kutta time-stepping schemes},
  author={Jameson, Antony and Schmidt, Wolfgang and Turkel, Eli and others},
  journal={AIAA paper},
  volume={1259},
  pages={1981},
  year={1981}
} */
/* from benchmark 1 : dt_cori = 1/Omega0,  dt_u > 2*dx/u (saturated)
 * from benchmark 2 : dt_b = (2.5 - 2.7)*dx/b (saturated)
 * from tilgner1 : dt_u = 4*dr/ur,  dt_u = 2.5*dh/uh
        [lmax=63, dt=0.035;  lmax=47, dt= 0.05;  lmax=95, dt=0.025;  lmax=31, dt=0.075]
 * from stratified case (Yue-Kin) : dt_cori = 0.9/Omega0.  TODO: check and implement a CFL condition for stable stratification.
 * from torsional waves, C_alfv=2.7 is stable
 */
  protected:
	int n_corr;
	double ax;
	void build_rhs(StateVector& Xrhs, const int icorr);
public:
	PC(unsigned nc) {
		if (nc > 2) nc = 2;		// nc > 2 does not improve stability
		nex_eval = nc+1;
		n_corr = nc;
		PRINTF0("=> Using PC(%d) time-stepper\n", n_corr);
		nim = 1;
		nex = (n_corr > 0) ? 2 : 1;

		if (jpar.pc2_ai == 1.0) runerr("pc2_ai = 1 not supported (Euler implicit)\n");
		if ((jpar.pc2_ai < 0.5) || (jpar.pc2_ax < 0.4)) runerr("bad PC2 coefficients. Use pc2_ai>=0.5 and pc2_ax>=0.4\n");
		
		ax = jpar.pc2_ax;
		CM_rhs = 1.0-jpar.pc2_ai;	// Crank-Nicolson part, linear operators multiplied by 0.5
		CM_lhs = -jpar.pc2_ai;		// Crank-Nicolson part, lhs operators multiplied by -0.5
		Cdt_rhs = 1.0;
		Cdt_lhs = 1.0;

		set_default_cfl_u_b_cori(0.65, 2.7, 0.9);		// default CFL safety factors for PC2.
		#ifdef XS_LINEAR
			dt_max = get_max_dt_CN();	// Crank-Nicolson has a maximum time-step for accurate diffusive decay rate.
		#endif
	}
	void step_3_end(int kill_sbr) override;
};


/// Additive Runge-Kutta (ARK) schemes are taken from:
/// Some new additive Runge-Kutta methods and their applications
/// Hongyu Liu and Jun Zou, Journal of Computational and Applied Mathematics 190 (2006) 74-98
class ARK : public Stepper {
  protected:
	double c[4];		// substep positions (actually increments from previous time)
	double ai[4][5];	// RK matrix for implicit part (DIRK) [including final weights]
	double ax[4][5];	// RK matrix for explicit part  [including final weights]

	void build_rhs(StateVector& Xrhs, const int k);
	void init_matrices() {
		CM_rhs = 0;
		Cdt_rhs = 1.0;
		CM_lhs = -ai[0][1];		// diagonal
		Cdt_lhs = 1.0;
		if (nex_eval == 0) nex_eval = nex;		// default value for nex_eval
	}
public:
	void step_3_end(int kill_sbr) override;
};


/// From page A49 of: BOSCARINO, PARESCHI, RUSSO (2013), SIAM J. SCI. COMPUT. Vol. 35, No. 1, pp. A22–A51
/// https://doi.org/10.1137/110842855   https://arxiv.org/abs/1110.4375
class BPR353 : public ARK {
  public:
	BPR353() {
		PRINTF0("=> Using ARK BPR353 time-stepper\n");
		nim = 5;		nex = 4;				nex_eval = 3;
		c[0] = 1;	c[1] = -1./3.;	c[2] = 1./3.;	c[3] = 0.0;		// time increments (total=1)
		ai[0][0] = 0.5;		ai[0][1] = 0.5;
		ai[1][0] = 5./18.;	ai[1][1] = -1./9.;	ai[1][2] = 0.5;
		ai[2][0] = 0.5;		ai[2][1] = 0;		ai[2][2] = 0;		ai[2][3] = 0.5;
		ai[3][0] = 0.25;	ai[3][1] = 0;		ai[3][2] = 0.75;	ai[3][3] = -0.5;		ai[3][4] = 0.5;
		ax[0][0] = 1.0;
		ax[1][0] = 4./9.;	ax[1][1] = 2./9.;
		ax[2][0] = 0.25;	ax[2][1] = 0;		ax[2][2] = 0.75;
		ax[3][0] = 0.25;	ax[3][1] = 0;		ax[3][2] = 0.75;	ax[3][3] = 0;	// this last zero saves a non-linear term evaluation.

		init_matrices();
		set_default_cfl_u_b_cori(0.6, 2.25, 0.85);		// default CFL safety factors for BPR353.
		dt_max = 1e-2 * get_diffusion_time();			// for accurate diffusive decay rate
	}
};

/// From slide 11 of http://dm.unife.it/lorenzo.pareschi/talks/imex_slides.pdf
class LRR322 : public ARK {
  public:
	LRR322() {
		PRINTF0("=> Using ARK LRR(322) time-stepper\n");
		nim = 4;		nex = 3;
		c[0] = 0.5;	c[1] = -1./6.;	c[2] = 2./3.;		// time increments (total=1)
		ai[0][0] = 0;	ai[0][1] = 0.5;
		ai[1][0] = 0;	ai[1][1] = 0;	ai[1][2] = 1./3.;
		ai[2][0] = 0;	ai[2][1] = 0;	ai[2][2] = 0.75;		ai[2][3] = 0.25;
		ax[0][0] = 0.5;
		ax[1][0] = 1./3.;	ax[1][1] = 0.0;
		ax[2][0] = 0.0;		ax[2][1] = 1.0;		ax[2][2] = 0;
		init_matrices();
	}
};

template <class T>
class MultiStep : public Stepper {		// Base class using CRTP, for static polymorphism.
  public:
	MultiStep() {
		nex_eval = 1;		// for all multi-step schemes: 1 explicit eval per step.
	}
	void step_2_adjust_dt(double delta_t, int& istep) override;
	void step_3_end(int kill_sbr) override;
	void full_rhs(StateVector& Xrhs, int k) { static_cast<T*>(this)->full_rhs(Xrhs, k); }
	void set_dt(double dt) { static_cast<T*>(this)->set_dt(dt); }
};

template <class T>
void MultiStep<T>::step_2_adjust_dt(double delta_t, int& istep)
{
	dt = adjust_dt(dt, delta_t, istep, dt_adjust);		// adjust time-step and istep
	set_dt(dt);
}

/// Generic multi-step step.
template <class T>
void MultiStep<T>::step_3_end(int kill_sbr)
{
	#pragma omp parallel
	{
		full_rhs(Xlm, i_nl);
		update_all_BC(dt);		// t + dt
		solve_state(Xlm, kill_sbr);
	}

	if (--i_nl < 0) i_nl = nex-1;
}


class CNAB2 : public MultiStep<CNAB2> {
	double dto;			///< previous time-step
	double cex[2];		///< coefficients for explicit term steps

  public:
    CNAB2() {
		if (jpar.pc2_ai == 0.5) {	// default value: Crank-Nicolson (order 2)
			PRINTF0("=> Using CNAB2 time-stepper\n");
		} else {	// theta-scheme, order 1 only !
			PRINTF0("=> Using CNAB time-stepper, with order 1 theta-scheme (theta=%.g) instead of Crank-Nicolson\n", jpar.pc2_ai);
			if ((jpar.pc2_ai < 0.5) || (jpar.pc2_ai >= 1.0)) runerr("bad Crank-Nicolson / theta-scheme coefficient. Use 0.5 <= pc2_ai <1\n");
		}

		nim = 1;	nex = 2;

		CM_rhs = 1.0-jpar.pc2_ai;	// Crank-Nicolson part, linear operators multiplied by 0.5
		CM_lhs = -jpar.pc2_ai;		// Crank-Nicolson part, lhs operators multiplied by -0.5
		Cdt_rhs = 1.0;
		Cdt_lhs = 1.0;
		dto = 0.0;  cex[0] = 1.0;  cex[1] = 0.0;		// Euler 1 for the first step !

		set_default_cfl_u_b_cori(0.2, 0.85, 0.25);		// default CFL safety factors for CNAB2.
		//**** For Torsional waves, C_alfv=1 is stable, but see SBDF2 for even higher efficiency. ****//
		//**** Coriolis CFL C_cori=0.3 works for most cases, except longitudinal libration, where C_cori=0.25 is required for a stable solution ****//
		#ifdef XS_LINEAR
			dt_max = get_max_dt_CN();	// Crank-Nicolson has a maximum time-step for accurate diffusive decay rate.
		#endif
	}
	void set_dt(double dt) {
		if (dto != 0.0) {
			double alpha = 0.5;
			if (dt_adjust) alpha = dt/(dto+dto);		// = 0.5 for constant time-step
			cex[0] = 1.0 + alpha;		cex[1] = -alpha;
		}
		if (differ(dto,dt))  calc_matrices(dt);
		dto = dt;		// store dt for next time-step.
	}
	void full_rhs(StateVector& Xrhs, int nl_0);
};

// (1.5*A/dt - M) X_{n+1} = 2*A/dt*X_{n} - 0.5*A/dt*X_{n-1}   + 2*NL_{n} - NL_{n-1}
class SBDF2 : public MultiStep<SBDF2> {
	double dto;			///< previous time-step
	double cex[2];		///< coefficients for explicit term steps
	double cim[2];		///< coefficient for implicit term steps

  public:
    SBDF2() {
		PRINTF0("=> Using SBDF2 time-stepper\n");
		nim = 2;	nex = 2;		nex_eval = 1;
		CM_rhs = 0.0;	// BDF part, linear operators are not involved in rhs
		CM_lhs = -1.0;	// BDF part, lhs linear operators are -1.0 times rhs operators
		Cdt_rhs = 1.0;
		Cdt_lhs = 1.0;
		dto = 0.0;  cex[0] = 1.0;  cex[1] = 0.0;		// Euler 1 for the first step !
		cim[0] = 1.0;		cim[1] = 0.0;

		set_default_cfl_u_b_cori(0.15, 0.8, 0.18);		// default CFL safety factors for SBDF2.
		//**** For Torsional waves, C_alfv=1.5 is stable, leading to a very fast time-to-solution. ****//
		//**** C_cori=0.2 is good except for Libration, which requires C_cori=0.18. ****//
	}
	void set_dt(double dt) {
		double Cdt_lhs_new = Cdt_lhs;		// flag to recompute matrices (this differs from most other schemes).
		if (dto != 0.0) {
			double alpha = 1.0;
			if (dt_adjust) alpha = dt/dto;		// = 1.0 for constant time-step
			cex[0] = 1. + alpha;		cex[1] = -alpha;
			cim[0] = 1. + alpha;		cim[1] = -alpha/(1.+alpha);		// differs from Wang&Ruth (2008) because we store (1/dto).U^n, not U^n
			Cdt_lhs_new = cim[0] + cim[1]*alpha;	//	(1. + 2.*alpha)/(1. + alpha);		// = 3/2 for constant time-step
		}
		if ((differ(Cdt_lhs,Cdt_lhs_new)) || (differ(dto,dt))) {
			Cdt_lhs = Cdt_lhs_new;
			calc_matrices(dt);
		}
		dto = dt;		// store dt for next time-step.
	}
	void full_rhs(StateVector& Xrhs, int nl_0);
};

// SBDF3: ((11./6.)*A/dt - M) X_{n+1} = 3*A/dt*X_{n} - 1.5*A/dt*X_{n-1} + 1/3*A/dt*X_{n-2}   + 3*NL_{n} -3*NL_{n-1} + NL_{n-2}
class SBDF3 : public MultiStep<SBDF3> {
	double cex[3];		///< coefficients for explicit term steps
	double cim[3];		///< coefficient for implicit term steps
	double dto[2];		///< previous time-steps
	int start_countdown = 2;

  public:
    SBDF3() {
		PRINTF0("=> Using SBDF3 time-stepper\n");
		nim = 3;	nex = 3;		nex_eval = 1;
		CM_rhs = 0.0;	// BDF part, linear operators are not involved in rhs
		CM_lhs = -1.0;	// BDF part, lhs linear operators are -1.0 times rhs operators
		Cdt_rhs = 1.0;
		Cdt_lhs = 1.0;
		dto[0] = 0.0;	dto[1] = 0.0;
		cex[0] = 1.0;	cex[1] = 0.0;	cex[2] = 0.0;		// Euler 1 for the first step !
		cim[0] = 1.0;	cim[1] = 0.0;	cim[2] = 0.0;

		set_default_cfl_u_b_cori(0.1, 0.5, 0.3);		// default CFL safety factors for SBDF3.
	}
	void set_dt(double dt) {
		double Cdt_lhs_new = Cdt_lhs;		// flag to recompute matrices (this differs from most other schemes).
		if (start_countdown==0) {
			cex[0] = 3;		cex[1] = -3;	cex[2] = 1;
			cim[0] = 3;		cim[1] = -1.5;	cim[2] = 1./3.;
			Cdt_lhs_new = 11./6.;
			if (dt_adjust) {
				const double dt1 = dto[0];
				const double dt2 = dto[1];
				const double dtot = dt+dt1+dt2;
				const double a = dt/dt1;
				const double b = dt1/dt2;
				// See Wang & Ruuth (2008). Note that cim[1] and cim[2] differ from their values because we store 1/dt_{k}.U_{k}.
				cim[0] = (1.+a)*dtot/(dt1+dt2);				// = 3 for constant time-step
				cim[1] = -a*dtot / ((1.+a)*dt2);			// = -3/2 for constant time-step
				cim[2] = (dt*(dt1+dt)) / ((dt2+dt1)*dtot);	// = 1/3 for constant time-step
				Cdt_lhs_new = cim[0] + a*(cim[1] + b*cim[2]);	// = 11/6 for constant time-step
				cex[0] = cim[0];		// same coeff (3 for constant time-step).
				cex[1] = -a*(1. + (1.+a)*b);		// = -3 for constant time-step
				cex[2] = 1. - (cex[0] + cex[1]);	// = 1 for constant time-step
			}
		}
		if (start_countdown > 0)	start_countdown --;		// decrease countdown.
		if ((differ(Cdt_lhs,Cdt_lhs_new)) || (differ(dto[0],dt))) {
			#ifdef XS_DEBUG
			if (dt/dto[0] > 1.5) printf(COLOR_ERR "!! zero-stability compromised. !!" COLOR_END "\n");
			#endif
			Cdt_lhs = Cdt_lhs_new;
			calc_matrices(dt);
		}
		dto[1] = dto[0];		// store two last dt for next time-step.
		dto[0] = dt;
	}
	void full_rhs(StateVector& Xrhs, int nl_0);
};


/// x = x0 + alpha*a
void vec_sum1(cplx* x, cplx* x0, cplx* a,
			double alpha, 
			int lms, int lme)
{
	#pragma omp simd
	for (size_t k=2*lms; k<2*lme+2; k++)
		((double*)x)[k] = ((double*)x0)[k] + alpha*((double*)a)[k];
}

/// x = alpha*a + beta*b
void vec_sum2(cplx* x, cplx* a, cplx* b,
			double alpha, double beta,
			int lms, int lme)
{
	#pragma omp simd
	for (size_t k=2*lms; k<2*lme+2; k++)
		((double*)x)[k] = alpha*((double*)a)[k] + beta*((double*)b)[k];
}

/// x = x0 + alpha*a + beta*b
void vec_sum2(cplx* x, cplx* x0, cplx* a, cplx* b,
			double alpha, double beta,
			int lms, int lme)
{
	#pragma omp simd
	for (size_t k=2*lms; k<2*lme+2; k++)
		((double*)x)[k] = ((double*)x0)[k] + alpha*((double*)a)[k] + beta*((double*)b)[k];
}

void vec_sum3(cplx* x, cplx* x0, cplx* a, cplx* b, cplx* c, 
			double alpha, double beta, double gamma,
			int lms, int lme)
{
	#pragma omp simd
	for (size_t k=2*lms; k<2*lme+2; k++)
		((double*)x)[k] = ((double*)x0)[k] + alpha*((double*)a)[k] + beta*((double*)b)[k]
						+ gamma*((double*)c)[k];
}

void vec_sum4(cplx* x, cplx* a, cplx* b, cplx* c, cplx* d,
			double alpha, double beta, double gamma, double delta,
			int lms, int lme)
{
	#pragma omp simd
	for (size_t k=2*lms; k<2*lme+2; k++)
		((double*)x)[k] = alpha*((double*)a)[k] + beta*((double*)b)[k]
						+ gamma*((double*)c)[k] + delta*((double*)d)[k];
}

void vec_sum5(cplx* x, cplx* a, cplx* b, cplx* c, cplx* d, cplx* e,
			double alpha, double beta, double gamma, double delta, double eta,
			int lms, int lme)
{
	#pragma omp simd
	for (size_t k=2*lms; k<2*lme+2; k++)
		((double*)x)[k] = alpha*((double*)a)[k] + beta*((double*)b)[k]
				+ gamma*((double*)c)[k] + delta*((double*)d)[k] + eta*((double*)e)[k];
}






void PC::build_rhs(StateVector& Xrhs, const int icorr)
{
	int lm0, lm1;
	lm0 = 0;		lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = 1.0;
		double beta = 0.0;
		if (icorr) {
			alpha = 1.0 - ax;		// default is 0.5
			beta = ax;
		}
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;
			beta = -beta;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic);
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[0]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		if (icorr) {
			xs_array2d<cplx> NL1 = NL[1]->get_comp(ic2);
			for (int ir=i0; ir<=i1; ir++) {
				vec_sum2(X[ir], L0[ir], NL0[ir], NL1[ir],
						alpha, beta, 
						lm0, lm1);
			}
		} else {
			for (int ir=i0; ir<=i1; ir++) {
				vec_sum1(X[ir], L0[ir], NL0[ir],
						alpha, 
						lm0, lm1);
			}
		}
	}
}

void PC::step_3_end(int kill_sbr)
{
	#pragma omp parallel
	{
		apply_Mrhs(*Lin[0], Xlm);	//implicit_rhs(Ylm, Xlm);
		build_rhs(Xlm, 0);
		//build_rhs2(Xlm, *Lin[0], *NL[0], *NL[0], 1.0, 0.0);		// first stage is explicit Euler + Crank-Nicolson
		update_all_BC(dt);		// t + dt
		solve_state(Xlm, kill_sbr);
	}
	for (int c=2; c>0; c--) {		// 2 corrector stages
		// corrector: Crank-Nicolson (order 2)
		explicit_terms(*NL[1], Xlm, 0);		// NL(t+dt) estimate
		#pragma omp parallel
		{
			//build_rhs2(Xlm, *Lin[0], *NL[0], *NL[1], 1.0-jpar.pc2_ax, jpar.pc2_ax);	// Crank-Nicolson type
			//build_rhs2(Xlm, *Lin[0], *NL[0], *NL[1], 0.5, 0.5);	// Crank-Nicolson type
			build_rhs(Xlm, 1);
			solve_state(Xlm, kill_sbr);	// corrector for t+dt
		}
	}
}


void ARK::build_rhs(StateVector& Xrhs, const int k)
{
	int lm0, lm1;

	lm0 = 0;		lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

/*	#pragma omp master
	{
	PRINTF0(" X = X0 + %.3fL0 + %.3f*NL0", ai[k][0], ax[k][0]);
	for (int l=1; l<=k; l++)
		PRINTF0(" + %.3f*L%d + %.3f*NL%d", ai[k][l-1], l, ax[k][l], l);
	PRINTF0("\n");
	}*/

	for (int ip=0; ip<comp_list.size(); ip++) {
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		const int ic = comp_list[ip].ic;
		const int ic2 = comp_list[ip].ic_nl;
		double nl_sign = 1.0;
		if (comp_list[ip].nl_sign < 0) nl_sign = -1.0;
		double lin_sgn = 1.0;
		if (ic==0) lin_sgn = -1.0;
		xs_array2d<cplx> lin[5];
		xs_array2d<cplx> nl[4];
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		lin[0] = Lin[0]->get_comp(ic);
		for (int l=0; l<=k; l++) {
			lin[l+1] = Lin[l+1]->get_comp(ic);
			nl[l] = NL[l]->get_comp(ic2);
		}

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
				vec_sum2(X[ir], lin[0][ir], lin[1][ir], nl[0][ir],
						lin_sgn*ai[k][0], nl_sign*ax[k][0],  lm0, lm1);
			for (int l=1; l<=k; l++) {
				vec_sum2(X[ir], X[ir], lin[l+1][ir], nl[l][ir], 
						lin_sgn*ai[k][l], nl_sign*ax[k][l],  lm0, lm1);
			}
		}
	}
}

void ARK::step_3_end(int kill_sbr)
{
	double CM_lhs_old = CM_lhs;		// either thread-local (stack) or only one copy (if not parallel yet)

	for (int k=0; k<nex; k++) {		// nex stages
		if ((k>0) && ((k<nex-1)||(ax[k][k]!=0.)))		// skip last explicit evaluation if not required (as in BPR353)
			explicit_terms(*NL[k], Xlm, (k==0));		// NL(k) at substep k

		#pragma omp parallel
		{
			if (k==0) {
				apply_Mrhs(*Lin[0], Xlm);
			}
			apply_M(*Lin[k+1], Xlm);
			build_rhs(Xlm, k);
			update_all_BC(dt*c[k]);		// t + dtk [total step is dt]
			if (CM_lhs_old != -ai[k][k+1]) {	// should we recompute the matrices ?
				#pragma omp barrier
					calc_matrices_parallel(dt, -ai[k][k+1]);
				#pragma omp barrier
			}
			solve_state(Xlm, kill_sbr);		// substep k completed.
		}
		CM_lhs_old = -ai[k][k+1];		// update to current value.
	}
	CM_lhs = CM_lhs_old;		// update global variable
}

void CNAB2::full_rhs(StateVector& Xrhs, int nl_0)
{
	apply_Mrhs(*Lin[0], Xrhs);

	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

//	#pragma omp master
//	printf("kk=%d,  cex=%g,%g\n", nl_0, cex[nl_0], cex[1-nl_0]);

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = cex[0];
		double beta  = cex[1];
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;		beta = -beta;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[nl_0]->get_comp(ic2);
		xs_array2d<cplx> NL1 = NL[1-nl_0]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum2(X[ir], L0[ir], NL0[ir], NL1[ir],
					alpha, beta, 
					lm0, lm1);
		}
	}
}

void SBDF2::full_rhs(StateVector& Xrhs, int nl_0)
{
	apply_Mrhs(*Lin[nl_0], Xrhs);		// 1/dt operator

	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = cex[nl_0];
		double beta  = cex[1-nl_0];
		double f0 = cim[nl_0];
		double f1 = cim[1-nl_0];
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;		beta = -beta;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> L1 = Lin[1]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[0]->get_comp(ic2);
		xs_array2d<cplx> NL1 = NL[1]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {		// overwrite Xrhs
			vec_sum4(X[ir], L0[ir], L1[ir], NL0[ir], NL1[ir],
					f0, f1, alpha, beta,
					lm0, lm1);
		}
	}
}

void SBDF3::full_rhs(StateVector& Xrhs, int nl_0)
{
	apply_Mrhs(*Lin[nl_0], Xrhs);		// 1/dt operator

	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	int ii0 = -nl_0;	if (ii0<0) ii0+=3;		// same as:  const int ii0 = (3-nl_0)%3;
	int ii1 = ii0-2;	if (ii1<0) ii1+=3;		// same as:  const int ii1 = (4-nl_0)%3;
	int ii2 = ii0-1;	if (ii2<0) ii2+=3;		// same as:  const int ii2 = (5-nl_0)%3;

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = cex[ii0];
		double beta  = cex[ii1];
		double gamma = cex[ii2];
		double f0 = cim[ii0];
		double f1 = cim[ii1];
		double f2 = cim[ii2];
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;		beta = -beta;	gamma = -gamma;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> L1 = Lin[1]->get_comp(ic);
		xs_array2d<cplx> L2 = Lin[2]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[0]->get_comp(ic2);
		xs_array2d<cplx> NL1 = NL[1]->get_comp(ic2);
		xs_array2d<cplx> NL2 = NL[2]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {		// overwrite Xrhs
			vec_sum4(X[ir], L0[ir], L1[ir], NL0[ir], NL1[ir],
					f0, f1, alpha, beta,
					lm0, lm1);
			vec_sum2(X[ir], X[ir], L2[ir], NL2[ir],
					f2, gamma, 
					lm0, lm1);
		}
	}
}



#ifdef XS_UNSAFE_STEPPERS
/* BAD TIME-STEPPER (performance) */

class CNAB3 : public MultiStep<CNAB3> {
	double cex[3];		///< coefficients for explicit term steps
	double dto[2];		///< previous time-steps

  public:
    CNAB3() {
		PRINTF0("=> Using CNAB3 time-stepper\n");
		nim = 1;	nex = 3;
		CM_rhs = 0.5;	// Crank-Nicolson part, linear operators multiplied by 0.5
		CM_lhs = -0.5;	// Crank-Nicolson part, lhs operators multiplied by -0.5
		Cdt_rhs = 1.0;
		Cdt_lhs = 1.0;
		cex[0] = 1.0;  	cex[1] = 0.0;	cex[2] = 0.0;	// Euler 1 for the first step !
		dto[0] = 0.0;	dto[1] = 0.0;
	}
	void set_dt(double dt) {
		double alpha = 0.0;
		double beta = 0.0;
		if (dto[1] != 0.0) {
			double dt2 = dto[0] + dto[1];
			double dt1 = dto[0];
			alpha = - (dt2/(2*dt1) + dt/(3*dt1)) / (dt2/dt - dt1/dt);
			beta = (dt1/(2*dt2) + dt/(3*dt2)) / (dt2/dt - dt1/dt);
		} else if (dto[0] != 0.0) {
			alpha = -dt/(dto[0]+dto[0]);		// = -0.5 for constant time-step
		}
		cex[0] = 1. - alpha - beta;		cex[1] = alpha;		cex[2] = beta;	// Adams-Bashforth 3: 23/12, -16/12, 5/12
		if (differ(dto[0],dt)) calc_matrices(dt);
		dto[1] = dto[0];
		dto[0] = dt;	// store dt for next time-step.
	}
	void full_rhs(StateVector& Xrhs, int nl_0);
};


class MCNAB2 : public MultiStep<MCNAB2> {
	double dto;			///< previous time-step
	double cex[2];		///< coefficients for explicit term steps
	double cim[2];

	void implicit_rhs(StateVector& Ym, StateVector& Ydt, const StateVector& X);

  public:
    MCNAB2() {
		PRINTF0("=> Using MCNAB time-stepper\n");
		nim = 3;	nex = 2;
		CM_rhs = 0.375;		// MCN part, rhs linear operators multiplied by 3/8
		CM_lhs = -0.5625;	// MCN part, lhs linear operators multiplied by 9/16
		Cdt_rhs = 0.0;
		Cdt_lhs = 1.0;
		dto = 0.0;  cex[0] = 1.0;  cex[1] = 0.0;		// Euler 1 for the first step !
		cim[0] = 7./6.;	cim[1] = 0.0;
	}
	void set_dt(double dt) {
		if (dto != 0.0) {
			double alpha = 0.5;
			if (dt_adjust) alpha = dt/(dto+dto);		// = 0.5 for constant time-step
			if (dt_adjust) runerr("variable time-step not supported by MCNAB2 scheme");
			cex[0] = 1.0 + alpha;		cex[1] = -alpha;
			cim[0] = 1.0;	cim[1] = 1./6.;
		}
		if (differ(dto,dt)) calc_matrices(dt);
		dto = dt;		// store dt for next time-step.
	}
	void full_rhs(StateVector& Xrhs, int nl_0);
};

class SBDF1 : public MultiStep<SBDF1> {
  public:
    SBDF1() {
		PRINTF0("=> Using SBDF1 time-stepper\n");
		nim = 1;	nex = 1;
		CM_rhs = 0.0;	// BDF part, linear operators are not involved in rhs
		CM_lhs = -1.0;	// BDF part, lhs linear operators are -1.0 times rhs operators
		Cdt_rhs = 1.0;
		Cdt_lhs = 1.0;
	}
	void full_rhs(StateVector& Xrhs, int nl_0);
};


class CNLF2 : public MultiStep<CNLF2> {
	double dto;			///< previous time-step
	double cex[1];		///< coefficients for explicit term steps

	void implicit_rhs(StateVector& Y, StateVector& Y1, StateVector& X);

  public:
    CNLF2() {
		PRINTF0("=> Using CNLF time-stepper\n");
		nim = 3;	nex = 1;
		CM_rhs = 0.5;	// CN part, rhs linear operators multiplied by 1/2
		CM_lhs = -0.5;	// CN part, lhs linear operators multiplied by -1/2
		Cdt_rhs = 1.0;
		Cdt_lhs = 1.0;		// the very first time-step is problematic...
		dto = 0.0;  cex[0] = 1.0;
	}
	void set_dt(double dt) {
		if (dto != 0.0) {
			if (dt_adjust) runerr("variable time-step not supported by CNLF scheme");
			cex[0] = 1.0;
		} else {
			if (evol_ubt & EVOL_U) Lin[1]->U.copy(Xlm.U);
			if (evol_ubt & EVOL_B) Lin[1]->B.copy(Xlm.B);
			if (evol_ubt & EVOL_T) Lin[1]->T.copy(Xlm.T);
			Cdt_rhs = 0.5;
			Cdt_lhs = 0.5;		// the very first time-step is problematic...
			calc_matrices(dt);
		}
		if (differ(dto,dt)) calc_matrices(dt);
		dto = dt;		// store dt for next time-step.
	}
	void full_rhs(StateVector& Xrhs, int nl_0);
};


/// Diagonaly Implicit-Explicit RK (DIRK), following Ascher (1997).
/// and in the special case where their equation (2.3) hold (ie. stiffly accurate schemes).
class DIRK : public Stepper {
  protected:
	double c[4];		// substep positions (actually increments from previous time)
	double ai[4][4];	// RK matrix for implicit part (DIRK) [including final weights]
	double ax[4][4];	// RK matrix for explicit part  [including final weights]

	void build_rhs(StateVector& Xrhs, const int k);
	void init_matrices() {
		CM_rhs = 0;
		Cdt_rhs = 1.0;
		CM_lhs = -ai[0][0];		// diagonal
		Cdt_lhs = 1.0;
		if (nex_eval == 0) nex_eval = nex;		// default value for nex_eval
	}
public:
	void step_3_end(int kill_sbr) override;
};

/// DIRK (2,2,2), see Ascher 1997, sec 2.6.
class DIRK222 : public DIRK {
  public:
	DIRK222() {
		PRINTF0("=> Using DIRK(2,2,2) time-stepper\n");
		double gamma = 1. - sqrt(0.5);
		double delta = -sqrt(0.5);
		nim = 2;		nex = 2;
		c[0] = gamma;		c[1] = 1.-gamma;		// time increments (total=1)
		ai[0][0] = gamma;	// rhs
		ai[1][0] = 1.-gamma;	ai[1][1] = gamma;
		ax[0][0] = gamma;
		ax[1][0] = delta;		ax[1][1] = 1.-delta;
		init_matrices();
	}
};

/// DIRK (4,4,3), see Ascher 1997, sec 2.8
class DIRK443 : public DIRK {
  public:
	DIRK443() {
		PRINTF0("=> Using DIRK(4,4,3) time-stepper\n");
		nim = 4;		nex = 4;
		c[0] = 0.5;		c[1] = 1./6.;	c[2] = -1./6.;	c[3] = 0.5;		// time increments (total=1)
		ai[0][0] = 0.5;		// on the rhs
		ai[1][0] = 1./6.;
		ai[2][0] = -0.5;		ai[2][1] = 0.5;
		ai[3][0] = 1.5;			ai[3][1] = -1.5;		ai[3][2] = 0.5;
		ax[0][0] = 0.5;
		ax[1][0] = 11./18.;		ax[1][1] = 1./18;
		ax[2][0] = 5./6.;		ax[2][1] = -5./6.;		ax[2][2] = 0.5;
		ax[3][0] = 0.25;		ax[3][1] = 1.75;		ax[3][2] = 0.75;		ax[3][3] = -1.75;
		init_matrices();
	}
};

/// scheme (9) from Duan 2016 : http://arxiv.org/pdf/1606.02053v1.pdf
class ASISSP332a : public DIRK {
  public:
	ASISSP332a() {
		PRINTF0("=> Using ASI-SSP(3',3,2)a time-stepper\n");
		nim = 3;		nex = 3;
		c[0] = 0.5;		c[1] = 0.5;		c[2] = 0;		// time increments (total=1)
		ai[0][0] = 0.5;		// on the rhs
		ai[1][0] = 0.5;
		ai[2][0] = 1.0;		ai[2][1] = -0.5;
		ax[0][0] = 0.5;
		ax[1][0] = 0.5;		ax[1][1] = 0.5;
		ax[2][0] = 1./3.;	ax[2][1] = 1./3.;		ax[2][2] = 1./3.;
		init_matrices();
	}
};

/// scheme (10) from Duan 2016 : http://arxiv.org/pdf/1606.02053v1.pdf
class ASISSP332b : public ARK {
  public:
	ASISSP332b() {
		PRINTF0("=> Using ASI-SSP(3',3,2)b time-stepper\n");
		nim = 3;		nex = 3;
		c[0] = 0.5;		c[1] = 0.5;		c[2] = 0;		// time increments (total=1)
		ai[0][0] = 0.0;		ai[0][1] = 0.5;		// on the rhs
		ai[1][0] = 0.0;		ai[1][1] = 23./25.;		ai[1][2] = 2./25.;
		ai[2][0] = 0.0;		ai[2][1] = 1.0;			ai[2][1] = -3./8.;		ai[2][2] = 3./8.;
		ax[0][0] = 0.5;
		ax[1][0] = 0.5;		ax[1][1] = 0.5;
		ax[2][0] = 1./3.;	ax[2][1] = 1./3.;		ax[2][2] = 1./3.;
		init_matrices();
	}
};

/// scheme (15) from Duan 2016 : http://arxiv.org/pdf/1606.02053v1.pdf
class ASISSP332c : public DIRK {
  public:
	ASISSP332c() {
		PRINTF0("=> Using ASI-SSP(3',3,2)c time-stepper\n");
		nim = 3;		nex = 3;
		c[0] = 5./6.;		c[1] = 5./6.;		c[2] = -2./3.;		// time increments (total=1)
		ai[0][0] = 5./6.;		// on the rhs
		ai[1][0] = 5./6.;
		ai[2][0] = 11./15.;		ai[2][1] = -17./30.;
		ax[0][0] = 5./6.;
		ax[1][0] = 5./6.;	ax[1][1] = 5./6.;
		ax[2][0] = 5./3.;	ax[2][1] = 1./5.;		ax[2][2] = 1./5.;
		init_matrices();
	}
};

/// scheme (17) from Duan 2016 : http://arxiv.org/pdf/1606.02053v1.pdf
class ASISSP442a : public DIRK {
  public:
	ASISSP442a() {
		PRINTF0("=> Using ASI-SSP(4',4,2)a time-stepper\n");
		nim = 4;		nex = 4;
		c[0] = 1./3.;		c[1] = 1./3.;		c[2] = 1./3.;	c[3] = 0.0;		// time increments (total=1)
		ai[0][0] = 1./3.;		// on the rhs
		ai[1][0] = 1./3.;
		ai[2][0] = 1./5.;	ai[2][1] = 7./15.;
		ai[3][0] = 0.5;		ai[3][1] = 0.5;		ai[3][2] = -1./3.;
		ax[0][0] = 1./3.;
		ax[1][0] = 1./3.;	ax[1][1] = 1./3.;
		ax[2][0] = 1./3.;	ax[2][1] = 1./3.;		ax[2][2] = 1./3.;
		ax[3][0] = 0.25;	ax[3][1] = 0.25;		ax[3][2] = 0.25;		ax[3][3] = 0.25;
		init_matrices();
	}
};

/// scheme (18) from Duan 2016 : http://arxiv.org/pdf/1606.02053v1.pdf
class ASISSP442b : public DIRK {
  public:
	ASISSP442b() {
		PRINTF0("=> Using ASI-SSP(4',4,2)b time-stepper\n");
		nim = 4;		nex = 4;
		c[0] = 1./3.;		c[1] = 1./3.;		c[2] = 1./3.;	c[3] = 0.0;		// time increments (total=1)
		ai[0][0] = 1./3.;		// on the rhs
		ai[1][0] = 1./3.;
		ai[2][0] = 10./9.;	ai[2][1] = -4./9.;
		ai[3][0] = 6./5.;	ai[3][1] = -9./10.;		ai[3][2] = 11./30.;
		ax[0][0] = 1./3.;
		ax[1][0] = 1./3.;	ax[1][1] = 1./3.;
		ax[2][0] = 1./3.;	ax[2][1] = 1./3.;		ax[2][2] = 1./3.;
		ax[3][0] = 0.25;	ax[3][1] = 0.25;		ax[3][2] = 0.25;		ax[3][3] = 0.25;
		init_matrices();
	}
};


class BPR342 : public ARK {
  public:
	BPR342() {
		PRINTF0("=> Using ARK BPR342 time-stepper\n");
		nim = 4;		nex = 3;
		c[0] = 1;	c[1] = -1./3.;	c[2] = 1./3.;		// time increments (total=1)
		ai[0][0] = 0.5;		ai[0][1] = 0.5;
		ai[1][0] = 5./18.;	ai[1][1] = -1./9.;	ai[1][2] = 0.5;
		ai[2][0] = 0.5;		ai[2][1] = 0;		ai[2][2] = 0;		ai[2][3] = 0.5;
		ax[0][0] = 1.0;
		ax[1][0] = 4./9.;	ax[1][1] = 2./9.;
		ax[2][0] = 0.25;	ax[2][1] = 0;		ax[2][2] = 0.75;
		init_matrices();
	}
};

class ARK2A2 : public ARK {
  public:
	ARK2A2() {
		PRINTF0("=> Using ARK(2A2) time-stepper\n");
		nim = 3;		nex = 2;
		c[0] = 0.5;		c[1] = 0.5;				// time increments (total=1)
		ai[0][0] = 0.0;		ai[0][1] = 0.5;
		ai[1][0] = 0.5;		ai[1][1] = 0.0;		ai[1][2] = 0.5;
		ax[0][0] = 0.5;
		ax[1][0] = 0;		ax[1][1] = 1;
		init_matrices();
	}
};

class ARK2A3 : public ARK {
  public:
	ARK2A3() {
		PRINTF0("=> Using ARK(2A3) time-stepper\n");
		nim = 3;		nex = 2;
		c[0] = 0.25;		c[1] = 0.75;				// time increments (total=1)
		ai[0][0] = -0.25;	ai[0][1] = 0.5;
		ai[1][0] = 0.5;		ai[1][1] = 0.0;		ai[1][2] = 0.5;
		ax[0][0] = 0.25;
		ax[1][0] = -1;		ax[1][1] = 2;
		init_matrices();
	}
};

/// Crank-Nicolson - Euler 1
class CNE1 : public ARK {
  public:
	CNE1() {
		PRINTF0("=> Using CNE1 time-stepper\n");
		nim = 2;		nex = 1;
		c[0] = 1.0;			// time increments (total=1)
		ai[0][0] = 0.5;		ai[0][1] = 0.5;
		ax[0][0] = 1.0;
		init_matrices();
	}
};

class PC12 : public ARK {
  public:
	PC12() {
		PRINTF0("=> Using PC12 time-stepper\n");
		nim = 3;		nex = 2;
		c[0] = 1.0;		c[1] = 0.0;			// time increments (total=1)
		ai[0][0] = 0.5;		ai[0][1] = 0.5;
		ai[1][0] = 0.5;		ai[1][1] = 0.0;		ai[1][2] = 0.5;
		ax[0][0] = 1.0;
		ax[1][0] = 0.5;		ax[1][1] = 0.5;
		init_matrices();
	}
};


class ARK2L1 : public ARK {
  public:
	ARK2L1() {
		PRINTF0("=> Using ARK(2L1) time-stepper\n");
		nim = 3;		nex = 2;
		c[0] = 0.5;		c[1] = 0.5;				// time increments (total=1)
		double g = sqrt(0.5);
		ai[0][0] = g-0.5;	ai[0][1] = 1.-g;
		ai[1][0] = 1.-g;	ai[1][1] = 2.*g-1.;		ai[1][2] = 1.-g;
		ax[0][0] = 0.5;
		ax[1][0] = 0;		ax[1][1] = 1;
		init_matrices();

		//set_default_cfl_u_b_cori(0.6, 2.25, 0.85);		// default CFL safety factors for BPR353.
	}
};

class ARK3A3 : public ARK {
  public:
	ARK3A3() {
		PRINTF0("=> Using ARK(3A3) time-stepper\n");
		nim = 5;		nex = 4;
		c[0] = 0.5;		c[1] = 0.0;		c[2] = 0.5;		c[3] = 0.0;		// time increments (total=1)
		ai[0][0] = 0;		ai[0][1] = 0.5;
		ai[1][0] = 0.25;	ai[1][1] = -5./12.;	ai[1][2] = 2./3.;
		ai[2][0] = 2;		ai[2][1] = -3.5;	ai[2][2] = 0.5;		ai[2][3] = 2;
		ai[3][0] = 1./6.;	ai[3][1] = 0;		ai[3][2] = 2./3.;	ai[3][3] = -5./6.;		ai[3][4] = 1;
		ax[0][0] = 0.5;
		ax[1][0] = 0.25;	ax[1][1] = 0.25;
		ax[2][0] = 0;		ax[2][1] = 1;		ax[2][2] = 0;
		ax[3][0] = 1./6.;	ax[3][1] = 0;		ax[3][2] = 2./3.;	ax[3][3] = 1./6.;
		init_matrices();

		set_default_cfl_u_b_cori(0.65, 2.8, 0.9);		// default CFL safety factors for ARK3A3.
	}
};

class ARK3A2 : public ARK {
  public:
	ARK3A2() {
		PRINTF0("=> Using ARK(3A2) time-stepper\n");
		nim = 5;		nex = 4;
		c[0] = 2./9.;		c[1] = 1./9.;		c[2] = 0.5;		c[3] = 1./6.;		// time increments (total=1)
		ai[0][0] = -44./45.;	ai[0][1] = 6./5.;
		ai[1][0] = -47./300.;	ai[1][1] = -71./100.;	ai[1][2] = 6./5.;
		ai[2][0] = 27./8.;		ai[2][1] = -13./4.;		ai[2][2] = -59./120.;	ai[2][3] = 6./5.;
		ai[3][0] = 89./50.;		ai[3][1] = -486./55.;	ai[3][2] = 89./10.;		ai[3][3] = -562./275.;	ai[3][4] = 6./5.;
		ax[0][0] = 2./9.;
		ax[1][0] = 71./420.;	ax[1][1] = 23./140.;
		ax[2][0] = -281./336.;	ax[2][1] = 187./112.;	ax[2][2] = 0;
		ax[3][0] = 1./10.;		ax[3][1] = 0;			ax[3][2] = 1./2.;	ax[3][3] = 2./5.;
		init_matrices();
	}
};

class ARK3L1 : public ARK {
  public:
	ARK3L1() {
		PRINTF0("=> Using ARK(3L1) time-stepper\n");
		nim = 5;		nex = 4;
		c[0] = 0.25;		c[1] = 0.25;		c[2] = 0.25;		c[3] = 0.25;		// time increments (total=1)
		ai[0][0] = 0.15;	ai[0][1] = 0.10;
		ai[1][0] = 0.9;		ai[1][1] = -1.3;	ai[1][2] = 0.9;
		ai[2][0] = 1.7;		ai[2][1] = -2.75;	ai[2][2] = 1.5;		ai[2][3] = 0.3;
		ai[3][0] = 1;		ai[3][1] = -10./3.;	ai[3][2] = 17./3.;	ai[3][3] = -10./3.;		ai[3][4] = 1;
		ax[0][0] = 0.25;
		ax[1][0] = 0;		ax[1][1] = 0.5;
		ax[2][0] = -0.5;	ax[2][1] = 1.25;	ax[2][2] = 0;
		ax[3][0] = 0;		ax[3][1] = 2./3.;	ax[3][2] = -1./3.;	ax[3][3] = 2./3.;
		init_matrices();
	}
};

class SMR32 : public ARK {
  public:
	SMR32() {
		PRINTF0("=> Using ARK SMR32 time-stepper (Spalart et al 1991)\n");
		nim = 4;		nex = 3;
		c[0] = 8./15.;		c[1] = 2./15.;	c[2] = 1./3.;		// time increments (total=1)
		ai[0][0] = 29./96.;		ai[0][1] = 37./160.;
		ai[1][0] = ai[0][0];	ai[1][1] = ai[0][1] - 3./40.;	ai[1][2] = 5./24.;
		ai[2][0] = ai[0][0];	ai[2][1] = ai[1][1];			ai[2][2] = ai[1][2] + 1./6.;	ai[2][3] = 1./6.;
		ax[0][0] = c[0];
		ax[1][0] = c[0] - 17./60.;	ax[1][1] = 5./12.;
		ax[2][0] = ax[1][0];		ax[2][1] = 0.0;		ax[2][2] = 3./4.;
		init_matrices();
	}
};


class FW22 : public ARK {
  public:
	FW22() {		// Fritzen and Wittekindt 1997
		PRINTF0("=> Using ARK FW22 time-stepper\n");
		nim = 3;		nex = 2;
		c[0] = 1;	c[1] = 0.0;			// time increments (total=1)
		ai[0][0] = 0.0;		ai[0][1] = 1.0;
		ai[1][0] = 0.5;		ai[1][1] = 0.0;		ai[1][2] = 0.5;
		ax[0][0] = 1.0;
		ax[1][0] = 0.5;		ax[1][1] = 0.5;
		init_matrices();
	}
};

class FW35 : public ARK {
  public:
	FW35() {		// Fritzen and Wittekindt 1997
		PRINTF0("=> Using ARK FW35 time-stepper\n");
		nim = 5;		nex = 4;
		c[0] = 1.0;		c[1] = 0.0;		c[2] = -0.5;	c[3] = 0.5;		// time increments (total=1)
		ai[0][0] = 0.0;		ai[0][1] = 1.0;
		ai[1][0] = 0.5;		ai[1][1] = 0.0;		ai[1][2] = 0.5;
		ai[2][0] = 0.25;	ai[2][1] = -0.25;	ai[2][2] = 0;		ai[2][3] = 0.5;
		ai[3][0] = 1./6.;	ai[3][1] = 1./6.;	ai[3][2] = -1./6.;	ai[3][3] = 4./6.;		ai[3][4] = 1./6.;
		ax[0][0] = 1.0;
		ax[1][0] = 0.5;		ax[1][1] = 0.5;
		ax[2][0] = 0.25;	ax[2][1] = 0.25;	ax[2][2] = 0;
		ax[3][0] = 1./6.;	ax[3][1] = 1./6.;	ax[3][2] = 0;	ax[3][3] = 4./6.;
		init_matrices();
	}
};

/// Same as PC2 (but less optimized).
class ARK32 : public ARK {		// Second order accurate, but larger time-steps expected ?
  public:
	ARK32() {
		PRINTF0("=> Using ARK32 time-stepper\n");
		nim = 4;		nex = 3;
		c[0] = 1.0;		c[1] = 0;		c[2] = 0;		// time increments (total=1)
		ai[0][0] = 0.5;	ai[0][1] = 0.5;
		ai[1][0] = 0.5;	ai[1][1] = 0;	ai[1][2] = 0.5;
		ai[2][0] = 0.5;	ai[2][1] = 0;	ai[2][2] = 0;		ai[2][3] = 0.5;
		ax[0][0] = 1.0;
		ax[1][0] = 0.5;		ax[1][1] = 0.5;
		ax[2][0] = 0.5;		ax[2][1] = 0;		ax[2][2] = 0.5;
		init_matrices();
		set_default_cfl_u_b_cori(0.65, 2.8, 1.0);		// default CFL safety factors for PC2.
	}
};

class SMR91 : public ARK {		// From Spalart, Moser, Rogers 1991 (JCP), appendix
  public:						// Peak CFL up to 2 are used, with CFL = 2pi/3*(u/dx+v/dy+w/dz)*dt
	SMR91() {
		PRINTF0("=> Using SMR91 time-stepper\n");
		nim = 4;		nex = 3;
		c[0] = 8./15.;		c[1] = 2./15.;		c[2] = 1./3.;		// time increments (total=1)
		//~ ai[0][0] = 29./96.;	ai[0][1] = 37./160.;
		//~ ai[1][0] = 0.0;		ai[1][1] = -3./40.;		ai[1][2] = 5./24.;
		//~ ai[2][0] = 0.0;		ai[2][1] = 0;			ai[2][2] = 1./6.;		ai[2][3] = 1./6.;
		//~ ax[0][0] = 8./15.;
		//~ ax[1][0] = -17./60.;	ax[1][1] = 5./12.;
		//~ ax[2][0] = 0.0;			ax[2][1] = -5./12.;		ax[2][2] = 3./4.;
		ai[0][0] = 29./96.;	ai[0][1] = 37./160.;
		ai[1][0] = 29./96.;	ai[1][1] = 5./32.;	ai[1][2] = 5./24.;
		ai[2][0] = 29./96.;	ai[2][1] = 5./32.;	ai[2][2] = 3./8.;	ai[2][3] = 1./6.;
		ax[0][0] = 8./15.;
		ax[1][0] = 1./4.;	ax[1][1] = 5./12.;
		ax[2][0] = 1./4.;	ax[2][1] = 0.0;		ax[2][2] = 3./4.;
		init_matrices();
		set_default_cfl_u_b_cori(0.65, 1.0, 1.0);		// default CFL safety factors for PC2.
	}
};

// See Cavaglieri & Bewley (2015), http://dx.doi.org/10.1016/j.jcp.2015.01.031.
class CNRKW3 : public ARK {
  public:
	CNRKW3() {
		PRINTF0("=> Using CN/RKW3 time-stepper\n");
		nim = 4;		nex = 3;
		c[0] = 8./15;	c[1] = 2./15.;	c[2] = 1./3.;		// time increments (total=1)
		ai[0][0] = 4./15.;	ai[0][1] = 4./15.;
		ai[1][0] = 4./15.;	ai[1][1] = 1./3.;	ai[1][2] = 1./15.;
		ai[2][0] = 4./15.;	ai[2][1] = 1./3.;	ai[2][2] = 7./30.;		ai[2][3] = 1./6.;
		ax[0][0] = 8./15.;
		ax[1][0] = 0.25;	ax[1][1] = 5./12.;
		ax[2][0] = 0.25;	ax[2][1] = 0;		ax[2][2] = 0.75;
		init_matrices();
	}
};



/*
/// DIRK (4,4,3), see Ascher 1997, sec 2.8
class DIRK443 : public ARK {
  public:
	DIRK443() {
		PRINTF0("=> Using DIRK(4,4,3) time-stepper\n");
		nim = 5;		nex = 4;
		c[0] = 0.5;		c[1] = 1./6.;	c[2] = -1./6.;	c[3] = 0.5;		// time increments (total=1)
		ai[0][0] = 0;	ai[0][1] = 0.5;
		ai[1][0] = 0;	ai[1][1] = 1./6.;	ai[1][2] = 0.5;
		ai[2][0] = 0;	ai[2][1] = -0.5;	ai[2][2] = 0.5;		ai[2][3] = 0.5;
		ai[3][0] = 0;	ai[3][1] = 1.5;		ai[3][2] = -1.5;	ai[3][3] = 0.5;		ai[3][4] = 0.5;
		ax[0][0] = 0.5;
		ax[1][0] = 11./18.;		ax[1][1] = 1./18;
		ax[2][0] = 5./6.;		ax[2][1] = -5./6.;		ax[2][2] = 0.5;
		ax[3][0] = 0.25;		ax[3][1] = 1.75;		ax[3][2] = 0.75;		ax[3][3] = -1.75;
		CM_rhs = 0;
		Cdt_rhs = 1.0;
		CM_lhs = -ai[0][1];		// diagonal
		Cdt_lhs = 1.0;
	}
};
*/


/// compute the linear right hand side term.
void MCNAB2::implicit_rhs(StateVector& Ym, StateVector& Ydt, const StateVector& X)
{
	int lm0, lm1;
	lm0 = 0;	lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	for (int op=0; op<nop3; op++) {
		int ic = op3[op].ic;
		op3[op].Mrhs->apply(X.get_comp(ic), Ym.get_comp(ic), lm0, lm1);
		int f = op3[op].f;
		int i0 = Ydt[f].irs;
		int i1 = Ydt[f].ire;
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {		// copy
			vec_cpy( Ydt.get_comp(ic)[ir], X.get_comp(ic)[ir], lm0, lm1);
		}
	}
	for (int op=0; op<nop5; op++) {
		int ic = op5[op].ic;
		op5[op].Mrhs->apply(X.get_comp(ic), Ym.get_comp(ic), lm0, lm1);
		op5[op].Lap->apply(X.get_comp(ic), Ydt.get_comp(ic), lm0, lm1);
	}
}

void MCNAB2::full_rhs(StateVector& Xrhs, int nl_0)
{
	implicit_rhs(*Lin[nl_0], *Lin[2], Xrhs);
	
	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	barrier_shared_mem();

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = cex[nl_0];
		double beta  = cex[1-nl_0];
		double f0 = cim[nl_0];
		double f1 = cim[1-nl_0];
		double f2 = 1.0/dt;
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int f = comp_list[ip].f;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;		beta = -beta;
		}
		if ((f==::U) && (ic == 0)) {		// poloidal velocity
			f2 = -f2;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> L1 = Lin[1]->get_comp(ic);
		xs_array2d<cplx> L2 = Lin[2]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[0]->get_comp(ic2);
		xs_array2d<cplx> NL1 = NL[1]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum5(X[ir], L0[ir], L1[ir], L2[ir], NL0[ir], NL1[ir],
					f0, f1, f2, alpha, beta,
					lm0, lm1);
		}
	}
}

void CNAB3::full_rhs(StateVector& Xrhs, int nl_0)
{
	apply_Mrhs(*Lin[0], Xrhs);

	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

//	#pragma omp master
//	printf("kk=%d,  cex=%g,%g,%g\n", nl_0, cx[(3-nl_0)%3], cx[(4-nl_0)%3], cx[(5-nl_0)%3]);

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = cex[(3-nl_0)%3];
		double beta  = cex[(4-nl_0)%3];
		double gamma = cex[(5-nl_0)%3];
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;		beta = -beta;	gamma = -gamma;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[0]->get_comp(ic2);
		xs_array2d<cplx> NL1 = NL[1]->get_comp(ic2);
		xs_array2d<cplx> NL2 = NL[2]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum3(X[ir], L0[ir], NL0[ir], NL1[ir], NL2[ir],
					alpha, beta, gamma,
					lm0, lm1);
		}
	}
}


void SBDF1::full_rhs(StateVector& Xrhs, int)
{
	apply_Mrhs(*Lin[0], Xrhs);

	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = 1.0;
		double f0 = 1.0;
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[0]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {		// overwrite Xrhs
			vec_sum2(X[ir], L0[ir], NL0[ir],
					f0, alpha,
					lm0, lm1);
		}
	}
}

/// compute the linear right hand side term.
void CNLF2::implicit_rhs(StateVector& Y, StateVector& Y1, StateVector& X)
{
	int lm0, lm1;
	lm0 = 0;	lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	for (int op=0; op<nop3; op++) {
		int f = op3[op].f;
		int ic = op3[op].ic;
		op3[op].Mrhs->apply(Y1.get_comp(ic), Y.get_comp(ic), lm0, lm1);
		int i0 = Y[f].irs;
		int i1 = Y[f].ire;
		thread_interval_rad(i0, i1);
		#pragma omp barrier
		for (int ir=i0; ir<=i1; ir++) {		// copy
			vec_cpy( Y1.get_comp(ic)[ir], X.get_comp(ic)[ir], lm0, lm1);
		}
	}
	for (int op=0; op<nop5; op++) {
		int f = op5[op].f;
		int ic = op5[op].ic;
		op5[op].Mrhs->apply(Y1.get_comp(ic), Y.get_comp(ic), lm0, lm1);
		int i0 = Y[f].irs;
		int i1 = Y[f].ire;
		thread_interval_rad(i0, i1);
		#pragma omp barrier
		for (int ir=i0; ir<=i1; ir++) {		// copy
			vec_cpy( Y1.get_comp(ic)[ir], X.get_comp(ic)[ir], lm0, lm1);
		}
	}
}

void CNLF2::full_rhs(StateVector& Xrhs, int)
{
	implicit_rhs(*Lin[0], *Lin[1], Xrhs);

	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);


	barrier_shared_mem();

	for (int ip=0; ip<comp_list.size(); ip++) {
		double alpha = cex[0];
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		int ic = comp_list[ip].ic;
		int ic2 = comp_list[ip].ic_nl;
		if (comp_list[ip].nl_sign < 0) {
			alpha = -alpha;
		}
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		xs_array2d<cplx> L0 = Lin[0]->get_comp(ic);
		xs_array2d<cplx> NL0 = NL[0]->get_comp(ic2);

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			vec_sum1(X[ir], L0[ir], NL0[ir],
					alpha, 
					lm0, lm1);
		}
	}
}


void DIRK::build_rhs(StateVector& Xrhs, const int k)
{
	int lm0, lm1;

	lm0 = 0;		lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

/*	#pragma omp master
	{
	PRINTF0(" X = L0 + %.3f*NL0", ax[k][0]);
	for (int l=1; l<=k; l++)
		PRINTF0(" + %.3f*L%d + %.3f*NL%d", ai[k][l-1], l, ax[k][l], l);
	PRINTF0("\n");
	}	*/

	for (int ip=0; ip<comp_list.size(); ip++) {
		int i0 = comp_list[ip].irs;
		int i1 = comp_list[ip].ire;
		const int ic = comp_list[ip].ic;
		const int ic2 = comp_list[ip].ic_nl;
		double nl_sign = 1;
		if (comp_list[ip].nl_sign < 0) nl_sign = -nl_sign;
		double lin_sgn = 1;
		if (ic==0) lin_sgn = -1;
		xs_array2d<cplx> lin[4];
		xs_array2d<cplx> nl[4];
		xs_array2d<cplx> X = Xrhs.get_comp(ic); 
		for (int l=0; l<=k; l++) {
			lin[l] = Lin[l]->get_comp(ic);
			nl[l] = NL[l]->get_comp(ic2);
		}

		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
				vec_sum1(X[ir], lin[0][ir], nl[0][ir],
						nl_sign*ax[k][0],  lm0, lm1);
			for (int l=1; l<=k; l++) {
				vec_sum2(X[ir], X[ir], lin[l][ir], nl[l][ir],
						lin_sgn*ai[k][l-1], nl_sign*ax[k][l],  lm0, lm1);
			}
		}
	}
}


void DIRK::step_3_end(int kill_sbr)
{
	for (int k=0; k<nim; k++) {		// nim stages
		if (k>0)  explicit_terms(*NL[k], Xlm, 0);		// NL(k) at substep k

		#pragma omp parallel
		{
			if (k==0) {
				apply_Mrhs(*Lin[0], Xlm);
			} else {
				apply_M(*Lin[k], Xlm);
			}
			build_rhs(Xlm, k);
			update_all_BC(dt*c[k]);		// t + dtk [total step is dt]
			solve_state(Xlm, kill_sbr);		// substep k completed.
		}
	}
}
#endif


Stepper* create_stepper(char* name, double dt, bool dt_adjust)
{
	Stepper* stpr = 0;
	if (name) {
		for (int i=0; name[i] != 0; i++) {
			if (name[i] == ' ' || name[i] == '\t') { name[i] = 0; break; }		// stop at space or tab
			name[i] = toupper((unsigned char) name[i]);		// convert name to uppercase:
		}
		if ((strcmp(name, "PC2") == 0)) stpr = new PC(2);		// same as ARK32
		else if ((strcmp(name, "CNAB") == 0) || (strcmp(name, "CNAB2") == 0)) stpr = new CNAB2;
		else if (strcmp(name, "SBDF2") == 0) stpr = new SBDF2;
		else if (strcmp(name, "SBDF3") == 0) stpr = new SBDF3;
		else if (strcmp(name, "BPR353") == 0) stpr = new BPR353;
	  #ifdef XS_UNSAFE_STEPPERS
		else if (strcmp(name, "LRR322") == 0) stpr = new LRR322;
		else if (strcmp(name, "MCNAB") == 0) stpr = new MCNAB2;
		else if (strcmp(name, "SBDF1") == 0) stpr = new SBDF1;
		else if (strcmp(name, "CNLF") == 0) stpr = new CNLF2;
		else if (strcmp(name, "CNAB3") == 0) stpr = new CNAB3;
		else if (strcmp(name, "DIRK222") == 0) stpr = new DIRK222;
		else if (strcmp(name, "DIRK443") == 0) stpr = new DIRK443;
		else if (strcmp(name, "ASISSP332a") == 0) stpr = new ASISSP332a;
		else if (strcmp(name, "ASISSP332b") == 0) stpr = new ASISSP332b;
		else if (strcmp(name, "ASISSP332c") == 0) stpr = new ASISSP332c;
		else if (strcmp(name, "ASISSP442a") == 0) stpr = new ASISSP442a;
		else if (strcmp(name, "ASISSP442b") == 0) stpr = new ASISSP442b;
		else if (strcmp(name, "ARK2L1") == 0) stpr = new ARK2L1;
		else if (strcmp(name, "ARK2A2") == 0) stpr = new ARK2A2;
		else if (strcmp(name, "ARK2A3") == 0) stpr = new ARK2A3;
		else if (strcmp(name, "ARK3A2") == 0) stpr = new ARK3A2;
		else if (strcmp(name, "ARK3A3") == 0) stpr = new ARK3A3;	// pass geodynamo bench with same dt as PC2
		else if (strcmp(name, "ARK3L1") == 0) stpr = new ARK3L1;
		else if (strcmp(name, "BPR342") == 0) stpr = new BPR342;
		else if (strcmp(name, "ARK32") == 0) stpr = new ARK32;
		else if (strcmp(name, "PC12") == 0) stpr = new PC12;
		else if (strcmp(name, "PC0") == 0) stpr = new PC(0);		// same as CNE1
		else if (strcmp(name, "PC1") == 0) stpr = new PC(1);		// same as PC12
		else if (strcmp(name, "PC3") == 0) stpr = new PC(3);
		else if (strcmp(name, "FW35") == 0) stpr = new FW35;
		else if (strcmp(name, "FW22") == 0) stpr = new FW22;
		else if (strcmp(name, "CNE1") == 0) stpr = new CNE1;
		else if (strcmp(name, "CNRKW3") == 0) stpr = new CNRKW3;
		else if (strcmp(name, "SMR91") == 0) stpr = new SMR91;
	  #endif
		else runerr("time stepper not found.");
	} else stpr = new PC(2);		// the default time-stepper.
	stpr->dt = dt;
	stpr->dt_adjust = dt_adjust;
	return stpr;
}

