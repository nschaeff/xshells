/** XSHELLS diagnostics:
*  record surface field up to degree max_degree in a dedicated file (binary format).
*/

	if ((evol_ubt & EVOL_B) && (Blm.ire == Blm.ir_bco)) {
		const int max_degree = 13;
		const int ir = Blm.ir_bco;
		const int mlim = (MMAX*MRES < max_degree) ? MMAX*MRES : max_degree;
		const int llim = (LMAX < max_degree) ? LMAX : max_degree;
		char fname[128];
		FILE* fp;
		sprintf(fname,"Blm_surf_lmax%d.%s", llim, jpar.job);		fp = fopen(fname,"ab");
		cplx* Plm = Blm.Pol[ir];
		double t = ftime;
		fwrite(&t, sizeof(double), 1, fp);		// time (double)
		for (int m=0; m<=mlim; m+=MRES) {
			const int lm = LM(shtns,m,m);
			fwrite(Plm + lm, sizeof(double), 2*(llim-m+1), fp);		// SH coefficients
		}
		fclose(fp);
	}
