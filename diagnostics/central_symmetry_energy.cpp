/** XSHELLS diagnostics:
*  kinetic energy in symmetric and antisymmetric modes with respect to central symmetry (about the sphere origin)
*/

	if (evol_ubt & EVOL_U) {
		double* diags = all_diags.append(4, "Epol_sym, Epol_asym, Etor_sym, Etor_asym\t ");		// append array for 4 diagnostics
		const int nlm_align = ((NLM+3)/4)*4;
		cplx* Q = (cplx*) VMALLOC(2*nlm_align*sizeof(cplx) + 2*nlm_align*sizeof(double));
		cplx* S = Q + nlm_align;
		double* Pspec = (double*)(S + nlm_align);
		double* Tspec = Pspec + nlm_align;
		memset(Pspec, 0, 2*nlm_align*sizeof(double));		// zero out spectrum.
		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
			Ulm.RadSph(ir, Q, S);
			double r2dr = r[ir]*r[ir]*Ulm.delta_r(ir);
			double dV = 0.5 * r2dr;
			double Esym  = 0.0; 	double Easym = 0.0;
			int lm=0;
			for (int im=0; im<=MMAX; im++) {
				// can we remove the SB rotation ?
				for (int l=im*MRES; l<=LMAX; l++, lm++) {
					double Plm = (norm(Q[lm]) + l2[l]*norm(S[lm])) * dV;
					double Tlm = 	l2[l]*norm(Ulm.Tor[ir][lm]) * dV;
					Pspec[lm] += Plm;
					Tspec[lm] += Tlm;
					diags[l&1] += Plm;
					diags[2+1-(l&1)] += Tlm;
				}
				dV = 1.0 * r2dr;		// count twice (+m and -m)
			}
		}
		VFREE(Q);
	}
