/** XSHELLS diagnostics:
*  Dissipation rates (kinetic and magnetic)
* */
if (evol_ubt & (EVOL_U|EVOL_B)) {
  const int nlm_align = ((NLM+3)/4)*4;
	cplx* Q = (cplx*) VMALLOC(4*nlm_align*sizeof(cplx));
	cplx* S = Q + nlm_align;	cplx* T = Q + 2*nlm_align;		cplx* W = Q + 3*nlm_align;

	if (evol_ubt & EVOL_U) {		// integral of vorticity^2 if u=0 at the boundaries.
		// Here, we actually compute the 'enstrophy' and the work of viscous forces in the bulk as 'D_nu'.
		// WARNING: In some cases (e.g. flow driven by boundaries) the work of viscous forces is not the same as viscous dissipation.
		// WARNING: We ignore the effect of hyper-viscosity, which may not be negligible.
		// The integral over the volume of the work of visous forces can be integrated by part, leading
		// to enstrophy term (integral ove the volume) + a surface term that is non-zero when velocity is non-zero at boundaries.

		double* diags = all_diags.append(2, "enstrophy D_nu\t ");		// append array for 2 diagnostics
		double Dnu = 0.0;
		double enstrophy = 0.0;
		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
			double r2dr = r[ir]*r[ir]*Ulm.delta_r(ir);
			Ulm.curl_QST(ir, Q, S, T);
			double Dnu_r = energy(Q,S,T) * r2dr;
			enstrophy += Dnu_r;
			if ((ir==Ulm.ir_bco  &&  Ulm.bco != BC_ZERO) || (ir==Ulm.ir_bci && Ulm.bci != BC_ZERO)) {	// add non-zero surface term
				Ulm.RadSph(ir, Q,W);	// compute velocity;  horizontal vorticity is still in (S,T)
				double Dnu_surf_contrib = coenergy(W,Ulm.Tor[ir],  T,S, -1) * r[ir]*r[ir];	// = integral( u_theta*w_phi - uphi*w_theta ) over the shell
				if (ir==Ulm.ir_bco) Dnu_surf_contrib = - Dnu_surf_contrib;		// change sign due to orientation of surface
				Dnu_r += Dnu_surf_contrib;
			}
			Dnu += Dnu_r;
		}
		diags[0] = enstrophy;
		Dnu += Dnu;		// we multiply by 2 because of the 1/2 in the energy definition
		diags[1] = Dnu*jpar.nu;		// TODO: fix this when there is hyperviscosity!
		#ifdef XS_NU_VAR
			diags[1] = NAN;		//  /!\ WRONG FORMULA WHEN VISCOSIY VARIES WITH RADIUS ==> return NAN
		#endif
	}

	if (evol_ubt & EVOL_B) {
		double* diags = all_diags.append(1, "D_eta\t ");		// append array for 1 diagnostic
		double Deta = 0.0;
		for (int ir=Blm.irs; ir<=Blm.ire; ir++) {
			double r2dr = r[ir]*r[ir]*Blm.delta_r(ir);
			Blm.curl_QST(ir, Q, S, T);
			double Deta_r = energy(Q,S,T) * r2dr;
			#ifdef XS_ETA_VAR
			Deta += Deta_r * etar[ir];
			#else
			Deta += Deta_r * jpar.eta;
			#endif
		}
		diags[0] = 2.0*Deta;
	}
	VFREE(Q);
}
