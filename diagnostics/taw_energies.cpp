/** XSHELLS diagnostics:
*  Energies in torsional Alfvén waves (axisymmetric) and magnetic energy in l=1,l=2 and other modes.
*/

	if (evol_ubt & EVOL_B) {
		double* diags = all_diags.append(3, "Eb10, Eb20, Eb_other\t ");		// append array for 2 diagnostics
		double b1 = Blm.energy_lm(1,0);		// get magnetic energy of l=1
		double b2 = Blm.energy_lm(2,0);		// get magnetic energy of l=2
		diags[0] = b1;
		diags[1] = b2;
		diags[2] = all_diags[1] - (b1+b2);		// magnetic energy of everything except l=1 and l=2.
	}

	// energy of Alfvén modes h+ and h-
	if ((evol_ubt & (EVOL_U | EVOL_B)) == (EVOL_U | EVOL_B)) {
		double* diags = all_diags.append(2, "Eh+, Eh-\t ");		// append array for 2 diagnostics
		const int nlm_align = ((NLM+3)/4)*4;
		cplx* Qp = (cplx*) VMALLOC(6*nlm_align*sizeof(cplx));
		cplx* Sp = Qp + nlm_align;		cplx* Tp = Qp + 2*nlm_align;
		cplx* Qm = Qp + 3*nlm_align;
		cplx* Sm = Qm + nlm_align;		cplx* Tm = Qm + 2*nlm_align;
		double Ep = 0.0;		double Em = 0.0;
		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
			Ulm.RadSph(ir, Qp, Sp);
			Blm.RadSph(ir, Qm, Sm);
			for (int lm=0; lm<NLM; lm++) {
				cplx qp = Qp[lm];		cplx sp = Sp[lm];
				Qp[lm] += Qm[lm];		Sp[lm] += Sm[lm];
				Qm[lm] = qp - Qm[lm];	Sm[lm] = sp - Sm[lm];
				Tp[lm] = Ulm.Tor[ir][lm] + Blm.Tor[ir][lm];
				Tm[lm] = Ulm.Tor[ir][lm] - Blm.Tor[ir][lm];
			}
			double r2dr = r[ir]*r[ir]*Ulm.delta_r(ir);
			double dV = 1.0 * r2dr;
			int lm=0;
			for (int im=0; im<=MMAX; im++) {
				double ep = 0.0;		double em = 0.0;
				for (int l=im*MRES; l<=LMAX; l++, lm++) {
					ep += norm(Qp[lm]) + l2[l]*(norm(Sp[lm]) + norm(Tp[lm]));
					em += norm(Qm[lm]) + l2[l]*(norm(Sm[lm]) + norm(Tm[lm]));
				}
				Ep += ep * dV;		Em += em * dV;
				dV = 2.0 * r2dr;		// count twice (+m and -m)
			}
		}
		diags[0] = Ep;		diags[1] = Em;
		VFREE(Qp);
	}
