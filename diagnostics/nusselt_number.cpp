/** XSHELLS diagnostics:
*  data needed to compute the Nusselt number.
* record dT/dr[l=0] and T[l=0] at inner/outer radii, as well as composition if available.
*/
	if (evol_ubt & EVOL_T) {
		double* diags = all_diags.append(4, "T(ri) dT/dr(ri) T(ro) dT/dr(ro) ");
		double norm = 1.0 / Y00_1;
		int ir = Tlm.ir_bci;	// inner boundary
		if (own(ir))  diags[0] = real(Tlm[ir][0]) * norm;
		if (r[ir]>0.0) {
			diags[1] = real( boundary_Grad_contrib(Tlm.Sca, ir, 1, 0) ) * norm;
		}

		ir = Tlm.ir_bco;		// outer boundary
		if (own(ir))  diags[2] = real(Tlm[ir][0]) * norm;
		diags[3] = real( boundary_Grad_contrib(Tlm.Sca, ir, -1, 0) ) * norm;
	}
	if (evol_ubt & EVOL_C) {
		double* diags = all_diags.append(4, "C(ri) dC/dr(ri) C(ro) dC/dr(ro) ");
		double norm = 1.0 / Y00_1;
		int ir = Clm.ir_bci;	// inner boundary
		if (own(ir))  diags[0] = real(Clm[ir][0]) * norm;
		if (r[ir]>0.0) {
			diags[1] = real( boundary_Grad_contrib(Clm.Sca, ir, 1, 0) ) * norm;
		}

		ir = Clm.ir_bco;		// outer boundary
		if (own(ir))  diags[2] = real(Clm[ir][0]) * norm;
		diags[3] = real( boundary_Grad_contrib(Clm.Sca, ir, -1, 0) ) * norm;
	}
