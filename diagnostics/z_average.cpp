/** XSHELLS diagnostics:
*  z-averaging: stores z-average of fields.
* */

	static int out_zavg = -1;
	static int ns;
	static double *s;
	static double *vs;
	char fname[80];
	if (out_zavg == -1) {
		out_zavg = mp.var["zavg"];		// get value from variable "zavg" in xshells.par file
		PRINTF0(" zavg = %d\n",out_zavg);
		if (out_zavg) {
			ns = NLAT/4;
	 		s = (double*) malloc(sizeof(double)*ns);
			for (int j=0; j<ns; j++) s[j] = (j+1)*r[Ulm.ir_bco]/ns;
			vs = (double*) malloc(sizeof(double)*NPHI*ns*5);
		}
	}
	if (out_zavg) {
		if (evol_ubt & EVOL_B) {
			#pragma omp parallel
			Blm.special_z_avg(ns, s, vs, 1);
			if (i_mpi==0) {
				sprintf(fname,"zavgB_%05d.%s",iter,jpar.job);	write_disc_data(fname, s, ns, NPHI, vs, vs+NPHI*ns, 0);
				sprintf(fname,"zavgB2_%05d.%s",iter,jpar.job);	write_disc_data(fname, s, ns, NPHI, vs+NPHI*ns*2, vs+NPHI*ns*3, vs+NPHI*ns*4);
			}
		}
		if (evol_ubt & EVOL_U) {
			#pragma omp parallel
			Ulm.special_z_avg(ns, s, vs, 1);
			if (i_mpi==0) {
				sprintf(fname,"zavgU_%05d.%s",iter,jpar.job);	write_disc_data(fname, s, ns, NPHI, vs, vs+NPHI*ns, 0);
				sprintf(fname,"zavgU2_%05d.%s",iter,jpar.job);	write_disc_data(fname, s, ns, NPHI, vs+NPHI*ns*2, vs+NPHI*ns*3, vs+NPHI*ns*4);
			}
		}
	}
