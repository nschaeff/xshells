/** XSHELLS diagnostics:
*  record l-spectrum
*/

	static std::vector<double> lspec(LMAX+1);	// backup spectrum
	if ((evol_ubt & EVOL_U) && (LMAX > 0)) {
		// generate labels
		static std::ostringstream labels;
		if (labels.str().length() == 0) {	// generate labels only once.
			for (int l=0; l<=LMAX; l++)  labels << "l=" << l << " ";
			labels << "\t";
		}
		// register diagnostics and labels
		double* diags = all_diags.append(LMAX+1, labels.str().c_str());		// append array for LMAX+1 diagnostics
		Ulm.spectrum(diags, 0);					// fill l-spectrum to be summed accross shells and stored in energy file.
		if (own(NR/2)) {
			// process at mid-shell computes ans display (local) growth rate.
			double gmax = -1e192;
			int lgrow = -1;
			for (int l=0; l<=LMAX; l++) {
				double g = log(diags[l]/lspec[l])/(2.*dt_log);		// growth rate
				if (g > gmax) {
					gmax = g;		lgrow = l;
				}
				lspec[l] = diags[l];	// backup spectrum for next time step
			}
			printf(" maximum growth rate = %g  for l=%d\n", gmax, lgrow);
		}
	}
