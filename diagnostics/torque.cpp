/** XSHELLS diagnostics:
*  data needed to compute the torques at both interfaces.
* record dU/dr[l=1] at inner/outer radii.
*/
	if (evol_ubt & EVOL_U) {
		const long lm11 = ((MMAX>0) && (MRES==1)) ? LiM(shtns, 1,1) : 0;
		const double n0 = 1.0 / Y10_ct;
		const double n1 = 1.0 / Y11_st;
		const double torx_cte = (8.*M_PI)/3.;		// from the integral over the sphere
		int ir = Ulm.ir_bci;	// inner boundary
		if (r[ir]>0.0) {
			double* diags = all_diags.append(3, "Omega_i_z Torque_i_z dU0_dri  ");
			double U0 = 0.0;
			if (own(ir)) U0 = real( Ulm.Tor[ir][1] ) * n0;
			diags[0] = U0 / r[ir];
			diags[1] = (real( boundary_Grad_contrib(Ulm.Tor, ir, 1, 1) ) * n0 * r[ir] - U0) * r[ir]*r[ir] * torx_cte;
			diags[2] = real( boundary_Grad_contrib(Ulm.Tor, ir, 1, 1) ) * n0;
			if (lm11 > 0) {
				double* diags = all_diags.append(6, "Ui_m1r Ui_m1i Torque_i_x Torque_i_y dU1_dri_r dU1_dri_i  ");
				cplx U1 = 0.0;
				if (own(ir)) U1 = Ulm.Tor[ir][lm11] * n1;
				diags[0] = real(U1);		diags[1] = imag(U1);
				cplx dU1 = (boundary_Grad_contrib(Ulm.Tor, ir, 1, lm11) * n1 * r[ir] - U1) * r[ir]*r[ir] * torx_cte;
				diags[2] = real(dU1);		diags[3] = -imag(dU1);
				dU1 = boundary_Grad_contrib(Ulm.Tor, ir, 1, lm11) * n1;
				diags[4] = real(dU1);		diags[5] = imag(dU1);
			}
		}

		ir = Ulm.ir_bco;		// outer boundary
		double* diags = all_diags.append(3, "Omega_o_z Torque_o_z dU0_dro  ");
		double U0 = 0.0;
		if (own(ir)) U0 = real( Ulm.Tor[ir][1] ) * n0;
		diags[0] = U0 / r[ir];
		diags[1] = (real( boundary_Grad_contrib(Ulm.Tor, ir, -1, 1) ) * n0 * r[ir] - U0) * r[ir]*r[ir] * torx_cte;
		diags[2] = real( boundary_Grad_contrib(Ulm.Tor, ir, -1, 1) ) * n0;
		if (lm11 > 0) {
			double* diags = all_diags.append(6, "Uo_m1r Uo_m1i Torque_o_x Torque_o_y dU1_dro_r dU1_dro_i  ");
			cplx U1 = 0.0;
			if (own(ir)) U1 = Ulm.Tor[ir][lm11] * n1;
			diags[0] = real(U1);		diags[1] = imag(U1);
			cplx dU1 = (boundary_Grad_contrib(Ulm.Tor, ir, -1, lm11) * n1 * r[ir] - U1) * r[ir]*r[ir] * torx_cte;
			diags[2] = real(dU1);		diags[3] = -imag(dU1);
			dU1 = boundary_Grad_contrib(Ulm.Tor, ir, -1, lm11) * n1;
			diags[4] = real(dU1);		diags[5] = imag(dU1);
		}
	}

	if (evol_ubt & EVOL_B) {	// compute magnetic torque
		if (Blm.ir_bci < Ulm.ir_bci) {	// need some electric conductor at inner boundary
			const int ir = Ulm.ir_bci;
			double* diags = all_diags.append(1, "TorqueMag_i_z ");
			if (own(ir)) diags[0] = calc_TorqueMag_surf(Blm, 0, ir);	// TODO: add B0lm when needed
		}
		if (Blm.ir_bco > Ulm.ir_bco) {	// need some electric conductor at outer boundary
			const int ir = Ulm.ir_bco;
			double* diags = all_diags.append(1, "TorqueMag_o_z ");
			if (own(ir)) diags[0] = calc_TorqueMag_surf(Blm, 0, ir);	// TODO: add B0lm when needed
		}
	}
