/** XSHELLS diagnostics:
*  Dissipation rates (kinetic and magnetic)
* */
if (evol_ubt & (EVOL_U|EVOL_B)) {
	const int nlm_align = ((NLM+3)/4)*4;
	cplx* Q = (cplx*) VMALLOC(3*nlm_align*sizeof(cplx));
	cplx* S = Q + nlm_align;	cplx* T = Q + 2*nlm_align;
	if (evol_ubt & EVOL_U) {		// integral of entropy^2 if u=0 at the boundaries.
		double* diags = all_diags.append(1, "D_nu\t ");		// append array for 1 diagnostic
		double Dnu = 0.0;
		for (int ir=Ulm.irs; ir<=Ulm.ire; ir++) {
			double r2dr = r[ir]*r[ir]*Ulm.delta_r(ir);
			Ulm.curl_QST(ir, Q, S, T);
			double Dnu_r = energy(Q,S,T) * r2dr;
			#ifdef XS_NU_VAR
			Dnu += Dnu_r * nur[ir];
			#else
			Dnu += Dnu_r * jpar.nu;
			#endif
		}
		diags[0] = 2.0*Dnu;
	}
	if (evol_ubt & EVOL_B) {
		double* diags = all_diags.append(3, "D_eta_c, D_eta_m, D_eta_cmb\t ");		// append array for 3 diagnostics
		double Deta[3] = {0.0, 0.0, 0.0};
		for (int ir=Blm.irs; ir<=Blm.ire; ir++) {
			Blm.curl_QST(ir, Q, S, T);
			double r2dr = r[ir]*r[ir]*Blm.delta_r(ir);
			double dV = 1.0 * r2dr;
			double Deta_r = 0.0;
			int lm=0;
			for (int im=0; im<=MMAX; im++) {
				double Dm = 0.0;
				for (int l=im*MRES; l<=LMAX; l++, lm++) {
					Dm += norm(Q[lm]) + l2[l]*(norm(S[lm]) + norm(T[lm]));
				}
				Deta_r += Dm * dV;
				dV = 2.0 * r2dr;		// count twice (+m and -m)
			}
			#ifdef XS_ETA_VAR
			if (ir==NM) {
				Deta_r *= (etar[ir-1]+etar[ir+1])*0.5;
			} else Deta_r *= etar[ir];
			#else
			Deta_r *= jpar.eta;
			#endif
			if (ir<NM) Deta[0] += Deta_r;			// split core and mantle dissipations.
			if (ir>NM) Deta[1] += Deta_r;
			if (ir==NM) Deta[2] += Deta_r;
		}
		diags[0] = Deta[0];		diags[1] = Deta[1];		diags[2] = Deta[2];
	}
	VFREE(Q);
}
