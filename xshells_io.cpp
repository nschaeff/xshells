/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xshells_io.cpp
/// Read and write files containing spherical harmonic fields.

/// the current file revision.
#define FILE_VERSION 13
/// added to FILE_VERSION to identify single precision files.
#define FILE_SINGLE_PREC 4096
#define FILE_VAR_LTR (4096*2)
#define FILE_FP48 (4096*4)

typedef std::complex<float> fcplx;

/* File format revision history :
0 : initial file format
1 : save two boundary conditions, BCi and BCo (only one BC for both before)
2 : save NG and NM (shell index of boundaries)
3 : save the norm of the spherical harmonic transform (from SHTns)
4 : save an identification string (hg id)
5 : save also one ghost point (which stores the d/dr or external potential boundary condition).
6 : BC_NONE value is no longer 0.
7 : save ltr[] and save only l<=ltr[]
8 : remove useless NG and NM (were stored in i3 and i4 respectively) and remove Omega0, nu, eta (stored in d1, d2, d3).
9 : correct saving with variable l-truncation.
10 : include number of components (nc=1 for scalar, 2 for Pol/Tor, 3 for general Q/S/T vector).
11 : include iteration and sub-iteration number, for restart.
12 : r_bci and r_bco store the start and end radii of the field.
13 : add support for fp48 compression.
*/

#ifndef XS_MPI
#include <sys/mman.h>
#endif

/// packing data into 48bit floating point numbers.
#include "fp48/fp48.h"

/// File-header containing metadata associated with a saved pol/tor spherical harmonic field.
struct FieldInfo {
	int32_t version, lmax, mmax, mres, nlm, nr, irs, ire;	// 8 integers (32 bits)
	int32_t BCi, BCo, i3, i4, shtnorm, nc, iter, step;		// 8 integers (32 bits)
	double r_bci, r_bco, d3, t, d5, d6, d7, d8;		// 8 doubles (hopefully 64 bits)
	char hgid[64];		// id string : 64 chars.
	char text[832];		// 832 chars => total 1024 bytes.
};

int IS_XS_FILE(const FieldInfo& jinfo) {
	return ( ( jinfo.nr > 0) && ( nlm_calc(jinfo.lmax, jinfo.mmax, jinfo.mres) == jinfo.nlm ) );
}
int GET_FILE_VERSION(const FieldInfo& jinfo) {
	return (jinfo.version & (FILE_SINGLE_PREC-1));
}
int GET_FILE_PREC(const FieldInfo& jinfo) {
	if (jinfo.version & FILE_SINGLE_PREC) return 1;		// 1 = single precision
	if (jinfo.version & FILE_FP48) return 3;			// 3 = FP48 1.5 precision
	return 2;											// 2 = double precision
}
int IS_VAR_LTR(const FieldInfo& jinfo) {
	return (jinfo.version & FILE_VAR_LTR);
}

int xsio_allow_interp = 0;		///< interpolate fields in case of grid mismatch ?
int xsio_debug_no_write = 0;	///< disable field write ? (for debug/timing purposes)
char* xsio_par_string = 0;		///< parameter list, as a string

void Spectral::save_single(const char *fn, int lmax, int mmax, double time, int iter, int step) const
{
	save_generic(fn, lmax, mmax, time, iter, step, 1);
}

void Spectral::save_double(const char *fn, int lmax, int mmax, double time, int iter, int step) const
{
	save_generic(fn, lmax, mmax, time, iter, step, 2);
}

void Spectral::save(const char *fn, double time, int iter, int step) const
{
	save_double(fn, LMAX, MMAX, time, iter, step);
}

/// save spherical harmonic representation of Poloidal/Toroidal field PT.
/// single or double precision (prec=1 or 2 respectively), and l,m truncation.
/// if prec=3, the data will be packed to the FP48 format (48 bits, with 40 bits mantissa).
void Spectral::save_generic(const char *fn, int lmax, int mmax, double time, int iter, int step, const int prec) const
{
	if UNLIKELY(xsio_debug_no_write) {
		#ifdef XS_MPI
		MPI_Barrier(MPI_COMM_WORLD);	// needed to keep correct semantic (and fix a bug when using MPI3 shared memory).
		#endif
		return;
	}
	fcplx * shf = 0;		// single precision buffer.
	cplx * sh2 = 0;
	char * fp48 = 0;
	long nlm;
	FieldInfo fh;
	int ncomp = this->ncomp;
	if (ncomp > 2) ncomp = 2;	// more than 2 components could be saved, but it is assumed here they are temporary data that should be discarded.

	if ((lmax<0)||(lmax>LMAX)) lmax=LMAX;
	if ((mmax<0)||(mmax>MMAX)) mmax=MMAX;
	nlm = nlm_calc(lmax,mmax,MRES);		// local NLM.

	memset(&fh ,0, sizeof(fh));
	fh.version = FILE_VERSION;	fh.BCi = bci;	fh.BCo = bco;
	fh.lmax = lmax;		fh.mmax = mmax;		fh.mres = MRES;	
	fh.nlm = nlm;		fh.shtnorm = sht_norm;
	fh.nr = NR;		fh.irs = ir_bci;	fh.ire = ir_bco;		fh.nc = ncomp;
	fh.t = time;	fh.iter = iter;		fh.step = step;
	fh.r_bci = r[ir_bci];		fh.r_bco = r[ir_bco];
	strncpy(fh.hgid, _HGID_, 63);	fh.hgid[63]='\0';
	if (xsio_par_string) {
		strncpy(fh.text, xsio_par_string, 831);	fh.text[831]='\0';
	}
#ifdef VAR_LTR
	fh.version += FILE_VAR_LTR;		// mark file as LTR saved.
#endif
	if (prec==1) fh.version += FILE_SINGLE_PREC;
	else if (prec==3) fh.version += FILE_FP48;

#ifndef XS_MPI
	FILE* fp = fopen(fn,"wb");
	if (fp == 0) runerr("[Spectral::save] File creation error");
	fwrite(&fh, sizeof(fh), 1, fp);		// Header.
	fwrite(r, sizeof(double), NR, fp);	// radial grid.
  #ifdef VAR_LTR
	fwrite(ltr, sizeof(unsigned short), NR, fp);	// save l truncation array.
  #endif
#else
	MPI_File fp;
	MPI_Offset ofs = sizeof(fh) + sizeof(double)*NR;

	// the const_cast below is to accommodate the C MPI interface, where the second parameter is sometimes defined as char* (without const)
	int ierr = MPI_File_open(MPI_COMM_WORLD, const_cast<char*>(fn), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fp);
	if (ierr) runerr("[Spectral::save] File creation error");

	if (i_mpi==0) {
		MPI_File_write(fp, &fh, sizeof(fh), MPI_BYTE, MPI_STATUS_IGNORE);		// Header.
		MPI_File_write(fp, r,   NR, MPI_DOUBLE, MPI_STATUS_IGNORE);				// radial grid.
	}
	#ifdef VAR_LTR
		if (i_mpi==0) MPI_File_write(fp, ltr, NR, MPI_UNSIGNED_SHORT, MPI_STATUS_IGNORE);
		ofs += NR*sizeof(unsigned short);
	#endif
	MPI_Datatype mpi_type = MPI_DOUBLE;
#endif
	size_t elem_sze = sizeof(cplx);
	size_t nwrites = nlm;
	if (prec==1) {
		shf = (fcplx *) malloc(sizeof(fcplx) * ncomp*nlm);
		elem_sze = sizeof(fcplx);
		#ifdef XS_MPI
		mpi_type = MPI_FLOAT;
		#endif
	} else if (prec==3) {	// fp48 compression
		elem_sze = 2;				// two byte granularity.
		nwrites = 6*nlm + 1;		// includes the exponent shift (2 bytes).
		fp48 = (char*) malloc( nwrites * ncomp * elem_sze );
		#ifdef XS_MPI
		mpi_type = MPI_BYTE;
		#endif
	}

  if (nshell > 0) {		// do we have something else to save?
#ifndef VAR_LTR
	if (nlm == NLM) {		// save everything : fast version
		for (int ir=ir_bci-1; ir<=ir_bco+1; ir++) {		// this includes the ghost shells.
			if ( ((ir>=irs)&&(ir<=ire)) || ((ir==irs-1)&&(ir==ir_bci-1)) || ((ir==ire+1)&&(ir==ir_bco+1)) ) {
				for (int ic=0; ic<ncomp; ic++) {
					void* ptr = get_data(ic,ir);
					if (shf) {
						for (long lm=0; lm<nlm; ++lm)
							shf[lm] = ((cplx*)ptr)[lm];		// convert to single precision
						ptr = shf;
					} else if (fp48) {
						fp48_write_stream((double*)ptr, fp48, 2*nlm);
						ptr = fp48;
					}
					#ifndef XS_MPI
					fwrite(ptr, elem_sze, nwrites, fp);		// component by component
					#else
					MPI_File_write_at(fp, ofs + (elem_sze*nwrites)*(ic + ncomp*(ir-ir_bci+1)), ptr, nwrites*2, mpi_type, MPI_STATUS_IGNORE);
					#endif
				}
			}
		}
	} else
#endif
	{	// discard some modes.
		sh2 = (cplx *) malloc(sizeof(cplx) * ncomp*nlm);		// intermediate buffer.
		void* ptr = sh2;	// write this buffer to disc
		int llim = lmax;
		for (int ir= ir_bci-1; ir<= ir_bco+1; ir++) {
			#ifdef VAR_LTR
				llim = ltr[ir];
				if (llim > lmax) llim = lmax;			// llim cannot be larger than lmax.
				nlm = nlm_calc(llim, mmax, MRES);		// number of modes.
				nwrites = nlm;
				if (prec==3) nwrites = 6*nlm + 1;		// different meaning of nreads.
			#endif
			if ( ((ir>=irs)&&(ir<=ire)) || ((ir==irs-1)&&(ir==ir_bci-1)) || ((ir==ire+1)&&(ir==ir_bco+1)) ) {
				long lm = 0;
				for (int im=0; im<=mmax; im++) {
					long lm2 = LiM(shtns,im*MRES,im);
					long n_l = llim-im*MRES +1;
					for (int j=0; j<ncomp; j++) {		// copy to contiguous buffer.
						cplx* src = get_data(j, ir) + lm2;
						cplx* dst = sh2 + j*nlm + lm;
						for (long l=0; l < n_l; l++)	dst[l] = src[l];
					}
					if (n_l > 0)	lm += n_l;
				}
				if UNLIKELY(lm != nlm) printf("[Spectral::save] Warning ! Problem while saving data, file may be corrupted ! [ir=%d, llim=%d, mmax=%d, nlm=%ld, lm=%ld]", ir, llim, mmax, nlm, lm);
				if (shf) {	// convert to single precision.
					for (long lm=0; lm<ncomp*nlm; lm++)	shf[lm] = sh2[lm];		// all components in a single loop.
					ptr = shf;
				} else if (fp48) {
					for (int j=0; j<ncomp; j++)
						fp48_write_stream((double*)(sh2 + j*nlm), fp48 + j*(12*nlm+2), 2*nlm);
					ptr = fp48;
				}
				#ifndef XS_MPI
				fwrite(ptr, elem_sze, ncomp*nwrites, fp);		// poloidal & toroidal
				#else
				MPI_File_write_at(fp, ofs, ptr, nwrites*ncomp*2, mpi_type, MPI_STATUS_IGNORE);
				#endif
			}
			#ifdef XS_MPI
			ofs += ncomp*nwrites*elem_sze;
			#endif
		}
	}
  }
	if (sh2) free(sh2);
	if (shf) free(shf);
	if (fp48) free(fp48);
	#ifndef XS_MPI
	fclose(fp);
	#else
	MPI_File_close(&fp);
	MPI_Barrier(MPI_COMM_WORLD);
	#endif
}

/// print info extracted from file to stdout.
void print_FieldInfo(FieldInfo *fh)
{
	printf("  NR=%d, Lmax=%d, Mmax=%d, Mres=%d (Nlm=%d, shtnorm=%d) | %d components\n", fh->ire-fh->irs+1, fh->lmax, fh->mmax, fh->mres, fh->nlm, fh->shtnorm, fh->nc);
	printf("  r=[%g, %g], BC=(%d, %d), t=%.10g (iter=%d)\n", fh->r_bci, fh->r_bco, fh->BCi, fh->BCo, fh->t, fh->iter);
	printf("  [version %d", GET_FILE_VERSION(*fh));
	int prec = GET_FILE_PREC(*fh);
	if (prec == 1) printf(", single precision data");
	else if (prec == 3) printf(", fp48 compressed data");
	if (IS_VAR_LTR(*fh)) {
		printf(", variable l-truncation");
		if (GET_FILE_VERSION(*fh) < 9) printf(" (old version, may be corrupted)");
	}
	printf(", code id: ");
	if (GET_FILE_VERSION(*fh) > 3) {
		printf("%s]\n",fh->hgid);
	} else printf("  unknown]\n");
	if (strlen(fh->text) > 0) printf("xshells.par: %s\n",fh->text);
}

/// change the endianness of a buffer of nelem elements of elem_sze bytes.
void swap_endian_buffer(const int elem_sze, void* ptr, long nelem) {
	for (long k=0; k<nelem; k++) {
		char* cp = ((char*)ptr) + k*elem_sze;
		for (int i = 0; i < elem_sze/2; i++) {
			char c = cp[i];
			cp[i] = cp[elem_sze-1-i];
			cp[elem_sze-1-i] = c;
		}
	}
}

/// Class to abstract the operations on existing spectral files.
/// Allows getting metadata, reading, converting, etc... using MPI-IO or standard IO.
class SpectralFile {
  public:
	FieldInfo fh;
	double* rr;				///< radial grid

  private:
	unsigned short* ltr;	///< ltr description (if any).
	size_t* shell_ofs;
	cplx* Shell_d;
	fcplx* Shell_f;
	char* Shell_fp48;
#ifndef XS_MPI
	FILE *fp;
	size_t ofs;
#else
	MPI_File fp;
	MPI_Offset ofs;
	MPI_Datatype mpi_type;
#endif
	long istart, iend;	///< first and last stored shells
	long elem_sze;		// type long to avoid bugs when computing large offset
	int swap_endian;

  public:
	int open(const char *fn);
	void prepare_read();	///< prepare reading (must be called before read_shell)
	void close();
	void read_shell(int ir, cplx *data, long dist);	///< read one shell and stores the data to data.
	cplx* mem_map();		///< map file into memory
};

int SpectralFile::open(const char *fn)
{
	ofs = 0;	swap_endian = 0;
	rr = 0;		ltr = 0;	shell_ofs = 0;
	Shell_d = 0;	Shell_f = 0;	Shell_fp48 = 0;
	memset(&fh ,0, sizeof(fh));		// zero out the meta-data structure.
	#ifndef XS_MPI
		fp = fopen(fn,"rb");
		if (fp == 0) return 0;		// runerr("File not found !");
		int read = fread(&fh, sizeof(FieldInfo), 1, fp);		// read Header
		if (read != 1) return 0;
	#else
		// the const_cast below is to accommodate the C MPI interface, where the second parameter is sometimes defined as char* (without const)
		int ierr = MPI_File_open(MPI_COMM_WORLD, const_cast<char*>(fn), MPI_MODE_RDONLY, MPI_INFO_NULL, &fp);
		if (ierr) return 0;		// runerr("File not found !");
		ierr = MPI_File_read_all(fp, &fh, sizeof(FieldInfo), MPI_BYTE, MPI_STATUS_IGNORE);
		if (ierr) return 0;
	#endif
	ofs += sizeof(FieldInfo);

	if (! IS_XS_FILE(fh) ) {		// if file not recognized, try to swap endianness of header
		swap_endian_buffer(4, &fh, 16);					// 16 int
		swap_endian_buffer(8, (double*)(&fh)+8, 8);		// 8 double
		swap_endian = 1;		// mark endianness
	}
	if (IS_XS_FILE(fh)) {
		istart = fh.irs-1;		iend = fh.ire+1;

		// perform corrections on metadata to ensure backward compatibility:
		int version = GET_FILE_VERSION(fh);
		if (version > FILE_VERSION) printf("Warning ! File version may not be supported (too recent) !");
		if (version < 3) fh.shtnorm = sht_orthonormal;	// only norm before V3.
		if (version < 1) {		// correct boundary conditions.
			fh.BCo = fh.BCi;		// only one BC saved to BCi before version 1.
		}
		if (version < 5) {		// boundary condition for derivative was not stored before version 5
			istart++;	iend--;
		}
		if (version < 6) {		// a BC of 0 was BC_NONE before version 6.
			if (fh.BCi == 0) fh.BCi = BC_NONE;
			if (fh.BCo == 0) fh.BCo = BC_NONE;		
		}
		if (version < 10) fh.nc = 2;		// only Pol/Tor fields before v10.
		if (version < 11) {
			fh.iter = 0;		fh.step = 0;		// iter was not saved before v11.
		}

		// load radial grid.
		rr = (double *) malloc(fh.nr * sizeof(double));
		#ifndef XS_MPI
			fread(rr, sizeof(double), fh.nr ,fp);
		#else
			ierr = MPI_File_read_at_all(fp, ofs, rr, fh.nr, MPI_DOUBLE, MPI_STATUS_IGNORE);
			if (ierr) printf("[load_FieldInfo] proc %d : MPI_File_read_at_all error (nr=%d).\n",i_mpi, fh.nr);
		#endif
		if (swap_endian) swap_endian_buffer(8, rr, fh.nr);		// correct endianness for radial grid.
		fh.r_bci = rr[fh.irs];		// set radii limits (they were not saved before v12).
		fh.r_bco = rr[fh.ire];
		ofs += fh.nr * sizeof(double);

		if (IS_VAR_LTR(fh)) {
			if (GET_FILE_VERSION(fh) < 9) printf("  *** WARNING! old VAR_LTR files might be corrupted !\n");
			ltr = (unsigned short *) malloc(sizeof(unsigned short) * (fh.nr +2));		// alloc ltr
			ltr += 1;	// ltr[-1] should be defined.
			#ifndef XS_MPI
			fread(ltr, sizeof(unsigned short), fh.nr, fp);		// read l-truncation
			#else
			MPI_File_read_at_all(fp, ofs, ltr, fh.nr, MPI_UNSIGNED_SHORT, MPI_STATUS_IGNORE);		
			#endif
			if (swap_endian) swap_endian_buffer(2, ltr, fh.nr);		// correct endianness for ltr.
			ltr[-1] = ltr[0];		ltr[fh.nr] = ltr[fh.nr-1];		// for ghost points.
			ofs += sizeof(unsigned short)*fh.nr;
		}

		return fh.nc;
	} else return 0;
}

void SpectralFile::prepare_read()
{
	elem_sze = sizeof(cplx);
	#ifdef XS_MPI
	mpi_type = MPI_DOUBLE;
	#endif
	const int prec = GET_FILE_PREC(fh);
	if (prec == 1) {		// single precision
		elem_sze = sizeof(fcplx);
		Shell_f = (fcplx *) malloc(fh.nlm*fh.nc * sizeof(fcplx));		// alloc one shell buffer
		#ifdef XS_MPI
		mpi_type = MPI_FLOAT;
		#endif
	} else if (prec == 3) {		// fp48 compressed data
		elem_sze = 2;
		size_t nreads = 6*fh.nlm + 1;
		Shell_fp48 = (char*) malloc(elem_sze * nreads * fh.nc);		// alloc one shell buffer
		#ifdef XS_MPI
		mpi_type = MPI_BYTE;
		#endif
	}
	Shell_d = (cplx *) malloc(fh.nlm*fh.nc * sizeof(cplx));		// alloc one shell buffer
	if (ltr) {		// precompute position of shells in VAR_LTR mode
		shell_ofs = (size_t *) malloc(sizeof(size_t) * (iend-istart+1));
		size_t d = ofs;
		for (int i=istart; i<=iend; i++) {
			int llim = fh.lmax;
			if (llim > ltr[i]) llim = ltr[i];
			shell_ofs[i-istart] = d;
			size_t shell_sze = nlm_calc(llim, fh.mmax, fh.mres) * fh.nc * elem_sze;
			if (prec == 3) shell_sze = shell_sze*6 + 2*fh.nc;		// for fp48, it is computed differently.
			d += shell_sze;
		}
	}
}

void SpectralFile::close()
{
	if (shell_ofs) free(shell_ofs);
	if (Shell_d) free(Shell_d);
	if (Shell_f) free(Shell_f);
	if (Shell_fp48) free(Shell_fp48);
	if (ltr) free(ltr-1);
	if (rr) free(rr);
	#ifndef XS_MPI
		if (fp) fclose(fp);
	#else
		MPI_File_close(&fp);
	#endif
}

// used by Spectral::load()
void SpectralFile::read_shell(int ir, cplx *PT, long dist)
{
	cplx *Sd = Shell_d;
	size_t nreads;
	long nlm, llim;
	const int ncomp = fh.nc;

	//printf("process %d reading shell %d (istart=%d, iend=%d)...\n",i_mpi, ir, istart, iend);
	if UNLIKELY((ncomp > 1) && (dist < NLM)) runerr("[read_shell] something went wrong.\n");

	nlm = fh.nlm;
	llim = fh.lmax;
	if ((ltr) && (llim > ltr[ir])) {
		llim = ltr[ir];		// llim cannot be larger than ltr[ir]
		nlm = nlm_calc(llim, fh.mmax, fh.mres);
	}
	nreads = nlm;
	if (Shell_fp48) nreads = 6*nlm + 1;		// different meaning of nreads.

	if ((ir>=istart) && (ir<=iend)) {
		size_t pos;
		if (ltr) {
			pos = shell_ofs[ir-istart];
		} else {
			pos = ofs + ((size_t)(ir-istart))*nreads*fh.nc*elem_sze;		// warning, we need size_t type here to avoid overflows.
		}
		#ifndef XS_MPI
			fseek(fp, pos, SEEK_SET);
		#endif

		int res;
		void *ptr = Sd;
		if (Shell_f)	ptr = Shell_f;
		else if (Shell_fp48) ptr = Shell_fp48;
		#ifndef XS_MPI
			size_t read = fread(ptr, elem_sze, nreads*ncomp, fp);		// load float or double data
			res = read - nreads*ncomp;
		#else
			res = MPI_File_read_at(fp, pos, ptr, nreads*ncomp*2, mpi_type, MPI_STATUS_IGNORE);
		#endif
		if (swap_endian) swap_endian_buffer(elem_sze/2, ptr, nlm*ncomp*2);	// correct Endianness
		if (Shell_f)	for (int lm=0; lm < nlm*ncomp; lm++)	Sd[lm] = Shell_f[lm];	// convert to double precision
		if (Shell_fp48)	{	// unpack to double precision
			for (long k=0; k<ncomp; k++)
				fp48_read_stream( Shell_fp48 + k*(12*nlm+2), (double*)(Sd+k*nlm), 2*nlm);
		}
		if UNLIKELY(res) {
			perror("file error: ");		// print meaningful error ?	
			printf("error=%d, process %d, shell %d, pos %ld, count %ld, sizeof(pos)=%ld\n", res, i_mpi, ir, pos, nreads*ncomp*elem_sze, sizeof(pos));
			runerr("error while reading file.");
		}
	} else {
		memset(Sd, 0, nlm*ncomp*sizeof(cplx));
	}

	const int lmax_nz = (llim < LMAX) ? llim : LMAX;			// minimum lmax between source and dest.
	for (long im=0, lm=0, imt=0, lmt=0; im<=MMAX; im++) {
		long m=im*MRES;
		while(m > imt*fh.mres) {		// skip data if not required.
			lmt+= llim +1 -imt*fh.mres;
			imt++;
		}
		if ((imt <= fh.mmax) && (m == imt*fh.mres)) {		// data present : copy
			for (int j=0; j<ncomp; j++) {
				int id = j*dist + lm;
				int is = j*nlm  + lmt;
				int l=m;
				for (; l <= lmax_nz; l++)	PT[id++] = Sd[is++];
				for (; l <= LMAX; l++)		PT[id++] = 0.0;
			}
			lmt += (llim+1-m);
			imt++;
		} else {	// data not present : set to zero
			for (int j=0; j<ncomp; j++) {
				int id = j*dist + lm;
				for (int l=m; l <= LMAX; l++)	PT[id++] = 0.0;
			}
		}
		lm  += (LMAX+1-m);
	}
}

#ifndef XS_MPI
cplx* SpectralFile::mem_map()
{
	const size_t cplx_sze = sizeof(cplx);		// we need alignement on a cplx.
	//const size_t page_size = sysconf(_SC_PAGESIZE);		// the page size.
	const size_t page_size = 4096;
	if ((ltr) || (GET_FILE_PREC(fh) != 2) || (ofs & (cplx_sze-1)) || (swap_endian)) return 0;		// not possible to mmap
	size_t size = ofs + cplx_sze*(fh.ire-fh.irs+3)*fh.nlm*fh.nc;
	size = ((size + (page_size-1))/page_size) * page_size;	// round up to a page
	cplx* map_ptr = (cplx*) mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fileno(fp), 0);
	if (map_ptr == MAP_FAILED) {
		// try a read-only map if the private read-write map failed
		printf(" Warning!  private read-write map failed, trying shared read-only mapping.\n  Some operations that alter the field will not be permitted !\n");
		map_ptr = (cplx*) mmap(NULL, size, PROT_READ, MAP_SHARED, fileno(fp), 0);
	}
	if (map_ptr == MAP_FAILED)	return 0;	// mapping failed.
	return (map_ptr + (ofs/cplx_sze));
}

/// returns the number of components or zero if the mapping failed.
int Spectral::map_file(SpectralFile& F)
{
	if ((F.fh.lmax != LMAX) || (F.fh.mmax != MMAX) || (F.fh.mres != MRES))
		return 0;		// cannot map if different sh truncation.
	cplx* map_ptr = F.mem_map();			// try mem_map
	if (map_ptr) {
		init_layout(F.fh.nc, F.fh.irs, F.fh.ire);
		nelem = F.fh.nlm;
		#ifdef XS_DEBUG
		printf(" note: using mmap (nshell=%d, ncomp=%d, nelem=%d)\n",nshell,ncomp,nelem);
		#endif
		allocated = 2;		// mark as allocated, but the memory should not be freed as it is a mapped file !
		data.init_from_buffer(map_ptr, nelem);
		return ncomp;
	}
	return 0;
}
#endif

/// load informations about Pol/Tor field stored in file.
int load_FieldInfo(char *fn, FieldInfo *fh=0)
{
	SpectralFile Field;

	int nc = Field.open(fn);
	if (fh) memcpy(fh, &Field.fh, sizeof(FieldInfo));
	Field.close();
	return nc;		/// returns number of components for a valid file, or zero if not valid.
}

void iter_from_file(char* fname, int *it)
{
	struct FieldInfo jinfo;
	int nc = load_FieldInfo(fname, &jinfo);
	if ((nc > 0) && (jinfo.iter > 0))	*it = jinfo.iter;
}


/// load spherical harmonic representation of field.
void Spectral::load(const char *fn, FieldInfo *fh)
{
	double *r_in;
	int ir;
	int interp;
	SpectralFile F;

	if (NLM == 0)		// spherical harmonic size not set
		runerr("[load_PolTor] NLM=0 !");
	if (r == 0)		// no grid
		runerr("[load_PolTor] radial grid must be initialized !");

	if ( F.open(fn) == 0 )	runerr("[Spectral::load] file not found or not valid !");
	if (fh) {
		memcpy(fh, &(F.fh), sizeof(FieldInfo));
	} else fh = &(F.fh);
	r_in = F.rr;

	if ( GET_FILE_VERSION(*fh) > FILE_VERSION ) runerr("[load_PolTor] file version is too recent !");
	if (fh->shtnorm != sht_norm) runerr("[load_PolTor] incompatible spherical harmonic norm !");

	if (bci == BC_NONE) bci = (boundary) fh->BCi;		// update boundary conditions if required.
	if (bco == BC_NONE) bco = (boundary) fh->BCo;

	interp = 1;		// interpolation is on.
	if (NR == fh->nr) {		// check if grids are matching.
		for (ir=0; ir<NR; ir++) {
			if ( fabs(r_in[ir]-r[ir]) > 1e-10*r[ir] ) break;
		}
		if (ir == NR) interp=0;		// grids match, no interpolation required.
	}
	if (interp) {		// interpolation required.
		if (xsio_allow_interp == 0) runerr("grid mismatch (interpolation disabled).\n");
		PRINTF0("  *** WARNING! grid mismatch => interpolating.\n");
		if (is_unallocated()) runerr("[load_PolTor] interpolation requires a valid, allocated PolTor field.");
	} else {		// check that fields are matching
		if (is_unallocated()) {
			#ifndef XS_MPI
			if (map_file(F)) {		// try to mmap the file
				F.close();
				return;
			}
			#endif
			alloc(fh->nc, NLM, fh->irs, fh->ire);
		}
		if ((ir_bci != fh->irs)||(ir_bco != fh->ire)) runerr("[Spectral::load] radial domain of fields mismatch !");
	}
	if (fh->nc != ncomp) runerr("[Spectral::load] wrong number of components !");

	const long dist = (ncomp > 1) ? get_data(1,ir_bci) - get_data(0,ir_bci) : 0;
	F.prepare_read();
	if (interp==0) {		// no interpolation required.
		ir = ir_bci;
		if (own(ir)) F.read_shell(ir-1, get_data(0,ir-1),  dist);	// lower ghost
		for (ir= ir_bci; ir<= ir_bco; ir++) {
			if (own(ir)) F.read_shell(ir, get_data(0,ir),  dist);
		}
		ir = ir_bco;
		if (own(ir)) F.read_shell(ir+1, get_data(0,ir+1), dist);	// upper ghost
	} else {		// interpolation required !
		cplx* Sd = (cplx *) malloc(2*nelem * ncomp*sizeof(cplx));	// alloc 2 shells
		cplx* Sd0 = Sd;		cplx* Sd1 = Sd0 + nelem*ncomp;

		// linear map for radial grids.
		int ire0 = fh->ire;
		int irs0 = fh->irs;
		double rs = r[ir_bci];
		double r0s = r_in[irs0];
		double ra = (r_in[ire0]-r0s)/(r[ir_bco]-rs);

		// inner boundary
		int ir0 = irs0;
		ir = ir_bci;
		if (own(ir)) {
			F.read_shell(ir0-1, get_data(0,ir-1), dist);		// ghost shell
			F.read_shell(ir0, get_data(0,ir), dist);			// inner boundary
		}

		ir0 = irs0;
		int ir_Sd1 = ir0 - 5;		// shell number stored in Sd1 (marked as not valid)
		// interior shells with interpolation
		for (ir= ir_bci+1; ir< ir_bco; ir++) {
			if (own(ir)) {
				double rr0 = (r[ir]-rs)*ra + r0s;		// map r to original r_in
				if (r_in[ir0] < rr0) {	
					while ((ir0<ire0)&&(r_in[ir0] < rr0))	++ir0;
					if (ir0-1 == ir_Sd1) {
						cplx* t = Sd0;		Sd0 = Sd1;		Sd1 = t;
					} else F.read_shell(ir0-1, Sd0, nelem);
					F.read_shell(ir0, Sd1, nelem);
					ir_Sd1 = ir0;
				}
				// interp
				double alpha = (rr0-r_in[ir0-1])/(r_in[ir0]-r_in[ir0-1]);
				double beta = 1.0-alpha;
				for (int j=0; j<ncomp; j++) {
					for (int lm=0; lm<nelem; ++lm) get_data(j,ir)[lm] = alpha*Sd1[j*nelem +lm] + beta*Sd0[j*nelem +lm];
				}
			}
		}

		// outer boundary
		ir = ir_bco;
		ir0 = ire0;
		if (own(ir)) {
			F.read_shell(ir0, get_data(0,ir), dist);			// outer boundary
			F.read_shell(ir0+1, get_data(0,ir+1), dist);		// ghost shell
		}

		free(Sd);
	}

	F.close();
	#ifdef XS_MPI
		MPI_Barrier(MPI_COMM_WORLD);
	#endif
	return;
}

/// load only the grid from a PolTor field.
int load_Field_grid(char *fn)
{
	SpectralFile F;
	int nr, nc;

	nr = 0;
	nc = F.open(fn);
	if ( nc > 0 ) {
		nr = F.fh.nr;
		if (r) {
			#ifdef XS_DEBUG
				printf("  *** WARNING! load_PolTor_grid is overwriting the grid.\n");
			#endif
			free(r);
		}
		r = F.rr;		// steal the grid we just loaded.
		F.rr = 0;		// prevent deletion of grid
		NR = nr;
		if (i_mpi == 0) printf("[load_PolTor_grid] %d radial grid points read from file '%s'\n",NR,fn);
	}
	F.close();
	return(nr);
}


/* TEXT (ASCII) DATA READING FUNCTIONS */

/// load radial grid points from text file (one grid point per line)
int load_grid(char *fname)
{
	double f;
	FILE *fp;
	int nr = 0;

	if (r != NULL) {
		#ifdef XS_DEBUG
			printf("  *** WARNING! load_grid is overwriting the grid.\n");
		#endif
		free(r);
	}

	if (i_mpi==0) {
		fp = fopen(fname,"r");
		if (fp) {	// count lines :
			while ( fscanf(fp, "%lf\n", &f) ) {
				nr++;
				if (feof(fp)) break;
			}
			if (nr <= 0) fclose(fp);		// not a valid grid
		}
	}
	#ifdef XS_MPI
		MPI_Bcast(&nr, sizeof(nr), MPI_BYTE, 0, MPI_COMM_WORLD);
	#endif

	if (nr > 0) {		// load values :
		NR = nr;
		r = (double *) malloc(NR * sizeof(double));		// allocation of radial grid
		if (i_mpi == 0) {
			rewind(fp);		// go back to beginning of file
			for (int i=0; i<NR; i++) {
				fscanf(fp, "%lf\n", &r[i]);
			}
			fclose(fp);
			printf("[load_grid] %d radial grid points read from file '%s'\n",NR,fname);
		}
		#ifdef XS_MPI
			MPI_Bcast(r, NR, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		#endif
	}
	return(nr);
}

#ifndef XS_MPI

/** Read Pol/Tor data from a text file.
 The text file is interpreted like this :
 \li line starting with % are comments,
 \li line starting with %XS are directives,
 \li line containing numbers are data (one line per radius).
 
 Two directives are currenty supported :
 \li field description : \code %XS Pol l=1 m=0 ir0=100 \endcode
 \c Pol means that the following data is a Poloidal scalar, l=1 m=0 defines the spherical harmonic, and ir0=100 say that the data starts
 at radius index 100 (optional). \c Pol can be replaced by \c Tor or \c PolTor so that the following data is read as Toroidal or both Poloidal and Toroidal.
 The Poloidal and/or Toroidal complex values are read as real and imaginary parts. Poloidal first then Toroidal for \c PolTor.
 \li scaling : \code %XS scale=1.2e-3 ir0=50 \endcode to scale the following data. ir0 is also optional here and has the same meaning.
*/
void load_PolTor_text(char *fn, PolTor *PT)
{
	int nb = 256;		// buffer size for reading a single line.
	int ir, l,m, j, ir0, ir1;
	FILE *fp;
	char *buf;
	double v1,v2,v3,v4, scale;
	xs_array2d<cplx> d1, d2;
	char s1[16], s2[16];

	fp = fopen(fn,"r");
	if (fp == 0) runerr("[load_PolTor_text] file not found !");

	PT->zero_out();		// start with all zeros.

	buf = (char *) malloc(sizeof(char)* nb);
	scale = 1.0;	ir1 = PT->irs;
	ir = ir1;	ir0 = ir1;
	while ( fgets(buf,nb,fp) ) {
		if (buf[0] == '%') {		// comments
			j = sscanf(buf,"%s %s l=%d m=%d",s1,s2,&l,&m);
			if ((j>=0) && (strcmp(s1,"%XS") == 0)) {	// we have a directive
				if (j==4) {
					if (d1) printf(" => %d radii read.\n",ir-ir0);
					if (strcmp(s2,"Pol") == 0) {	d1 = PT->Pol;		d2.clear();	}
					if (strcmp(s2,"Tor") == 0) {	d1 = PT->Tor;		d2.clear();	}
					if (strcmp(s2,"PolTor") == 0) {	d1 = PT->Pol;		d2 = PT->Tor;	}
					printf("       %s l=%d m=%d",s2,l,m);
				} else {
					j = sscanf(buf,"%s scale=%lf r0=%lf",s1,&scale,&v1);
					if (j>=2) printf("       scaling by %e\n",scale);
					if (j==3) {
						ir1 = r_to_idx(v1);
						printf("       starting radius is %f (ir=%d)\n",v1,ir1);
					}
				}
				ir = ir1;	ir0 = ir1;
			}
		} else {
			if ((!d1)&&(!d2)) runerr("[load_PolTor_text] bad file\n");
			v1 = 0.0;	v2 = 0.0;	v3 = 0.0;	v4 = 0.0;
			j = sscanf(buf,"%lf %lf %lf %lf",&v1, &v2, &v3, &v4);
			if (j>0) {
				if ((ir>=0)&&(ir<NR)) {
					if (d1) set_Ylm(d1[ir], l, m, cplx(v1,v2)*scale );
					if (d2) set_Ylm(d2[ir], l, m, cplx(v3,v4)*scale );
					ir ++;
				} else runerr("[load_PolTor_text] something went wrong...");
			}
		}
	}
	if (d1) printf(" => %d radii read.\n",ir-ir0);
	free(buf);
	fclose(fp);
}

#endif

int load_sh_text(char *fname, cplx* sh, double *freq)
{
	double c0 = 1.0;
	int lmax, mmax, norm;
	int l, m, k, ok, skipped;
	char *buf2, *buf3;
	FILE *fp;
	char buf[256];

	for (l=0; l<NLM; l++) sh[l] = 0.0;		// zero out field.
	if (freq) for (l=0; l<NLM; l++) freq[l] = 0.0;		// zero out frequency.
	ok = 0;

  if (i_mpi==0) {
	printf("[load_sh_text] Reading SH surface data from text file '%s' ",fname);
	fp = fopen(fname,"r");
	if (fp == 0) runerr("[load_sh_text] file not found !");

	lmax=0;		mmax=0;		norm=0;
	while(1) {	// skip comments
		fgets(buf, 254, fp);		// read line
		if (feof(fp)) {
			fclose(fp);		return 0;		// end of file reached
		}
		if (buf[0] == '%') {		// try to read lmax and mmax values.
			if ( (buf2 = strstr(buf, "lmax=")) ) {
				double c = strtod(buf2+5,&buf3);		if (buf3 != buf2) lmax = c;
			}
			if ( (buf2 = strstr(buf, "mmax=")) ) {
				double c = strtod(buf2+5,&buf3);		if (buf3 != buf2) mmax = c;
			}
			if ( (buf2 = strstr(buf, "norm=")) ) {
				double c = strtod(buf2+5,&buf3);		if (buf3 != buf2) norm = c;
			}
		} else break;
	};

//	if (norm==0)	c0 = sqrt(8*M_PI);	// to match Alexandra's data
// load data : import from orthonormalized SH.
	ok=0;	skipped = 0;
	for (m=0; m<=mmax; m++) {
		for (l=m; l<=lmax; l++) {
			double c=0;		double s=0;		double f=0;
			k = sscanf(buf, "%lf %lf %lf", &c, &s, &f);
			if (k < 2) runerr("[load_sh_text] wrong format !");
			if ((c != 0.0) || (s != 0.0)) {	// mode is non-zero.
				if ( (m<=MMAX*MRES) && (l<=LMAX) && (!(m % MRES)) ) {
					cplx z = cplx(c, -s);
					if (norm==0)	z *= c0*((1-2*(m&1)) / sqrt(2*l +1));		// Schmidt normalized, CS phase
					set_Ylm(sh, l, m, z);
					if (freq) freq[LM(shtns,l,m)] = f;
					ok++;
				} else skipped++;
			}
			fgets(buf, 254, fp);		// next line
		}
	}
	fclose(fp);
	printf("(lmax=%d, mmax=%d, norm=%d) : %d modes set, %d discarded.\n",lmax, mmax, norm, ok,skipped);
  }
  #ifdef XS_MPI
	MPI_Bcast(sh, NLM*2, MPI_DOUBLE, 0, MPI_COMM_WORLD);		// Broadcast data
	if (freq) MPI_Bcast(freq, NLM, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Bcast(&ok, 1, MPI_INT, 0, MPI_COMM_WORLD);
  #endif
	return(ok);		// return number of lines read.
}

/* USEFULL TEXT OUPUT FUNCTIONS */

/// save the array doubles vec[N] to ASCII file 'fn'
/// with optional comment 'cmt'.
int write_vect(const char *fn, double *vec, long int N, const char *cmt)
{
	FILE *fp; 
	long int i;

	fp = fopen(fn,"w");
	if (fp == 0) {
		printf("[write_vect] File '%s' could not be written\n", fn);
		return 0;		// fail
	}
	if (cmt == 0) {
		fprintf(fp,"%% [XSHELLS] vector\n");
	} else {
		fprintf(fp,"%% [XSHELLS] %s\n",cmt);
	}
	for (i=0;i<N;i++)
		fprintf(fp,"%.6lg\n",vec[i]);
	fclose(fp);
	return 1;		// success
}

/* WRITE IN NUMPY ARRAY FORMAT FILES
 * 		adapted from https://github.com/rogersce/cnpy
 * 		see also https://docs.scipy.org/doc/numpy/neps/npy-format.html for format description
 */
FILE* create_npy(const char* fname, const int nx, const int ny=1, const int nz=1, const char* dtype = "f8")
{
	FILE* fp = NULL;
    char intstring[16];
    unsigned char endian[] = {1,0};		// for big/little endian test
	unsigned char header[] = {0x93,'N','U','M','P','Y', 0x01,0x00, 0,0};		// 10 bytes (the last two will contain the dict length)

	fp = fopen(fname,"wb");
	if (fp) {
		std::string dict;
		dict += "{'descr':'";
		short y = *(short*) endian;
		dict += (y == 1) ? '<' : '>';		// big or little endian
		dict += dtype;		//  "f8" for double, "f4" for float, "c16" for complex double, etc...
		dict += "','fortran_order':False,'shape':(";
		sprintf(intstring, "%d", nx);
		dict += intstring;
		dict += ",";
		if (ny > 1) {
			sprintf(intstring, "%d", ny);
			dict += intstring;
		}
		if (nz > 1) {
			dict += ",";
			sprintf(intstring, "%d", nz);
			dict += intstring;
		}
		dict += "),}";
		//pad with spaces so that header+dict is modulo 16 bytes. header is 10 bytes. dict needs to end with \n
		int remainder = 16 - (10 + dict.size()) % 16;
		dict.insert(dict.end(),remainder,' ');
		//dict.back() = '\n';   // C++ 11
		*dict.rbegin() = '\n';	// C++ 03

		unsigned short sze = dict.size();
		header[8] = (unsigned char) (sze & 0xFF);		// write size of dict into header (always little endian !)
		header[9] = (unsigned char) (sze >> 8);

		fwrite(&header[0],sizeof(char),10,fp);			// write header
		fwrite(dict.c_str(),sizeof(char),dict.size(),fp);	// write dict
	}
	return fp;
	// then, to write the data, simply use: fwrite(data,sizeof(double),n,fp);
}

/// save the array doubles vec[N] to numpy file 'fn' (as double)
int write_vect_npy(const char *fn, const double *vec, long int N)
{
	FILE *fp = create_npy(fn, N);
	if (fp == 0) {
		printf("[write_vect_npy] File '%s' could not be written\n", fn);
		return 0;	// fail
	}
	fwrite(vec, sizeof(double), N, fp);
	fclose(fp);
	return 1;	// success
}

/* **** XSHELLS NumPy format *****
 * 1,2 or 3-dimensional array.
 * 1D array: simple vector (no metadata)
 * 2D array: some slice, or SH vs r
 * 3D array: collection of slices (e.g. for vector components)
 * for 2D array, the entry [0,0] is used for encoding some metadata
 *  slice-type (S): 0=merid (r,angle,non-periodic); 1=disc (r,angle,periodic);  2=surf (theta,phi);  3=SH (lm,r);  : 4 bits
 * 	field-name (F): 2* ascii-char - 64 : 2*6 = 12 bits
 *  component (C): none,r,theta,phi,s,x,y,z,P,T : 4 bits
 */

class NumpyFile {
	FILE *fp;
	int slicetype;
	int n1,n2;
	int n1_pad;
	int sliceidx, nslices;
	int sze;
	float* x;
	float* y;
	float* data;

  public:
	NumpyFile() { fp=0; slicetype=0; n1=0; n2=0; nslices=0; sliceidx=0; x=0; }
	NumpyFile(const int nx, const int ny, const int nz=1, const int nx_pad=0) { init(nx,ny,nz, nx_pad); }
	~NumpyFile() { close(); }
	void init(const int nx, const int ny, const int nz=1, const int nx_pad=0);
	void write_slice(const double *v, int field=0, int comp=0);
	void write_slice_cplx(const cplx* z, int field=0, int comp=0);
	void close();

	void create_merid(const char* fname, const double* r, const double* cos_theta);
	void create_disc(const char* fname, const double* r, const double* phi=0);
	void create_surf(const char* fname, const double* cos_theta, const double* phi=0);
	void create_SH(const char* fname, const double* r, const double* l=0);
};

void NumpyFile::init(const int nx, const int ny, const int nz, const int nx_pad)
{
	fp=0; 	slicetype=0;	sliceidx=0;
	nslices = nz;
	n1 = nx;	n2 = ny;
	n1_pad = (nx_pad >= nx) ? nx_pad : nx;
	x = (float*) malloc((2*n1+n2) * sizeof(float));
	y = x + n1;
	data = y + n2;
}

void NumpyFile::create_merid(const char* fname, const double* r, const double* cos_theta)
{
	slicetype = 0 << 16;
	fp = create_npy(fname, nslices, n2+1, n1+1, "f4");
	if (fp == 0) runerr("[NumpyFile::create_merid] File creation error");

	for (int i=0; i<n2; i++)	y[i] = r[i];
	for (int i=0; i<n1; i++)	x[i] = acos(cos_theta[i]);
}

void NumpyFile::create_disc(const char* fname, const double* r, const double* phi)
{
	slicetype = 1 << 16;
	fp = create_npy(fname, nslices, n2+1, n1+1, "f4");
	if (fp == 0) runerr("[NumpyFile::create_disc] File creation error");

	for (int i=0; i<n2; i++)	y[i] = r[i];
	if (phi) {
		for (int i=0; i<n1; i++)	x[i] = phi[i];
	} else {
		for (int i=0; i<n1; i++)	x[i] = (2.*M_PI*i)/(MRES*NPHI);
	}
}

void NumpyFile::create_surf(const char* fname, const double* cos_theta, const double* phi)
{
	slicetype = 2 << 16;
	fp = create_npy(fname, nslices, n2+1, n1+1, "f4");
	if (fp == 0) runerr("[NumpyFile::create_surf] File creation error");

	for (int i=0; i<n1; i++)	x[i] = acos(cos_theta[i]);	
	if (phi) {
		for (int i=0; i<n2; i++)	y[i] = phi[i];
	} else {
		for (int i=0; i<n2; i++)	y[i] = (2.*M_PI*i)/(MRES*NPHI);
	}
}

void NumpyFile::create_SH(const char* fname, const double* r, const double* l)
{
	slicetype = 3 << 16;
	fp = create_npy(fname, nslices, n2+1, n1+1, "c8");
	if (fp == 0) runerr("[NumpyFile::create_SH] File creation error");

	for (int i=0; i<n2; i++)	y[i] = r[i];
	if (l) {
		for (int i=0; i<n1; i++)	x[i] = l[i];		
	} else {
		for (int i=0; i<n1; i++)	x[i] = el[i];
	}
}

void NumpyFile::write_slice(const double *v, int field, int comp)
{
	if (sliceidx >= nslices) runerr("[NumpyFile::write_slice] too many slices");
	switch(comp) {
		case 'r' : comp = 1;	break;
		case 't' : comp = 2;	break;
		case 'p' : comp = 3;	break;		
		case 's' : comp = 4;	break;
		case 'x' :
		case 'y' :
		case 'z' : comp = comp - 'x' + 5; break;
		case 'P' : comp = 8;	break;	// poloidal
		case 'T' : comp = 9;	break;	// toroidal
	}
	if (comp > 15) comp = 0;
	float tmp = slicetype + (comp&15) + (((field-64)&63) << 4);
	if (field>>8) tmp += ((((field>>8)-64)&63) << 10);
	fwrite(&tmp, sizeof(float), 1, fp);		// metadata
	fwrite(x, sizeof(float), n1, fp);		// write x
	for (int i=0;i<n2;i++) {
		tmp = y[i];
		fwrite(&tmp, sizeof(float), 1, fp);		// first row = y
		for(int j=0; j<n1; j++)  data[j] = v[i*n1_pad + j];			// convert to float
		fwrite(data, sizeof(float), n1, fp);		// data
	}
	sliceidx += 1;
}

void NumpyFile::close()
{
	if (fp) {
		fclose(fp);
		fp = 0;
	}
	if (x) {
		free(x);
		x = 0;
	}
}
