/// FINITE DIFFERENCE APPROXIMATION ///

#include "findiff.hpp"

#ifdef XS_O4
// Eigen dense matrix library
#define EIGEN_DONT_PARALLELIZE
#include "eigen3/Eigen/LU"
typedef Eigen::Matrix<double, 3, 3> Matrix3d;
typedef Eigen::Matrix<double, 5, 5> Matrix5d;
#endif

void fd_deriv_o2(const double* x, const int i, double* Dx, double* D2x)
{
  #ifdef XS_O4
	Matrix3d fd, inv;
	// SECOND ORDER EXPANSION
	double dxl = x[i]-x[i-1];
	double dxu = x[i+1]-x[i];
	fd <<	1.0, -dxl, dxl*dxl/2,		// expansion of f(i-1)
			1.0, 0, 0,					// expansion of f(i)
			1.0, dxu, dxu*dxu/2;		// expansion of f(i+1)

	inv = fd.inverse();
	if (Dx)  for (int j=0; j<3; j++)	Dx[j] = inv(1,j);
	if (D2x) for (int j=0; j<3; j++)	D2x[j] = inv(2,j);
  #else
	// second order FD formulas:
	double dl = x[i]-x[i-1];
	double du = x[i+1]-x[i];
	if (Dx) {
		Dx[0] = -du/(dl*(dl + du));
		Dx[1] = (-dl + du)/(dl*du);
		Dx[2] = dl/(du*(dl + du));
	}
	if (D2x) {
		D2x[0] = 2/(dl*(dl + du));
		D2x[1] = -2/(dl*du);
		D2x[2] = 2/(du*(dl + du));
	}
  #endif
}

/// boundary condition specified by:  a0*F + a1*F' + a2*F" = ghost
/// sign of dx specifies inner (dx > 0) or outer (dx < 0) boundary condition
void fd_deriv_o2_bc(const double dx, const double a0, const double a1, const double a2,
					double* Dx, double* D2x)
{
  #ifdef XS_O4
	Matrix3d fd, inv;
	fd <<	a0, a1, a2,		// expansion of f(i-1)
			1.0, 0, 0,		// expansion of f(i)
			a0, a1, a2;		// expansion of f(i+1)
	int line = 0;
	if (dx > 0.) line = 2;
	fd(line,0) = 1.0;		fd(line,1) = dx;		fd(line,2) = dx*dx/2;

	inv = fd.inverse();
	if (Dx)  for (int j=0; j<3; j++)	Dx[j] = inv(1,j);
	if (D2x) for (int j=0; j<3; j++)	D2x[j] = inv(2,j);
  #else
	// second order FD formulas:
	double m0,m1,m2;
	double p0,p1,p2;
	m0 = 1.0;	m1 = dx;	m2 = dx*dx/2;
	p0 = a0;	p1 = a1;	p2 = a2;
	if (dx > 0.) {
		m0 = a0;	m1 = a1;	m2 = a2;		
		p0 = 1.0;	p1 = dx;	p2 = dx*dx/2;
	}
	double det_1 = 1.0/(m2*p1 - m1*p2);
	if (Dx) {
		Dx[0] = -p2 * det_1;				// p2/(m1 p2-m2 p1);
		Dx[1] =  (m0*p2 - m2*p0) * det_1;	// (m2 p0-m0 p2)/(m1 p2-m2 p1)
		Dx[2] = m2 * det_1;					// m2/(m2 p1-m1 p2)
	}
	if (D2x) {
		D2x[0] = p1 * det_1;				// p1/(m2 p1-m1 p2)
		D2x[1] = (m1*p0 - m0*p1) * det_1;	// (m1 p0-m0 p1)/(m2 p1-m1 p2)
		D2x[2] = -m1 * det_1;				// m1/(m1 p2-m2 p1))
	}
  #endif
}

/// Boundary condition sepcified by F^(k)[i_bc] = ghost
/// where k = dk_zero is the order of the derivative specified in the ghost shell.
void fd_deriv_o2_bc(const double dx, const int dk_zero, double* Dx, double* D2x)
{
	double a[3] = {0.0, 0.0, 0.0};
	a[dk_zero] = 1.0;
	fd_deriv_o2_bc(dx, a[0], a[1], a[2], Dx, D2x);
}

#ifdef XS_O4
/*
void fd_taylor_o4(double dxll, double dxl, double dxu, double dxuu, Matrix5d &fd)
{
	double dx2l = dxl*dxl;
	double dx2u = dxu*dxu;
	double dx2ll = dxll*dxll;
	double dx2uu = dxuu*dxuu;
	fd <<	1.0, -dxll, dx2ll/2, -dxll*dx2ll/6, dx2ll*dx2ll/24,		// expansion of f(i-2)
			1.0, -dxl,  dx2l/2,  -dxl*dx2l/6,   dx2l*dx2l/24,		// expansion of f(i-1)
			1.0, 0, 0, 0, 0,										// expansion of f(i)
			1.0, dxu,  dx2u/2,  dxu*dx2u/6,   dx2u*dx2u/24,			// expansion of f(i+1)
			1.0, dxuu, dx2uu/2, dxuu*dx2uu/6, dx2uu*dx2uu/24;		// expansion of f(i+2)
}
*/

/// Fourth order Taylor expansion
void fd_taylor_o4(const double dx, Matrix5d& fd, int di)
{
	const double dx2 = dx*dx;
	di += 2;
	fd(di, 0) = 1.0;
	fd(di, 1) = dx;
	fd(di, 2) = dx2/2;
	fd(di, 3) = dx2*dx/6;
	fd(di, 4) = dx2*dx2/24;
}

void fd_deriv_o4(const double* x, const int i, double* Dx, double* D2x, double* D3x, double* D4x)
{
	Matrix5d fd, inv;

	for (int j=-2; j<=2; j++)	fd_taylor_o4(x[i+j]-x[i], fd, j);

	inv = fd.inverse();
	if (Dx)  for (int j=0; j<5; j++)	Dx[j]  = inv(1,j);
	if (D2x) for (int j=0; j<5; j++)	D2x[j] = inv(2,j);
	if (D3x) for (int j=0; j<5; j++)	D3x[j] = inv(3,j);
	if (D4x) for (int j=0; j<5; j++)	D4x[j] = inv(4,j);
}

/// Boundary condition sepcified by F^(k)[i_bc] = ghost
/// where k = dk_zero is the order of the derivative specified in the ghost shell.
void fd_deriv_o4_bc(const double* x, const int i, const int i_bc, const int dk_zero, double* Dx, double* D2x, double* D3x, double* D4x)
{
	Matrix5d fd, inv;
	int line_ghost, line_bc;
	int j0 = -2;
	int j1 = 2;

	line_bc = 0;
	if ((i-i_bc == 1) || (i-i_bc == -1)) {		// inner or outer boundary
		if (i-i_bc == -1) {
			line_ghost = 4;		// outer boundary
			j1 = 1;
		} else {
			line_ghost = 0;		// inner boundary
			j0 = -1;
		}
		line_bc = line_ghost + (i-i_bc);
	}
	for (int j=j0; j<=j1; j++)	fd_taylor_o4(x[i+j]-x[i], fd, j);
	if (line_bc) {
		for (int j = 0; j<dk_zero; j++)  fd(line_ghost, j) = 0.0;
		for (int j = dk_zero; j<5; j++)  fd(line_ghost, j) = fd(line_bc, j-dk_zero);
	}

	inv = fd.inverse();
	if (Dx)  for (int j=0; j<5; j++)	Dx[j]  = inv(1,j);
	if (D2x) for (int j=0; j<5; j++)	D2x[j] = inv(2,j);
	if (D3x) for (int j=0; j<5; j++)	D3x[j] = inv(3,j);
	if (D4x) for (int j=0; j<5; j++)	D4x[j] = inv(4,j);
}
#endif	/* XS_O4 */
