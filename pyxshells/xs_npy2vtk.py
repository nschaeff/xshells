#!/bin/python

import xsplot
from pylab import *

for i in range(10,1000):
	print(i)
	t = load('fieldT_%04d.ra2e10_ek1e8_movie_equat.npy' % i)
	u = load('fieldU_%04d.ra2e10_ek1e8_movie_equat.npy' % i)
	w = load('fieldU_%04d.ra2e10_ek1e8_movie_W_equat.npy' % i)
	xsplot.slice2vtk('equat_%04d' % i, (t,u,w))

	t = load('fieldT_%04d.ra2e10_ek1e8_movie_merid0.npy' % i)
	u = load('fieldU_%04d.ra2e10_ek1e8_movie_merid0.npy' % i)
	w = load('fieldU_%04d.ra2e10_ek1e8_movie_W_merid0.npy' % i)
	xsplot.slice2vtk('merid_a_%04d' % i, (t,u,w), phi=0)

	t = load('fieldT_%04d.ra2e10_ek1e8_movie_merid1.npy' % i)
	u = load('fieldU_%04d.ra2e10_ek1e8_movie_merid1.npy' % i)
	w = load('fieldU_%04d.ra2e10_ek1e8_movie_W_merid1.npy' % i)
	xsplot.slice2vtk('merid_b_%04d' % i, (t,u,w), phi=pi)
	
	t = load('fieldT_%04d.ra2e10_ek1e8_movie_surf.npy' % i)
	u = load('fieldU_%04d.ra2e10_ek1e8_movie_surf.npy' % i)
	w = load('fieldU_%04d.ra2e10_ek1e8_movie_W_surf.npy' % i)
	xsplot.slice2vtk('surf_%04d' % i, (t,u,w), r=1)  # actual r=0.999

