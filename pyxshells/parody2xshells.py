#!/bin/python

import pyxshells	# to write xshells files
import struct		# to read crazy fortran records
import sys
from numpy import *

if len(sys.argv) < 2:
	print("Convert Parody 'D' file to xshells field files")
	print("  usage: %s <parody_D_file> [output_jobname]" % sys.argv[0])
	exit()
fname = sys.argv[1]
job_out = 'parody'
if len(sys.argv) == 3:
	job_out = sys.argv[2]


##### READ PARODY 'D' FILE #####
print('reading parody file ', fname, ' ...')
f=open(fname,"rb")

### header
head=f.read(72)
h = struct.unpack(">iiiidiidiiiiiiii", head)		## apparently, parody files are big endian.
par = {}
par['version'] = h[1]
par['time'] = h[4]
par['dt'] = h[7]
par['nlm'] = h[10]
par['NG'],par['NR'] = h[13],h[14]

nlm = par['nlm']
NR = par['NR']
NG = par['NG']-1	# convert from 1-based fortran to 0-based index

print(par)
if par['version'] != 3:
	 print("error, only version 3 is supported")
	 exit()

### radial grid
r = zeros(NR)
for i in range(0, NR):
	x = f.read(16)
	r[i] = struct.unpack(">idi", x)[1]
print("radial grid from r=%f to %f" % (r[0], r[-1]))

#### l,m list
larr = zeros(nlm, dtype=int)
marr = zeros(nlm, dtype=int)
for lm in range(0, nlm):
	x = f.read(16)
	s = struct.unpack(">iiii", x)
	larr[lm], marr[lm] = s[1],s[2]

print("lmax=",amax(larr), "  mmax=",amax(marr))

# omega, omega_old, couple, couple_old  ==> ignored
x = f.read(16*4)
s = struct.unpack(">idiidiidiidi", x)
omega, omega_old = s[1],s[4]
couple, couple_old = s[7],s[10]
print(s)

# allocate space for data:
data = zeros((5, NR+1, nlm) ,dtype=complex128)		# 5 scalars : Vtor, Vpol, Btor, Bpol, Temp

#### data within inner-core:
for ir in range(0,NG):
	for k in range(0,2):	# magnetic field only (2 components)
		s = f.read(4)	# skip record header
		x = fromfile(f, dtype='>d', count=2*nlm)
		data[k+2,ir,:] = x[0::2] + 1j*x[1::2]			# Btor, Bpol
		s = f.read(4)
	f.seek((2*nlm*8 + 8)*2, 1)		# skip 2 Adams

#### data within fluid:
for ir in range(NG,NR):
	for k in range(0,5):	# all 3 fields (5 components)
		s = f.read(4)	# skip record header
		x = fromfile(f, dtype='>d', count=2*nlm)
		data[k,ir,:] = x[0::2] + 1j*x[1::2]
		s = f.read(4)
	f.seek((2*nlm*8 + 8)*5, 1)		# skip 5 Adams

print(f.tell(), " bytes parsed.")
f.close()
### DONE READING PARODY ###

##### WRITE XSHELLS FILES ######
print('converting & writing ...')

## generate xshells grid
grid = pyxshells.Grid(r)

lmax, mmax = amax(larr), amax(marr)
mres = sort(unique(marr))
if len(mres) > 1:
	mres = mres[1]-mres[0]
else:
	mres = 1

### the 3 fields:
Ulm = pyxshells.PolTor(grid, (int(lmax),int(mmax),int(mres)))
Blm = pyxshells.PolTor(grid, (int(lmax),int(mmax),int(mres)))
Tlm = pyxshells.ScalarSH(grid, (int(lmax),int(mmax),int(mres)))

Ulm.alloc(NG, NR-1)
Blm.alloc(0, NR-1)
Tlm.alloc(NG, NR-1)

## Boundary conditions
Ulm.BC = (0,0)		# 0 = no-slip, 1 = free-slip
Blm.BC = (3,3)		# magnetic boundary conditions
Tlm.BC = (1,1)		# 0 = fixed value, 1 = fixed flux

for f in (Ulm, Blm, Tlm):
	f.time = par["time"]		# record the time

### convert spherical harmonic coefficients to orthonormalized as used by xshells.
renorm = sqrt(2*pi) * (1-2*(marr&1))
renorm[marr==0] = sqrt(4*pi)

for ir in range(0,NR):
	Blm.set_tor(ir, data[2,ir,:] * renorm)
	Blm.set_pol(ir, data[3,ir,:] * renorm)

for ir in range(NG,NR):
	Ulm.set_tor(ir, data[0,ir,:] * renorm)
	Ulm.set_pol(ir, data[1,ir,:] * renorm)
	Tlm.set_sh(data[4,ir,:] * renorm, ir)

Ulm.tofile('fieldU.'+job_out)
Blm.tofile('fieldB.'+job_out)
Tlm.tofile('fieldT.'+job_out)

print("done. Don't forget that fields are not scaled the same, in particular the magnetic field must be rescaled.")
