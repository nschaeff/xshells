from pylab import *
from numpy import *
from pyxshells import *
import shtns
import xsplot

ext = 'st2_big6'
istart = 221
iend = 487

def save_merid(filename, r, ct, V):
	b = zeros((len(r)+1, len(ct)+1))
	b[1:,0] = r
	b[0,1:] = arccos(ct)
	b[1:,1:] = V
	save(filename, b.reshape(1,b.shape[0],b.shape[1])

info,r =get_field_info('fieldU_%04d.%s' % (istart,ext))
sh = shtns.sht(info['lmax'], info['mmax'], info['mres'])
sh.set_grid(nl_order=2, flags=shtns.sht_gauss)
sh.print_info()
Y00_1 = sh.sh00_1()
nr = info['nr']
l2 = sh.l*(sh.l+1)

Urms = 0
Wrms = 0
Havg = zeros((nr, sh.nlat))
U2avg = zeros((nr, sh.nlat))
W2avg = zeros((nr, sh.nlat))
count = 0
for i in range(istart, iend+1):
	try:
		Ulm = load_field( 'fieldU_%04d.%s' % (i,ext))
	except FileNotFoundError:
		print('[%04d] missing file' % i)
	else:
		print('[%04d]\r' % i)
		count += 1
		# compute helicity
		for ir in range(Ulm.irs, Ulm.ire+1):
			Ulm.curl = 0	# not curl
			ur_lm = Ulm.rad(ir).astype(complex128)
			us_lm = Ulm.sph(ir).astype(complex128)
			ut_lm = Ulm.tor(ir).astype(complex128)
			ur_lm[sh.m==0] = 0		# non-zonal only
			us_lm[sh.m==0] = 0
			ut_lm[sh.m==0] = 0
			u2 = square(abs(ur_lm)) + (square(abs(us_lm))+square(abs(ut_lm)))*l2
			u2[sh.m==0] *= 0.5
			Ulm.curl = 1	# curl of field
			wr_lm = Ulm.rad(ir).astype(complex128)
			ws_lm = Ulm.sph(ir).astype(complex128)
			wt_lm = Ulm.tor(ir).astype(complex128)
			wr_lm[sh.m==0] = 0		# non-zonal only
			ws_lm[sh.m==0] = 0
			wt_lm[sh.m==0] = 0
			w2 = square(abs(wr_lm)) + (square(abs(ws_lm))+square(abs(wt_lm)))*l2
			w2[sh.m==0] *= 0.5
			ur,ut,up = sh.synth(ur_lm, us_lm, ut_lm)
			wr,wt,wp = sh.synth(wr_lm, ws_lm, wt_lm)
			h = ur*wr + ut*wt + up*wp	# helicity
			u2spat = ur*ur + ut*ut + up*up
			w2spat = wr*wr + wt*wt + wp*wp
			Havg[ir-Ulm.irs,:] += mean(h,axis=1)
			U2avg[ir-Ulm.irs,:] += mean(u2spat,axis=1)
			W2avg[ir-Ulm.irs,:] += mean(w2spat,axis=1)
			Urms += sum(u2) *r[ir]**2 * Ulm.delta_r(ir)
			Wrms += sum(w2) *r[ir]**2 * Ulm.delta_r(ir)
		
Havg /= count
Urms /= count
Wrms /= count
U2avg /= count
W2avg /= count

save_merid('o_Havg_nz.npy',r,sh.cos_theta, Havg)
save_merid('o_U2avg_nz.npy',r,sh.cos_theta, U2avg)
save_merid('o_W2avg_nz.npy',r,sh.cos_theta, W2avg)

hnorth = mean(Havg[:,0:sh.nlat/2])
hsouth = mean(Havg[:,sh.nlat/2:]) 
print('hnorth=',hnorth)
print('hsouth=',hsouth)

#xsplot.plot_merid(transpose(matrix(r[20:-20])),sh.cos_theta, Havg[20:-20,:])
#show()

