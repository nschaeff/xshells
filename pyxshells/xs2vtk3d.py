#!/usr/bin/env python

## Convert xshells field* files to VTK files that can be read by paraview.

import pyxshells
import numpy
import sys
import shtns
from evtk.hl import structuredToVTK


if len(sys.argv) < 2:
    print('Convert xshells field* files to VTK files (.vts) that can be read by paraview.')
    print('  usage: %s [-cart] <field.from_xshells> [<another_field>] [<with_same_grid>]' % sys.argv[0])
    exit()

cart=False   # spherical coordinates by default
fname_out=""

flist = []
for i in range(1, len(sys.argv)):
    print(sys.argv[i])
    if sys.argv[i] == '-cart':
        print("convert to cart")
        cart = True
    else:
        f=pyxshells.load_field(sys.argv[i])
        f.sht.set_grid(flags=shtns.sht_reg_poles)  # grid with poles
        f.id = sys.argv[i][5]
        flist.append(f)
        if fname_out == "":
            fname_out = sys.argv[i]

## construct the grid
ct = f.sht.cos_theta
st = numpy.sqrt(1.-ct**2)
r = f.grid.r

nr=len(r)
nt=len(ct)
np=f.sht.nphi+1
phi = numpy.arange(np) * 2*numpy.pi / (f.sht.mres*(np-1))
phi[-1] = 0 if f.sht.mres == 1 else phi[-2]+phi[1]  # loop around

Z1 = (r.reshape(nr,1) * ct.reshape(1,nt))
S1 = (r.reshape(nr,1) * st.reshape(1,nt))
X = ( S1.reshape(-1,1) * numpy.cos(phi.reshape(1,np)) ).reshape(nr,nt,np).astype('float32')
Y = ( S1.reshape(-1,1) * numpy.sin(phi.reshape(1,np)) ).reshape(nr,nt,np).astype('float32')
Z = numpy.repeat(Z1,np).reshape(nr,nt,np).astype('float32')


## convert and reorganize data:
pointData = {}
for f in flist:
    field_id = f.id
    print(field_id)
    cname = ['',] if f.ncomp() == 1 else ['r', 't', 'p']
    ## synthetize data onto spatial grid
    F = f.spat_full().reshape(nr,-1,nt,np-1)
    ## loop around in phi (must be done in spherical coordinates):
    F2 = numpy.empty((nr,F.shape[1],nt,np))
    F2[:,:,:,:-1] = F[:,:,:,:]
    F2[:,:,:,-1] = F[:,:,:,0]  # loop around
    F = F2

    ## convert to cartesian
    print(f.ncomp(), cart)
    if f.ncomp()==2 and cart:
        print("convert to cart")
        cost = ct.reshape(1,-1,1)
        sint = st.reshape(1,-1,1)
        cosp = numpy.cos(phi).reshape(1,1,-1)
        sinp = numpy.sin(phi).reshape(1,1,-1)
        Fz = F[:,0,:,:]*cost - F[:,1,:,:]*sint
        Fs = F[:,0,:,:]*sint + F[:,1,:,:]*cost
        Fx = Fs*cosp - F[:,2,:,:]*sinp
        Fy = Fs*sinp + F[:,2,:,:]*cosp
        F[:,0,:,:] = Fx
        F[:,1,:,:] = Fy
        F[:,2,:,:] = Fz
        cname = ['x', 'y', 'z']

    # put into dict with name
    for i in range(3 if f.ncomp()==2 else 1):
        pointData[field_id + cname[i]] = F[:,i,:,:].astype('float32').copy()

## write data to file
structuredToVTK(fname_out, X, Y, Z, pointData = pointData)
