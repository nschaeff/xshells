/*
 * Copyright (c) 2010-2016 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xsavg.c
/// XShells Averaging : average field and spectra.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <shtns.h>

#include "grid.cpp"
#include "xshells_spectral.cpp"
#include "xshells_io.cpp"

#define fftw_fcplx(x) reinterpret_cast<fftwf_complex*>(x)

fcplx *big = NULL;

double pi = M_PI;

FieldInfo jpar;	// parameters from loaded file
FieldInfo jpar2;	// parameters from loaded file

void usage()
{
	printf("\nUsage: xsavg <base_name> <extension> [options] istart step iend\n");
	printf("   xsavg performs average of a collection of fields stored in files base_name<%%04d>.extension\n");
	printf(" filtering options may help reduce memory usage :\n");
	printf(" -lmax=<lmax> : use only spherical harmonic degrees up to lmax.\n");
	printf(" -mmax=<mmax> : use only spherical harmonic orders up to mmax.\n");
}

int main (int argc, char *argv[])
{
	double *Espec;
	double tmin, tmax;
	int i, istart, iend, istep, count, missing, iend_true;
	int k, nf, nc;
	int irs, ire, nr;
	char fname[128];
	char *fbase;
	char *fext;
	FILE *fel, *fem;
	Spectral *SH_avg, *SH;

	printf(" [XSFFT] Xshells Averaging   by N. Schaeffer / CNRS, build %s, %s\n",__DATE__,__TIME__);
	if (argc < 3) { usage(); exit(1); }

	fbase = argv[1];	fext = argv[2];
	istart = 0;		istep = 1;	iend = 1024*1024;	// end when no more files are left.
	MMAX = 1024*1024;		LMAX = 1024*1024;		MRES = 0;		// no limits

	i = 3;
	while(i < argc) {
		if (argv[i][0] == '-') {		//option
			sscanf(argv[i],"-%[^=]=%lf",fname,&tmin);
			if (strcmp(fname,"lmax") == 0) LMAX = tmin;
			if (strcmp(fname,"mmax") == 0) MMAX = tmin;
			i++;
		} else break;
	}
	if (i < argc) {
		sscanf(argv[i], "%d", &istart);
		if (i+1 < argc) sscanf(argv[i+1], "%d", &istep);
		if (i+2 < argc) sscanf(argv[i+2], "%d", &iend);
	}

	sprintf(fname, "%s%04d.%s", fbase, istart, fext);

// get File infos
	if (load_FieldInfo(fname, &jpar)) {
		printf("\nmetadata for file '%s' : \n",fname);
		print_FieldInfo(&jpar);		// print metadata
		printf("\n");
	} else {		// not a poltor file
		usage(); exit(1);
	}
	tmin = jpar.t;
	irs = jpar.irs;		ire = jpar.ire;
	nr = ire-irs+1;
	nc = jpar.nc;		// number of components.

// set correct spherical harmonic sizes and grid.
	if (jpar.lmax < LMAX) LMAX = jpar.lmax;
	if (jpar.mmax < MMAX) MMAX = jpar.mmax;
	if (MRES == 0) MRES = jpar.mres;
	if (MMAX*MRES > LMAX) MMAX = LMAX/MRES;
	NLM = nlm_calc(LMAX, MMAX, MRES);
	init_sh_vars(jpar.shtnorm);		// initialize some sh variables
	load_Field_grid(fname);		// load radial grid
	init_Deriv_sph(r, NR);			// compute derivatives

	SH = make_Spectral(nc, irs, ire);
// allocate memory for field average:
	SH_avg = make_Spectral(nc, irs, ire);
	SH_avg->zero_out();
	SH_avg->bci = (boundary) jpar.BCi;
	SH_avg->bco = (boundary) jpar.BCo;

// prepare arrays for spectra.
	double *Elr, *Emr, *Elri, *Emri;
	Elr = (double*) malloc(sizeof(double) * nr * (LMAX+1));
	Emr = (double*) malloc(sizeof(double) * nr * (MMAX+1));
	Elri = (double*) malloc(sizeof(double) * nr * (LMAX+1));
	Emri = (double*) malloc(sizeof(double) * nr * (MMAX+1));
	// zero out accumulator
	for (int j=0; j<nr*(LMAX+1); j++) {
		Elr[j] = 0.0;
	}
	for (int j=0; j<nr*(MMAX+1); j++) {
		Emr[j] = 0.0;
	}

// load files, compute and accumulate spectrum
	printf(" => averaging %d files...\n", (iend-istart)/istep+1);
	count = 0;		missing = 0;
	SpectralDiags sd(LMAX,MMAX);
	for (i=istart; i<=iend; i+=istep) {
		sprintf(fname, "%s%04d.%s", fbase, i, fext);
		if (load_FieldInfo(fname, &jpar2)) {
			if ((jpar.irs != jpar2.irs) || (jpar.ire != jpar2.ire)) runerr("fields do not match");
			count ++;
			tmax = jpar2.t;
			printf("[%04d] loading %s (t=%g)\r", count, fname, tmax);		fflush(stdout);
			SH->load(fname, &jpar);
			for (int ir=irs; ir<=ire; ir++) {
				SH->r_spectrum(ir, sd);
				for (int l=0; l<=LMAX; l++) Elri[(ir-irs)*(LMAX+1) + l] = sd.El[l];
				for (int m=0; m<=MMAX; m++) Emri[(ir-irs)*(MMAX+1) + m] = sd.Em[m];
			}
			*SH_avg += *SH;
			for (int j=0; j<nr*(LMAX+1); j++) {
				Elr[j] += Elri[j];
			}
			for (int j=0; j<nr*(MMAX+1); j++) {
				Emr[j] += Emri[j];
			}
			iend_true = i;
		} else {
			printf("!! missing file '%s'\n",fname);		missing++;
		}
	}

	// normalize
	for (int j=0; j<nr*(LMAX+1); j++) {
		Elr[j] /= count;
	}
	for (int j=0; j<nr*(MMAX+1); j++) {
		Emr[j] /= count;
	}
	SH_avg->scale(1.0/count);

	// remove path from fbase (store in current directory)
	char* p = strrchr(fbase, '/');
	if (p)   fbase = p+1;

	// save average field
	sprintf(fname, "%savg_%04d-%04d.%s", fbase, istart, iend_true, fext);
	printf("\n> saving average field to '%s'\n",fname);
	SH_avg->save(fname, tmax-tmin, count);

	// save average spectra
	sprintf(fname, "%sElr_%04d-%04d.%s", fbase, istart, iend_true, fext);
	printf("> saving l-spectrum to '%s'\n",fname);
	fel = fopen(fname,"w");
	sprintf(fname, "%sEmr_%04d-%04d.%s", fbase, istart, iend_true, fext);
	printf("> saving m-spectrum to '%s'\n",fname);
	fem = fopen(fname,"w");
	fprintf(fel,"%% [XSHELLS] Energy spectrum : LMAX=%d. first row is r", LMAX);
	fprintf(fem,"%% [XSHELLS] Energy spectrum : MMAX=%d, MRES=%d. first row is r", MMAX, MRES);
	for (int ir=irs;ir<=ire;ir++) {
		fprintf(fem,"\n%%  ir=%d, r=%f\n%.6g ",ir,r[ir],r[ir]);
		fprintf(fel,"\n%%  ir=%d, r=%f\n%.6g ",ir,r[ir],r[ir]);
		for (int im=0; im<=MMAX; im++)
			fprintf(fem, "%.6g ", Emr[(ir-irs)*(MMAX+1)+im]);
		for (int l=0; l<=LMAX; l++)
			fprintf(fel, "%.6g ", Elr[(ir-irs)*(LMAX+1)+l]);
	}
	fprintf(fel,"\n");	fclose(fel);
	fprintf(fem,"\n");	fclose(fem);

	printf("[xsavg] done averaging %d files (%d files were missing in the series)\n", count, missing);
}
