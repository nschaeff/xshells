/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

///\file xshells_spectral.cpp
/// 3D Fields definition and operations, in spectral space.

#include "xs_array.cpp"

inline void mpi_interval(int& ir0, int& ir1) {
  #ifdef XS_MPI
	if (ir0 < irs_mpi) ir0 = irs_mpi;
	if (ir1 > ire_mpi) ir1 = ire_mpi;
  #endif
}

/// transform the global interval ir0 and ir1 (inclusive) into an interval assigned to a thread.
inline void thread_interval_any(int& i0, int& i1, const int chunk=4)		// 2 divs
{
  #if XS_OMP > 0
	int nth = omp_get_num_threads();
	int ith = omp_get_thread_num();
	int n = (i1 - i0 + 1 + (chunk-1))/chunk;		// howmany chunks are there?
	int i1new = i0-1 + (((ith+1)*n)/nth)*chunk;
	i0 += ((ith*n)/nth)*chunk;
	if (i1new < i1) i1 = i1new;
  #endif
}

/// transform the global interval ir0 and ir1 (inclusive) into an interval assigned to a thread.
inline void thread_interval_rad(int& ir0, int& ir1)
{
  #if XS_OMP == 1
	thread_interval_any(ir0, ir1, 1);
  #endif
}

/// transform the global interval lm0 and lm1 (inclusive) into an interval assigned to a thread.
/// and keep vsize multiples !
inline void thread_interval_lm(int& lm0, int& lm1)
{
  #if XS_OMP == 2
	thread_interval_any(lm0, lm1, CACHE_LINE/sizeof(cplx));		// don't split cache lines.
  #endif
}

/// Bondary condition definition.
enum boundary {
	BC_ZERO=0,				///< the field is zero at the boundary (ie no-slip with zero velocity).
	BC_NO_SLIP=1,			///< no-slip (prescribed velocity).
	BC_FREE_SLIP=2,			///< free-slip (stress-free boundary condition).
	BC_MAGNETIC=3,			///< magnetic field boundary condition for an electrical insulator.
	BC_CURL_FREE_SLIP=4,	///< for the curl of a free-slip field.
	BC_ELLIPTIC=5,			///< elliptic boundary, with radial velocity prescribed by the tangential one.
	BC_FIXED_TEMPERATURE=1,	///< fixed temperature.
	BC_IMPOSED_FLUX=2,		///< imposed flux = radial gradient is zero.
	BC_ROBIN=6,				///< Robin boundary condition: flux depends linearly on temperature.
	BC_NONE=127,			///< no boundary condition (use first order approx, or central condition for r=0)
};

#ifdef XS_MPI
  #ifdef XS_DEBUG
	#define CHECK_R_RANGE( i, ret, msg ) if ((i<ir_bci-1)||(i>ir_bco+1)) { fprintf(stderr,"proc %d, shell %i :: " msg, i_mpi,i); return ret; } \
		if ((unsigned)(i-irs+h0) >= nshell) return ret;
  #else
	// ignore unexisting shells (owned by others) but allow to read/write halo.
	#define CHECK_R_RANGE( i, ret, msg ) if ((unsigned)(i-irs+h0) >= nshell) return ret;
  #endif
#else
  #ifdef XS_DEBUG
	#define CHECK_R_RANGE( i, ret, msg ) if ((unsigned)(i-irs+h0) >= nshell) { fprintf(stderr,"shell %i :: " msg, i); return ret; }
  #else
	#define CHECK_R_RANGE( i, ret, msg )
  #endif
#endif

class Shell2d_base {
  protected:
	double* data;
	int n1_dist, nelem;
	explicit Shell2d_base(shtns_cfg sh) {	// protected constructor to avoid instanciation.
	  #ifndef XS_CUDA
		#ifdef SHT_ALLOW_PADDING
		n1_dist = sh->nlat_padded;
		#else
		n1_dist = sh->nlat;
		#endif
	  #else
		n1_dist = sh->nphi;
	  #endif
		nelem = sh->nspat;
	}
  public:
	double& operator() (int it, int ip) const {
	  #ifndef XS_CUDA
		return data[n1_dist*ip + it];		// standard "cpu" layout
	  #else
		return data[(it>>1)*n1_dist*2 + ip*2 + (it&1)];	// cuFFT "gpu" spatial layout
	  #endif
	}
	operator double*() const { return data; }	// pointer to data is returned via implicit conversion.
	void zero() const {
		memset(data, 0, sizeof(double)*nelem);
	}
};

/// Shell2d : with self-allocated memory
class Shell2d : public Shell2d_base {
  public:
	explicit Shell2d(shtns_cfg sh) : Shell2d_base(sh) {
		data = (double*) shtns_malloc(nelem * sizeof(double));
	  #ifdef XS_DEBUG
		if (data==0) runerr("[Shell2d] allocation error");
	  #endif
  }
  ~Shell2d() {	shtns_free(data);	}
};

/// Shell2d_mbk : with memory obtained from a LinAlloc object.
class Shell2d_mbk : public Shell2d_base {
  public:
	Shell2d_mbk(shtns_cfg sh, LinAlloc& mem) : Shell2d_base(sh) {
		data = mem.alloc_double(nelem);
	  #ifdef XS_DEBUG
		if (data==0) runerr("[Shell2d_mbk] allocation error");
	  #endif
  }
};

/// solid parts : described by their rotation vector.
struct SolidBody {
	int irs, ire;			///< radial boundaries (local).
	int ir_bci, ir_bco;		///< global boundaries.
	int ir_interface;		///< the index of the interface with the fluid (must be ir_bci or ir_bco)
	int freely_rotate = 0;	///< 0: don't rotate freely; 1: rotate freely around z-axis
	double Omega_x, Omega_y, Omega_z;		///< rotation speeds.
	double phi0;			///< keep track of azimutal position.
	double Tb_z;			///< magnetic torque along z-axis.
	double Tu_z;			///< viscous torque along z-axis.
	double Tzo;				///< previous torque (used for 2nd order integration).
	#ifdef XS_ELLIPTIC
		double eps_z;	// axisymmetric ellipticity.
	#endif
	double vol;			///< volume
	double IM;		// inertia moment (divided by rho)
	// associated potential magnetic field :
	int lmax, mmax;			/// truncation for applied potential field
	cplx *PB0lm;	/// spherical harmonic decomposition of externally aplied potential field.
	double *vt_r;		/// spatial velocity field v_theta/r
	double *vp_r;		/// spatial velocity field v_phi/r

	SolidBody();
	~SolidBody();
	void zero_out();
	void free_spatial();
	void invalidate_spatial();
	void calc_spatial();			/// alloc and compute spatial field.
	int spatial_ok() const;

	void set_limits(int i0, int i1);
	void set_external_B(cplx *Pol);
	void set_rotation(double rr, cplx *Tor);
	void set_inertia_moment(double im_rel, double r_ref) {		///<<< set the inertia moment, using a relative value to the one of a sphere of radius r_ref, of same density (1)
		double r2 = r_ref*r_ref;
		IM = im_rel * 8.*M_PI/15. * (r2*r2*r_ref);
		freely_rotate = (IM==0) ? 0 : 1;
	}
};

/// a static spectral vector, ready for SHT. Usefull to add "for free" a background variable vector value.
struct StatSpecVect {
	unsigned nlm;	///< number of non-zero elements
	unsigned short lmax;		///< maximum degree
	unsigned short mmax;		///< maximum order
	unsigned *lm;		///< array of corresponding indices
	cplx *QST;	///< data ready for SHT. QST[ir*nlm*3 + 3*j] is Pol/(r*l(l+1)) ,QST[ir*nlm*3 + 3*j+1] is spheroidal, QST[ir*nlm*3 + 3*j+2] is toroidal.
	unsigned short *li;
	unsigned short *mi;
};

/// a static spectral vector, ready for SHT. Usefull to add "for free" a background variable vector value.
struct StatSpecScal {
	unsigned nlm;	///< number of non-zero elements
	unsigned short lmax;		///< maximum degree
	unsigned short mmax;		///< maximum order
	unsigned *lm;		///< array of corresponding indices
	cplx *TdT;	///< data ready for SHT. TdT[ir*nlm*2 + 2*j] is Scal[ir][lm[j]], TdT[ir*nlm*2 + 2*j+1] is d(Scal)/dr
	unsigned short *li;
	unsigned short *mi;
};

/// allows access in the Esplit array of SpectralDiags.
enum { E_cs, E_ca, Ez_es, Ez_ea, Enz_es, Enz_ea, N_Esplit };

/// a structure recording Spectral diagnostics, used in several methods of the Spectral class.
struct SpectralDiags {
	double Sconv[2];	///< Specral convergence measure (in harmonic degree l and order m)
	double* Esplit;		///< energy split into several symmetries
	double* El;			///< energy spectrum as a function of harmonic degree l [length lmax+1]
	double* Em;			///< energy spectrum as a function of harmonic order m [length mmax+1]
	double* Erl;		///< energy spectrum of the radial component (as a function of harmonic degree l) [length lmax+1]
	double* Etot;		///< total energy (one scalar value)
	int ndbl;			///< total number of double values, for MPI sum reduction

	void clear() {
		#pragma omp simd
		for (int k=0; k<ndbl; k++) Esplit[k] = 0.0;
		Sconv[0] = 0.0;		Sconv[1] = 0.0;
	}
	SpectralDiags(int lmax, int mmax) {
		ndbl = 3*(lmax+1) + mmax+1 + 2*N_Esplit +1;
		Esplit = (double*) malloc( sizeof(double) * ndbl );
		Etot = Esplit + 2*N_Esplit;
		El = Esplit + 2*N_Esplit +1;
		Em = El + lmax+1;
		Erl = Em + mmax+1;		// Erl includes some scratch space.
		clear();
	}
	~SpectralDiags() {
		if (Esplit) ::free(Esplit);
	}
	double energy() {
		return *Etot;
	}
	double energy_nz() {
		return Esplit[Enz_es]+Esplit[Enz_ea]+Esplit[N_Esplit+Enz_es]+Esplit[N_Esplit+Enz_ea];
	}
	double lEl(int lmax) {		///< the sum of the spectrum multiplied by degree l
		double lel = 0.0;
		for (int l=1; l<=lmax; l++)		lel += l*El[l];
		return lel;
	}
	double lErl(int lmax) {		///< the sum of the spectrum of radial component multiplied by degree l
		double lel = 0.0;
		for (int l=1; l<=lmax; l++)		lel += l*Erl[l];
		return lel;
	}
	void reduce(const SpectralDiags& sd, const double f = 1.0) {
		#pragma omp simd
		for (int k=0; k<ndbl; k++) Esplit[k] += f * sd.Esplit[k];
		for (int k=0; k<2; k++)
			if (Sconv[k] < sd.Sconv[k]) Sconv[k] = sd.Sconv[k];
	}
  #ifdef XS_MPI
	void reduce_mpi() {
		MPI_Allreduce( MPI_IN_PLACE, Esplit, ndbl, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
		MPI_Allreduce( MPI_IN_PLACE, Sconv, 2, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD );
	}
  #endif
	void get_lmax_mmax(int& lmax, int& mmax, const double eps = 1e-25) {
		double* Elm[2] = {El, Em};
		int imax[2] = {lmax, mmax};

		for (int k=0; k<2; k++) {		// loop over l and m spectra
			double emax = 0;
			double* Ej = Elm[k];
			int	jmax = imax[k];
			for (int j=0; j<=jmax; j++) {		// find max of spectrum
				if (Ej[j] > emax) emax = Ej[j];
			}
			emax *= eps;
			for (int j=0; j<=jmax; j++) {		// find last index of significant energy.
				if (Ej[j] > emax) imax[k] = j;
			}
		}
		lmax = imax[0];		mmax = imax[1];
	}
};

/// Common spectral field info.
class Spectral {
  public:
	int irs, ire;			///< radial boundaries (local).
	boundary bci, bco;		///< inner and outer boundary conditions.
	int ir_bci, ir_bco;			///< absolute (global) boundaries.
	#ifdef XS_MPI
	MPI_Win win;				///< an MPI window associated with the shared memory.
	char share_dn, share_up;	///< share boundary with other processes
	#endif

  protected:
	#ifdef XS_MPI
	unsigned char h0, h1;		///< ghost or halo for lower and upper layers.
	#else
	static const int h0 = 1;	///< ghost is always 1 without MPI (for boundary conditions).
	static const int h1 = 1;	///<
	#endif
	xs_array2d<cplx> data;		///< the actual data is stored in an xs_array2d object.
	unsigned ncomp, nshell, nelem;
  public:
	#ifdef XS_MPI
	unsigned char halo;		///< required halo.
	#else
	static const unsigned halo = 1;		///< required halo.
	#endif
  protected:
	char allocated;			///< indicate an allocated field, even though there may be no data on some processes.
	#ifdef XS_MPI
	char sharing_mem;		///< indicate if shared memory is used for this field.
	#endif

	void init_layout(int nc, int istart, int iend, int h=1, int shmem=1);
	int map_file(class SpectralFile& F);

  public:
	Spectral();
	Spectral(int ncomp, int nelem, int istart, int iend, int h=1);
	~Spectral();
	void alloc(int ncomp, int nelem, int istart, int iend, int h=1, int shmem=0);
	void free_field();
	int get_ncomp() const { return ncomp; }
	int is_unallocated() const {  return (allocated == 0);  }

	Spectral & operator*=(const double val);		///< multiply by a real value.
	Spectral & operator+=(const Spectral &rhs);		///< add other field
	Spectral & operator-=(const Spectral &rhs);		///< subtract other field

	cplx* get_data(int ic, int ir) const {
		//return data[ic*nshell + ir-irs+h0];		// shift by halo for ghost shell
		return data[ic + ncomp*(ir-irs+h0)];		// shift by halo for ghost shell, interleaved allocation (legacy compatible).
	}
	xs_array2d<cplx> get_comp(int ic) const {
		xs_array2d<cplx> comp;
		comp.init_from_buffer(data[ic], ncomp*nelem, irs-h0);
		return comp;
	}

	void zero_out_shell(int ir);
	void zero_out();
	void zero_out_comp(unsigned c);
	void copy(const Spectral& src);
	void zero_out_comp_shell(unsigned c, int ir);
	void add_random(double norm, int mmin=0, int mmax=(1<<27), int sym=0);
	void scale(const double s);		///< rescale field by value.

	void filter_scale(double scale);
	void filter_lm(int lmin, int lmax, int mmin, int mmax);
	void filter_parity(int sym);
	void Zrotate(double phi);		///< rotate field by angle phi around the z-axis.

	virtual void load(const char *fn, struct FieldInfo *fh=NULL);	///< read PolTor field from file "fn".
	void save(const char *fn, double time=0.0, int iter=0, int istep=0) const;		///< writes full field data to disk.
	void save_double(const char *fn, int lmax, int mmax, double time=0.0, int iter=0, int istep=0) const;	///< write data in double precision.
	void save_single(const char *fn, int lmax, int mmax, double time=0.0, int iter=0, int istep=0) const;	///< write data in single precision.
	void save_generic(const char *fn, int lmax, int mmax, double time, int iter, int step, const int prec) const;

	double radius(int ir)  const;	///< returns the shell radius (take into account boundaries).
	double delta_r(int ir) const;	///< returns the shell width (take into account boundaries).

	void energy(SpectralDiags& sd, int irstart=-10, int irend=(1<<27)) const;		///< computes energy related diagnostics.
	double rms(int irstart=-10, int irend=(1<<27)) const;			///< computes rms value (accross processes).
	double spectrum(double *El=0, double *Em=0) const;				///< computes spectrum averaged over whole domain.
	void acc_spectrum(int ir, SpectralDiags& sd) const;
	void r_spectrum(int ir, SpectralDiags& sd) const;
	void energy_split(double* Esplit, int irstart=-10, int irend=(1<<27)) const;
	double absmax(int irstart=-10, int irend=(1<<27)) const;		///< compute the maximum of the absolute value of field.

	virtual double r_absmax2(int ir) const {							///< compute the squared maximum of the absolute value of field at given shell
		return -1.0;	// implemented by derived class.
	}
	virtual void _spectrum(int ir, int llim, int imlim, SpectralDiags& sd, const double w=1.0) const {
		return;	// implemented by derived class.
	}

#ifdef XS_MPI
	void sync_mpi();
	void sync1_mpi(int ic, int istart, int iend, int lmstart, int lmend, int tag, MPI_Request **req);
#endif
};


/// Poloidal/Toroidal spherical harmonic decomposition of a field on many radial shells.
class PolTor final : public Spectral {
  public:
	xs_array2d<cplx> Pol;
	xs_array2d<cplx> Tor;
	#if defined(XS_LEGACY) || defined(XS_LINEAR)
	int zero_curl;				///< mark field as beeing curl free.
	#endif

	cplx Prlm(int ir, int l, int im) const;		///< poloidal sh of degree l and order m.
	cplx Trlm(int ir, int l, int im) const;		///< toroidal sh of degree l and order m.
	void set_Prlm(int ir, int l, int m, cplx val);
	void set_Trlm(int ir, int l, int m, cplx val);

	PolTor() { ncomp = 2; }
	PolTor(int istart, int iend, int h=2);
	void alloc(int istart, int iend, int h=2, int shmem=0);
	void clone(const PolTor& PT);

	void curl(int istart=-10, int iend=(1<<27));		///< take curl of field (in place).
	void curl_from_TQS(Spectral* S, int istart, int iend, int sync_tag);			///< take curl of a general field (not PolTor). if sync_tag>=0, do MPI sync with tag sync_tag.

	void RadSph(int ir, cplx *Qlm, cplx *Slm, int lms=0, int lme=NLM-1) const;
	void qst(int ir, int lm, cplx *Q, cplx *S, cplx *T) const;
	void curl_QST(int ir, cplx *Qlm, cplx *Slm, cplx *Tlm, int lms=0, int lme=NLM-1) const;
	void curl_qst(int ir, int lm, cplx *Q, cplx *S, cplx *T) const;
	void RadSph_curl_T(int ir, cplx *Qlm, cplx *Slm, cplx *Tlm, int lms=0, int lme=NLM-1) const;		// Q & S for field, T for its curl
	void curl_QS(int ir, cplx *Qlm, cplx *Slm, int lms=0, int lme=NLM-1) const;


	void _spectrum(int ir, int llim, int imlim, SpectralDiags& sd, const double w=1.0) const override;
	double r_absmax2(int ir) const override;						///< compute the squared maximum of the absolute value of field at given shell
	double r_energy_split(int ir, double* Esplit=0) const;			///< computes energies at given shell, returns total energy and other diagnostics
	double energy_lm(int l, int m, int irstart=-10, int irend=(1<<27)) const;		///< computes energy of single mode (l,m)

	#ifdef XS_LEGACY
	int make_static(struct StatSpecVect **pssv, struct StatSpecVect **pcurl) const;
	#endif

	/* spatial methods */
	void curl_from_spat(class VectField *V);
	void to_spat(class VectField *V) const;
	void to_spat(class VectField *V, int istart, int iend) const;		///< convert to spatial field
	void cte_to_spat(struct StatSpecVect *V0, class VectField *V) const;
	void cte_to_spat(struct StatSpecVect *V0, class VectField *V, int istart, int iend) const;
	void to_curl_spat(class VectField *W, int istart, int iend, double Wz0=0.0) const;
	void cte_to_curl_spat(struct StatSpecVect *W0, class VectField *W, int istart, int iend, double Wz0=0.0) const;

	void special_z_avg(int ns, const double *s, double *buf, const int rstep=1, double rmax=0) const;

	/* rendering methods */
	void to_point(int ir, double cost, double phi,
					double *vr, double *vt, double *vp) const;
	void to_point_interp(double rr, double cost, double phi,
					double *vr, double *vt, double *vp) const;
	void uncurl_to_point_interp(double rr, double cost, double phi,
					double *vr, double *vt, double *vp) const;
	void to_lat(int ir, double cost,
					 double *vr, double *vt, double *vp, int np) const;
	void to_merid(double phi, double *vr, double *vt, double *vp, int irs, int ire) const;		///< render a meridional slice, at longitude phi.
	int to_zdisc(double z0, int nphi, double* s, double* vs, double *vp, double *vz, int ir0, int ir1) const;
	void to_surf(int ir, double *vr, double *vt, double *vp) const;
	void prolongate_field(double rmin, double rmax);		// prolongate field

	/* io methods */
	virtual void load(const char *fn, FieldInfo *fh=0) override;
	void write_box_hdf5(const char *fn, double x0, double y0, double z0, double lx, double ly, double lz, int nx, int ny, int nz, int uncurl=0);
	void write_hdf5(const char *fn, int istart, int iend, int cartesian, int irstep) const;

  private:
	inline void update_ptr() {
		/* interleaved Poloidal/Toroidal components (original layout) */
		Pol.init_from_buffer(data[0], 2*nelem, irs-h0);
		Tor.init_from_buffer(data[1], 2*nelem, irs-h0);
		#if defined(XS_LEGACY) || defined(XS_LINEAR)
		zero_curl = 0;		// mark as not curl-free.
		#endif
	}
};

/// scalar spherical harmonic decomposition of a field on many radial shells.
class ScalarSH final : public Spectral {
  public:
	xs_array2d<cplx> Sca;
	cplx rlm(int ir, int l, int im) const;		///< poloidal sh of degree l and order m.
	void set_rlm(int ir, int l, int m, cplx val);

	ScalarSH() { ncomp = 1; }
	ScalarSH(int istart, int iend, int h=1) {  alloc(istart, iend, h);  }
	void alloc(int istart, int iend, int h=1, int shmem=0);
	void clone(const ScalarSH& S);

	cplx* operator[](int ir) const {
		//CHECK_R_RANGE( ir, 0, "[] attempt to access unexisting shell !\n" )		// DOES NOT WORK AS INTENDED WITH SHARED MEMORY.
		return Sca[ir];
	}

	void Gradr(int ir, cplx *Qlm, cplx *Slm, int lms=0, int lme=NLM-1) const;

	void shell_grad(StatSpecScal *S0lm, int ir, cplx *Q, cplx *S, const int lms, const int lme) const;		///< gradient after adding the grad(S0lm) in spectral Q and S.
	void shell_r_grad(StatSpecScal *S0lm, int ir, cplx *S, double *gt, double *gp) const;					///< lateral gradient after adding grad(S0lm), times r

	void _spectrum(int ir, int llim, int imlim, SpectralDiags& sd, const double w=1.0) const override;

	double r_absmax2(int ir) const override;					///< compute the squared maximum of the absolute value of field at given shell
	double energy_lm(int l, int m, int irstart=-10, int irend=(1<<27)) const;		///< computes energy of single mode (l,m)

	int make_static(struct StatSpecScal **psss) const;

	/* spatial methods */
	void from_spat(class Spatial *V);		///< from scalar spatial field (or first component).
	void from_grad_spat(VectField &V);		///< find scalar potential from spatial gradient.
	void to_spat(class ScalField *S) const;
	void to_spat(class ScalField *S, int istart, int iend) const;
	void cte_to_spat(struct StatSpecScal *S0, ScalField *S) const;
	void cte_to_spat(struct StatSpecScal *S0, ScalField *S, int istart, int iend) const;
	void to_grad_spat(class VectField *V) const;
	void to_grad_spat(class VectField *V, int istart, int iend) const;
	void cte_to_grad_spat(struct StatSpecScal *T0, class VectField *V, int istart, int iend) const;
	
	/* rendering methods */
	double to_point(int ir, double cost, double phi) const;
	double to_point_interp(double rr, double cost, double phi) const;
	void to_lat(int ir, double cost, double *s, int np) const;
	void to_grad_lat(int ir, double cost, 
					double *vr, double *vt, double *vp, int np) const;
	void to_merid(double phi, double *s, int irs, int ire) const;
	int to_zdisc(double z0, int nphi, double* s, double*data, int ir0, int ir1) const;
	void to_grad_merid(double phi, double *vr, double *vt, double *vp, int irs, int ire) const;
	void to_surf(int ir, double *s) const;	

	/* io methods */
	virtual void load(const char *fn, FieldInfo *fh=0) override;
	void write_box_hdf5(const char *fn, double x0, double y0, double z0, double lx, double ly, double lz, int nx, int ny, int nz);
	void write_hdf5(const char *fn, int istart, int iend, int irstep) const;

  private:
	inline void update_ptr() {
		Sca.init_from_buffer(data[0], nelem, irs-h0);
	}
};


#undef LM_LOOP
///\def LM_LOOP : loop avor all lm's and perform "action". only lm is defined, neither l nor m.
#ifndef VAR_LTR
  #define FOR_SCHED_SH static
  #define LM_LOOP( action ) { int lm=0;	do { action } while(++lm < NLM); }
  #define SPAT_SH( Vr, Q ) spat_to_SH(shtns, Vr, (cplx *) Q )
  #define SH_SPAT( Q, Vr ) SH_to_spat( shtns, (cplx *) Q, Vr )
  #define SPAT_SHV( Vt, Vp, S, Tor ) spat_to_SHsphtor( shtns, Vt, Vp, (cplx *) S, (cplx *) Tor )
  #define SHV_SPAT( S, Tor, Vt, Vp ) SHsphtor_to_spat( shtns, (cplx *) S, (cplx *) Tor, Vt, Vp )
  #define SHT_SPAT( Tor, Vt, Vp ) SHtor_to_spat( shtns, (cplx *) Tor, Vt, Vp )
  #define SHS_SPAT( S, Vt, Vp ) SHsph_to_spat( shtns, (cplx *) S, Vt, Vp )
  #define SHV3_SPAT( Q, S, Tor, Vr, Vt, Vp ) SHqst_to_spat( shtns, (cplx *) Q, (cplx *) S, (cplx *) Tor, Vr, Vt, Vp )
  #define SPAT_SHV3( Vr, Vt, Vp, Q, S, Tor ) spat_to_SHqst( shtns, Vr, Vt, Vp, (cplx *) Q, (cplx *) S, (cplx *) Tor )
#else
  #define FOR_SCHED_SH guided,1
  #define LM_LOOP( action ) { int lm=0;	do { if (li[lm]<=LTR) { action } } while(++lm < NLM); }
//  #define LM_LOOP( action ) { int im=0; int lm=0; do { int l=im*MRES;  if (l>LTR) break; do { { action } lm++; } while(++l<=LTR); lm+=(LMAX-LTR); } while(++im <= MMAX); }
  #define SPAT_SH( Vr, Q ) spat_to_SH_l( shtns, Vr, (cplx *) Q, LTR )
  #define SH_SPAT( Q, Vr ) SH_to_spat_l( shtns, (cplx *) Q, Vr, LTR )
  #define SPAT_SHV( Vt, Vp, S, Tor ) spat_to_SHsphtor_l( shtns, Vt, Vp, (cplx *) S, (cplx *) Tor, LTR )
  #define SHV_SPAT( S, Tor, Vt, Vp ) SHsphtor_to_spat_l( shtns, (cplx *) S, (cplx *) Tor, Vt, Vp, LTR )
  #define SHT_SPAT( Tor, Vt, Vp ) SHtor_to_spat_l( shtns, (cplx *) Tor, Vt, Vp, LTR )
  #define SHS_SPAT( S, Vt, Vp ) SHsph_to_spat_l( shtns, (cplx *) S, Vt, Vp, LTR )
  #define SHV3_SPAT( Q, S, Tor, Vr, Vt, Vp ) SHqst_to_spat_l( shtns, (cplx *) Q, (cplx *) S, (cplx *) Tor, Vr, Vt, Vp, LTR )
  #define SPAT_SHV3( Vr, Vt, Vp, Q, S, Tor ) spat_to_SHqst_l( shtns, Vr, Vt, Vp, (cplx *) Q, (cplx *) S, (cplx *) Tor, LTR )
  #define LTR ltr[ir]
#endif

#define SSE __attribute__((aligned (16)))


inline int isvalid_lm( int l, int m) {
  	unsigned im = (unsigned)m / MRES;
	unsigned mmod = (unsigned)m % MRES;
	return (((unsigned)l <= LMAX) && (im <= MMAX) && (mmod == 0));
}

double shell_volume(double r0, double r1) {
	return (4.*M_PI/3.)*(r1*r1*r1 - r0*r0*r0);
}

Spectral* make_Spectral(int nc, int irs, int ire) {
	Spectral* s = 0;
	if (nc == 2)		s = new PolTor(irs, ire);
	else if (nc == 1)	s = new ScalarSH(irs, ire);
	return s;
}

/* SOLIDBODY METHODS */

SolidBody::SolidBody()
{
	vt_r = NULL;	vp_r = NULL;
	irs = 1;		ire = 0;
	ir_bci = 1;		ir_bco = 0;		vol = 0;
	zero_out();
}

SolidBody::~SolidBody()
{
	free_spatial();
}

void SolidBody::free_spatial()
{
	if (vt_r) {
		VFREE(vt_r);	vt_r = NULL;	vp_r = NULL;
	}
}

void SolidBody::invalidate_spatial()
{
	vp_r = NULL;		// this marks the spatial field as invalid.
}

int SolidBody::spatial_ok() const
{
	return (vp_r != NULL);
}

void SolidBody::calc_spatial()
{
	if (irs <= ire) {	// non-zero solid body:
		#pragma omp single
		if (vp_r == NULL) {		// if invalid, recompute.
			cplx* tlm;
			long nspat = shtns->nspat;

			if (vt_r == NULL) {	// if not allocated
				vt_r = (double *) VMALLOC( (2*nspat + 2*(LMAX+2)) * sizeof(double) );
				memset(vt_r, 0, 2*nspat*sizeof(double));	// zero out, so that padding does not mess up.
			}
		  #ifdef XS_DEBUG
			if (vt_r == NULL) runerr("[SolidBody::calc_spatial] VMALLOC error");
		  #endif
			vp_r = vt_r + nspat;
			tlm = (cplx*) (vp_r + nspat);
			tlm[LiM(shtns,1,0)] = Y10_ct * Omega_z;
			if ((MRES==1)&&(MMAX>0)) tlm[LiM(shtns,1,1)] = Y11_st * cplx(Omega_x, -Omega_y);
			SHtor_to_spat_l(shtns, tlm, vt_r, vp_r, 1);
		}
	}
}

void SolidBody::zero_out()
{
	free_spatial();
	Omega_x = 0;	Omega_y = 0;	Omega_z = 0;
	phi0 = 0;
	Tb_z = 0;	Tu_z = 0;	Tzo = 0;
	IM = 0;		// no inertia moment by default
	#ifdef XS_ELLIPTIC
		eps_z = 0;
	#endif
	lmax = -1;	mmax = -1;
	PB0lm = NULL;
}

void SolidBody::set_limits(int i0, int i1)
{
	ir_bci = i0;	ir_bco = i1;
	if (ir_bci==0) i1--;		// exclude fluid boundaries.
	if (ir_bco==NR-1) i0++;
	if (i0 < irs_mpi) i0 = irs_mpi;
	if (i1 > ire_mpi) i1 = ire_mpi;
	irs = i0;		ire = i1;
	vol = shell_volume(r[ir_bci], r[ir_bco]);
	ir_interface = (ir_bci==0) ? ir_bco : ir_bci;
}

/// Copy externaly imposed B to SolidBody, to allow rotating magnetic field.
void SolidBody::set_external_B(cplx *Pol)
{
	cplx* P0;
	int l,im, lm, lmx, mmx;

	if (PB0lm != NULL) runerr("External BC already set");

	lmx = -1;	mmx = -1;
	for (im=0; im<=MMAX; im++) {			// find lmax and mmax for the BC.
		for (l=im*MRES; l<=LMAX; l++) {
			lm = LiM(shtns,l, im);
			if (Pol[lm] != 0.0) {
				if (l > lmx) lmx = l;
				if (im > mmx) mmx = im;
			}
		}
	}

	lmax = lmx;		mmax= mmx;
	if (lmx >= 0) {		// non-zero external field :
		P0 = (cplx*) malloc(sizeof(cplx) * nlm_calc(lmx, mmx, MRES));
		if (P0==0) runerr("[SolidBody::set_external_B] allocation error");
		PB0lm = P0;
		for (im=0; im<=mmx; im++) {
			for (l=im*MRES; l<=lmx; l++) {
				lm = LiM(shtns,l,im);
				*P0++ = Pol[lm];		// store value.
				// the externally imposed field has been multiplied by 2*(l+1) in load_init().
			}
		}
	}
}

/// Copy solid-body rotation of a shell to SolidBody, to impose it.
void SolidBody::set_rotation(double rr, cplx *Tor)
{

	if ((MRES==1)&&(MMAX>0)) {
		int lm = LiM(shtns,1,1);
		Omega_x = real(Tor[lm]) / (rr * Y11_st);
		Omega_y = - imag(Tor[lm]) / (rr * Y11_st);
	}
	Omega_z = real(Tor[LiM(shtns,1,0)]) / (rr * Y10_ct);
}


/* FIELD METHODS */

/// radius of shell
double Spectral::radius(int ir) const
{
	if (ir<ir_bci) ir=ir_bci;		// boundary condition shells have same radius as boundary.
	if (ir>ir_bco) ir=ir_bco;
	return r[ir];
}

/// weight for radial integration
double Spectral::delta_r(int ir) const
{
	int ir_p = ir+1;
	int ir_m = ir-1;
	if (ir <= ir_bci)      ir_m = ir;
	else if (ir >= ir_bco) ir_p = ir;
	return (r[ir_p]-r[ir_m])*0.5;
}

Spectral::Spectral() {
	ncomp = 0;		nshell = 0;		nelem = 0;
	irs = 10;		ire = 0;
	ir_bci = irs;	ir_bco = ire;
	bci = BC_NONE;	bco = BC_NONE;
	allocated = 0;	// marks the field as globally unallocated (or invalid)
	// we can have data == 0 and the field is still allocated on other processes.
	#ifdef XS_MPI
	sharing_mem = 0;		// do not use shared memory for this field by default.
	#endif
}

Spectral::Spectral(int nc, int ne, int istart, int iend, int h) {
	bci = BC_NONE;		bco = BC_NONE;
	allocated = 0;
	alloc(nc, ne, istart, iend, h);
}

/*
void free_shells_deprecated(double **data, int nshell, int h0=0, int h1=0) {
	VFREE(data[0]);
}
*/

void Spectral::free_field() {
	if ((allocated == 1) && (data)) {	// we own the data and should free the memory
		#if defined( XS_MPI ) && MPI_VERSION >= 3
		if (sharing_mem) {	// is this field using shared memory?
			MPI_Win_unlock_all(win);
			MPI_Win_free(&win);
		}
		else
		#endif
		VFREE(data[0]);		// free memory if there is some allocated on this process.
	}
	data.clear();	// mark as unaloccated
	allocated = 0;	// mark field as unallocated
}

Spectral::~Spectral() {
	free_field();
}

#ifdef XS_MPI
/// This will exchange data between processes to have up-to-date ghost shells.
void Spectral::sync_mpi()
{
	MPI_Request req[4];
	int hmax = (h0 > h1) ? h0 : h1;

	if (n_mpi_shared > 1) MPI_Barrier(comm_shared);

	// WARNING! The shells must be treated one by one, because of the allocation scheme.
	// Sync first halo shell, then second one, etc... to allow 1 shell/process.
	for (int j=0; j<hmax; j++) {
		MPI_Request *req_ptr = req;
		if ((share_dn)&&(j<h0)) {
			MPI_Isend(get_data(0, irs+j),   2*nelem*ncomp, MPI_DOUBLE, i_mpi-1, j, MPI_COMM_WORLD, req_ptr++);	// down sending
			MPI_Irecv(get_data(0, irs-1-j), 2*nelem*ncomp, MPI_DOUBLE, i_mpi-1, j, MPI_COMM_WORLD, req_ptr++);	// down receive
		}
		if ((share_up)&&(j<h1)) {
			MPI_Isend(get_data(0, ire-j),   2*nelem*ncomp, MPI_DOUBLE, i_mpi+1, j, MPI_COMM_WORLD, req_ptr++);	// up sending
			MPI_Irecv(get_data(0, ire+1+j), 2*nelem*ncomp, MPI_DOUBLE, i_mpi+1, j, MPI_COMM_WORLD, req_ptr++);	// up receive
		}
		if (req_ptr - req) {
			int ierr = MPI_Waitall(req_ptr - req, req, MPI_STATUSES_IGNORE);
			if (ierr != MPI_SUCCESS) runerr("[Spectral::sync_mpi] MPI_Waitall failed.\n");
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
}

void Spectral::sync1_mpi(int ic, int istart, int iend, int lmstart, int lmend, int tag, MPI_Request **req)
{
	if ((ire < istart) || (irs > iend)) return;		// reject processes not involved.
	int shdn = (irs > istart) && (share_dn);
	int shup = (ire < iend) && (share_up);
	int nelem = lmend-lmstart+1;
	if (shdn) {
		MPI_Isend(get_data(ic,irs) + lmstart, 2*nelem, MPI_DOUBLE, i_mpi-1, tag, MPI_COMM_WORLD, *req);
		MPI_Irecv(get_data(ic,irs-1) + lmstart, 2*nelem, MPI_DOUBLE, i_mpi-1, tag, MPI_COMM_WORLD, *req +1);
		*req += 2;
	}
	if (shup) {
		MPI_Isend(get_data(ic,ire) + lmstart, 2*nelem, MPI_DOUBLE, i_mpi+1, tag, MPI_COMM_WORLD, *req);
		MPI_Irecv(get_data(ic,ire+1) + lmstart, 2*nelem, MPI_DOUBLE, i_mpi+1, tag, MPI_COMM_WORLD, *req +1);
		*req += 2;
	}
}
#endif

void Spectral::copy(const Spectral& src)
{
	if (src.is_unallocated()) runerr("[Spectral::copy] empty field");
	if (is_unallocated()) {
		alloc(src.ncomp, src.nelem, src.ir_bci, src.ir_bco, src.halo);			// alloc if it is not yet allocated.
	} else {
		ir_bci = src.ir_bci;	ir_bco = src.ir_bco;
	}
	if ((ncomp != src.ncomp)||(nshell != src.nshell)||(nelem != src.nelem))
		runerr("[Spectral::copy] field mismatch");
	bci = src.bci;		bco = src.bco;		// copy boundary conditions.

	size_t size = ncomp*nshell*nelem * 2*sizeof(double);
	memcpy(data[0], src.data[0], size);		// copy everything.
}

/*
/// allocate shells.
/// h0 and h1 specify the number of shells (included in nshell) that are lower and upper halo.
void alloc_shells_deprecated(double **data, int ncomp, long ndouble, int nshell, int h0=0,int h1=0)
{
	size_t stride = ncomp*ndouble;		size_t dist = ndouble;		// interleaved allocation.

	// we don't care about halo here
	size_t size = ncomp*nshell*ndouble * sizeof(double);
	int ir = 0;
		data[ir] = (double *) VMALLOC( size );
		if (data[ir] == 0) runerr("[alloc_shells] allocation error 1");
		memset(data[ir], 0, size);		// zero out.
		for (int j=1; j<ncomp; j++) data[j*nshell + ir] = data[ir] + j*dist;
	for (ir = 1; ir < nshell; ir++) {		// shell by shell allocation.
		for (int j=0; j<ncomp; j++) data[j*nshell + ir] = data[j*nshell + ir-1] + stride;
	}
}
*/

void Spectral::init_layout(int nc, int istart, int iend, int h, int shmem)
{
	if (!is_unallocated()) runerr("[Spectral::alloc] already allocated.");
	ir_bci = istart;	ir_bco = iend;		// global boundaries.
	#ifdef XS_MPI
		halo = h;		// halo: number of neighbour shell storage (MPI).
		h0 = 1;		h1 = 1;						// ghost shells (1 by default).
		share_dn = 0;	share_up = 0;
		if (istart < irs_mpi) {
			share_dn = 1;		h0 = h;
			istart = irs_mpi;
			if ((shmem) && (irs_shared < irs_mpi)) {		// no need for ghost shells, they are provided by shared memory.
				share_dn = 0;	h0 = 0;
			}
		}
		if (iend > ire_mpi) {
			share_up = 1;		h1 = h;
			iend = ire_mpi;
			if ((shmem)&&(ire_shared > ire_mpi)) {		// no need for ghost shells, they are provided by shared memory.
				share_up = 0;	h1 = 0;
			}
		}
	#endif
	irs = istart;	ire = iend;		// local boundaries.
	nshell = iend-istart+1 + h0+h1;		// add ghost or halo shells.
	ncomp = nc;
	if (iend >= istart) {
		if (iend-istart+1 < 1) runerr("[Spectral::alloc] at least 1 shells per process required.");
	} else {
		// no field in this process
		if (irs > ir_bco) irs = ir_bco+1;	// for the shared memory view to be consistent.
		#ifdef XS_MPI
		share_dn = 0;	share_up = 0;
		#endif
		nshell = 0;
	}
	allocated = 1;		// mark as allocated
}

/// istart,iend : global boundaries.
void Spectral::alloc(int nc, int ne, int istart, int iend, int h, int shmem)
{
	init_layout(nc, istart, iend, h, shmem);
	const int align = (VSIZE > 8) ? VSIZE/2 : CACHE_LINE/sizeof(cplx);		// align on cache line
	//const int align = 4096/sizeof(cplx);				// align on 4Kb pages.
	ne = ((ne + align-1)/align)*align;					// padding for cache-line adjustement, to avoid false sharing.

//For Intel® Xeon Phi™ processor x200 product family, codenamed Knights Landing, 
//align your matrices on 4096-byte boundaries and set the leading dimension to the following integer expression:
//(((n * element_size + 511) / 512) * 512 + 64) /element_size,

//	ne = ((ne+31)/32) * 32 + 4;		// for KNL

	nelem = ne;
	const size_t sze = sizeof(cplx) * nc*ne*nshell;
	cplx* mem = 0;
	#if defined( XS_MPI ) && MPI_VERSION >= 3
	// even if nshell == 0, the process must participate to a shared allocation here.
	if ((n_mpi_shared > 1) && (shmem == 1)) {
		int disp_unit;
		MPI_Aint ssze;		
		int nshell_all = 0;
		// To workaround an OpenMPI bug, that returns NULL pointer if size==0, we need to perform a scan, allocate on 1 process, and query the baseptr.
		MPI_Scan(&nshell, &nshell_all, 1, MPI_INT, MPI_SUM, comm_shared);		// each process knows the total number of shells in previous processes.
		size_t sze_all = sizeof(cplx) * nc*ne*nshell_all;
		// to ensure proper alignement on cache lines, allocate a bit more memory (sze_all+CACHE_LINE).
		MPI_Win_allocate_shared( (i_mpi_shared==n_mpi_shared-1) ? sze_all+CACHE_LINE : 0, sizeof(cplx), MPI_INFO_NULL, comm_shared, &mem, &win );		// shared memory, should allow faster communications in solver.
		MPI_Win_shared_query(win, n_mpi_shared-1, &ssze, &disp_unit, &mem);
		mem = (cplx*) ((((size_t)(mem)) + (CACHE_LINE-1)) & (~((size_t)(CACHE_LINE-1))));	// align pointer on cache-line
		// compute local memory pointer:
		mem += (nshell_all-nshell)*nc*ne;
		MPI_Win_lock_all(0, win);
		memset(mem, 0, sze);		// zero out 'local' memory
		data.init_from_buffer(mem, ne);
		sharing_mem = 1;			// mark field as using MPI shared memory.
		win_list.push_back(win);
		print_debug("[Spectral::alloc] shared memory window allocated.\n");
	} else
	#endif
	if (sze > 0) {
		mem = (cplx*) VMALLOC( sze );		// when n_mpi_shared == 1
		memset(mem, 0, sze);		// zero out memory
		data.init_from_buffer(mem, ne);
	}
}

void Spectral::zero_out()
{
	if (data) {
		int i0 = 0;		int i1 = nshell-1;
		thread_interval_rad(i0, i1);
		memset(data[i0*ncomp], 0, sizeof(cplx)*nelem*ncomp*(i1-i0+1));		// assumes interleaved components.
		#pragma omp barrier
	}
}

void Spectral::zero_out_comp(unsigned c)
{
	if ((data)&&(c<ncomp)) {
		int i0 = 0;		int i1 = nshell-1;
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			v2d* sh = (v2d*) data[ir*ncomp + c];
			for (long k=nelem; k>0; k--) {
				*sh++ = vdup(0.0);
			}
		}
		#pragma omp barrier
	}
}

void Spectral::zero_out_shell(int ir)
{
	if ( (data) && ((unsigned)(ir-irs+1) < nshell) ) {
		int lm0 = 0;
		int lm1 = nelem*ncomp - 1;
		thread_interval_lm(lm0, lm1);
		v2d* sh = (v2d*) get_data(0, ir);
		for (int lm=lm0; lm<=lm1; lm++) {		// assumes interleaved components.
			sh[lm] = vdup(0.0);
		}
	}
}

void Spectral::zero_out_comp_shell(unsigned c, int ir)
{
	if ( (data) && ((unsigned)(ir-irs+h0) < nshell) && (c<ncomp) ) {
		int lm0 = 0;
		int lm1 = nelem - 1;
		thread_interval_lm(lm0, lm1);
		v2d* sh = (v2d*) get_data(c, ir);
		for (int lm=lm0; lm<=lm1; lm++)	sh[lm] = vdup(0.0);
	}
}

void Spectral::scale(const double scale)
{
	if (scale == 0.0) {
		zero_out();
	}
	else if ( (data) && (scale != 1.0) ) {
		s2d vscale = vdup(scale);
		int i0 = 0;
		int i1 = nshell-1;
		thread_interval_rad(i0, i1);
		int lm0 = 0;
		int lm1 = nelem*ncomp -1;
		thread_interval_lm(lm0, lm1);
		for (int ir=i0; ir<=i1; ir++) {
			v2d* sh = (v2d*) data[ir*ncomp];		// assumes interleaved components.
			for (long lm=lm0; lm<=lm1; lm++) {		// assumes interleaved components.
				sh[lm] *= vscale;
			}
		}
		#pragma omp barrier
	}
}

Spectral & Spectral::operator*=(const double val)
{
	scale(val);
	return *this;
}

Spectral & Spectral::operator+=(const Spectral &rhs)
{
	if ((rhs.ncomp != ncomp)||(rhs.nelem != nelem)) runerr("[+=] field mismatch");
	if ((rhs.irs < irs)||(rhs.ire > ire)) runerr("[+=] radial domain mismatch");

	int i0 = rhs.irs - rhs.h0;
	int i1 = rhs.ire + rhs.h1;
	thread_interval_rad(i0, i1);
	int lm0 = 0;
	int lm1 = nelem*ncomp -1;
	thread_interval_lm(lm0, lm1);
	for(int i=i0; i<=i1; i++) {
		v2d* d1 = (v2d*) get_data(0, i);
		v2d* d2 = (v2d*) rhs.get_data(0, i);
		for (int lm=lm0; lm<=lm1; lm++) {
			d1[lm] += d2[lm];		// assumes components stored together.
		}
	}
	#pragma omp barrier
	return *this;
}

Spectral & Spectral::operator-=(const Spectral &rhs)
{
	if ((rhs.ncomp != ncomp)||(rhs.nelem != nelem)) runerr("[-=] field mismatch");
	if ((rhs.irs < irs)||(rhs.ire > ire)) runerr("[-=] radial domain mismatch");

	int i0 = rhs.irs - rhs.h0;
	int i1 = rhs.ire + rhs.h1;
	thread_interval_rad(i0, i1);
	int lm0 = 0;
	int lm1 = nelem*ncomp -1;
	thread_interval_lm(lm0, lm1);
	for(int i=i0; i<=i1; i++) {
		v2d* d1 = (v2d*) get_data(0, i);
		v2d* d2 = (v2d*) rhs.get_data(0, i);
		for (int lm=lm0; lm<=lm1; lm++) {
			d1[lm] -= d2[lm];		// assumes components stored together.
		}
	}
	#pragma omp barrier
	return *this;
}

/// low-pass filter of 3D field at spatial scale "scale".
void Spectral::filter_scale(double scale)
{
	int ir0, ir1;
	xs_array2d_mem<cplx> Dlm(ncomp*nshell, nelem, (irs_mpi-h0)*ncomp);	// temporary array to hold source data.
	const double gwidth = 3.0;		// the width for gaussian filtering.

	if (scale == 0.0) return;		// nothing to do.
	scale = 1.0/scale;				// spatial frequency.
	ir0 = ir_bci;		ir1 = ir_bco;

	for (int i=irs_mpi-h0; i<=ire_mpi+h1; i++) {	// copy everything, shell by shell.
		for (int k=0; k<ncomp; k++)
			memcpy(Dlm[i*ncomp+k], get_data(k,i), nelem*sizeof(cplx));
	}
	zero_out();	// clear original

	double* dr2 = new double[NR];
	dr2[ir0] = 0.5*(r[ir0+1]-r[ir0]);
	dr2[ir1] = 0.5*(r[ir1]-r[ir1-1]);
	for (int ir=ir0+1; ir< ir1; ir++) dr2[ir] = 0.5*(r[ir+1]-r[ir-1]);		// for integration.

	cplx* buf = new cplx[nelem*ncomp];		// a buffer for 1 shell
// radial "quasi-gaussian" filter + spectral SH filter.
	for (int ir=ir0+1; ir<ir1; ir++) {		// exclude boundaries
		double r0 = r[ir];
		double norm = 0.0;

		for (long k=0; k<nelem*ncomp; k++)  buf[k] = 0.;		// start with zeros
		for (int jr=ir0; jr<=ir1; jr++) {		// convolution over all shells
			double  x2 = (r[jr] - r0)*scale;
			x2 *= x2;		// x2;
			//	this is a truncated gaussian filter
			if ( (x2 < 3.2) ) {
				double f = exp(-gwidth*x2)*dr2[ir];
				norm += f;			// the norm is computed on all processes.
				if (own(jr)) {		// for owned shells, compute the local contribution in buf:
					for (int k=0; k<ncomp; k++) 
						for (int lm=0; lm<nelem; lm++)
							buf[k*nelem + lm] += Dlm[jr*ncomp+k][lm] * f;
				}
			}
		}
		#ifdef XS_MPI
			// sum the contributions from all processes.
			MPI_Reduce(own(ir) ? MPI_IN_PLACE : buf, buf, nelem*ncomp*2, MPI_DOUBLE, MPI_SUM, r_owner[ir], MPI_COMM_WORLD);
		#endif
		if (own(ir)) {
			double lc = M_PI * r0 * scale;
			for (int lm=0; lm<NLM; lm++) {
				double f = exp( -l2[lm]/(gwidth*lc*lc) )/norm;
				for (int k=0; k<ncomp; k++)	get_data(k,ir)[lm] = buf[k*nelem + lm] * f;
			}
		}
	}
	delete[] buf;
	for (int ir=ir0; ir<=ir1;  ir+=(ir1-ir0)) {		// boundaries : no convolution (preserve boundary conditions)
		double lc = M_PI * r[ir] * scale;
		if (own(ir) && (r[ir] != 0.0)) {		// avoid NaN.
			for (int lm=0; lm<NLM; lm++) {
				double f = exp( -l2[lm]/(gwidth*lc*lc) );
				for (int k=0; k<ncomp; k++) get_data(k,ir)[lm] = f * Dlm[ir*ncomp+k][lm];
			}
		}	// else = 0
	}
	delete[] dr2;
}

/// apply (l,m) filter
void Spectral::filter_lm(int lmin, int lmax, int mmin, int mmax)
{
	int ir, im, m, l, lm;

	if (lmax > LMAX) lmax = LMAX;	if (mmax > MMAX) mmax = MMAX;

	for (ir=0; ir < ncomp * nshell; ir++) {		// filter all data, including ghost shells...
		for (im=0; im<mmin; im++) {
			m = im*MRES;
			for(l=m; l<=LMAX; l++) {
				lm = LM(shtns,l,m);	data[ir][lm] = 0.0;
			}
		}
		for (im=mmin; im<=mmax; im++) {
			m = im*MRES;
			for(l=m; l<lmin; l++) {
				lm = LM(shtns,l,m);	data[ir][lm] = 0.0;
			}
			for(l=lmax+1; l<=LMAX; l++) {
				lm = LM(shtns,l,m);	data[ir][lm] = 0.0;
			}
		}
		for (im=mmax+1; im<=MMAX; im++) {
			m = im*MRES;
			for(l=m; l<=LMAX; l++) {
				lm = LM(shtns,l,m);	data[ir][lm] = 0.0;
			}
		}
	}
}

/// keep only symmetric or anti-symmetric part (with respect to the equator).
/// sym = 1 : anti-symmetric (odd). sym = 0 : symmetric (even)
void Spectral::filter_parity(int sym)
{
	int ir, im, m, l, lm;

	sym &= 1;		// parity (0 or 1)
	for (ir=0; ir<nshell; ir++) {
		for (im=0; im<=MMAX; im++) {
			m = im*MRES;
			for(l=m+1-sym; l<=LMAX; l+=2) {		// scalar or poloidal
				lm = LM(shtns,l,m);	data[ir*ncomp][lm] = 0.0;
			}
			if (ncomp > 1) {
				for(l=m+sym; l<=LMAX; l+=2) {		// toroidal
					lm = LM(shtns,l,m);	data[ir*ncomp+1][lm] = 0.0;
				}
			}
		}
	}
}

/// rotate a spherical harmonic field along the z-axis by 'phi' radians (rotate reference frame by angle -phi).
void Spectral::Zrotate(double phi)
{
	cplx shift;
	int i,im,l,m;

	for (i=0; i<ncomp*nshell; i++) {		// loop over all shells (including ghost shells).
		for (im=0; im<=MMAX; im++) {		// loop over all m's
			m=im*MRES;
			shift = cplx( cos(m*phi),  - sin(m*phi) );		// rotate reference frame by angle -phi
			for (l=m;l<=LMAX; l++) {
				data[i][LiM(shtns,l,im)] *= shift;
			}
		}
	}
}

/// Add random data to field.
/// select a symmetry : sym=0 : no symmetry;  sym=1 : odd;  sym=2 : even
void Spectral::add_random(double norm, int mmin, int mmax, int sym)
{
	long int pshift = 0;
	long int tshift = 0;
	long int linc = 1;
	
	if (mmin<0) mmin=0;
	if (mmax>MMAX) mmax=MMAX;
	if (sym > 0) {
		linc=2;
		if (sym == 1) pshift=1;
		if (sym == 2) tshift=1;
	}
	norm *= 1.0/RAND_MAX;

	/// random field.
	int i0 = irs;
	int i1 = ire;
	if (i0 == ir_bci) i0++;		// exclude boundaries.
	if (i1 == ir_bco) i1--;
	for (int i=i0; i<=i1; i++) {
		cplx* d = get_data(0, i);
		for (int im=mmin;im<=mmax;im++) {		// non-zero initial value for mmin<= m <= mmax
			int lmax = LMAX;
			#ifdef VAR_LTR
				lmax = ltr[i];		// stop at ltr.
			#endif
			for (int l=im*MRES+pshift; l<=lmax; l+=linc) {		// first component is poloidal or scalar
				int lm = LiM(shtns,l,im);
				double clm = norm * el[lm]*l_2[lm];		// for a scalar
				if (ncomp == 2)
					clm *= r[i]*delta_r(i) * l_2[lm]*l_2[lm]*l_2[lm];		// for poloidal scalar
				d[lm] += clm * cplx( (rand()-RAND_MAX/2) , (rand()-RAND_MAX/2) );
				if (im==0) d[lm] = d[lm].real();		// keep m=0 real
			}
			if (ncomp == 2) {
				cplx* d2 = get_data(1, i);
				for (int l=im*MRES+tshift; l<=lmax; l+=linc) {	// second component is toroidal
					int lm = LiM(shtns,l,im);
					double clm = norm*delta_r(i) * ((im+1.)/(mmax+1))*el[lm]*l_2[lm]*l_2[lm];
					d2[lm] += clm * cplx( (rand()-RAND_MAX/2) , (rand()-RAND_MAX/2) );
					if (im==0) d2[lm] = d2[lm].real();		// keep m=0 real
				}
			}
		}
	}
}

/* POLTOR METHODS */

PolTor::PolTor(int istart, int iend, int h) {
	alloc(istart, iend, h);
}

/// Allocate memory for a Spectral representation of a field.
/// This will reset the Boundary Conditions to NONE.
void PolTor::alloc(int istart, int iend, int h, int shmem)
{
	Spectral::alloc(2, NLM, istart, iend, h, shmem);
	update_ptr();
}

void PolTor::clone(const PolTor& PT)
{
	alloc(PT.ir_bci, PT.ir_bco, PT.halo);
	bci = PT.bci;		bco = PT.bco;		// copy boundary conditions.
	update_ptr();
}

void PolTor::load(const char *fn, FieldInfo *fh)
{
	Spectral::load(fn, fh);
	if (ncomp != 2) runerr("[load] expected 2 components");
	update_ptr();
}

void ScalarSH::alloc(int istart, int iend, int h, int shmem)
{
	Spectral::alloc(1, NLM, istart, iend, h, shmem);
	update_ptr();
}

void ScalarSH::clone(const ScalarSH& S)
{
	alloc(S.ir_bci, S.ir_bco);
	bci = S.bci;		bco = S.bco;		// copy boundary conditions.
}

void ScalarSH::load(const char *fn, FieldInfo *fh)
{
	Spectral::load(fn, fh);
	if (ncomp != 1) runerr("[load] expected 1 component");
	update_ptr();
}


double ScalarSH::energy_lm(int l, int m, int irstart, int irend) const
{
	double Elm = 0.0;
	
	if (irstart >= irend) return 0.0;
	if (irstart < irs) irstart = irs;
	if (irend > ire)   irend = ire;

	if (isvalid_lm(l,m)) {
		int lm = LM(shtns,l,m);
		for (int ir = irstart; ir <= irend; ir++) {
			Elm += norm(Sca[ir][lm]) * r[ir]*r[ir] * delta_r(ir);
		}
		if (m==0) Elm *= 0.5;
	}
	return Elm;
}


double PolTor::energy_lm(int l, int m, int irstart, int irend) const
{
	cplx Qlm, Slm;
	double Elm = 0.0;
	
	if (irstart >= irend) return 0.0;
	if (irstart < irs) irstart = irs;
	if (irend > ire)   irend = ire;

	if (isvalid_lm(l,m)) {
		int lm = LM(shtns,l,m);
		for (int ir = irstart; ir <= irend; ir++) {
			RadSph(ir, &Qlm - lm, &Slm - lm, lm, lm);
			double er = norm(Qlm) + l2[l]*(norm(Slm) + norm(Tor[ir][lm]));
			Elm += er * r[ir]*r[ir] * delta_r(ir);
		}
		if (m==0) Elm *= 0.5;
	}
	return Elm;
}

double coenergy_lrange(cplx* Q1, cplx* Q2, int lm0, int lm1)
{
	double Er = 0.0;
	double Ei = 0.0;
	for (long l=lm0; l<=lm1; l++) {
		Er += real(Q1[l])*real(Q2[l]);
		Ei += imag(Q1[l])*imag(Q2[l]);
	}
	return Er+Ei;
}

double coenergy(cplx* Q1, cplx* Q2)
{
	return 0.5*coenergy_lrange(Q1, Q2, 0, LMAX) 
			 + coenergy_lrange(Q1, Q2, LMAX+1, NLM-1);
}

/// use fac_S2=-1 to change the sign of S2, computing cross-product co-energy (when exchanging S2 and T2 and with fac_S2=-1)
double coenergy(cplx* S1, cplx* T1,  cplx* S2, cplx* T2, const double fac_S2=1.0)
{
	s2d Es = vdup(0.);
	s2d Et = vdup(0.);
	for (int im=0; im<=MMAX; im++) {
		int lm = LiM(shtns,im*MRES,im);
		for (int l=im*MRES; l<=LMAX; l++) {
			#ifndef XS_VEC
			Es += l2[l]*(real(S1[lm])*real(S2[lm]) + imag(S1[lm])*imag(S2[lm]));
			Et += l2[l]*(real(T1[lm])*real(T2[lm]) + imag(T1[lm])*imag(T2[lm]));
			#else
			Es += vdup(l2[l]) * ((v2d*) S1)[lm] * ((v2d*) S2)[lm];
			Et += vdup(l2[l]) * ((v2d*) T1)[lm] * ((v2d*) T2)[lm];
			#endif
			lm++;
		}
		if (im == 0) {		// factor for energy (1/2)
			Es *= vdup(0.5);	Et *= vdup(0.5);
		}
	}
	return VSUM(Et + vdup(fac_S2)*Es);
}

double coenergy(cplx* Q1, cplx* S1, cplx* T1,  cplx* Q2, cplx* S2, cplx* T2)
{
	return coenergy(Q1, Q2)  +  coenergy(S1,T1,  S2,T2);
}

double energy(cplx* Q)
{
	return coenergy(Q, Q);
}

double energy(cplx* Q, cplx* S, cplx* T)
{
	return coenergy(Q,S,T,  Q,S,T);
}

double coenergy(const ScalarSH& S1, const ScalarSH& S2)
{
	int irs = S1.irs;
	int ire = S1.ire;
	if (S2.irs > irs) irs = S2.irs;
	if (S2.ire < ire) ire = S2.ire;

	double E = 0;
	#if XS_OMP == 1
	#pragma omp parallel for schedule(static) reduction(+ : E)
	#endif
	for (int ir=irs; ir <= ire; ir++) {
		double Er = coenergy(S1[ir], S2[ir]);
		E += Er * r[ir]*r[ir] * S1.delta_r(ir);
	}
	return E;
}

double coenergy(const PolTor& V1, const PolTor& V2)
{
	int irs = V1.irs;
	int ire = V1.ire;
	if (V2.irs > irs) irs = V2.irs;
	if (V2.ire < ire) ire = V2.ire;

	double E = 0;
	#if XS_OMP == 1
	#pragma omp parallel for schedule(static) reduction(+ : E)
	#endif
	for (int ir=irs; ir <= ire; ir++) {
		cplx *Q1 = (cplx*) malloc(4*NLM*sizeof(cplx));
		#ifdef XS_DEBUG
		if (Q1==0) runerr("[coenergy] allocation error");
		#endif
		cplx *Q2 = Q1 + 2*NLM;
		cplx *S1 = Q1 + NLM;			cplx *S2 = Q2 + NLM;
		V1.RadSph(ir, Q1, S1);
		V2.RadSph(ir, Q2, S2);
		double Er = coenergy(Q1,S1,V1.Tor[ir], Q2,S2,V2.Tor[ir]);
		free(Q1);
		E += Er * r[ir]*r[ir] * V1.delta_r(ir);
	}
	return E;
}


/** compute the energy of several components of the field.
 Esplit must be an array of 8 doubles that will be filled with:
 4 doubles of zonal energy (Poloidal symmetric, Poloidal anti-symmetric, Toroidal symmetric, Toroidal anti-symmetric),
 followed by 4 doubles of the corresponding non-zonal energies.
 [even indices are symmetric components and odd indices are anti-symmetric components].
 MPI: The result is local to each process, and should be summed using MPI_Reduce()
 TODO: remove, as this is now deprecated (for backward compatibility only).
*/
void Spectral::energy_split(double* Esplit, int irstart, int irend) const
{
	for (int k=0; k<8; k++) Esplit[k] = 0.0;

	if (irstart >= irend) return;
	if (irstart < irs) irstart = irs;
	if (irend > ire)   irend = ire;

	#if XS_OMP == 1
	#pragma omp parallel firstprivate(irstart, irend)
	#endif
	{
		SpectralDiags sd(LMAX,MMAX);
		thread_interval_rad(irstart, irend);
		for (int ir = irstart; ir <= irend; ir++) {
			acc_spectrum(ir, sd);
		}
		#pragma omp critical
		{
			Esplit[0] += sd.Esplit[Ez_es];
			Esplit[1] += sd.Esplit[Ez_ea];
			Esplit[2] += sd.Esplit[Ez_es + N_Esplit];
			Esplit[3] += sd.Esplit[Ez_ea + N_Esplit];
			Esplit[4] += sd.Esplit[Enz_es];
			Esplit[5] += sd.Esplit[Enz_ea];
			Esplit[6] += sd.Esplit[Enz_es + N_Esplit];
			Esplit[7] += sd.Esplit[Enz_ea + N_Esplit];
		}
	}
	return;
}

static int sconv_lmin = 3;
static int sconv_mmin = 3;

/// compute spectral convergence, which is a value between 0 [perfectly converged] and 1 [not converged].
double spectral_convergence(double* El, int nmin, int nmax, const int nend=4)
{
	double Sconv = 0.0;
	if (nmax >= (nmin + 2*nend)) {	// Sconv meaningful if the spectrum is large enough.
		double E0 = 0.0;
		for (int l=nmin; l<=nmax; l++) if (E0 < El[l]) E0=El[l];		// max of spectrum
		double E1 = 0.0;
		for (int l=nmax-nend+1; l<=nmax; l++) if (E1 < El[l]) E1=El[l];		// max of end of spectrum
		if (E0 > 0.0)	Sconv = E1/E0;
	}
	return Sconv;
}

// for internal use
void PolTor::_spectrum(int ir, int llim, int imlim, SpectralDiags& sd, const double w) const
{
	cplx *Q, *S, *T;
	double *const El = sd.El;		///< l-sepctrum
	double *const Em = sd.Em;		///< m-spectrum
	double *const Erl = sd.Erl;		///< l-spectrum of radial component (poloidal)
	double *const Esplit = sd.Esplit;
	const double *const l2_ = l2;	/// avoid to always reload

	const long cl = CACHE_LINE/sizeof(cplx);
	long n_alloc = ((llim+cl)/cl)*cl;		// round to cache line
	Q = (cplx*) VMALLOC(2*n_alloc*sizeof(cplx));
	S = Q + n_alloc;
	T = Tor[ir];

	double Er = 0.0;
  #if VSIZE < 4
	double pre = 0.5 * w;		// factor for energy (1/2 r2*dr)
	for (int im=0; im<=imlim; im++) {
		int m = im*MRES;
		long lm = LiM(shtns,m,im);
		RadSph(ir, Q-lm, S-lm, lm, lm+llim-m);
		double ep0 = 0.0;
		double ep1 = 0.0;
		double et0 = 0.0;
		double et1 = 0.0;
		int l=m;
		for (; l<llim; l+=2) {
			double v2r0 = norm(Q[l-m]);
			double v2r1 = norm(Q[l-m+1]);
			double v2p0 = v2r0 + l2_[lm]*norm(S[l-m]);			// l2[lm] was used in RadSph, we reuse it here (cached).
			double v2p1 = v2r1 + l2_[lm+1]*norm(S[l-m+1]);
			double v2t0 = l2_[lm]*norm(T[lm]);
			double v2t1 = l2_[lm+1]*norm(T[lm+1]);
			ep0 += v2p0;		et0 += v2t0;
			ep1 += v2p1;		et1 += v2t1;
			lm+=2;
			El[l]    += pre*(v2p0+v2t0);
			El[l+1]  += pre*(v2p1+v2t1);
			Erl[l]   += pre*v2r0;
			Erl[l+1] += pre*v2r1;
		}
		if (l==llim) {
			double v2r0 = norm(Q[l-m]);
			double v2p0 = v2r0 + l2_[lm]*norm(S[l-m]);
			double v2t0 = l2_[lm]*norm(T[lm]);
			ep0 += v2p0;		et0 += v2t0;
			El[l]  += pre*(v2p0+v2t0);
			Erl[l] += pre*v2r0;
		}
		ep0 *= pre;		et0 *= pre;
		ep1 *= pre;		et1 *= pre;
		double em_ = ep0+et1+ep1+et0;
		Er += em_;
		Em[im] = em_;
		pre = w;		// for m>0, we count it twice (+m and -m)
		int ofs = (im & MRES) & 1;	// parity of im*MRES.
		Esplit[ofs]   += ep0;		// pol center sym/asym (m)
		Esplit[1-ofs] += ep1;		// pol center sym/asym (m)
		Esplit[6+ofs] += et1;		// tor center sym/asym (m)
		Esplit[7-ofs] += et0;		// tor center sym/asym (m)
		ofs = 0;	if (im > 0) ofs = 2;
		Esplit[2+ofs] += ep0;		// pol equatorially symmetric
		Esplit[3+ofs] += ep1;		// pol equatorially anti-sym
		Esplit[8+ofs] += et1;		// tor equatorially symmetric
		Esplit[9+ofs] += et0;		// tor equatorially anti-sym
	}
  #else
	v4d pre = vall4(0.5 * w);		// factor for energy (1/2 r2*dr)
	v4d E_m_sym[3];
	for (int j=0; j<3; j++) E_m_sym[j] = vall4(0.0);
	for (int im=0; im<=imlim; im++) {
		int m = im*MRES;
		long lm = LiM(shtns,m,im);
		RadSph(ir, Q-lm, S-lm, lm, lm+llim-m);
		v4d ep01 = vall4(0.0);
		v4d et01 = vall4(0.0);
		int l=m;
		for (; l<llim; l+=2) {
			v4d vl2 = vread_dup4( l2_+lm );			// l2[lm] was used in RadSph, we reuse it here (cached).
			v4d v2r = vread4(Q+l-m, 0);	v2r *= v2r;
			v4d v2p = vread4(S+l-m, 0);	v2p *= v2p;
			v4d v2t = vread4(T+lm, 0);	v2t *= v2t;
			v2p = v2p * vl2 + v2r;
			v2t = v2t * vl2;
			ep01 += v2p;		et01 += v2t;
			lm+=2;
			v2p += v2t;
			v4d acc = vread4(Erl+2*l, 0) + pre*vhadd4(v2p, v2r);    // don't use HADD on KNL !!
			vstore4(Erl+2*l, 0, acc);
		}
		if (l==llim) {
			v2d vl2 = vdup(l2_[lm]);
			v2d v2r = *(v2d*)(Q+l-m);	v2r *= v2r;
			v2d v2p = *(v2d*)(S+l-m);	v2p *= v2p;
			v2d v2t = *(v2d*)(T+lm);	v2t *= v2t;
			v2p = v2p * vl2 + v2r;
			v2t = v2t * vl2;
			ep01 += (v4d)_mm256_castpd128_pd256( v2p );
			et01 += (v4d)_mm256_castpd128_pd256( v2t );
			v2p += v2t;
			v2d acc = vread2(Erl+2*l, 0) + (v2d)_mm256_castpd256_pd128(pre)*vhadd2(v2p, v2r);
			vstore2(Erl+2*l, 0, acc);
		}
		// ep0r ep0i ep1r ep1i
		// et0r et0i et1r et1i
		int ofs = (im & 1) + (im > 0);		// im=0 => ofs=0;  im even => ofs=1;  im odd => ofs=2
		ep01 = pre * vhadd4(ep01, et01);		// ep0, et0, ep1, et1
		double em_ = reduce_add4(ep01);
		E_m_sym[ofs] += ep01;
		Em[im] = em_;
		Er += em_;
		pre = vall4(w);		// for m>0, we count it twice (+m and -m)
	}
	const int ofs = MRES & 1;		// parity of even im.
	Esplit[2] = E_m_sym[0][0];		// zonal, pol, equatorially symmetric
	Esplit[3] = E_m_sym[0][2];		// zonal, pol, equatorially anti-sym
	Esplit[8] = E_m_sym[0][3];		// zonal, tor, equatorially symmetric
	Esplit[9] = E_m_sym[0][1];		// zonal, tor, equatorially anti-sym

	Esplit[4] = E_m_sym[1][0] + E_m_sym[2][0];		// non-zonal, pol, equatorially symmetric
	Esplit[5] = E_m_sym[1][2] + E_m_sym[2][2];		// non-zonal, pol, equatorially anti-sym
	Esplit[11] = E_m_sym[1][1] + E_m_sym[2][1];		// non-zonal, tor, equatorially anti-sym
	Esplit[10] = E_m_sym[1][3] + E_m_sym[2][3];		// non-zonal, tor, equatorially symmetric

	Esplit[0] = E_m_sym[1][0] + E_m_sym[0][0];		// pol center sym (m)
	Esplit[7] = E_m_sym[1][1] + E_m_sym[0][1];		// tor center asym (m)
	Esplit[1] = E_m_sym[1][2] + E_m_sym[0][2];		// pol center asym (m)
	Esplit[6] = E_m_sym[1][3] + E_m_sym[0][3];		// tor center sym (m)

	Esplit[0+ofs] += E_m_sym[2][0];		// pol center sym/asym (m)
	Esplit[1-ofs] += E_m_sym[2][2];		// pol center sym/asym (m)
	Esplit[6+ofs] += E_m_sym[2][3];		// tor center sym/asym (m)	
	Esplit[7-ofs] += E_m_sym[2][1];		// tor center sym/asym (m)

	for (int l=0; l<=llim; l++) {		// unpack l-spectra:
		El[l] = Erl[2*l];
		Erl[l] = Erl[2*l+1];
	}
  #endif
	VFREE(Q);
	*sd.Etot = Er;
}

// for internal use
void ScalarSH::_spectrum(int ir, int llim, int imlim, SpectralDiags& sd, const double w) const
{
	double Er, pre;
	int l, im;
	double *const El = sd.El;
	double *const Em = sd.Em;
	double *const Esplit = sd.Esplit;

	const cplx* Q = Sca[ir];

	Er = 0.0;
	pre = 0.5 * w;		// factor for energy (1/2 r2*dr)
	for (im=0; im<=imlim; im++) {
		double ep0 = 0.0;
		double ep1 = 0.0;
		int lm = LiM(shtns,im*MRES,im);
		for (l=im*MRES; l<llim; l+=2) {
			double v2p0 = pre * norm(Q[lm]);
			double v2p1 = pre * norm(Q[lm+1]);
			ep0 += v2p0;
			ep1 += v2p1;
			lm+=2;
			El[l]   += v2p0;
			El[l+1] += v2p1;
		}
		if (l==llim) {
			double v2p0 = pre * norm(Q[lm]);
			ep0 += v2p0;
			El[l] += v2p0;
		}
		double em_ = ep0+ep1;
		Er += em_;
		Em[im] = em_;
		pre = w;		// for m>0, we count it twice (+m and -m)
		int ofs = (im & MRES) & 1;	// parity of im*MRES
		Esplit[ofs]   += ep0;		// center symmetric/asym (m)
		Esplit[1-ofs] += ep1;		// center symmetric/asym (m)
		ofs = 0;	if (im > 0) ofs = 2;
		Esplit[2+ofs] += ep0;		// equatorially symmetric
		Esplit[3+ofs] += ep1;		// equatorially anti-sym
	}
	*sd.Etot = Er;
}

/// Accumulate spectra.
void Spectral::acc_spectrum(int ir, SpectralDiags& sd) const
{
	if ((ir < irs)||(ir > ire)) return;

	double r2dr;
	int llim, imlim;
	SpectralDiags sdr(LMAX,MMAX);

	imlim = MMAX;
	#ifndef VAR_LTR
		llim = LMAX;
	#else
		llim = ltr[ir];
		if (imlim*MRES > llim) imlim = llim/MRES;
	#endif

	r2dr = r[ir]*r[ir]*delta_r(ir);
	_spectrum(ir, llim, imlim, sdr, r2dr);

	// compute Sconv
	sdr.Sconv[0] = spectral_convergence(sdr.El, sconv_lmin, llim);
	sdr.Sconv[1] = spectral_convergence(sdr.Em, sconv_mmin, imlim);
/*	if ((sdr.Sconv[0] > 0.01) || (sdr.Sconv[1] > 0.01)) {		// DEBUG: print spectra when convergence is bad
		printf("ir=%d,llim=%d,imlim=%d, Sconv-l,m=%g, %g \n  l-spec:", ir,llim,imlim, sdr.Sconv[0], sdr.Sconv[1]);
		for (int l=0;l<=llim;l++) printf(" %.2g", sdr.El[l]);
		printf("\n  m-spec:");
		for (int m=0;m<=imlim;m++) printf(" %.2g ", sdr.Em[m]);
		printf("\n");
	}
*/
	sd.reduce(sdr);
}

void Spectral::r_spectrum(int ir, SpectralDiags& sd) const
{
	sd.clear();	
	if ((ir < irs)||(ir > ire)) return;

	int imlim = MMAX;
	int llim = LMAX;
	#ifdef VAR_LTR
		llim = ltr[ir];
		if (imlim*MRES > llim) imlim = llim/MRES;
	#endif

	_spectrum(ir, llim, imlim, sd);

	sd.Sconv[0] = spectral_convergence(sd.El, sconv_lmin, llim);
	sd.Sconv[1] = spectral_convergence(sd.Em, sconv_mmin, imlim);
}


/** compute the energy of the spatial field described by its spectral components.
 For \b orthonormal spherical harmonics :
 for a Scalar field:
 \f[ e(r) = \frac{1}{2} \sum_{l,m} \, (2 - \delta_{m0}) |S_l^m|^2 \f]
 for a PolTor field:
 \f[ e(r) = \frac{1}{2} \sum_{l,m} \, (2 - \delta_{m0}) \left(\frac{l(l+1)}{r} |P_l^m| \right)^2 + l(l+1) (|S_l^m|^2 + |T_l^m|^2) \f]
 and \f[ E = \int_{r0}^{r1} e(r) r^2 dr \f]
 MPI: The result is local to each process, and should be summed using MPI_Reduce()
*/
void Spectral::energy(SpectralDiags& sd, int irstart, int irend) const
{
	sd.clear();

	if (irstart >= irend) return;
	if (irstart < irs) irstart = irs;
	if (irend > ire)   irend = ire;

	#if XS_OMP == 1
	#pragma omp parallel firstprivate(irstart, irend)
	#endif
	{
		thread_interval_rad(irstart, irend);
		SpectralDiags sdr(LMAX,MMAX);
		for (int ir = irstart; ir <= irend; ir++) {
			acc_spectrum(ir, sdr);
		}
		#pragma omp critical
		{
			sd.reduce(sdr);
		}
	}
}

/// return the rms value of a field.
/// MPI: The result is summed and each process has the right answer
double Spectral::rms(int irstart, int irend) const
{
	double nrj, vol;

	if (irstart >= irend) return 0.0;	
	if (irstart < ir_bci) irstart = irs;
	if (irend > ir_bco)   irend = ire;

	SpectralDiags sdr(LMAX,MMAX);
	energy(sdr, irstart, irend);
	nrj = sdr.energy();
	#ifdef XS_MPI
		double nrj2;
		MPI_Allreduce(&nrj, &nrj2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
		nrj = nrj2;			// all ranks have the correct nrj here.
	#endif
	vol = shell_volume(r[irstart], r[irend]);
	return sqrt(2.0*nrj / vol);
}

/// deprecated, for backward compatibility TODO: remove
double Spectral::spectrum(double *El, double *Em) const
{
	SpectralDiags sd(LMAX,MMAX);
	energy(sd);
	if (El) for (int l=0; l<=LMAX; l++) El[l] = sd.El[l];
	if (Em) for (int m=0; m<=MMAX; m++) Em[m] = sd.Em[m];
	return sd.energy();
}


/*
// accurate energy ? (by linear interpolation of field)
// (s-r)*(
//    a2* (s-r)^2 * (6*s^2 + 3*r*s +r^2)/30
//  + b2* (s*s + r*s + r*r)/3
//  + ab* (s-r)*(3*s^2 + 2*r*s + r^2)/6 )
double PolTor::energy(int irstart, int irend) const
{
	double E = 0.0;
	v2d *Q0, *Q1, *S0, *S1, *mem;

	if (irstart >= irend) return 0.0;	
	if (irstart < irs) irstart = irs;
	if (irend > ire)   irend = ire;
	
	mem = (v2d*) VMALLOC(sizeof(v2d)*4*NLM);
	Q0 = mem;	Q1 = Q0 + NLM;		S0 = Q0 + 2*NLM;		S1 = Q0 + 3*NLM;
	
	RadSph(irstart, (cplx*) Q0, (cplx*) S0);

	for (int ir = irstart; ir < irend; ir++) {
		RadSph(ir+1, (cplx*) Q1, (cplx*) S1);
		double dx = r[ir+1]-r[ir];
		double rs = r[ir]*r[ir+1];
		double r2 = r[ir]*r[ir];
		double s2 = r[ir+1]*r[ir+1];
		s2d c_aa = vdup( (6*s2 + 3*rs + r2)/30 );
		s2d c_bb = vdup( (s2 + rs + r2)/3 );
		s2d c_ab = vdup( (3*s2 +2*rs + r2)/6 );
		s2d der = vdup(0.0);
		for (int lm=0; lm<NLM; ++lm) {
			v2d b = Q0[lm];
			v2d a = Q1[lm]-Q0[lm];
			if (lm <= LMAX)
				der += vdup(0.5)*(a*a*c_aa  + a*b*c_ab  + b*b*c_bb);
			else 
				der += (a*a*c_aa  + a*b*c_ab  + b*b*c_bb);
		}
		for (int lm=0; lm<NLM; ++lm) {
			v2d sb = S0[lm];
			v2d sa = S1[lm]-S0[lm];
			v2d tb = ((v2d**)Tor)[ir][lm];
			v2d ta = ((v2d**)Tor)[ir+1][lm] - tb;
			if (lm <= LMAX)
				der += vdup(0.5)*((sa*sa + ta*ta)*c_aa  + (sa*sb + ta*tb)*c_ab  + (sb*sb + tb*tb)*c_bb) * vdup(l2[lm]);
			else
				der += ((sa*sa + ta*ta)*c_aa  + (sa*sb + ta*tb)*c_ab  + (sb*sb + tb*tb)*c_bb) * vdup(l2[lm]);
		}
		E += dx*VSUM(der);
		v2d *qx = Q0;	Q0 = Q1;	Q1 = qx;		// exchange buffers.
		v2d *sx = S0;	S0 = S1;	S1 = sx;
	}
	VFREE(mem);
	return E;
}
*/


/* SCALARSH METHODS */

/// compute radial derivative of scalar (handle boundary conditions)
/// Qlm = dT/dr;  Slm = T/r
void ScalarSH::Gradr(int ir, cplx *Qlm, cplx *Slm, int lms, int lme) const
{
	v2d *Q, *S, *Sr, *Si, *Sg;

	#ifdef XS_DEBUG
		if UNLIKELY(lms > lme) runerr("[Gradr] nothing to do");
		if UNLIKELY((ir<irs)||(ir>ire))  runerr("[Gradr] out of radial boundaries !");
	#endif

	Q = (v2d*) Qlm;		Sr = (v2d*) Sca[ir];		S = (v2d*) Slm;
	if LIKELY(IN_RANGE_INCLUSIVE(ir, ir_bci+1, ir_bco-1)) {
		OpGrad& G = GRAD(ir);
	  #ifdef XS_VEC_UNSAFE
		//	DO NOT USE: mem-mapped files are not rounded to cache-line, so this vectorized code may segfault.
		rnd Gl = vall(G.Gl);		rnd Gd = vall(G.Gd);	rnd Gu = vall(G.Gu);
		rnd ri_1 = vall(G.r_1);
		v2d* Sl = (v2d*) Sca[ir-1];		v2d* Su = (v2d*) Sca[ir+1];		// MPI: assumes halo is up to date.
		for (int lm=lms; lm<lme; lm+=VSIZE/2) {		// overflow allowed (rounded to cache-line or vector).
			rnd s = vread(Sr+lm, 0);
			rnd q = Gl * vread(Sl+lm, 0) + Gd * s + Gu*vread(Su+lm, 0);
			s *= ri_1;
			vstore(S+lm, 0, s);
			vstore(Q+lm, 0, q);
		}
	  #else
		// radial derivative
		s2d Gl = vdup(G.Gl);	s2d Gd = vdup(G.Gd);	s2d Gu = vdup(G.Gu);
		s2d ri_1 = vdup(G.r_1);
		v2d* Sl = (v2d*) Sca[ir-1];		v2d* Su = (v2d*) Sca[ir+1];		// MPI: assumes halo is up to date.
		LM_LOOP2(  lms, lme,
			Q[lm] = Gl*Sl[lm] + Gd*Sr[lm] + Gu*Su[lm];
			S[lm] = Sr[lm]*ri_1;
		)
	  #endif
	} else {	// handle boundary conditions:
		int BC, ii, ig;

		BC = bco;	ii = ir-1;	ig = ir+1;		// values if ir==ir_bco
		if (ir==ir_bci) {
			BC = bci;	ii = ir+1;	ig = ir-1;
			if (r[ir] == 0.0) {
				s2d dr_1 = vdup(r_1[ii]);
				LM_LOOP2( lms, lme,
					v2d s = vdup(0.);
					if (li[lm] == 1) s = ((v2d*) Sca[ii])[lm] * dr_1;		// vector: l=1 only
					Q[lm] = s;		S[lm] = s;
				)
				return;
			}
		}
		Sg = (v2d*) Sca[ig];		Si = (v2d*) Sca[ii];
		s2d dr_1 = vdup(1.0/(r[ii]-r[ir]));
		s2d ri_1 = vdup(r_1[ir]);
		switch(BC) {
			case BC_ZERO :
				LM_LOOP2( lms, lme,  Q[lm] = Si[lm]*dr_1; 	S[lm] = vdup(0.0); )
				break;
			case BC_IMPOSED_FLUX :
				LM_LOOP2( lms, lme,  Q[lm] = Sg[lm];		S[lm] = Sr[lm]*ri_1; )
				break;
			default:		// also BC_FIXED_TEMPERATURE
				LM_LOOP2( lms, lme,  Q[lm] = (Si[lm] - Sr[lm])*dr_1; 	S[lm] = Sr[lm]*ri_1; )		// order 1 approx of gradient
		}
	}
}

/// returns the gradient after adding the grad(S0lm) in spectral Q and S.
void ScalarSH::shell_grad(StatSpecScal *S0lm, int ir, cplx *Q, cplx *S, const int lms, const int lme) const
{
	Gradr(ir, Q, S, lms, lme);
	if (S0lm) {
		int nj = S0lm->nlm;
		unsigned* lma = S0lm->lm;
		cplx* TdT = S0lm->TdT + ir*nj*2;	//QST[ir*nlm*2 + 2*j]
		for (int j=0; j<nj; j++) {		// Add Background Spectral
			int lm = lma[j];
			#ifdef XS_SPARSE
			if (lm>0)		// lm=0 is already in the sparse matrix part.
			#endif
			if ((lm>=lms) && (lm<=lme)) {
				S[lm] += TdT[j*2]*r_1[ir];		Q[lm] += TdT[j*2 +1];
			}
		}
	}
}

/// returns the lateral gradient after adding grad(S0lm), times r
void ScalarSH::shell_r_grad(StatSpecScal *S0lm, int ir, cplx *S, double *gt, double *gp) const
{
	cplx *tlm = Sca[ir];
	if (S0lm) {
		LM_LOOP(  S[lm] = tlm[lm];  )		// we need a copy here
		int nj = S0lm->nlm;
		unsigned* lma = S0lm->lm;
		cplx* TdT = S0lm->TdT + ir*nj*2;	//QST[ir*nlm*2 + 2*j]
		for (int j=0; j<nj; j++) {		// Add Background Spectral
			int lm = lma[j];
			S[lm] += TdT[j*2];
		}
		tlm = S;
	}
	SHS_SPAT(tlm, gt, gp);
}

/// set a spherical harmonic coefficient, with range checking.
inline void set_Ylm(cplx *ylm, int l, int m, cplx cval)
{
	if ((ylm == NULL)||(!isvalid_lm(l, m))) {
		runerr("attempt to set inexistant spherical harmonic coefficient");
	}
	ylm[LM(shtns,l,m)] = cval;
}

/// read a spherical harmonic coefficient, with range checking.
inline cplx get_Ylm(cplx *ylm, int l, int m)
{
	if ((ylm == NULL)||(!isvalid_lm(l, m))) {
		fprintf(stderr,"WARNING: attempt to read unexisting mode.\n");
		return 0.0;
	}
	return ylm[LM(shtns,l,m)];
}



inline cplx ScalarSH::rlm(int ir, int l, int m) const
{
	CHECK_R_RANGE( ir, 0, "WARNING: attempt to read unexisting shell.\n" )
	return get_Ylm(Sca[ir],l,m);
}

inline void ScalarSH::set_rlm(int ir, int l, int m, cplx val) {
	CHECK_R_RANGE( ir, , "WARNING: attempt to write unexisting shell.\n" )
	set_Ylm(Sca[ir], l, m, val);
}


inline cplx PolTor::Prlm(int ir, int l, int m) const
{
	CHECK_R_RANGE( ir, 0, "WARNING: attempt to read unexisting shell.\n" )
	return get_Ylm(Pol[ir],l,m);
}
inline cplx PolTor::Trlm(int ir, int l, int m) const
{
	CHECK_R_RANGE( ir, 0, "WARNING: attempt to read unexisting shell.\n" )
	return get_Ylm(Tor[ir],l,m);
}

inline void PolTor::set_Prlm(int ir, int l, int m, cplx val) {
	CHECK_R_RANGE( ir, , "WARNING: attempt to write unexisting shell.\n" )
	set_Ylm(Pol[ir], l, m, val);
}
inline void PolTor::set_Trlm(int ir, int l, int m, cplx val) {
	CHECK_R_RANGE( ir, , "WARNING: attempt to write unexisting shell.\n" )
	set_Ylm(Tor[ir], l, m, val);
}

/// vector vr, vt, vp is converted to cartesian coordinates.
void spher_to_cart(double ct,double p,double *vr,double *vt,double *vp)
{
	double st, cp,sp;
	double vx,vy,vz,vs;

	st = sqrt((1.0-ct)*(1.0+ct));	cp = cos(p);	sp = sin(p);
	vz = *vr*ct - *vt*st;		vs = *vr*st + *vt*ct;
	vx = vs*cp - *vp*sp;		vy = vs*sp + *vp*cp;
	*vr = vx;	*vt = vy;	*vp = vz;
}

inline
void _curlP(int ir, const double* Mu, const xs_array2d<cplx> &Pol, cplx*__restrict Q, cplx*__restrict S, int lms, int lme)
{
	const cplx* Pl = Pol[ir-1];		const cplx* Pr = Pol[ir];		const cplx* Pu = Pol[ir+1];
	const double* l2_ = l2;
	const unsigned short* li_ = li;
	int lm=lms;
  #if VSIZE >= 16
	v4d vWl = vall4(Mu[0]);	v4d vWd = vall4(Mu[1]);	v4d vWu = vall4(Mu[2]);		v4d vr_1 = vall4(Mu[3]);
	s2d Wl = v2d_lo4(vWl);		s2d Wd = v2d_lo4(vWd);		s2d Wu = v2d_lo4(vWu);		s2d ri_1 = v2d_lo4(vr_1);
	for (; lm<lme; lm+=2) {		// overflow allowed (aligned on cache line)
		int la = li_[lm];	int lb = li_[lm+1];
		v4d l2_dup = vdup_x2(l2_+la, l2_+lb);
		//v4d l2_dup = vread_dup4(l2_+lm);
		v4d Pd0 = vread4(Pr+lm, 0);
		v4d Pl0 = vread4(Pl+lm, 0);		v4d Pu0 = vread4(Pu+lm, 0);
		v4d Q0 = Pd0*vr_1 * l2_dup;
		v4d S0 = vWl*Pl0 + vWd*Pd0 + vWu*Pu0;
		vstore4(Q+lm, 0, Q0);
		vstore4(S+lm, 0, S0);
	}
  #else
 	s2d Wl = vdup(Mu[0]);		s2d Wd = vdup(Mu[1]);		s2d Wu = vdup(Mu[2]);		s2d ri_1 = vdup(Mu[3]);
  #endif
	for (; lm<=lme; lm++) {
		v2d q = vdup(l2_[li_[lm]]) * ri_1 * ((v2d*)Pr)[lm];
		//v2d q = vdup(l2_[lm]) * ri_1 * ((v2d*)Pr)[lm];
		v2d s = Wl*((v2d*)Pl)[lm] + Wd*((v2d*)Pr)[lm] + Wu*((v2d*)Pu)[lm];
		((v2d*)Q)[lm] = q;		((v2d*)S)[lm] = s;
	}
}

inline
void _laplP(int ir, const double* Mw, const double* l2_, const xs_array2d<cplx> &Pol, cplx*__restrict T, int lms, int lme)
{
	const cplx* Pl = Pol[ir-1];		const cplx* Pr = Pol[ir];		const cplx* Pu = Pol[ir+1];
	const unsigned short* li_ = li;
	int lm=lms;
  #if VSIZE >= 16
	v4d vWl = vall4(Mw[0]);	v4d vWd = vall4(Mw[1]);	v4d vWu = vall4(Mw[2]);		v4d vr_2 = vall4(Mw[3]);
	s2d Wl = v2d_lo4(vWl);	s2d Wd = v2d_lo4(vWd);	s2d Wu = v2d_lo4(vWu);		s2d ri_2 = v2d_lo4(vr_2);
	for (; lm<lme; lm+=2) {
		int la = li_[lm];	int lb = li_[lm+1];
		v4d l2_r2 = vr_2 * vdup_x2(l2_+la, l2_+lb)  - vWd;
		v4d Pl0 = vread4(Pl+lm, 0);		v4d Pd0 = vread4(Pr+lm, 0);		v4d Pu0 = vread4(Pu+lm, 0);
		v4d T0 = l2_r2*Pd0 - vWl*Pl0 - vWu*Pu0;
		vstore4(T+lm, 0, T0);
	}
  #else
	s2d Wl = vdup(Mw[0]);		s2d Wd = vdup(Mw[1]);		s2d Wu = vdup(Mw[2]);		s2d ri_2 = vdup(Mw[3]);
  #endif
	for (; lm<=lme; lm++) {
		((v2d*)T)[lm] = (ri_2*vdup(l2_[li_[lm]]) - Wd)*((v2d*)Pr)[lm] - Wl*((v2d*)Pl)[lm] - Wu*((v2d*)Pu)[lm];
	}
}

inline
void _curl_laplP(int ir, const double* Mu, const xs_array2d<cplx> &Pol, cplx*__restrict Qu, cplx*__restrict Su, cplx*__restrict Tw, int lms, int lme)
{
	const cplx* Pl = Pol[ir-1];		const cplx* Pr = Pol[ir];		const cplx* Pu = Pol[ir+1];
	const double* l2_ = l2;
	const unsigned short* li_ = li;
	int lm=lms;
  #if VSIZE >= 16
	v4d vWl = vall4(Mu[0]);	v4d vWd = vall4(Mu[1]);	v4d vWu = vall4(Mu[2]);		v4d vr_1 = vall4(Mu[3]);
	v4d vLl = vall4(Mu[4]);	v4d vLd = vall4(Mu[5]);	v4d vLu = vall4(Mu[6]);		v4d vr_2 = vall4(Mu[7]);
	s2d Wl = v2d_lo4(vWl);	s2d Wd = v2d_lo4(vWd);	s2d Wu = v2d_lo4(vWu);		s2d ri_1 = v2d_lo4(vr_1);
	s2d Ll = v2d_lo4(vLl);	s2d Ld = v2d_lo4(vLd);	s2d Lu = v2d_lo4(vLu);		s2d ri_2 = v2d_lo4(vr_2);
	for (; lm<lme; lm+=2) {
		int la = li_[lm];	int lb = li_[lm+1];
		v4d l2_dup = vdup_x2(l2_+la, l2_+lb);
		v4d Pl0 = vread4(Pl+lm, 0);		v4d Pd0 = vread4(Pr+lm, 0);		v4d Pu0 = vread4(Pu+lm, 0);
		v4d Q0 = Pd0*vr_1*l2_dup;
		v4d T0 = (vr_2*l2_dup-vLd)*Pd0 - vLl*Pl0 - vLu*Pu0;
		v4d S0 = vWl*Pl0 + vWd*Pd0 + vWu*Pu0;
		vstore4(Qu+lm, 0, Q0);
		vstore4(Tw+lm, 0, T0);
		vstore4(Su+lm, 0, S0);
	}
  #else
	s2d Wl = vdup(Mu[0]);		s2d Wd = vdup(Mu[1]);		s2d Wu = vdup(Mu[2]);		s2d ri_1 = vdup(Mu[3]);
	s2d Ll = vdup(Mu[4]);		s2d Ld = vdup(Mu[5]);		s2d Lu = vdup(Mu[6]);		s2d ri_2 = vdup(Mu[7]);
  #endif
	for (; lm<=lme; lm++) {
		s2d l2_dup = vdup(l2_[li_[lm]]);
		v2d q = ri_1*l2_dup * ((v2d*)Pr)[lm];
		v2d t = (ri_2*l2_dup - Ld)*((v2d*)Pr)[lm] - Ll*((v2d*)Pl)[lm] - Lu*((v2d*)Pu)[lm];
		v2d s = Wl*((v2d*)Pl)[lm] + Wd*((v2d*)Pr)[lm] + Wu*((v2d*)Pu)[lm];
		((v2d*)Qu)[lm] = q;		((v2d*)Su)[lm] = s;		((v2d*)Tw)[lm] = t;
	}
}


/* BOUNDARY AWARE Q,S,T FUNCTIONS */

/// requires lms <= lme
void PolTor::RadSph(int ir, cplx *Qlm, cplx *Slm, int lms, int lme) const		// Q & S
{
	v2d *Q, *S, *Pr, *Pi, *Pg;
	const double* l2_ = l2;

	#ifdef XS_DEBUG
		if UNLIKELY(lms > lme) runerr("[RadSph] nothing to do");
		if UNLIKELY((ir<irs)||(ir>ire))  runerr("[RadSph] out of radial boundaries !");
	#endif

	Q = (v2d*) Qlm;		S = (v2d*) Slm;		Pr = (v2d*) Pol[ir];
	if LIKELY(IN_RANGE_INCLUSIVE(ir, ir_bci+1, ir_bco-1)) {			// MPI: assumes halo is up to date.
		/*
		// Solenoidal deduced from radial derivative of Poloidal
		OpCurlLapl& WL = CURL_LAPL(ir);
	  #ifdef XS_VEC_UNSAFE
		//	DO NOT USE: mem-mapped files are not rounded to cache-line, so this vectorized code may segfault.
		rnd Wl = vall(WL.Gl);	rnd Wd = vall(WL.Gd);	rnd Wu = vall(WL.Gu);
		rnd ri_1 = vall(WL.r_1);
		v2d* Pl = (v2d*) Pol[ir-1];		v2d* Pu = (v2d*) Pol[ir+1];
		for (int lm=lms; lm<lme; lm+=VSIZE/2) {		// overflow allowed (rounded to cache-line or vector).
			rnd vl2 = vread_dup(l2_ + lm);
			rnd pr = vread(Pr+lm, 0);
			rnd q = pr * vl2*ri_1;
			rnd s = Wl * vread(Pl+lm, 0) + Wd * pr + Wu*vread(Pu+lm, 0);
			vstore(Q+lm, 0, q);
			vstore(S+lm, 0, s);
		}
	  #else
		s2d Wl = vdup(WL.Wl);	s2d Wd = vdup(WL.Wd);	s2d Wu = vdup(WL.Wu);
		s2d ri_1 = vdup(WL.r_1);
		v2d* Pl = (v2d*) Pol[ir-1];		v2d* Pu = (v2d*) Pol[ir+1];
		LM_LOOP2(  lms, lme,
			v2d q = ri_1*vdup(l2_[lm]) * Pr[lm];
			v2d s = Wl*Pl[lm] + Wd*Pr[lm] + Wu*Pu[lm];
			Q[lm] = q;		S[lm] = s;
		)
	  #endif
		*/
		_curlP(ir, &CURL_LAPL(ir).Wl, Pol, Qlm, Slm, lms, lme);
	} else {	// handle boundary conditions:
		int BC, ii, ig;
		BC = bco;	ii = ir-1;	ig = ir+1;		// values if ir==ir_bco
		if (ir==ir_bci) {
			BC = bci;	ii = ir+1;	ig = ir-1;
			if (r[ir] == 0.0) {	// field at r=0 : Pol=0, Tor=0, but S = 2.dP/dr (l=1 only) and Q=S
				const unsigned short* li_ = li;
				s2d dr_1 = vdup(r_1[ii]);
				LM_LOOP2( lms, lme,
					v2d s = vdup(0.);
					if (li_[lm] == 1) s = ((v2d*) Pol[ii])[lm] * (dr_1+dr_1);
					S[lm] = s;		Q[lm] = s;
				)
				return;
			}
		}
		s2d ri_1 = vdup(r_1[ir]);
		LM_LOOP2(  lms, lme,  Q[lm] = ri_1*vdup(l2_[lm]) * Pr[lm];  )
		s2d dr_1 = vdup(1.0/(r[ii]-r[ir]));
		Pg = (v2d*) Pol[ig];		Pi = (v2d*) Pol[ii];
		switch(BC) {
			case BC_ZERO :
				LM_LOOP2( lms, lme,  S[lm] = vdup(0.0);  Q[lm] = vdup(0.0); )		// force zero velocity
				break;
			case BC_NO_SLIP :	// Velocity field (no slip => Pol,Tor,dP/dr prescribed)
				LM_LOOP2( lms, lme,  S[lm] = ri_1*Pr[lm] + Pg[lm];  )	// Pol/r + dP/dr  [dP/dr stored at ig]
				break;
		/*	case BC_FREE_SLIP :	// Velocity field (free slip BC) : Pol = 0, dT/dr = Tor/r, S = dP/dr
				LM_LOOP2( lms, lme,  S[lm] = dr_1 * Pi[lm];  )
				break;	*/
			case BC_MAGNETIC :	// Magnetic field (insulator BC) : Tor=0, dP/dr = l/r.Pol - (2l+1)/r.Pext => S = (l+1)Pol/r - (2l+1)/r.Pext
				if (ir==ir_bci) {
					LM_LOOP2( lms, lme,  S[lm] = ri_1 * (vdup(el[lm])*Pr[lm] + Pr[lm] - Pg[lm]);  )	// [(2l+1).Pext stored at ig]
				} else {	// ir==ir_bco
					LM_LOOP2( lms, lme,  S[lm] = ri_1 * (Pg[lm] - vdup(el[lm])*Pr[lm]);  )	// [(2l+1).Pext stored at ig]
				}
				break;
			case BC_CURL_FREE_SLIP :	// dP/dr = Pol/r => S = 2P/r
				LM_LOOP2( lms, lme,  S[lm] = (ri_1+ri_1) * Pr[lm];  )
				break;
			case BC_FREE_SLIP :
			case BC_ELLIPTIC :	// Pol imposed, d2P/dr2 deduced from stress-free condition.
			{	s2d dx = vdup(r[ii]-r[ir])*ri_1*ri_1;
				LM_LOOP2( lms, lme,
					S[lm] = (ri_1 - dr_1 - dx + dx*vdup(0.5*l2_[lm])) * Pr[lm] + dr_1 * Pi[lm];  )	// don't assume Pol=0
				break;
			}
			default :	// use order 1 approx for dP/dr
				LM_LOOP2( lms, lme,  S[lm] = (ri_1 - dr_1) * Pr[lm] + dr_1 * Pi[lm];  )		// don't assume Pol=0
		}
	}
}

/// Computes the Q,S,Tor coefficients of a PolTor field, including proper boundary conditions.
void PolTor::qst(int ir, int lm, cplx *Q, cplx *S, cplx *T) const
{
	RadSph(ir, Q-lm, S-lm, lm, lm);
	*T = Tor[ir][lm];
}

/// requires lms <= lme
void PolTor::RadSph_curl_T(int ir, cplx *Qlm, cplx *Slm, cplx *Tlm, int lms, int lme) const		// Q & S for field, T for its curl
{
	v2d *Q, *S, *T, *Pr, *Pi, *Pg;
	const double* l2_ = l2;

	#ifdef XS_DEBUG
		if UNLIKELY(lms > lme) runerr("[RadSph] nothing to do");
		if UNLIKELY((ir<irs)||(ir>ire))  runerr("[RadSph] out of radial boundaries !");
	#endif

	Q = (v2d*) Qlm;		S = (v2d*) Slm;		T = (v2d*) Tlm;		Pr = (v2d*) Pol[ir];
	if LIKELY(IN_RANGE_INCLUSIVE(ir, ir_bci+1, ir_bco-1)) {			// MPI: assumes halo is up to date.
		_curl_laplP(ir, &CURL_LAPL(ir).Wl, Pol, Qlm, Slm, Tlm, lms, lme);
	} else {	// handle boundary conditions:
		int BC, ii, ig;

		BC = bco;	ii = ir-1;	ig = ir+1;		// values if ir==ir_bco
		if (ir==ir_bci) {
			BC = bci;	ii = ir+1;	ig = ir-1;
			if (r[ir] == 0.0) {	// field at r=0 : Pol=0, Tor=0, but S = 2.dP/dr (l=1 only) and Q=S
				const unsigned short* li_ = li;
				s2d dr_1 = vdup(r_1[ii]);
				LM_LOOP2( lms, lme,
					v2d s = vdup(0.);
					T[lm] = s;
					if (li_[lm] == 1) s = ((v2d*) Pol[ii])[lm] * (dr_1+dr_1);
					S[lm] = s;		Q[lm] = s;
				)
				return;
			}
		}
		s2d ri_1 = vdup(r_1[ir]);
		LM_LOOP2(  lms, lme,  Q[lm] = ri_1*vdup(l2[lm]) * Pr[lm];  )
		s2d dr_1 = vdup(1.0/(r[ii]-r[ir]));
		Pg = (v2d*) Pol[ig];		Pi = (v2d*) Pol[ii];
		switch(BC) {
			case BC_ZERO :
				LM_LOOP2( lms, lme,  S[lm] = vdup(0.0);  Q[lm] = vdup(0.0); )		// force zero velocity
			{	s2d dr_2 = dr_1*dr_1;		dr_2 += dr_2;		// 2/dr2
				v2d* Pir = (v2d*) Pol[ir];		v2d* Pig = (v2d*) Pol[ig];		v2d* Pii = (v2d*) Pol[ii];
				LM_LOOP2( lms, lme,  T[lm] = (dr_2 + vdup(l2[lm])*ri_1*ri_1) * Pir[lm]
											- dr_2 * Pii[lm]
											+ vdup(2.)*(dr_1-ri_1) * Pig[lm];  )  // [dP/dr stored at ig]
			}
				break;
			case BC_NO_SLIP :	// Velocity field (no slip => Pol,Tor,dP/dr prescribed)
				LM_LOOP2( lms, lme,  S[lm] = ri_1*Pr[lm] + Pg[lm];  )	// Pol/r + dP/dr  [dP/dr stored at ig]
			{	s2d dr_2 = dr_1*dr_1;		dr_2 += dr_2;		// 2/dr2
				v2d* Pir = (v2d*) Pol[ir];		v2d* Pig = (v2d*) Pol[ig];		v2d* Pii = (v2d*) Pol[ii];
				LM_LOOP2( lms, lme,  T[lm] = (dr_2 + vdup(l2[lm])*ri_1*ri_1) * Pir[lm]
											- dr_2 * Pii[lm]
											+ vdup(2.)*(dr_1-ri_1) * Pig[lm];  )  // [dP/dr stored at ig]
			}
				break;
		/*	case BC_FREE_SLIP :	// Velocity field (free slip BC) : Pol = 0, dT/dr = Tor/r, S = dP/dr
				LM_LOOP2( lms, lme,  S[lm] = dr_1 * Pi[lm];  )
				break;	*/
			case BC_MAGNETIC :	// Magnetic field (insulator BC) : Tor=0, dP/dr = l/r.Pol - (2l+1)/r.Pext => S = (l+1)Pol/r - (2l+1)/r.Pext
			{	s2d cg = ri_1*(ri_1 - dr_1);	cg += cg;			// ghost coeff.
				s2d dr_2 = dr_1*dr_1;			dr_2 += dr_2;		// 2/dr2
				s2d ri_2 = ri_1*ri_1;		// 1/r2
				s2d dx = ri_1*dr_1;				dx += dx;		// 2/(rdr)
				if (ir==ir_bci) {	// [(2l+1).Pext stored at ig]
					LM_LOOP2( lms, lme,  S[lm] = ri_1 * (vdup(el[lm])*Pr[lm] + Pr[lm] - Pg[lm]);
										 T[lm] = (dr_2 + dx*vdup(el[lm]) + vdup(el[lm]*el[lm]-el[lm])*ri_2) * Pr[lm]
												- dr_2 * Pi[lm]
												+ cg * Pg[lm];  )
				} else {	// ir==ir_bco	// [(2l+1).Pext stored at ig]
					LM_LOOP2( lms, lme,  S[lm] = ri_1 * (Pg[lm] - vdup(el[lm])*Pr[lm]);
										 T[lm] = (dr_2 - dx*vdup(el[lm]+1.) + vdup((el[lm]+2.)*(el[lm]+1.))*ri_2) * Pr[lm]
												- dr_2 * Pi[lm]
												- cg * Pg[lm];  )
				}
				break;
			}
			case BC_CURL_FREE_SLIP :	// dP/dr = Pol/r => S = 2P/r
				LM_LOOP2( lms, lme,  S[lm] = (ri_1+ri_1) * Pr[lm];  )
				break;
			case BC_FREE_SLIP :
			case BC_ELLIPTIC :	// Pol imposed, d2P/dr2 deduced from stress-free condition.	// don't assume Pol=0
			{	s2d dx = vdup(r[ii]-r[ir])*ri_1*ri_1;
				LM_LOOP2( lms, lme,
					S[lm] = (ri_1 - dr_1 - dx + dx*vdup(0.5*l2_[lm])) * Pr[lm] + dr_1 * Pi[lm];
					T[lm] = (ri_1+ri_1) * ( Pr[lm] * (dr_1-ri_1+dx + ri_1*vdup(l2[lm]) - dx*vdup(0.5*l2[lm]))
																- dr_1*Pi[lm] );  )
				break;
			}
			default :	// use order 1 approx for dP/dr
				LM_LOOP2( lms, lme,  S[lm] = (ri_1 - dr_1) * Pr[lm] + dr_1 * Pi[lm];  )		// don't assume Pol=0
				LM_LOOP2( lms, lme,	 T[lm] = vdup(0.0);  )		// ???? \TODO Check this !
		}
	}
}

void PolTor::curl_QS(int ir, cplx *Qlm, cplx *Slm, int lms, int lme) const
{
	v2d *Q, *S;

	#ifdef XS_DEBUG
		if UNLIKELY(lms > lme) runerr("[curl_QST] nothing to do");
		if UNLIKELY((ir<irs)||(ir>ire))  runerr("[curl_QST] out of radial boundaries !");
	#endif

	Q = (v2d*) Qlm;		S = (v2d*) Slm;
	if LIKELY(IN_RANGE_INCLUSIVE(ir, ir_bci+1, ir_bco-1)) {			// MPI: assumes halo is up to date.
		_curlP(ir, &CURL_LAPL(ir).Wl, Tor, Qlm, Slm, lms, lme);
	} else {	// handle boundary conditions:
		int BC, ii, ig;
		BC = bco;	ii = ir-1;	ig = ir+1;		// values if ir==ir_bco
		if (ir==ir_bci) {
			BC = bci;	ii = ir+1;	ig = ir-1;
			if (r[ir] == 0.0) {	// field at r=0 : Pol=0, Tor=0, but S = 2.dP/dr (l=1 only) and Q=S
				const unsigned short* li_ = li;
				s2d dr_1 = vdup(r_1[ii]);
				LM_LOOP2( lms, lme,
					v2d s = vdup(0.);
					if (li_[lm] == 1) s = ((v2d*)Tor[ii])[lm] * (dr_1+dr_1);
					S[lm] = s;		Q[lm] = s;
				)
				return;
			}
		}
		s2d dr_1 = vdup(1.0/(r[ii]-r[ir]));
		s2d ri_1 = vdup(r_1[ir]);
		switch(BC) {
			default :	// use order 1 approx for dP/dr
			case BC_ZERO:
			case BC_NO_SLIP :
				break;
		/*	case BC_FREE_SLIP :	// Velocity field (free slip BC) : Pol = 0, dT/dr = Tor/r, S = dP/dr
			{	s2d dr_2 = -(ri_1+ri_1)*dr_1;
				v2d* Tir = (v2d*) Tor[ir];		v2d* Pii = (v2d*) Pol[ii];
				LM_LOOP2( lms, lme,	 Q[lm] = vdup(l2[lm])*ri_1 * Tir[lm];
									 S[lm] = (ri_1+ri_1) * Tir[lm];
									 T[lm] = dr_2 * Pii[lm];  )
				return;
			}	*/
			case BC_MAGNETIC :	// Magnetic field (insulator BC) : Tor=0, dP/dr = l/r.Pol - (2l+1)/r.Pext => S = (l+1)Pol/r - (2l+1)/r.Pext
			{	v2d* Tii = (v2d*) Tor[ii];
				LM_LOOP2( lms, lme,	 Q[lm] = vdup(0.0);
									 S[lm] = dr_1 * Tii[lm];  )		// Jr=0, T=0
				return;
			}
			case BC_FREE_SLIP :	// Velocity field (free slip BC) : dT/dr = Tor/r => S = T/r + dT/dr = 2T/r;  T = -Lap(P)
			case BC_ELLIPTIC :	// Pol imposed by ellipticity, d2P/dr2 deduced from stress-free condition.
			{	v2d* Tir = (v2d*) Tor[ir];
				LM_LOOP2( lms, lme,	 Q[lm] = vdup(l2[lm])*ri_1 * Tir[lm];
									 S[lm] = (ri_1+ri_1) * Tir[lm];  )
				return;
			}
		}
		v2d* Tir = (v2d*) Tor[ir];		v2d* Tii = (v2d*) Tor[ii];
		LM_LOOP2( lms, lme,	 Q[lm] = vdup(l2[lm])*ri_1 * Tir[lm];
							 S[lm] = Tir[lm] * (ri_1 - dr_1) + dr_1 * Tii[lm];  )	// Tor/r + dT/dr
	}
}


void PolTor::curl_QST(int ir, cplx *Qlm, cplx *Slm, cplx *Tlm, int lms, int lme) const
{
	v2d *Q, *S, *T;

	#ifdef XS_DEBUG
		if UNLIKELY(lms > lme) runerr("[curl_QST] nothing to do");
		if UNLIKELY((ir<irs)||(ir>ire))  runerr("[curl_QST] out of radial boundaries !");
	#endif

	Q = (v2d*) Qlm;		S = (v2d*) Slm;		T = (v2d*) Tlm;
	if LIKELY(IN_RANGE_INCLUSIVE(ir, ir_bci+1, ir_bco-1)) {			// MPI: assumes halo is up to date.

		/*
		OpCurlLapl& WL = CURL_LAPL(ir);
	  #ifdef XS_VEC_UNSAFE
		//	DO NOT USE: mem-mapped files are not rounded to cache-line, so this vectorized code may segfault.
		rnd Wl = vall(WL.Wl);	rnd Wd = vall(WL.Wd);	rnd Wu = vall(WL.Wu);
		rnd ri_1 = vall(WL.r_1);
		v2d* Tl = (v2d*) Tor[ir-1];	v2d* Td = (v2d*) Tor[ir];	v2d* Tu = (v2d*) Tor[ir+1];
		for (int lm=lms; lm<lme; lm+=VSIZE/2) {		// overflow allowed (rounded to cache-line or vector).
			rnd vl2 = vread_dup(l2_ + lm);
			rnd tr = vread(Td+lm, 0);
			rnd q = tr * vl2*ri_1;
			rnd s = Wl * vread(Tl+lm, 0) + Wd * tr + Wu*vread(Tu+lm, 0);
			vstore(Q+lm, 0, q);
			vstore(S+lm, 0, s);
		}
		rnd ri_2 = ri_1 * ri_1;	// 1/r^2
		rnd Ll = vall(WL.Ll);	rnd Ld = vall(WL.Ld);	rnd Lu = vall(WL.Lu);
		v2d* Pl = (v2d*) Pol[ir-1];	v2d* Pd = (v2d*) Pol[ir];	v2d* Pu = (v2d*) Pol[ir+1];
		for (int lm=lms; lm<lme; lm+=VSIZE/2) {		// overflow allowed (rounded to cache-line or vector).
			rnd vl2 = vread_dup(l2_ + lm);
			rnd t = (ri_2*vl2 - Ld)*vread(Pd+lm, 0) - Ll*vread(Pl+lm, 0) - Lu*vread(Pu+lm, 0);
			vstore(T+lm, 0, t);
		}
	  #else
		s2d Wl = vdup(WL.Wl);	s2d Wd = vdup(WL.Wd);	s2d Wu = vdup(WL.Wu);
		s2d ri_1 = vdup(WL.r_1);
		v2d* PTl = (v2d*) Tor[ir-1];		v2d* PTd = (v2d*) Tor[ir];			v2d* PTu = (v2d*) Tor[ir+1];
		LM_LOOP2( lms, lme,  Q[lm] = ri_1*vdup(l2_[lm]) * PTd[lm];
					S[lm] = Wl*PTl[lm] + Wd*PTd[lm] + Wu*PTu[lm];  )
		s2d ri_2 = ri_1*ri_1;		// 1/r^2
		Wl = vdup(WL.Ll);	Wd = vdup(WL.Ld);	Wu = vdup(WL.Lu);
		PTl = (v2d*) Pol[ir-1];			PTd = (v2d*) Pol[ir];			PTu = (v2d*) Pol[ir+1];
		LM_LOOP2( lms, lme,  T[lm] = (ri_2*vdup(l2_[lm]) - Wd)*PTd[lm] - Wl*PTl[lm] - Wu*PTu[lm];  )
	  #endif
		*/
		_laplP(ir, &CURL_LAPL(ir).Ll, l2, Pol, Tlm, lms, lme);
		_curlP(ir, &CURL_LAPL(ir).Wl,     Tor, Qlm, Slm, lms, lme);
	} else {	// handle boundary conditions:
		int BC, ii, ig;
		BC = bco;	ii = ir-1;	ig = ir+1;		// values if ir==ir_bco
		if (ir==ir_bci) {
			BC = bci;	ii = ir+1;	ig = ir-1;
			if (r[ir] == 0.0) {	// field at r=0 : Pol=0, Tor=0, but S = 2.dP/dr (l=1 only) and Q=S
				const unsigned short* li_ = li;
				s2d dr_1 = vdup(r_1[ii]);
				LM_LOOP2( lms, lme,
					v2d s = vdup(0.);	T[lm] = s;
					if (li_[lm] == 1) s = ((v2d*)Tor[ii])[lm] * (dr_1+dr_1);
					S[lm] = s;		Q[lm] = s;
				)
				return;
			}
		}
		s2d dr_1 = vdup(1.0/(r[ii]-r[ir]));
		s2d ri_1 = vdup(r_1[ir]);
		switch(BC) {
			case BC_ZERO:
			case BC_NO_SLIP :	// Velocity field (no slip => Pol,Tor,dP/dr prescribed)
			{	s2d dr_2 = dr_1*dr_1;		dr_2 += dr_2;		// 2/dr2
				v2d* Pir = (v2d*) Pol[ir];		v2d* Pig = (v2d*) Pol[ig];		v2d* Pii = (v2d*) Pol[ii];
				LM_LOOP2( lms, lme,  T[lm] = (dr_2 + vdup(l2[lm])*ri_1*ri_1) * Pir[lm]
											- dr_2 * Pii[lm]
											+ vdup(2.)*(dr_1-ri_1) * Pig[lm];  )  // [dP/dr stored at ig]
				break;
			}
		/*	case BC_FREE_SLIP :	// Velocity field (free slip BC) : Pol = 0, dT/dr = Tor/r, S = dP/dr
			{	s2d dr_2 = -(ri_1+ri_1)*dr_1;
				v2d* Tir = (v2d*) Tor[ir];		v2d* Pii = (v2d*) Pol[ii];
				LM_LOOP2( lms, lme,	 Q[lm] = vdup(l2[lm])*ri_1 * Tir[lm];
									 S[lm] = (ri_1+ri_1) * Tir[lm];
									 T[lm] = dr_2 * Pii[lm];  )
				return;
			}	*/
			case BC_MAGNETIC :	// Magnetic field (insulator BC) : Tor=0, dP/dr = l/r.Pol - (2l+1)/r.Pext => S = (l+1)Pol/r - (2l+1)/r.Pext
			{	v2d* Tii = (v2d*) Tor[ii];
				LM_LOOP2( lms, lme,	 Q[lm] = vdup(0.0);
									 S[lm] = dr_1 * Tii[lm];  )		// Jr=0, T=0
				s2d cg = ri_1*(ri_1 - dr_1);	cg += cg;			// ghost coeff.
				s2d dr_2 = dr_1*dr_1;			dr_2 += dr_2;		// 2/dr2
				s2d ri_2 = ri_1*ri_1;		// 1/r2
				s2d dx = ri_1*dr_1;				dx += dx;		// 2/(rdr)
				v2d* Pir = (v2d*) Pol[ir];		v2d* Pig = (v2d*) Pol[ig];		v2d* Pii = (v2d*) Pol[ii];
				if (ir==ir_bci) {
					LM_LOOP2( lms, lme,  T[lm] = (dr_2 + dx*vdup(el[lm]) + vdup(el[lm]*el[lm]-el[lm])*ri_2) * Pir[lm]
												- dr_2 * Pii[lm]
												+ cg * Pig[lm];  )	// [(2l+1).Pext stored at ig]
				} else {	// ir==ir_bco
					LM_LOOP2( lms, lme,  T[lm] = (dr_2 - dx*vdup(el[lm]+1.) + vdup((el[lm]+2.)*(el[lm]+1.))*ri_2) * Pir[lm]
												- dr_2 * Pii[lm]
												- cg * Pig[lm];  )	// [(2l+1).Pext stored at ig]
				}
				return;
			}
			case BC_FREE_SLIP :	// Velocity field (free slip BC) : dT/dr = Tor/r => S = T/r + dT/dr = 2T/r;  T = -Lap(P)
			case BC_ELLIPTIC :	// Pol imposed by ellipticity, d2P/dr2 deduced from stress-free condition.
			{	s2d dx = vdup(r[ii]-r[ir])*ri_1*ri_1;
				v2d* Pir = (v2d*) Pol[ir];		v2d* Pii = (v2d*) Pol[ii];	v2d* Tir = (v2d*) Tor[ir];				
				LM_LOOP2( lms, lme,	 Q[lm] = vdup(l2[lm])*ri_1 * Tir[lm];
									 S[lm] = (ri_1+ri_1) * Tir[lm];  )
				LM_LOOP2( lms, lme,	 T[lm] = (ri_1+ri_1) * ( Pir[lm] * (dr_1-ri_1+dx + ri_1*vdup(l2[lm]) - dx*vdup(0.5*l2[lm]))
																- dr_1*Pii[lm] );  )
				return;
			}
			default :	// use order 1 approx for dP/dr
				LM_LOOP2( lms, lme,	 T[lm] = vdup(0.0);  )		// ???? \TODO Check this !
		}
		v2d* Tir = (v2d*) Tor[ir];		v2d* Tii = (v2d*) Tor[ii];
		LM_LOOP2( lms, lme,	 Q[lm] = vdup(l2[lm])*ri_1 * Tir[lm];
							 S[lm] = Tir[lm] * (ri_1 - dr_1) + dr_1 * Tii[lm];  )	// Tor/r + dT/dr
	}
}


/// Computes the Q,S,Tor coefficients of the curl of a PolTor field, including proper boundary conditions.
void PolTor::curl_qst(int ir, int lm, cplx *Q, cplx *S, cplx *T) const
{
	curl_QST(ir, Q-lm, S-lm, T-lm, lm, lm);
}

/// compute curl of PolTor vector.
void PolTor::curl(int istart, int iend)
/// Pol = Tor;  Tor = - Lap.Pol
{
	cplx *lapl, *lapd, *lapt, *mem, *scratch;
	int ir;

	if (istart < irs) istart = irs;
	if (iend > ire) iend = ire;
	if (istart >= iend) return;

	mem = (cplx*) VMALLOC( 3*NLM*sizeof(cplx) );
	#ifdef XS_DEBUG
	if (mem==0) runerr("[PolTor::curl] allocation error");
	#endif
	lapl = mem;	lapd = mem + NLM;	scratch = mem + 2*NLM;

	ir = istart;
		curl_QST(ir, scratch, scratch, lapl);		// only lapl is interesting here.
	for (ir=istart+1; ir < iend; ir++) {
		OpCurlLapl& L = CURL_LAPL(ir);
		LM_LOOP(  lapd[lm] = (l2[lm]*L.r_2 - L.Ld)*Pol[ir][lm] - L.Ll*Pol[ir-1][lm] - L.Lu*Pol[ir+1][lm];  )
		LM_LOOP(  Pol[ir-1][lm] = Tor[ir-1][lm];  )
		LM_LOOP(  Tor[ir-1][lm] = lapl[lm];  )
		lapt = lapl;	lapl = lapd;	lapd = lapt;	// rotate buffers.
	}
	ir = iend;
		curl_QST(ir, scratch, scratch, lapd);
		LM_LOOP(  Pol[ir-1][lm] = Tor[ir-1][lm];  )
		LM_LOOP(  Tor[ir-1][lm] = lapl[lm];  )
		LM_LOOP(  Pol[ir][lm] = Tor[ir][lm];  )
		LM_LOOP(  Tor[ir][lm] = lapd[lm];  )

	VFREE(mem);
	// update BC
	bci = (bci == BC_FREE_SLIP) ? BC_CURL_FREE_SLIP : BC_NONE;
	bco = (bco == BC_FREE_SLIP) ? BC_CURL_FREE_SLIP : BC_NONE;
	#ifdef XS_MPI
		sync_mpi();
	#endif
}


/// compute the curl of the field.
/// on input, Pol must be the toroidal component T, Tor must be the radial component Q,
/// the third (extra) component must be the spheroidal one S.
/// on output, Pol is unchanged, Tor = Q/r - 1/r.d(rS)/dr = (Q-S)/r - dS/dr
void PolTor::curl_from_TQS(Spectral* S, int istart, int iend, int sync_tag)
{
#ifdef XS_DEBUG
	if (S==0) runerr("[PolTor::curl_from_TQS] missing extra component");
#endif
#ifdef XS_MPI
	if ((ire < istart) || (irs > iend)) return;		// reject processes not involved.
	if (sync_tag >= 0) {
		#pragma omp barrier
		#pragma omp master
		{
			MPI_Request req[4];
			MPI_Request *req_ptr = req;
			if (n_mpi_shared > 1) MPI_Barrier(comm_shared);
			S->sync1_mpi(0, istart, iend, 0, NLM-1, sync_tag, &req_ptr);
			if (req_ptr-req > 0) {
				int ierr = MPI_Waitall(req_ptr-req, req, MPI_STATUSES_IGNORE);
				if (ierr != MPI_SUCCESS) runerr("[curl_from_TQS] MPI_Waitall failed.\n");
			}
			if (n_mpi_shared > 1) MPI_Barrier(comm_shared);
		}
		#pragma omp barrier
	}
#endif
	int ir0 = (istart < irs) ? irs : istart;
	int ir1 = (iend > ire)   ? ire : iend;
	thread_interval_rad(ir0, ir1);
	int lm00 = 0;
	int lm11 = NLM-1;
	thread_interval_lm(lm00, lm11);
	const int nlm_block = 400;		// fits in a 32KB L1 cache
	for (int lm0=lm00; lm0<=lm11; lm0+=nlm_block) {
		int lm1 = lm0 + nlm_block -1;
		if (lm1 > lm11) lm1 = lm11;
		for (int ir=ir0; ir <= ir1; ir++) {
			v2d* Tr = (v2d*) Tor[ir];		v2d* Sd = (v2d *) S->get_data(0,ir);
			int lm=lm0;
			if (lm==0) Tr[lm++] = vdup(0.0);		// l=0 contribution is zero after curl.
			if LIKELY(IN_RANGE_INCLUSIVE(ir, istart+1, iend-1)) {
				OpCurlLapl& W = CURL_LAPL(ir);
				v2d* Sl = (v2d *) S->get_data(0,ir-1);		v2d* Su = (v2d *) S->get_data(0,ir+1);
				s2d Wl = vdup(W.Wl);	s2d Wd = vdup(W.Wd);	s2d Wu = vdup(W.Wu);
				s2d r1 = vdup(W.r_1);
				for (; lm<=lm1; lm++) {
					Tr[lm] = r1*Tr[lm] - Wl * Sl[lm] - Wd * Sd[lm] - Wu * Su[lm];
				}
			} else {
				int ii = (ir==istart) ? ir+1 : ir-1;
				s2d r1 = vdup(r_1[ir]);
				s2d dx = vdup(1.0/(r[ii]-r[ir]));
				v2d* Si = (v2d *) S->get_data(0,ii);
				for (; lm<=lm1; lm++) {
					Tr[lm] = r1*(Tr[lm] - Sd[lm]) - dx*(Si[lm] - Sd[lm]);
				}
			}
		}
	}
}


/****************************
 *** DIAGNOSTIC FUNCTIONS ***
 ****************************/
 
inline double reduce_max(rnd a)
{
  #ifndef XS_VEC
	return a;
  #else
	#ifdef __AVX512F__
		return _mm512_reduce_max_pd(a);
	#else
		#ifdef __AVX__
			s2d max1 = _mm_max_pd( _mm256_castpd256_pd128(a), _mm256_extractf128_pd(a, 1) );
		#else
			s2d max1 = a;
		#endif
		return _mm_cvtsd_f64(_mm_max_sd(max1, _mm_unpackhi_pd(max1,max1)));
	#endif
  #endif
}

/// compute the maximum of the squared norm of vector field (vr,vt,vp)
double absmax2(double *vr, double *vt, double *vp, size_t nspat0, size_t nspat1)
{
	const long nk = (nspat1-nspat0)/VSIZE;
	vr += nspat0;
	vt += nspat0;
	vp += nspat0;
  #ifdef XS_VEC
	rnd v2_max0 = vall(0.0);	// 2 accumulators are enough here
	rnd v2_max1 = vall(0.0);
	long k = 0;
	for (; k<nk-1; k+=2) {
		rnd vvr = vread(vr, k);
		rnd vvt = vread(vt, k);
		rnd vvp = vread(vp, k);
		rnd v2 = vvr*vvr + vvt*vvt + vvp*vvp;
		v2_max0 = vmax(v2_max0, v2);
		
		vvr = vread(vr, k+1);
		vvt = vread(vt, k+1);
		vvp = vread(vp, k+1);
		v2 = vvr*vvr + vvt*vvt + vvp*vvp;
		v2_max1 = vmax(v2_max1, v2);
	}
	if (k<nk) {
		rnd vvr = vread(vr, k);
		rnd vvt = vread(vt, k);
		rnd vvp = vread(vp, k);
		rnd v2 = vvr*vvr + vvt*vvt + vvp*vvp;
		v2_max0 = vmax(v2_max0, v2);
	}
	v2_max0 = vmax( v2_max0, v2_max1 );
	return reduce_max(v2_max0);
  #else
	double v2_max = 0.0;
	#pragma omp simd reduction(max:v2_max)
	for (size_t k=0; k<nk; k++) {
		rnd vvr = vread(vr, k);
		rnd vvt = vread(vt, k);
		rnd vvp = vread(vp, k);
		rnd v2 = vvr*vvr + vvt*vvt + vvp*vvp;
		v2_max = vmax(v2_max, v2);
	}
	return v2_max;
  #endif
}

/// compute the maximum of the squared norm of vector field (vt,vp)
double absmax2(double *vr, double *vt, size_t nspat0, size_t nspat1)
{
	const long nk = (nspat1-nspat0)/VSIZE;
	vr += nspat0;
	vt += nspat0;
  #ifdef XS_VEC
	rnd v2_max0 = vall(0.0);	// 4 accumulators are enough here
	rnd v2_max1 = vall(0.0);
	rnd v2_max2 = vall(0.0);
	rnd v2_max3 = vall(0.0);
	long k = 0;
	for (; k<nk-3; k+=4) {
		rnd vvr = vread(vr, k);
		rnd vvt = vread(vt, k);
		rnd v2 = vvr*vvr + vvt*vvt;
		v2_max0 = vmax(v2_max0, v2);

		vvr = vread(vr, k+1);
		vvt = vread(vt, k+1);
		v2 = vvr*vvr + vvt*vvt;
		v2_max1 = vmax(v2_max1, v2);

		vvr = vread(vr, k+2);
		vvt = vread(vt, k+2);
		v2 = vvr*vvr + vvt*vvt;
		v2_max2 = vmax(v2_max2, v2);

		vvr = vread(vr, k+3);
		vvt = vread(vt, k+3);
		v2 = vvr*vvr + vvt*vvt;
		v2_max3 = vmax(v2_max3, v2);
	}
	for (; k<nk; k++) {
		rnd vvr = vread(vr, k);
		rnd vvt = vread(vt, k);
		rnd v2 = vvr*vvr + vvt*vvt;
		v2_max0 = vmax(v2_max0, v2);
	}
	v2_max2 = vmax( v2_max2, v2_max3 );
	v2_max0 = vmax( v2_max0, v2_max1 );
	v2_max0 = vmax( v2_max0, v2_max2 );
	return reduce_max(v2_max0);
  #else
	double v2_max = 0.0;
	#pragma omp simd reduction(max:v2_max)
	for (size_t k=0; k<nk; k++) {
		rnd vvr = vread(vr, k);
		rnd vvt = vread(vt, k);
		rnd v2 = vvr*vvr + vvt*vvt;
		v2_max = vmax(v2_max, v2);
	}
	return v2_max;
  #endif
}

/// compute the maximum of the squared norm of scalar field s
double absmax2(double *s, size_t nspat0, size_t nspat1)
{
	const long nk = (nspat1-nspat0)/VSIZE;
	s += nspat0;
  #ifdef XS_VEC
	const rnd msk = vabs_and_mask;
	rnd v_max0 = vall(0.0);
	rnd v_max1 = vall(0.0);
	rnd v_max2 = vall(0.0);
	rnd v_max3 = vall(0.0);
	rnd v_max4 = vall(0.0);
	rnd v_max5 = vall(0.0);
	rnd v_max6 = vall(0.0);
	rnd v_max7 = vall(0.0);		// 8 accumulators to saturate the skylake pipeline.
	long k = 0;
	for (; k<nk-7; k+=8) {
		rnd vs = vand( vread(s, k), msk);
		v_max0 = vmax(v_max0, vs);
		vs = vand( vread(s, k+1), msk);
		v_max1 = vmax(v_max1, vs);
		vs = vand( vread(s, k+2), msk);
		v_max2 = vmax(v_max2, vs);
		vs = vand( vread(s, k+3), msk);
		v_max3 = vmax(v_max3, vs);
		vs = vand( vread(s, k+4), msk);
		v_max4 = vmax(v_max4, vs);
		vs = vand( vread(s, k+5), msk);
		v_max5 = vmax(v_max5, vs);
		vs = vand( vread(s, k+6), msk);
		v_max6 = vmax(v_max6, vs);
		vs = vand( vread(s, k+7), msk);
		v_max7 = vmax(v_max7, vs);
	}
	for (; k<nk; k++) {
		rnd vs = vand( vread(s,k) , msk);			// it is faster to take absolute value than to square.
		v_max0 = vmax(v_max0, vs);
	}
	v_max2 = vmax( v_max2, v_max1 );
	v_max4 = vmax( v_max4, v_max3 );
	v_max6 = vmax( v_max6, v_max5 );
	v_max0 = vmax( v_max0, v_max7 );
	v_max2 = vmax( v_max2, v_max4 );
	v_max0 = vmax( v_max0, v_max6 );
	v_max0 = vmax( v_max0, v_max2 );
	double mx = reduce_max(v_max0);
	return mx*mx;
  #else
	double v_max = 0.0;
	#pragma omp simd reduction(max:v_max)
	for (long k=0; k<nk; k++) {
		double x = fabs(s[k]);			// it is faster to take absolute value than to square.
		v_max = vmax(v_max, x);
	}
	return v_max*v_max;
  #endif
}

void zero_spat_padding(double* s) {
	for (int i=0; i<NPHI; i++) {
		for (int j=NLAT; j<NLAT_PADDED; j++)	s[j] = 0;
		s += NLAT_PADDED;
	}
}

double PolTor::r_absmax2(int ir) const
{
	cplx *Q, *S;
	double *vr, *vt, *vp;
	const long nspat = shtns->nspat;

	Q = (cplx *) VMALLOC( 3*nspat * sizeof(double) + 2*NLM * sizeof(cplx));
	#ifdef XS_DEBUG
	if (Q==0) runerr("[PolTor::r_absmax2] allocation error");
	#endif
	S = Q + NLM;
	vr = (double *) (S + NLM);
	vt = vr + nspat;	vp = vt + nspat;
	zero_spat_padding(vr);
	zero_spat_padding(vt);
	zero_spat_padding(vp);

	RadSph(ir, Q, S);
	SHV3_SPAT(Q, S, Tor[ir], vr, vt, vp);
	double max2 = absmax2(vr, vt, vp, 0,shtns->nspat);

	VFREE(Q);
	return max2;
}

double ScalarSH::r_absmax2(int ir) const
{
	double* s = (double *) VMALLOC( shtns->nspat * sizeof(double));
	#ifdef XS_DEBUG
	if (s==0) runerr("[ScalarSH::r_absmax2] allocation error");
	#endif
	memset(s, 0x49, shtns->nspat*sizeof(double));
	zero_spat_padding(s);
	SH_SPAT(Sca[ir], s);
	double max2 = absmax2(s, 0,shtns->nspat);

	VFREE(s);
	return max2;
}

/// compute Linf norm (maximum value) of the spatial field described by its spectral components.
/// First and last shells are ignored, so that boundary conditions are not important.
/// MPI: reduction is performed, all ranks have the correct result.
double Spectral::absmax(int irstart, int irend) const
{
	double maxmax = 0.0;

	if (irstart > irend) return 0.0;
	if (irstart < irs) irstart = irs;
	if (irend > ire)   irend = ire;

	#if XS_OMP == 1
	#pragma omp parallel shared(maxmax) firstprivate(irstart, irend)
	#endif
	{
		double max = 0.0;
		thread_interval_rad(irstart, irend);
		for (int ir=irstart; ir<=irend; ir++) {
			double maxr = r_absmax2(ir);
			if (maxr > max) max = maxr;
		}
		#pragma omp critical
		{
			if (max > maxmax) maxmax = max;
		}
	}
	#ifdef XS_MPI
		double max2;
		MPI_Allreduce( &maxmax, &max2, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD );
		maxmax = max2;			// all ranks have the correct max here.
	#endif
	return sqrt(maxmax);
}

/// compute z-averages in spatial domain (using interpolation).
/// buf is the output buffer, where vs,vp,vz,vs2,vp2,vsvp averaged over z is stored.
/// It must contain 6*ns*NPHI doubles.
/// Uses multiple threads if called from an OpenMP parallel region.
void PolTor::special_z_avg(int ns, const double *s, double *buf, const int rstep, double rmax) const
{
	double z, dz, H_1, ztop;
	double s2, cost, sint;
	double *buf2;
	double *vs1, *vp1, *vz1, *vs2, *vp2, *vsvp;
	int i0, is_tc;
	const int nphi = NPHI;

	// TODO: make it work with XS_OMP 1 or 2.
	#ifdef _OPENMP
		#pragma omp single
		{
			memset(buf, 0, sizeof(double)*6*ns*nphi);
		}
		// allocate temporary fields for zavg (private)
		buf2 = (double*) malloc(sizeof(double)*ns*nphi*6);
		#ifdef XS_DEBUG
		if (buf2==0) runerr("[PolTor::special_z_avg] allocation error 1");
		#endif
	#else
		buf2 = buf;
	#endif
	vs1 = buf2;		vp1 = buf2 + nphi*ns;	vz1 = buf2 + 2*nphi*ns;
	vs2 = buf2 + 3*nphi*ns;		vp2 = buf2 + 4*nphi*ns;		vsvp = buf2 + 5*nphi*ns;
	memset(buf2, 0, sizeof(double)*6*ns*nphi);

	// allocate temporary fields for one shell (private)
	MemBlock mem( sizeof(cplx)*2*NLM + sizeof(double)*3*shtns->nspat, 5 );
	Shell2d_mbk vr(shtns,mem), vt(shtns,mem), vp(shtns,mem);
	cplx* Q = mem.alloc_cplx(NLM);
	cplx* S = mem.alloc_cplx(NLM);

	i0 = ((irs + rstep-1)/rstep);		// start at an rstep multiple.
	is_tc = 0;
	while( s[is_tc] < r[ir_bci] ) is_tc++;		// find the tangent-cylinder boundary in s-domain.
	i0 *= rstep;		// rstep multiple.

	int i1 = ire;
	int i_top = ir_bco;
	if (rmax>0) {
		int ii = r_to_idx(rmax);
		if (ii < i1) i1 = ii;
		if (ii < i_top) i_top = ii;
	}

	#pragma omp for schedule(dynamic) nowait
	for (int ir=i0; ir<=i1; ir+=rstep) {		// span radial domain of each process.
		RadSph(ir, Q, S);
		SHV3_SPAT(Q, S, Tor[ir], vr, vt, vp);		// render one shell
		int it = 1;			// start at north pole.
		for (int is=0; is<ns; is++) {
			if (s[is] > r[ir]) break;		// span whole equatorial domain contained in the spherical shell.
			s2 = s[is]*s[is];
			z = sqrt(r[ir]*r[ir] - s2);
			ztop = sqrt(r[i_top]*r[i_top] - s2);
			cost = z * r_1[ir];		sint = s[is] * r_1[ir];
			if (is < is_tc) {		// inside tangent cylinder.
				H_1 = 1.0/(ztop - sqrt(r[ir_bci]*r[ir_bci]-s2));		// inverse height of column.
			} else {
				H_1 = 0.5/ztop;		// inverse height of half-column outside tangent-cylinder.
			}
			if (r[ir] == 0.0) {
				cost = 1.0;		sint = 0.0;
			}
			double r_p = (ir+rstep <= i_top) ? r[ir+rstep] : r[i_top];
			double r_m = (ir-rstep >= ir_bci) ? r[ir-rstep] : r[ir_bci];
			dz = sqrt( r_p*r_p - s2)  -  sqrt( r_m*r_m - s2 );
			if (r_m < s[is])	dz = sqrt( r_p*r_p - s2) + z;		// count twice the space below z !
			dz *= 0.5*H_1;		// normalize.
			if (s[is]==r[i_top]) {		// only one point contributes !
				dz = 1.0;		H_1 = 1.0;
				cost = 0.0;		sint = 1.0;
			}

			// find intersection
			while( (it < NLAT/2) && (shtns->ct[it] > cost) )  it++;
			double a = (cost - ct[it])/(ct[it-1]-ct[it]);		// angular interpolation (1D)
			double b = 1.0 - a;		// interpolation coeffs
			int nit = NLAT-1-it;	// south index

			for (int ip=0; ip<NPHI; ip++) {
				double vrn = vr(it -1, ip)*a + vr(it, ip)*b;
				double vrs = vr(nit+1, ip)*a + vr(nit,ip)*b;
				double vtn = vt(it -1, ip)*a + vt(it, ip)*b;
				double vts = vt(nit+1, ip)*a + vt(nit,ip)*b;
				double vpn = vp(it -1, ip)*a + vp(it, ip)*b;
				double vps = vp(nit+1, ip)*a + vp(nit,ip)*b;
				double vsn = vrn*sint + vtn*cost;
				double vss = vrs*sint - vts*cost;
				double vzn = vrn*cost - vtn*sint;
				double vzs = -vrs*cost - vts*sint;
				if (is < is_tc) {		// store north and south hemisphere in successive radial points.
					vs1[is*nphi +ip]     += vsn *dz;
					vs1[(is+1)*nphi +ip] += vss *dz;
					vz1[is*nphi +ip]     += vzn *dz;
					vz1[(is+1)*nphi +ip] += vzs *dz;
					vs2[is*nphi +ip]     += vsn*vsn *dz;
					vs2[(is+1)*nphi +ip] += vss*vss *dz;
					vp1[is*nphi +ip]     += vpn *dz;
					vp1[(is+1)*nphi +ip] += vps *dz;
					vp2[is*nphi +ip]     += vpn*vpn *dz;
					vp2[(is+1)*nphi +ip] += vps*vps *dz;
					vsvp[is*nphi +ip]    += vsn*vpn *dz;
					vsvp[(is+1)*nphi +ip] += vss*vps *dz;
				} else {
					vs1[is*nphi +ip] += (vsn+vss) *dz;
					vz1[is*nphi +ip] += (vzn+vzs) *dz;
					vs2[is*nphi +ip] += (vsn*vsn + vss*vss) *dz;
					vp1[is*nphi +ip] += (vpn+vps) *dz;
					vp2[is*nphi +ip] += (vpn*vpn + vps*vps) *dz;
					vsvp[is*nphi +ip] += (vsn*vpn + vss*vps) *dz;
				}
			}
			if (is < is_tc)  is++;	// another point is consumed !
		}
	}
	#ifdef _OPENMP
		#pragma omp critical
		{	// commit to shared buffer
			for (int i=0; i<5*ns*nphi; i++) buf[i] += buf2[i];
		}
		free(buf2);
	#endif

	#ifdef XS_MPI
		#pragma omp barrier
		#pragma omp master
		MPI_Reduce((i_mpi==0) ? MPI_IN_PLACE : buf, buf , 5*nphi*ns, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);		// sum all contributions.
	#endif
 	#pragma omp barrier
}



#ifdef XS_LEGACY
/// Allocate memory for a dynamic vector field (like U or B) and its pol/tor representation + non-linear term storage.
/// (fields are optional)
void alloc_DynamicField(PolTor *PT, PolTor *NL1, PolTor *NL2, PolTor *PTtmp, int istart, int iend)
{
	if (PT != NULL)		PT->alloc(istart, iend);
	if (PTtmp != NULL)	PTtmp->alloc(istart, iend);
  #ifndef XS_PURE_SPECTRAL
	if (NL1 != NULL)	NL1->alloc(istart, iend);
	if (NL2 != NULL)	NL2->alloc(istart, iend);
  #else
	if (NL1 != NULL)	NL1->alloc(istart, iend, 3);		// temporary extra component needed
	if (NL2 != NULL)	NL2->alloc(istart, iend, 3);		// temporary extra component needed
  #endif
}

void alloc_DynamicField(ScalarSH *SH, ScalarSH *NL1, ScalarSH *NL2, ScalarSH *SHtmp, int istart, int iend)
{
	if (SH != NULL)		SH->alloc(istart, iend);
	if (SHtmp != NULL)	SHtmp->alloc(istart, iend);
	if (NL1 != NULL)	NL1->alloc(istart, iend);
	if (NL2 != NULL)	NL2->alloc(istart, iend);
}

/// Isolate the non-zero modes and store them pre-computed (ready for SHT) in a minimal way.
/// \param **pssv, **pcurl point on pointers on StatSpecVect structures => allocated if needed, or NULL is returned.
/// Gives the ability to add "for free" background fields (B, ...)
/// Boundary condition should be carefuly set (usually to BC_NONE)
/// return value is the number of non-zero modes.
int PolTor::make_static(struct StatSpecVect **pssv, struct StatSpecVect **pcurl) const
{
	struct StatSpecVect *ssv, *curl;
	void *ptr;
	double dx, max;
	int i,j,lm;
	int lmcount, maxl, maxm, m;
	double *A2;		// max amplitudes for each lm-mode.
	int *lms;
	unsigned short *lis, *mis;

	lms = (int*) malloc( NLM*(sizeof(int)+2*sizeof(unsigned short)) );
	lis = (unsigned short*) (lms+NLM);		mis = lis + NLM;

	A2 = (double*) malloc( NLM*sizeof(double) );
	if (A2==0) runerr("[PolTor::make_static] allocation error 1");
	// get max amplitude
	for (lm=0; lm<NLM; lm++) {
		A2[lm] = 0.0;
		for (i=irs; i<=ire; i++) {
			dx = norm(Pol[i][lm]) + norm(Tor[i][lm]);		// |P|^2 + |T|^2
			dx *= l2[lm];
			if (dx > A2[lm]) A2[lm] = dx;
		}
	}
  #ifdef XS_MPI
	double *tmp = (double*) malloc( NLM*sizeof(double) );
	if (tmp==0) runerr("[PolTor::make_static] allocation error 2");
	MPI_Allreduce(A2, tmp, NLM, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	free(A2);
	A2 = tmp;
  #endif
	max = 0.0;
	for (lm=0; lm<NLM; lm++) {
		if (A2[lm] > max) max = A2[lm];
	}

	maxl = 0;	maxm = 0;	m=0;
	lmcount = 0;
	// count "non-zero" modes
	max *= 1.e-20;		// no more than 1e-10 amplitude contrast allowed between modes.
	for (lm=0; lm<NLM; lm++) {
		int l = li[lm];
		if (A2[lm] > max) {		// mode is non-zero
			lms[lmcount] = lm;	// store mode
			lis[lmcount] = l;
			mis[lmcount] = m;
			lmcount++;		// count
			if (l > maxl) maxl = l;
			if (m > maxm) maxm = m;
		}
		if (l == LMAX) m += MRES;
	}
	free(A2);
	if (lmcount == 0) {
		free(lms);
		if (pssv != NULL) *pssv = NULL;
		if (pcurl != NULL) *pcurl = NULL;		/// \todo Possible memory leak !
		return lmcount;	// stop here.
	}
	if ((zero_curl) && (pcurl != NULL)) {		// field has been marked as zero curl.
		*pcurl = NULL;		/// \todo Possible memory leak !
		pcurl = NULL;
	}

	ssv = NULL;		curl = NULL;
	if (pssv != NULL) {
		if (*pssv == NULL)	*pssv = (struct StatSpecVect *) malloc( sizeof(struct StatSpecVect) );
		ssv = *pssv;		ssv->nlm = lmcount;
	}
	if (pcurl != NULL) {
		if (*pcurl == NULL)	*pcurl = (struct StatSpecVect *) malloc( sizeof(struct StatSpecVect) );
		curl = *pcurl;		curl->nlm = lmcount;
	}

	if (ssv != NULL) {
		// TODO MPI: use only local shell numbers instead of NR
		j = ((lmcount+3)>>2)*4;		// lmcount rounded up for SSE alignement.
		ptr = VMALLOC( j*sizeof(int) + lmcount*3*NR*sizeof(cplx) + 2*lmcount*sizeof(unsigned short) );
		if (ptr==0) runerr("[PolTor::make_static] allocation error 3");
		ssv->lm = (unsigned *) ptr;
		ptr = ssv->lm + j;
		ssv->QST = (cplx *) ptr;		// should be SSE aligned.
		ptr = ssv->QST + 3*lmcount*NR;
		ssv->li = (unsigned short*) ptr;
		ssv->mi = ssv->li + lmcount;

		for (j=0; j<lmcount; j++) {
			ssv->lm[j] = lms[j];		// copy lm indices
			ssv->li[j] = lis[j];		// copy l
			ssv->mi[j] = mis[j];		// copy m
		}
		ssv->lmax = maxl;		ssv->mmax = maxm;			// save max values.

		for (i=0; i<NR; i++) {				// zero out modes
			for (j=0; j<lmcount*3; j++) ssv->QST[i*lmcount*3 +j] = 0.0;
		}

		// pre-compute and store QST values.
		for (i=irs; i<=ire; i++) {
			if ((i==ir_bci)||(i==ir_bco)) {
				for (j=0; j<lmcount; j++) {
					lm = lms[j];
					qst(i, lm, &ssv->QST[(i*lmcount+j)*3], &ssv->QST[(i*lmcount+j)*3+1], &ssv->QST[(i*lmcount+j)*3+2]);
				}
			} else {
				OpCurlLapl& W = CURL_LAPL(i);
				for (j=0; j<lmcount; j++) {
					lm = lms[j];
					ssv->QST[(i*lmcount +j)*3] = W.r_1*l2[lm] * Pol[i][lm];
					ssv->QST[(i*lmcount +j)*3 + 1] = W.Wl*Pol[i-1][lm] + W.Wd*Pol[i][lm] + W.Wu*Pol[i+1][lm];
					ssv->QST[(i*lmcount +j)*3 + 2] = Tor[i][lm];
				}
			}
		}
	}
	if (curl != NULL) {
		// TODO MPI: use only local shell numbers instead of NR
		j = ((lmcount+3)>>2)*4;		// lmcount rounded up for SSE alignement.
		ptr = VMALLOC( j*sizeof(int) + lmcount*3*NR*sizeof(cplx) + 2*lmcount*sizeof(unsigned short) );
		if (ptr==0) runerr("[PolTor::make_static] allocation error 4");
		curl->lm = (unsigned *) ptr;
		ptr = curl->lm + j;
		curl->QST = (cplx *) ptr;		// should be SSE aligned.
		ptr = curl->QST + 3*lmcount*NR;
		curl->li = (unsigned short*) ptr;
		curl->mi = curl->li + lmcount;

		for (j=0; j<lmcount; j++) {
			curl->lm[j] = lms[j];		// copy lm indices
			curl->li[j] = lis[j];		// copy l
			curl->mi[j] = mis[j];		// copy m
		}
		curl->lmax = maxl;		curl->mmax = maxm;			// save max values.

		for (i=0; i<NR; i++) {				// zero out modes
			for (j=0; j<lmcount*3; j++) curl->QST[i*lmcount*3 +j] = 0.0;
		}
		// pre-compute and store QST values.
		for (i=irs; i<=ire; i++) {
			if ((i==ir_bci)||(i==ir_bco)) {
				for (j=0; j<lmcount; j++) {
					lm = lms[j];
					curl_qst(i, lm, &curl->QST[(i*lmcount+j)*3], &curl->QST[(i*lmcount+j)*3+1], &curl->QST[(i*lmcount+j)*3+2]);
				}
			} else {
				OpCurlLapl& W = CURL_LAPL(i);
				for (j=0; j<lmcount; j++) {
					lm = lms[j];
					curl->QST[(i*lmcount +j)*3] = W.r_1*l2[lm] * Tor[i][lm];
					curl->QST[(i*lmcount +j)*3 + 1] = W.Wl*Tor[i-1][lm] + W.Wd*Tor[i][lm] + W.Wu*Tor[i+1][lm];
					curl->QST[(i*lmcount +j)*3 + 2] = (W.r_2*l2[lm] - W.Ld)*Pol[i][lm] - W.Ll*Pol[i-1][lm] - W.Lu*Pol[i+1][lm];
				}
			}
		}
	}
	free(lms);
	return lmcount;
}
#endif

/// Isolate the non-zero modes and store them pre-computed (ready for SHT) in a minimal way.
/// \param **sss, point on pointers on StatSpecScal structures => allocated if needed, or NULL is returned.
/// Gives the ability to add "for free" background scalar fields and gradients (T0, grad(T0))
/// Boundary condition should are treated as BC_NONE.
/// return value is the number of non-zero modes.
int ScalarSH::make_static(struct StatSpecScal **psss) const
{
	struct StatSpecScal *sss;
	void *ptr;
	double dx, max;
	int i,j,lm;
	int lmcount, maxl, maxm, m;
	double *A2;		// max amplitudes for each lm-mode.
	int *lms;
	unsigned short *lis, *mis;

	lms = (int*) malloc( NLM*(sizeof(int)+2*sizeof(unsigned short)) );
	lis = (unsigned short*) (lms+NLM);		mis = lis + NLM;

	A2 = (double*) malloc( NLM*sizeof(double) );
	if (A2==0) runerr("[ScalarSH::make_static] allocation error 1");
	// get max amplitude
	for (lm=0; lm<NLM; lm++) {
		A2[lm] = 0.0;
		for (i=irs; i<=ire; i++) {
			dx = norm(Sca[i][lm]);		// |Sca|^2
			if (dx > A2[lm]) A2[lm] = dx;
		}
	}
  #ifdef XS_MPI
	double *tmp = (double*) malloc( NLM*sizeof(double) );
	if (tmp==0) runerr("[ScalarSH::make_static] allocation error 2");
	MPI_Allreduce(A2, tmp, NLM, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	free(A2);
	A2 = tmp;
  #endif
	max = 0.0;
	for (lm=0; lm<NLM; lm++) {
		if (A2[lm] > max) max = A2[lm];
	}

	maxl = 0;	maxm = 0;	m=0;
	lmcount = 0;
	// count "non-zero" modes
	max *= 1.e-20;		// no more than 1e-10 amplitude contrast allowed between modes.
	for (lm=0; lm<NLM; lm++) {
		int l = li[lm];
		if (A2[lm] > max) {		// mode is non-zero
			lms[lmcount] = lm;	// store mode
			lis[lmcount] = l;
			mis[lmcount] = m;
			lmcount++;		// count
			if (l > maxl) maxl = l;
			if (m > maxm) maxm = m;
		}
		if (l == LMAX) m += MRES;
	}
	free(A2);

	if (lmcount == 0) {
		free(lms);
		if (psss != NULL) *psss = NULL;		/// \todo Possible memory leak !
		return lmcount;	// stop here.
	}

	sss = NULL;
	if (psss != NULL) {
		if (*psss == NULL)	*psss = (struct StatSpecScal *) malloc( sizeof(struct StatSpecScal) );
		sss = *psss;		sss->nlm = lmcount;
	}

	if (sss != NULL) {
		// TODO MPI: use only local shell numbers instead of NR
		j = ((lmcount+3)>>2)*4;		// lmcount rounded up for SSE alignement.
		ptr = VMALLOC( j*sizeof(int) + lmcount*2*NR*sizeof(cplx) + 2*lmcount*sizeof(unsigned short) );
		if (ptr==0) runerr("[ScalarSH::make_static] allocation error 3");
		sss->lm = (unsigned *) ptr;
		ptr = sss->lm + j;
		sss->TdT = (cplx *) ptr;		// should be SSE aligned.
		ptr = sss->TdT + 2*lmcount*NR;
		sss->li = (unsigned short*) ptr;
		sss->mi = sss->li + lmcount;

		for (j=0; j<lmcount; j++) {
			sss->lm[j] = lms[j];		// copy lm indices
			sss->li[j] = lis[j];		// copy l
			sss->mi[j] = mis[j];		// copy m
		}
		sss->lmax = maxl;		sss->mmax = maxm;			// save max values.

		for (i=0; i<NR; i++) {				// zero out modes
			for (j=0; j<lmcount*2; j++) sss->TdT[i*lmcount*2 +j] = 0.0;
		}

		// pre-compute and store T and dT/dr values.
		for (i=irs; i<=ire; i++) {
			if ((i==ir_bci)||(i==ir_bco)) {
				int ii=i+1;
				if (i==ir_bco) ii = i-1;
				dx = 1.0/(r[ii]-r[i]);
				for (j=0; j<lmcount; j++) {
					lm = lms[j];
					sss->TdT[(i*lmcount +j)*2] = Sca[i][lm];
					if ((r[i] == 0.0) && (lis[j] != 1)) {
						sss->TdT[(i*lmcount +j)*2 + 1] = 0.0;
					} else {
						sss->TdT[(i*lmcount +j)*2 + 1] = (Sca[ii][lm] - Sca[i][lm]) * dx;		// WARNING: less acurate (1st order)
					}
				}
			} else {
				OpGrad& G = GRAD(i);
				for (j=0; j<lmcount; j++) {
					lm = lms[j];
					sss->TdT[(i*lmcount +j)*2] = Sca[i][lm];
					sss->TdT[(i*lmcount +j)*2 + 1] = G.Gl * Sca[i-1][lm] + G.Gd * Sca[i][lm] + G.Gu * Sca[i+1][lm];
				}
			}
		}
	}
	free(lms);
	return lmcount;
}

void free_StatSpecVect(struct StatSpecVect *ss) {
	if (ss) VFREE(ss->lm);
}

void free_StatSpecScal(struct StatSpecScal *ss) {
	if (ss) VFREE(ss->lm);
}


#ifdef XS_LINEAR
/// Class holding a spatial vector field, with specialization for axisymmetric fields.
class SpatVect {
  public:
	int irs, ire;			///< radial boundaries.

  private:
	int poltor;		///< 0: no component. 1=pol (vr+vt), 2=tor (vp), 3=pol+tor (vr+vt+vp)
	size_t nelem;	///< number of elements in a shell. Can be NLAT for axisymmetric fields, or shtns->nspat for general fields.
	double *vr;
	double *vt;
	double *vp;
	double *v2_max;

	void alloc();	///< allocate memory to hold the data.
	void precompute_max();			///< Compute maximum vectors for CFL

  public:
	SpatVect() {
		poltor = 0;
		vr = 0;		vt = 0;		vp = 0;		v2_max = 0;
	}
	~SpatVect() {
		if (vr) VFREE(vr);
		if (vt) VFREE(vt);
		if (vp) VFREE(vp);
		if (v2_max) VFREE(v2_max);
	}

	int from_SH(const PolTor& Vlm, int curl=0);		///< compute spatial field from Vlm.
	int from_SH(const ScalarSH& Tlm);				///< compute spatial field from gradient of ScalarSH
	int from_cross_prod(const SpatVect& J, const SpatVect& B);		///< compute the cross-product of two constant fields

	int V0_cross_w(int ir, double* xr, double* xt, double *xp, double *wr, double *wt, double *wp, int add = 0) const;
	int w_cross_V0(int ir, double* xr, double* xt, double *xp, double *wr, double *wt, double *wp, int add = 0) const;
	int V0_dot_w(int ir, double* dot, double *wr, double *wt, double *wp, int add = 0) const;
	int V0_times_s(int ir, double* xr, double* xt, double *xp, double *s, int add = 0) const;
	int add_V0(int ir, double* xr, double* xt, double *xp, int add = 0) const;
	void absmax2(int ir, double& vr2_max, double& vh2_max) const;
	void absmax2(int ir, double& v2max) const;
	double absmax2() const;
};

void SpatVect::alloc() {
	const int nr = (ire - irs + 1);
	const size_t mem_sze = nelem * nr * sizeof(double);
	if (poltor & 1) {
		vr = (double*) VMALLOC(mem_sze);
		vt = (double*) VMALLOC(mem_sze);
		if ((vr==0) || (vt==0)) runerr("[SpatVect::alloc] allocation error 1");
		memset(vr, 0, mem_sze);
		memset(vt, 0, mem_sze);
	}
	if (poltor & 2) {
		vp = (double*) VMALLOC(mem_sze);
		if (vp==0) runerr("[SpatVect::alloc] allocation error 2");
		memset(vp, 0, mem_sze);
	}
	v2_max = (double*) VMALLOC(2*nr*sizeof(double));
}

void SpatVect::precompute_max() {
	for (int ir=irs; ir<=ire; ir++) {
		double vr2_max = 0.0;
		double vh2_max = 0.0;
		for (int it=0; it<nelem; it++) {
			double vri = 0.0;		double vti = 0.0;		double vpi = 0.0;
			if (poltor & 1) {
				vri = vr[nelem*(ir-irs)+it];
				vti = vt[nelem*(ir-irs)+it];
			}
			if (poltor & 2) {
				vpi = vp[nelem*(ir-irs)+it];
			}
			double vr2 = vri*vri;
			double vh2 = vti*vti + vpi*vpi;
			if (vr2 > vr2_max) vr2_max = vr2;
			if (vh2 > vh2_max) vh2_max = vh2;
		}
		v2_max[2*(ir-irs)] = vr2_max;
		v2_max[2*(ir-irs)+1] = vh2_max;
	}
}

int SpatVect::from_cross_prod(const SpatVect& J, const SpatVect& B)
{
	const char cross_prod_type[4][4] = {{0,0,0,0},  // empty x ?
										{0,2,1,3},  // pol x ?
										{0,1,0,1},  // tor x ?
										{0,3,1,3}}; // all x ?

	irs = J.irs;		ire = J.ire;
	nelem = J.nelem;
	poltor = cross_prod_type[J.poltor][B.poltor];	// pol or tor or all ?
	alloc();
	if (poltor == 0) return 0;	// empty field

	const long ntot = nelem*(ire-irs+1);
	if (poltor & 1) {	// poloidal part (vr,vt)
		if ((J.vt) && (B.vp)) {
			for (long i=0; i<ntot; i++) {
				double bp = B.vp[i];
				vr[i] = J.vt[i]*bp;
				vt[i] = -J.vr[i]*bp;
			}
		} else {
			memset(vr, 0, ntot * sizeof(double));
			memset(vt, 0, ntot * sizeof(double));
		}
		if ((J.vp) && (B.vt)) {
			for (long i=0; i<ntot; i++) {
				double jp = J.vp[i];
				vr[i] -= jp*B.vt[i];
				vt[i] += jp*B.vr[i];
			}
		}
	}
	if (poltor & 2) {	// toroidal part (implies poloidal part of both field is set)
		for (long i=0; i<ntot; i++)		vp[i] = J.vr[i]*B.vt[i] - J.vt[i]*B.vr[i];
	}

	precompute_max();
	return poltor;
}

int SpatVect::from_SH(const PolTor& Vlm, int curl)
{
	double Epol0, Etor0;

	irs = Vlm.irs;		ire = Vlm.ire;
	vr = 0;			vt = 0;			vp = 0;
	poltor = 0;
	if ((curl) && (Vlm.zero_curl)) return 0;

	if (NLAT % VSIZE) runerr("[SpatVect::from_SH] NLAT must be a multiple of vector size.");

	SpectralDiags sd(LMAX,MMAX);
	Vlm.energy(sd);
	#ifdef XS_MPI
		sd.reduce_mpi();
	#endif
	if (sd.energy() == 0.0) runerr("empty field");
	if (curl==0) {
		Epol0 = sd.Esplit[Ez_es]+sd.Esplit[Ez_ea];
		Etor0 = sd.Esplit[N_Esplit+Ez_es]+sd.Esplit[N_Esplit+Ez_ea];
	} else {
		Etor0 = sd.Esplit[Ez_es]+sd.Esplit[Ez_ea];
		Epol0 = sd.Esplit[N_Esplit+Ez_es]+sd.Esplit[N_Esplit+Ez_ea];
	}
	if (sd.energy_nz() > 0.0) {		// non-axisymmetric field.
		nelem = shtns->nspat;
		PRINTF0("  non-axisymmetric field\n");
		poltor = 3;		// all components.
	} else {		// axisymmetric field.
		nelem = NLAT;
		PRINTF0("  axisymmetric field\n");
		if (Epol0 > 0.0) poltor |= 1;
		if (Etor0 > 0.0) poltor |= 2;
	}
	alloc();

	if (nelem == NLAT) {		// axisymmetric case
		#if XS_OMP == 1
		#pragma omp parallel for
		#endif
		for (int ir=irs; ir<=ire; ir++) {
			cplx qlm[LMAX+1] SSE;		// alloc private variables on stack
			cplx slm[LMAX+1] SSE;
			cplx tlm[LMAX+1] SSE;
			cplx spat1[NLAT] SSE;
			cplx spat2[NLAT] SSE;
			if (curl==0) {
				Vlm.RadSph(ir, qlm, slm, 0, LMAX);		// m=0
				for (int l=0; l<=LMAX; l++) tlm[l] = Vlm.Tor[ir][l];
			} else {
				Vlm.curl_QST(ir, qlm, slm, tlm, 0, LMAX);		// m=0
			}
			if (poltor & 1) {
				SHsph_to_spat_ml(shtns, 0, slm, spat1, spat2, LMAX);
				SH_to_spat_ml(shtns, 0, qlm, spat2, LMAX);
				for (size_t it=0; it<nelem; it++) {
					vr[nelem*(ir-irs)+it] = real(spat2[it]);
					vt[nelem*(ir-irs)+it] = real(spat1[it]);
				}
			}
			if (poltor & 2) {
				SHtor_to_spat_ml(shtns, 0, tlm, spat1, spat2, LMAX);
				for (size_t it=0; it<nelem; it++) {
					vp[nelem*(ir-irs)+it] = real(spat2[it]);
				}
			}
		}
	} else {					// non-axisymmetric case
		#if XS_OMP == 1
		#pragma omp parallel for
		#endif
		for (int ir=irs; ir<=ire; ir++) {
			cplx* qlm = (cplx*) VMALLOC(sizeof(cplx) * 3*NLM);		// alloc private large buffers
			cplx* slm = qlm + NLM;		cplx* tlm = qlm + 2*NLM;
			double* vrr = (double*) VMALLOC(sizeof(double) * 3*shtns->nspat);	// alloc private spatial buffers (with additional space for fft)
			double* vtt = vrr + shtns->nspat;		double* vpp = vrr + 2*shtns->nspat;
			if ((qlm==0)||(vrr==0)) runerr("[SpatVect::from_SH] allocation error 3");
			memset(vrr, 0, sizeof(double) * 3*shtns->nspat);
			if (curl==0) {
				Vlm.RadSph(ir, qlm, slm, 0, NLM-1);		// m=0
				for (int l=0; l<NLM; l++) tlm[l] = Vlm.Tor[ir][l];
			} else {
				Vlm.curl_QST(ir, qlm, slm, tlm, 0, NLM-1);		// m=0
			}
			SHV3_SPAT(qlm, slm, tlm, vrr, vtt, vpp);
			memcpy(vr+nelem*(ir-irs), vrr, sizeof(double)*nelem);	// copy to tight shells
			memcpy(vt+nelem*(ir-irs), vtt, sizeof(double)*nelem);
			memcpy(vp+nelem*(ir-irs), vpp, sizeof(double)*nelem);
			VFREE(vrr);		VFREE(qlm);
		}
	}

	precompute_max();
	return poltor;
}

int SpatVect::from_SH(const ScalarSH& Tlm)
{
	irs = Tlm.irs;		ire = Tlm.ire;
	vr = 0;			vt = 0;			vp = 0;
	poltor = 0;

	if (NLAT % VSIZE) runerr("[SpatVect::from_SH] NLAT must be a multiple of vector size.");

	SpectralDiags sd(LMAX,MMAX);
	Tlm.energy(sd);
	#ifdef XS_MPI
		sd.reduce_mpi();
	#endif
	if (sd.energy() == 0.0) runerr("empty field");
	if (sd.energy_nz() > 0.0) {		// non-axisymmetric field.
		nelem = shtns->nspat;
		PRINTF0("  non-axisymmetric field\n");
		poltor = 3;		// all components.		
	} else {		// axisymmetric field
		nelem = NLAT;
		PRINTF0("  axisymmetric field\n");
		poltor = 1;		// only r and theta components		
	}
	alloc();

	if (nelem == NLAT) {		// axisymmetric case
		#if XS_OMP == 1
		#pragma omp parallel for
		#endif
		for (int ir=irs; ir<=ire; ir++) {
			cplx qlm[LMAX+1] SSE;		// alloc private variables on stack
			cplx slm[LMAX+1] SSE;
			cplx spat1[NLAT] SSE;
			cplx spat2[NLAT] SSE;
			Tlm.Gradr(ir, qlm, slm, 0, LMAX);		// m=0;
			if (poltor & 1) {
				SHsph_to_spat_ml(shtns, 0, slm, spat1, spat2, LMAX);
				SH_to_spat_ml(shtns, 0, qlm, spat2, LMAX);
				for (size_t it=0; it<nelem; it++) {
					vr[nelem*(ir-irs)+it] = real(spat2[it]);
					vt[nelem*(ir-irs)+it] = real(spat1[it]);
				}
			}
		}
	} else {					// non-axisymmetric case
		#if XS_OMP == 1
		#pragma omp parallel for
		#endif
		for (int ir=irs; ir<=ire; ir++) {
			cplx* qlm = (cplx*) VMALLOC(sizeof(cplx)*2*NLM);		// alloc private large buffers
			cplx* slm = qlm + NLM;
			double* vrr = (double*) VMALLOC(sizeof(double) * 3*shtns->nspat);	// alloc private spatial buffers (with additional space for fft)
			double* vtt = vrr + shtns->nspat;		double* vpp = vrr + 2*shtns->nspat;
			if ((qlm==0)||(vrr==0)) runerr("[SpatVect::from_SH] allocation error 3");
			Tlm.Gradr(ir, qlm, slm, 0, NLM-1);
			SH_SPAT(qlm, vrr);
			SHS_SPAT(slm, vtt, vpp);
			memcpy(vr+nelem*(ir-irs), vrr, sizeof(double)*nelem);	// copy to tight shells
			memcpy(vt+nelem*(ir-irs), vtt, sizeof(double)*nelem);
			memcpy(vp+nelem*(ir-irs), vpp, sizeof(double)*nelem);
			VFREE(vrr);		VFREE(qlm);
		}
	}

	precompute_max();
	return poltor;
}


void SpatVect::absmax2(int ir, double& vr2max, double& vh2max) const
{
	vr2max = v2_max[2*(ir-irs)];
	vh2max = v2_max[2*(ir-irs)+1];
}

void SpatVect::absmax2(int ir, double& v2max) const
{
	double v2r = v2_max[2*(ir-irs)];
	double v2h = v2_max[2*(ir-irs)+1];
	if (v2h > v2r) v2r = v2h;
	v2max = v2r;
}

double SpatVect::absmax2() const
{
	double v2r=0, v2h=0;
	for (int ir=irs; ir<=ire; ir++) {
		double v2ri, v2hi;
		absmax2(ir, v2ri, v2hi);
		if (v2ri > v2r) v2r = v2ri;
		if (v2hi > v2h) v2h = v2hi;
	}
	if (v2h > v2r) v2r = v2h;
	return v2r;
}

int SpatVect::V0_cross_w(int ir, double* xr, double* xt, double *xp, double *wr, double *wt, double *wp, const int add) const
{
	const size_t vnelem = nelem/VSIZE;
	const size_t vnspat = shtns->nspat / VSIZE;
	double* Vr = vr + nelem*(ir-irs);
	double* Vt = vt + nelem*(ir-irs);
	double* Vp = vp + nelem*(ir-irs);

	if ((ir < irs) || (ir > ire)) return 0;
	if ((poltor & 3) == 3) {	// pol+tor
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvt = vread(Vt,it);		rnd vwt = vread(wt,k);
			rnd vvp = vread(Vp,it);		rnd vwp = vread(wp,k);
			rnd rr = vvt*vwp - vvp*vwt;	// AxB
			rnd vvr = vread(Vr,it);		rnd vwr = vread(wr,k);
			rnd rt = vvp*vwr - vvr*vwp;
			rnd rp = vvr*vwt - vvt*vwr;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);	vmemadd(xt,k, rt);	vmemadd(xp,k, rp);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
			}
		}
		return 1;
	} else if (poltor & 2) {	// tor only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvp = vread(Vp,it);
			rnd rr = - vvp * vread(wt,k);	// AxB
			rnd rt =   vvp * vread(wr,k);
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);		vmemadd(xt,k, rt);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);
			}
		}
		if (add==0) memset(xp, 0, sizeof(double)*vnspat*VSIZE);
		return 1;
	} else if (poltor & 1) {		// pol only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvt = vread(Vt,it);		rnd vwt = vread(wt,k);
										rnd vwp = vread(wp,k);
			rnd rr = vvt*vwp;	// AxB
			rnd vvr = vread(Vr,it);		rnd vwr = vread(wr,k);
			rnd rt = - vvr*vwp;
			rnd rp = vvr*vwt - vvt*vwr;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);		vmemadd(xt,k, rt);		vmemadd(xp,k, rp);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
			}
		}
		return 1;
	}
	return 0;
}

int SpatVect::w_cross_V0(int ir, double* xr, double* xt, double *xp, double *wr, double *wt, double *wp, const int add) const
{
	const size_t vnelem = nelem/VSIZE;
	const size_t vnspat = shtns->nspat / VSIZE;
	double* Vr = vr + nelem*(ir-irs);
	double* Vt = vt + nelem*(ir-irs);
	double* Vp = vp + nelem*(ir-irs);

	if ((ir < irs) || (ir > ire)) return 0;
	if ((poltor & 3) == 3) {	// pol+tor
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvt = vread(Vt,it);		rnd vwt = vread(wt,k);
			rnd vvp = vread(Vp,it);		rnd vwp = vread(wp,k);
			rnd rr = vvp*vwt - vvt*vwp;	// BxA
			rnd vvr = vread(Vr,it);		rnd vwr = vread(wr,k);
			rnd rt = vvr*vwp - vvp*vwr;
			rnd rp = vvt*vwr - vvr*vwt;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);	vmemadd(xt,k, rt);	vmemadd(xp,k, rp);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
			}
		}
		return 1;
	} else if (poltor & 2) {	// tor only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvp = vread(Vp,it);
			rnd rr =   vvp * vread(wt,k);	// BxA
			rnd rt = - vvp * vread(wr,k);
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);	vmemadd(xt,k, rt);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);
			}
		}
		if (add==0) memset(xp, 0, sizeof(double)*vnspat*VSIZE);
		return 1;
	} else if (poltor & 1) {		// pol only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvt = vread(Vt,it);		rnd vwt = vread(wt,k);
										rnd vwp = vread(wp,k);
			rnd rr = - vvt*vwp;	// BxA
			rnd vvr = vread(Vr,it);		rnd vwr = vread(wr,k);
			rnd rt =   vvr*vwp;
			rnd rp = vvt*vwr - vvr*vwt;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);	vmemadd(xt,k, rt);	vmemadd(xp,k, rp);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
			}
		}
		return 1;
	}
	return 0;
}


int SpatVect::V0_dot_w(int ir, double* dot, double *wr, double *wt, double *wp, int add) const
{
	const size_t vnelem = nelem/VSIZE;
	const size_t vnspat = shtns->nspat / VSIZE;
	double* Vr = vr + nelem*(ir-irs);
	double* Vt = vt + nelem*(ir-irs);
	double* Vp = vp + nelem*(ir-irs);

	if ((poltor & 3) == 3) {	// pol+tor
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvr = vread(Vr,it);		rnd vwr = vread(wr,k);
			rnd vvt = vread(Vt,it);		rnd vwt = vread(wt,k);
			rnd vvp = vread(Vp,it);		rnd vwp = vread(wp,k);
			rnd d = vvr*vwr + vvt*vwt + vvp*vwp;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(dot,k, d);
			} else {
				vstore(dot,k, d);
			}
		}
		return 1;
	} else if (poltor & 2) {	// tor only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvp = vread(Vp,it);		rnd vwp = vread(wp,k);
			rnd d = vvp*vwp;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(dot,k, d);
			} else {
				vstore(dot,k, d);
			}
		}
		return 1;
	} else if (poltor & 1) {		// pol only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd vvr = vread(Vr,it);		rnd vwr = vread(wr,k);
			rnd vvt = vread(Vt,it);		rnd vwt = vread(wt,k);
			rnd d = vvr*vwr + vvt*vwt;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(dot,k, d);
			} else {
				vstore(dot,k, d);
			}
		}
		return 1;
	}
	return 0;
}

int SpatVect::V0_times_s(int ir, double* xr, double* xt, double *xp, double *s, const int add) const
{
	const size_t vnelem = nelem/VSIZE;
	const size_t vnspat = shtns->nspat / VSIZE;
	double* Vr = vr + nelem*(ir-irs);
	double* Vt = vt + nelem*(ir-irs);
	double* Vp = vp + nelem*(ir-irs);

	if ((ir < irs) || (ir > ire)) return 0;
	if ((poltor & 3) == 3) {	// pol+tor
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd sk = vread(s, k);
			rnd rr = vread(Vr,it) * sk;
			rnd rt = vread(Vt,it) * sk;
			rnd rp = vread(Vp,it) * sk;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);	vmemadd(xt,k, rt);	vmemadd(xp,k, rp);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
			}
		}
		return 1;
	} else if (poltor & 2) {	// tor only
		for (size_t k=0, it=0; k<vnspat; k++) {			
			rnd rp = vread(Vp,it) * vread(s,k);
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xp,k, rp);
			} else {
				vstore(xp,k, rp);
			}
		}
		if (add==0) {
			memset(xr, 0, sizeof(double)*vnspat*VSIZE);
			memset(xt, 0, sizeof(double)*vnspat*VSIZE);
		}
		return 1;
	} else if (poltor & 1) {		// pol only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd sk = vread(s, k);
			rnd rr = vread(Vr,it) * sk;
			rnd rt = vread(Vt,it) * sk;
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);		vmemadd(xt,k, rt);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);
			}
		}
		if (add==0) memset(xp, 0, sizeof(double)*vnspat*VSIZE);
		return 1;
	}
	return 0;
}

int SpatVect::add_V0(int ir, double* xr, double* xt, double *xp, const int add) const
{
	const size_t vnelem = nelem/VSIZE;
	const size_t vnspat = shtns->nspat / VSIZE;
	double* Vr = vr + nelem*(ir-irs);
	double* Vt = vt + nelem*(ir-irs);
	double* Vp = vp + nelem*(ir-irs);

	if ((ir < irs) || (ir > ire)) return 0;
	if ((poltor & 3) == 3) {	// pol+tor
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd rr = vread(Vr,it);
			rnd rt = vread(Vt,it);
			rnd rp = vread(Vp,it);
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);	vmemadd(xt,k, rt);	vmemadd(xp,k, rp);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
			}
		}
		return 1;
	} else if (poltor & 2) {	// tor only
		for (size_t k=0, it=0; k<vnspat; k++) {			
			rnd rp = vread(Vp,it);
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xp,k, rp);
			} else {
				vstore(xp,k, rp);
			}
		}
		if (add==0) {
			memset(xr, 0, sizeof(double)*vnspat*VSIZE);
			memset(xt, 0, sizeof(double)*vnspat*VSIZE);
		}
		return 1;
	} else if (poltor & 1) {		// pol only
		for (size_t k=0, it=0; k<vnspat; k++) {
			rnd rr = vread(Vr,it);
			rnd rt = vread(Vt,it);
			if (++it >= vnelem) it=0;
			if (add) {
				vmemadd(xr,k, rr);		vmemadd(xt,k, rt);
			} else {
				vstore(xr,k, rr);	vstore(xt,k, rt);
			}
		}
		if (add==0) memset(xp, 0, sizeof(double)*vnspat*VSIZE);
		return 1;
	}
	return 0;
}

#endif

